<?php

use App\Domain\Directories\Client\Client;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Главная', '/');
});

// Reports
// Reports > Cars
Breadcrumbs::for('reports.park-cars.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Отчеты по грузовикам', route('reports.park-cars.index'));
});

Breadcrumbs::for('reports.park-cars.show', function ($trail, ParkCar $parkCar) {
    $trail->parent('reports.park-cars.index');
    $trail->push($parkCar->getNumber(), route('reports.park-cars.show', $parkCar));
});

// Reports > Trailers
Breadcrumbs::for('reports.trailers.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Отчеты по прицепам', route('reports.trailers.index'));
});

Breadcrumbs::for('reports.trailers.show', function ($trail, Trailer $trailer) {
    $trail->parent('reports.trailers.index');
    $trail->push($trailer->getNumber(), route('reports.trailers.show', $trailer));
});


// Reports > Office expenses
Breadcrumbs::for('reports.office-expenses.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Затраты офиса', route('reports.office-expenses.index'));
});

// Reports > Clients
Breadcrumbs::for('reports.clients.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Контрагенты', route('reports.clients.index'));
});

Breadcrumbs::for('reports.clients.show', function ($trail, Client $client) {
    $trail->parent('reports.clients.index');
    $trail->push($client->getName(), route('reports.clients.show', $client));
});

// Directories
// Directories > Vehicles
// Directories > Vehicles > Cars
// Directories > Vehicles > Cars > Park
Breadcrumbs::for('directories.park-cars.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Грузовики (автопарка)', route('directories.park-cars.index'));
});

// Directories > Vehicles > Cars > Office
Breadcrumbs::for('directories.office-cars.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Автомобили (офисные)', route('directories.office-cars.index'));
});

// Directories > Vehicles > Trailers
Breadcrumbs::for('directories.trailers.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Прицепы', route('directories.trailers.index'));
});

// Directories > Driver
Breadcrumbs::for('directories.drivers.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Водители', route('directories.drivers.index'));
});

// Directories > Clients
Breadcrumbs::for('directories.clients.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Контрагенты', route('directories.clients.index'));
});

// Directories > Workers
Breadcrumbs::for('directories.workers.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Сотрудники офиса', route('directories.workers.index'));
});

// Directories > Managers
Breadcrumbs::for('directories.managers.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Менеджеры', route('directories.managers.index'));
});

// Directories > Addresses
Breadcrumbs::for('directories.direction-addresses.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Адреса', route('directories.direction-addresses.index'));
});

// Directories > Driver categories
Breadcrumbs::for('directories.drivers-categories.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Категории водителей', route('directories.drivers-categories.index'));
});

// Directories > Driver categories
Breadcrumbs::for('settings.email-templates.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Шаблоны e-mail\'ов', route('settings.email-templates.index'));
});


// Exchange Rates
Breadcrumbs::for('exchange-rates.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Курсы валют', route('exchange-rates.index'));
});


// Logs
Breadcrumbs::for('logs.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Логи', route('logs.index'));
});
