<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Domain\Directories\Client\ClientController;
use App\Domain\Directories\DirectionAddress\DirectionAddressController;
use App\Domain\Directories\Vehicle\Children\Car\Office\Http\OfficeCarController;
use App\Domain\Directories\Vehicle\Children\Car\Park\Http\ParkCarController;
use App\Domain\Directories\Vehicle\Children\Trailer\Http\TrailerController;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\ManagerController;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategoryController;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriversController;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\WorkersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::namespace('App\Domain\UsersAndRoles\Users\AdminOrManager\General\Http\Controller')->group(function () {
    Auth::routes([
        'register' => false,
    ]);
});

Route::group([
    'middleware' => ['auth'],
], function () {

    Route::get('/', function () {
        return view('main-page');
    })->name('home');


    // Directories
    Route::group(['prefix' => 'directories', 'as' => 'directories.'], function () {
        // vehicles
        Route::resource('park-cars', ParkCarController::class);

        Route::resource('office-cars', OfficeCarController::class);

        Route::resource('trailers', TrailerController::class);

        Route::resource('drivers', DriversController::class);
        Route::resource('drivers-categories', DriverCategoryController::class);
        Route::resource('clients', ClientController::class);
        Route::resource('workers', WorkersController::class);
        Route::resource('managers', ManagerController::class);
        Route::resource('direction-addresses', DirectionAddressController::class);
    });
});
