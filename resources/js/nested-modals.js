(() => {
    const baseZIndex = 1040;

    $(document).on({
        'show.bs.modal': function () {
            const zIndex = baseZIndex + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);

            setTimeout(function () {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        },

        'hidden.bs.modal': function () {
            if ($('.modal:visible').length === 0) {
                return;
            }

            // restore the modal-open class to the body element, so that scrolling works
            // properly after de-stacking a modal.
            setTimeout(function () {
                $(document.body).addClass('modal-open');
            }, 0);
        }
    }, '.modal');
})();
