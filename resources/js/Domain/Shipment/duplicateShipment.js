import deepClone from "../../Base/JS/Utils/deepClone";

export default function (clonedShipment) {
    clonedShipment.invoice_id = null;

    return clonedShipment;
}
