import OfficeExpenseType from "../../Expense/Office/OfficeExpenseType";
import ParkExpenseType from "../../Expense/Park/ParkExpenseType";
import VatExpenseType from "../../Expense/Vat/VatExpenseType";
import ResaleMoneyTransferType from "../../Resale/ResaleMoneyTransferType";

import IncomeType from "../../Income/IncomeType";

export default {
    OFFICE: OfficeExpenseType.id,
    PARK: ParkExpenseType.id,
    VAT: VatExpenseType.id,
    INCOME: IncomeType.id,
    RESALE: ResaleMoneyTransferType.id,
}
