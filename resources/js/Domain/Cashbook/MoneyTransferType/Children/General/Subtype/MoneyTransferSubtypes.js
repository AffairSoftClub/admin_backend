import ParkExpenseType from "../../Expense/Park/ParkExpenseType";
import OfficeExpenseType from "../../Expense/Office/OfficeExpenseType";
import VatExpenseType from "../../Expense/Vat/VatExpenseType";
import IncomeType from "../../Income/IncomeType";
import ResaleMoneyTransferType from "../../Resale/ResaleMoneyTransferType";

export default [
    ParkExpenseType,
    OfficeExpenseType,
    VatExpenseType,
    IncomeType,
    ResaleMoneyTransferType,
];
