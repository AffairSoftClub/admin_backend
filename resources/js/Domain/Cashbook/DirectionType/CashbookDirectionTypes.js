import IncomeCashbookType from "./Instances/IncomeCashbookDirectionType";
import OutcomeCashbookType from "./Instances/OutcomeCashbookDirectionType";

export default [
    OutcomeCashbookType,
    IncomeCashbookType,
];
