export default {
    WARNING: 'warning',
    CREATED: 'created',
    UPDATED: 'updated',
}
