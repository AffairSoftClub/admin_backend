export default {
    OTHER: 0,

    INCOME: 1,

    ROAD: 11, // park only
    SALARY: 12, // park only
    FUEL: 13, // park only
    LEASING_AND_INSURANCE: 14,
    REPAIR: 15,
}
