import WorkStatusGood from "../WorkStatuses/WorkStatusGood";
import WorkStatusRepair from "../WorkStatuses/WorkStatusRepair";

export default [
    WorkStatusGood,
    WorkStatusRepair,
    // [WorkStatusGood.id]: WorkStatusGood.name_ru,
    // [WorkStatusRepair.id]: WorkStatusRepair.name_ru,
]
