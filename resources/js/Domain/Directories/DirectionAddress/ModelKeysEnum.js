export default {
    ID: 'id',
    DIRECTION_ID: 'direction_id',
    ADDRESS: 'address',
    EMAIL: 'email',
    PHONE: 'phone',
    COMMENT: 'comment',
}
