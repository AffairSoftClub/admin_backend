export default {
    ID: 'id',

    DIRECTION_ID: 'direction_id',
    DIRECTION_CODE: 'direction_code',

    CITY_ID: 'city_id',
    CITY_NAME: 'city_name',

    EMAIL: 'email',
    PHONE: 'phone',
    ADDRESS: 'address',
    COMMENT: 'comment',
}
