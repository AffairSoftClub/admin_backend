import CashMoneyAccountType from "./CashMoneyAccountType";
import NonCashMoneyAccountType from "./NonCashMoneyAccountType";

export default {
    CASH: CashMoneyAccountType.id,
    NON_CASH: NonCashMoneyAccountType.id,
}
