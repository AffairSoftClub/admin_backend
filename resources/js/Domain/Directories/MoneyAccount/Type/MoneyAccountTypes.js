import CashMoneyAccountType from "./CashMoneyAccountType";
import NonCashMoneyAccountType from "./NonCashMoneyAccountType";

export default [
    NonCashMoneyAccountType,
    CashMoneyAccountType,
];
