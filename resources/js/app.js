/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import moment from "moment";
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// import plugin
import Vue from 'vue';
import VueToastr from "vue-toastr";
import deepClone from "./Base/JS/Utils/deepClone";
import Fragment from 'vue-fragment';
import DefaultShowDateFormat from "./Base/JS/DateAndTime/Date/Format/DefaultDateViewFormat";
import valueOrDash from "./Base/JS/valueOrDash";
import callbacks from "./callbacks";
import AjaxEntityManagersProvider from "./Base/JS/AjaxManager/AjaxEntityManagersProvider";
import isEqual from "./Base/JS/Utils/isEqual";
import VueJustSuper from "vue-just-super";

require('./bootstrap');
require('./components');

window.Vue = Vue;
var _ = require('lodash');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
export const eventEmitter = new Vue();
export const mixErrors = {
    methods: {
        getFirstError: function (errorData) {
            let data = errorData;
            let fError = data[Object.keys(data)[0]];
            return fError;
        },
    }
};

// use plugin
Vue.use(VueToastr, {
    /* OverWrite Plugin Options if you need */
    defaultPosition: "toast-bottom-right",
    defaultClassNames: [
        "animated",
        "zoomInUp"
    ],
});

Vue.use(Fragment.Plugin)

/// filters
Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format(DefaultShowDateFormat)
    }
});

Vue.filter('valueOfDash', valueOrDash);

Vue.use(VueJustSuper);

Vue.component('pagination', require('laravel-vue-pagination'));


// Fix permissions props merge strategy
const defaultStrategies = Vue.config.optionMergeStrategies;
const defaultPropsMergeStrategy = defaultStrategies.props;
const defaultDataMergeStrategy = defaultStrategies.data;

Vue.config.optionMergeStrategies.props = function (toVal, fromVal, vm, key) {
    if (!toVal || !fromVal || !toVal.permissions || !fromVal.permissions || !toVal.permissions.default || !fromVal.permissions.default) {
        return defaultPropsMergeStrategy(toVal, fromVal, vm, key);
    }

    const toPermissions = toVal.permissions;
    const fromPermissions = fromVal.permissions;

    const toPermissionsDefault = toVal.permissions.default;
    const fromPermissionsDefault = fromVal.permissions.default;

    delete (fromVal.permissions);
    delete (toVal.permissions);

    const resultProps = defaultPropsMergeStrategy(toVal, fromVal, vm, key);

    toVal.permissions = toPermissions;
    fromVal.permissions = fromPermissions;

    const permissionsOnlyProps = defaultDataMergeStrategy(toPermissionsDefault, fromPermissionsDefault, vm);

    resultProps.permissions = {
        type: Object,
        default: function() {
            return permissionsOnlyProps();
        },
    };


    return resultProps;
}


// Vue.filter('formatDateTime', function (value) {
//     if (value) {
//         return moment(String(value)).format('DD-MM-YYYY hh:mm')
//     }
// });


// Base
// Base > Controls
// Vue.component('select-control', require('./Domain/Base/Controls/ObjectKeyValueWithoutAutoCompleteWithoutGroupsSelectWithLabelControl').default);
Vue.component('button-create', require('./Base/Components/Framework/Controls/ActionButtons/Form/Entity/ButtonCreate').default);

Vue.component('file-uploader', require('./Base/Components/Framework/Controls/FileUploaders/FileUploader').default);
Vue.component('csv-file-uploader', require('./Base/Components/Framework/Controls/FileUploaders/CsvFileUploader').default);
Vue.component('pdf-file-uploader', require('./Base/Components/Framework/Controls/FileUploaders/PdfFileUploader').default);
Vue.component('xls-file-uploader', require('./Base/Components/Framework/Controls/FileUploaders/XlsFileUploader').default);
Vue.component('xlsx-file-uploader', require('./Base/Components/Framework/Controls/FileUploaders/XlsxFileUploader').default);

// Directories > Vehicle

// Directories > Vehicle > Car

// Directories > Vehicle > Car > Old
Vue.component('car-group', require('./Domain/Directories/Vehicle/Car/CarGroup').default);
Vue.component('car-form', require('./Domain/Directories/Vehicle/Car/CarForm').default);
Vue.component('car-table', require('./Domain/Directories/Vehicle/Car/CarTable').default);

// Directories > Vehicle > Car > Park
Vue.component('park-car-table', require('./Domain/Directories/Vehicle/Car/Park/ParkCarTable').default);
Vue.component('park-car-modal-form', require('./Domain/Directories/Vehicle/Car/Park/ParkCarModalForm').default);

// Directories > Vehicle > Car > Office
Vue.component('office-car-table', require('./Domain/Directories/Vehicle/Car/Office/OfficeCarTable').default);
Vue.component('office-car-modal-form', require('./Domain/Directories/Vehicle/Car/Office/OfficeCarModalForm').default);

// Directories > Vehicle > Trailer
Vue.component('trailer-table', require('./Domain/Directories/Vehicle/Trailer/TrailerTable').default);
Vue.component('trailer-modal-form', require('./Domain/Directories/Vehicle/Trailer/TrailerModalForm').default);

// Directories > Client
Vue.component('client-form', require('./Domain/Directories/Client/ClientForm').default);
Vue.component('client-table', require('./Domain/Directories/Client/ClientTable').default);
Vue.component('client-filter', require('./Domain/Directories/Client/ClientFilter').default);

// Directories > DirectionAddress
Vue.component('address-form', require('./Domain/Directories/DirectionAddress/DirectionAddressForm').default);
Vue.component('address-table', require('./Domain/Directories/DirectionAddress/DirectionAddressTable').default);

// Directories > DriverCategory
Vue.component('driver-category-table', require('./Domain/Directories/DriverCategory/DriverCategoryTable').default);
Vue.component('driver-category-form', require('./Domain/Directories/DriverCategory/DriverCategoryForm').default);

// Directories > Email Templates
Vue.component('email-template-table', require('./Domain/Directories/EmailTemplate/EmailTemplateTable').default);
Vue.component('email-template-modal-form', require('./Domain/Directories/EmailTemplate/EmailTemplateModalForm').default);

// Directories > PayoutType
Vue.component('payout-type-select-control', require('./Domain/Directories/PayoutType/PayoutTypeSelectControl').default);

// Directories > Trailer
Vue.component('trailer-select-control', require('./Domain/Directories/Vehicle/Trailer/TrailerSelectControl').default);


// Users > Driver
Vue.component('driver-form', require('./Domain/Users/Driver/DriverForm').default);
Vue.component('driver-table', require('./Domain/Users/Driver/DriverTable').default);
Vue.component('driver-select-control', require('./Domain/Users/Driver/DriverSelectControl').default);

// Users > Worker
Vue.component('worker-form', require('./Domain/Users/Worker/WorkerForm').default);
Vue.component('worker-table', require('./Domain/Users/Worker/WorkerTable').default);
Vue.component('worker-select-control', require('./Domain/Users/Worker/WorkerSelectControl').default);

// Users > Managers
// Vue.component('manager-form', require('./Domain/Users/Manager/WorkerForm').default);
Vue.component('manager-table', require('./Domain/Users/Manager/ManagerTable').default);
Vue.component('button-create-manager', require('./Domain/Users/Manager/ButtonCreateManager').default);
// Vue.component('manager-modal', require('./Domain/Users/Manager/ManagerModal').default);
Vue.component('manager-modal-form', require('./Domain/Users/Manager/ManagerModalForm').default);
// Vue.component('manager-select-control', require('./Domain/Users/Worker/WorkerSelectControl').default);

// Logs
Vue.component('log-table', require('./Domain/Log/LogTable').default);

// Vue.component('test-modal-classic', require('./Domain/Test/TestModalClassic').default);
// Vue.component('test-modal-new', require('./Domain/Test/TestModalNew').default);
// Vue.component('test-nunjucks', require('./Domain/Invoice/Outgoing/Shipment/TestNunjucks').default);


// Test
// Vue.component('modal-form-child', require('./Domain/Test/ModalFormChild').default);
// Vue.component('test-wrapper', require('./Domain/Test/TestWrapper').default);
// Vue.component('test-wrapper-child-with-template', require('./Domain/Test/TestWrapperChildWithTemplate').default);
// Vue.component('test-wrapper-child-with-zero-extended-template', require('./Domain/Test/TestWrapperChildWithZeroExtendedTemplate').default);
// Vue.component('test-wrapper-child-with-non-zero-extended-template', require('./Domain/Test/TestWrapperChildWithNonZeroExtendedTemplate').default);

// Vue.component('test-base', require('./Domain/Test/TestBase').default);
// Vue.component('text-input-control', require('./Domain/Base/Controls/TextInputWithLabelControl').default);
// Vue.component('test-child', require('./Domain/Test/TestChild').default);
// Vue.component('test-sub-child', require('./Domain/Test/TestSubChild').default);
// Vue.component('test-sub-child', require('./Domain/Test/TestSubChild').default);
// Vue.component('modal-form-child', require('./Domain/Test/ModalFormChild').default);
// Vue.component('modal', require('./Base/Components/Framework/Containers/Modals/Modal').default);
// Vue.component('SimpleEntityModalForm', require('./Domain/Base/Forms/Entity/SimpleEntityModalForm').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.config.errorHandler = function(err, vm, info) {
//     console.error(err);
//     vm.$toastr.e(err.toString(), info);
// }
//
// Vue.config.warnHandler = function(msg, vm, trace) {
//     console.warn(msg, trace);
//     vm.$toastr.w(msg, trace);
// }

new Vue({
    created() {
        // You are able to access plugin from everywhere via this.$toastr
        this.initGlobalErrorHandler();
    },
    methods: {
        initGlobalErrorHandler() {
            this.prevErrorHandler = window.onerror;

            // Переопределить прошлый обработчик события.
            window.onerror = this.globalErrorHandler;
        },

        globalErrorHandler(errorMsg, url, lineNumber, columnNumber, error) {
            this.$toastr.e(error.toString(), url + ':' + lineNumber + ':' + columnNumber);

            if (this.prevErrorHandler)
                // Вызвать прошлый обработчик события.
                return this.prevErrorHandler(errorMsg, url, lineNumber);

            // Просто запустить обработчик события по умолчанию.
            return false;
        },
    }
});


const app = new Vue({
    el: '#app',
    data() {
        return {
            sidebar: {
                collapsed: false,
                wrapper: true,
                autocollapse: false,
            },

            models: window.pagination ? window.pagination.data : [],
            model: window.model, //todo: is it used anywheres?

            selectedModel: null,
            selectedModelIsVirtual: false,// true after clone real selected
            selectedByCheckboxesModels: [],

            isModalVisible: false,

            behavior: window.behavior,

            summary: window.summary,

            references: window.references,
            extras: window.extras,

            routes: window.routes,
            // entityUrl: window.entityUrl,

            filter: window.filter,
            initialFilter: deepClone(window.filter),

            extraModals: this.getExtraModals(window.extraModalsNames),

            entityManagersProvider: new AjaxEntityManagersProvider(this.$toastr),
        }
    },
    props: {
        confirmSelectModelMessage: {
            type: String,
            default: "В форму были внесены изменения!\nВы дейстивельно хотите загрузить в форму новые данные без сохранения внесённых изменений?",
        },
    },
    created() {
        //init dynamic callbacks
        this.initDynamicCallbacks();
    },
    methods: {
        initDynamicCallbacks() {
            const usedCallbacks = {};

            for (const [key, value] of Object.entries(window.usedCallbacks)) {
                usedCallbacks[key] = callbacks[value];
            }

            this.callbacks = usedCallbacks;
        },

        getExtraModals(extraModalsNames) {
            const ret = {};

            extraModalsNames.forEach(name => {
                ret[name] = {
                    connector: {
                        attrs: {},
                        listeners: {},
                    },
                    model: null,
                    disabled: false,
                    visible: false,
                };
            });

            return ret;
        },

        // MODELS > Find/Get
        getModelIndexById(modelId) {
            return _.findIndex(this.models, {id: modelId});
        },
        getModelsIds(models) {
            return models.map((model) => {
                return model.id
            });
        },

        // MODELS > Add
        unshiftModel(model) {
            this.models.unshift(model);
        },
        // MODELS > Update
        setModelsModelByIndex(index, model) {
            Vue.set(this.models, index, model);
        },
        updateModelsModelByModel(model) {
            let index = this.getModelIndexById(model.id);
            this.setModelsModelByIndex(index, model);
        },

        // MODELS > delete
        deleteModelsModelByIndex(index) {
            Vue.delete(this.models, index);
        },
        deleteModelFromModels(modelId) {
            let index = this.getModelIndexById(modelId);
            this.deleteModelsModelByIndex(index);
        },

        // SELECTED MODEL
        selectModel(model, isVirtual = false) {
            this.selectedModel = model;
            this.selectedModelIsVirtual = isVirtual;
        },
        unselectModel() {
            this.selectModel(null);
        },
        updateSelectedModelIfRequired(model) {
            const selectedModel = this.selectedModel;

            if (!selectedModel) {
                return;
            }

            this.unselectModel();
            // if (selectedModel.id !== model.id) {
            //     return;
            // }
            //
            // this.selectModel(model);
        },
        unselectModelIfRequired(modelId) {
            const selectedModel = this.selectedModel;

            if (!selectedModel) {
                return;
            }

            if (selectedModel.id !== modelId) {
                return;
            }

            this.unselectModel()
        },
        cloneSelected(model) {
            let modelClone = this.cloneModel(model);

            if (!this.callbacks && !this.callbacks.duplicate) {
                return modelClone;
            }

            return this.callbacks.duplicate(modelClone);
        },
        cloneModel(model) {
            const newModel = deepClone(model);

            if (newModel.id) {
                newModel.id = null;
            }

            if (newModel.created_at) {
                newModel.created_at = null;
            }

            if (newModel.updated_at) {
                newModel.updated_at = null;
            }

            if (newModel.deleted_at) {
                newModel.deleted_at = null;
            }

            return newModel;
        },

        // SELECTED MODEL > CAN?
        canSelectModel(model) {
            const form = this.$refs.form;

            if (!form) {
                return true;
            }

            return this.confirmSelectModelIfRequired(form);
        },
        confirmSelectModelIfRequired(form) {
            if (!this.isDirty) {
                return true;
            }

            return this.confirmSelectModel();
        },
        confirmSelectModel(){
            return confirm(this.confirmLoadMessage);
        },

        canUnselectModel() {
            return this.canSelectModel(null);
        },

        // selectedByCheckboxesModels
        // Selected by checkboxes
        onToggleSelectedByCheckbox(model) {
            const modelIndex = this.selectedByCheckboxesModels.indexOf(model);
            (modelIndex > -1)
                ? this.$delete(this.selectedByCheckboxesModels, modelIndex)
                : this.selectedByCheckboxesModels.push(model);
        },
        onModelUnselectedByCheckbox(model) {
            const index = this.selectedByCheckboxesModels.find(model);
            this.selectedByCheckboxesModels.$delete(index);
        },
        onUnselectAllByCheckboxes() {
            this.selectedByCheckboxesModels = [];
        },
        canBeSelectedByCheckboxes(model) {
            if (!this.callbacks && !this.callbacks.canBeSelectedByCheckboxes) {
                return true;
            }

            return this.callbacks.canBeSelectedByCheckboxes(model);
        },
        updateSelectedByCheckboxesIfRequired(model) {
            if (!this.selectedByCheckboxesModels.length) {
                return;
            }

            const index = this.getSelectedByCheckboxesIndexById(model.id);

            if (this.canBeSelectedByCheckboxes(model)) {
                this.setSelectedByCheckboxesByIndex(index, model);
            } else {
                this.deleteSelectedByCheckboxesByIndex(index);
            }
        },
        getSelectedByCheckboxesIndexById(modelId) {
            return _.findIndex(this.selectedByCheckboxesModels, {id: modelId});
        },
        setSelectedByCheckboxesByIndex(index, model) {
            this.selectedByCheckboxesModels[index] = model;
        },
        deleteSelectedByCheckboxesByIndex(index) {
            this.$delete(this.selectedByCheckboxesModels, index);
        },

        // isModalVisible
        showModal() {
            this.isModalVisible = true;
        },
        showModalIfRequired() {
            if (this.isModalVisible) {
                return;
            }

            this.showModal();
        },
        hideModal() {
            this.isModalVisible = false;
        },

        // REFERENCES
        updateReferences(references) {
            for (const [key, values] of Object.entries(references)) {
                this.references[key] = values;
            }
        },
        onAddRelation(relationName, relationModel, sortKey) {
            const relation = this.references[relationName];
            relation.push(relationModel);

            relation.sort(function (a, b) {
                const aValue = a[sortKey];
                const bValue = b[sortKey];

                if (aValue > bValue) {
                    return 1;
                }

                if (aValue < bValue) {
                    return -1;
                }

                return 0;
            });
        },

        //todo: move sidebar out of app scope
        // SIDEBAR
        collapseHandlerClick: function () {
            const sidebar = this.sidebar;

            if (!sidebar.autocollapse) {
                sidebar.autocollapse = true;
                sidebar.collapsed = true;
            } else {
                sidebar.autocollapse = false;
            }
        },
        sidebarMouseEnter: function () {
            const sidebar = this.sidebar;

            if (!sidebar.autocollapse) {
                return;
            }

            sidebar.collapsed = false;
        },
        sidebarMouseLeave: function () {
            const sidebar = this.sidebar;

            if (!sidebar.autocollapse) {
                return;
            }

            this.sidebar.collapsed = true;
        },

        // EVENT HANDLERS
        // EVENT HANDLERS > MANAGE
        onNew() {
            if (!this.canUnselectModel()) {
                return;
            }

            this.unselectModel();
            this.showModal();
        },
        onModelCreatedOrUpdatedByDetail(modelWithDetail) {
            const modelId = modelWithDetail.id;

            let modelIndex = this.getModelIndexById(modelId);
            (modelIndex !== -1) ? this.onModelUpdated(modelWithDetail) : this.onModelCreated(modelWithDetail, false);
        },
        // onModelUpdatedByDetail(modelWithDetail, modelIndex) {
        //     const details = modelWithDetail.details;
        //     delete modelWithDetail.details;
        //
        //     const oldModel = deepClone(this.models[modelIndex]);
        //
        //     const newModel = Object.assign(oldModel, modelWithDetail);
        //
        //     this.onModelUpdated(newModel);
        // },
        onModelCreated(model, withSelect = true) {
            //todo: why on create update model? If created child item? Then `updated` event should be thrown.
            // const modelIndex = this.getModelIndexById(model);

            // if (modelIndex === -1) {
            this.unshiftModel(model);

            if (withSelect && (!this.hasModal || this.isModalVisible)) {
                this.selectModel(model);
            } else {
                this.unselectModel();
            }
            // } else {
            //     this.setModelsModelByIndex(modelIndex, model);
            //     this.selectModel(model);
            // }
        },
        onModelsCreated(models) {
            models.forEach(function(model) {
                this.onModelCreated(model, false);
            }, this);
        },
        onModelUpdate(id, model, customUrl) {
            this.updateModel(id, model, customUrl);
        },
        updateModel(id, model, customUrl = null) {
            const ajaxEntityManager = this.getAjaxEntityManagerByUrl(customUrl);
            ajaxEntityManager.update(id, model);
        },
        getAjaxEntityManagerByUrl(customUrl = null) {
            const entityManagerConfig = {
                onSuccessUpdate: this.onModelUpdated,
            };

            return this.entityManagersProvider.get(customUrl, entityManagerConfig);
        },

        onModelUpdated(model) {
            this.updateModelsModelByModel(model);
            this.updateSelectedModelIfRequired(model);
            this.updateSelectedByCheckboxesIfRequired(model);
        },
        onModelsUpdated(models) {
            models.forEach(this.onModelUpdated);
        },
        onModelDeleted(modelId) {
            this.deleteModelFromModels(modelId);
            this.unselectModel();
        },
        onDuplicate() {
            const selectedClone = this.cloneSelected(this.selectedModel);
            this.selectModel(selectedClone, true);
        },
        onFileUploaded(response, fileList) {
            const responseData = response.data;

            this.updateReferences(responseData.references);
            this.onModelCreated(responseData.model);
            this.notifySuccess(responseData.message);
        },

        // EVENT HANDLERS > SELECT
        onModelSelect(model) {
            if (this.canSelectModel(model)) {
                this.selectModel(model);
            }

            if (this.showModalOnSelect) {
                this.showModalIfRequired();
            }
        },
        onModelUnselect() {
            if (!this.canUnselectModel(null)) {
                return;
            }

            this.unselectModel();
        },
        // onNew() {
        //     this.unselectModel();
        // },

        // EVENT HANDLERS > MODAL
        onModalClose() {
            this.hideModal();
            this.unselectModel();
        },

        // EVENT HANDLERS > EXTRA MODALS
        onExtraModalShow(modalName) {
            this.extraModals[modalName].connector.attrs.visible = true;
        },
        onExtraModalHide(modalName) {
            this.extraModals[modalName].connector.attrs.visible = false;
        },
        onExtraModalDisable(modalName) {
            this.extraModals[modalName].connector.attrs.disabled = true;
        },
        onExtraModalEnable(modalName) {
            this.extraModals[modalName].connector.attrs.disabled = false;
        },

        // Notify methods
        notifySuccess(message) {
            this.$toastr.s(message);
        },

        // data setters
        setModel(model) {
            this.model = model;
        },
    },
    computed: {
        showModalOnSelect() {
            return (this.behavior.showModalOnSelect !== undefined) ? this.behavior.showModalOnSelect : true;
        },
        hasModal() {
            return !!this.$refs.modal;
        },

        filterIsDirty() {
            return !isEqual(this.filter, this.initialFilter);
        },
    }
});
