@extends('layouts.page.two-columns.table-and-form')

@section('header')Соответствия видов затрат DKV-инвойсов@endsection

@section('table')
    <dkv-expense-conformity-table
        :models="models"
        :selected-model="selectedModel"

        :expenses="references.expenses"
        :expense-money-transfer-type-subcategories="references.expenseMoneyTransferTypeSubcategories"

        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></dkv-expense-conformity-table>
@endsection

@section('form')
    <dkv-expense-conformity-form
        :value="selectedModel"

        :expenses="references.expenses"
        :expense-money-transfer-type-subcategories="references.expenseMoneyTransferTypeSubcategories"

        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="form"
    ></dkv-expense-conformity-form>
@endsection
