@extends('layouts.page.two-columns.table-and-form')

@section('header')Соответствия видов затрат банковских выписок@endsection

@section('table')
    <bank-expense-conformity-table
        :models="models"
        :selected-model="selectedModel"

        :expenses="references.expenses"
        :expense-money-transfer-type-subcategories="references.expenseMoneyTransferTypeSubcategories"

        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></bank-expense-conformity-table>
@endsection

@section('form')
    <bank-expense-conformity-form
        :value="selectedModel"

        :expenses="references.expenses"
        :expense-money-transfer-type-subcategories="references.expenseMoneyTransferTypeSubcategories"

        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="form"
    ></bank-expense-conformity-form>
@endsection
