@extends('layouts.page.one-column.table')

@section('header')Отчёты по грузовикам (парковым)@endsection

@section('table')
    <park-car-table-report
        :models="models"
    ></park-car-table-report>
@endsection
