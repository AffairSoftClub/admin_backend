@extends('reports.vehicles.base.show')

@section('header')Грузовик (парковый) {{$model->getNumber()}}@endsection

@section('summary')
    <vehicle-info-total
        :summary="{{json_encode($summary)}}"
    ></vehicle-info-total>
@endsection

@section('filter')
    <park-car-report-filter
        :references="references"
        :value="filter"
    ></park-car-report-filter>
@endsection

@section('table')
    <park-car-shipment-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></park-car-shipment-table>

    <park-car-shipment-modal
        :value="selectedModel"
        @unselect="onModelUnselect"
    ></park-car-shipment-modal>
@endsection

@section('form')
    <park-car-expense-groups-block
        :models="model.cashbook_items_with_relations"
        ref="form"
    ></park-car-expense-groups-block>
@endsection
