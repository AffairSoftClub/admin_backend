@extends('layouts.page.one-column.table')

@section('header')Отчёты по прицепам@endsection

@section('table')
    <trailer-table-report
        :models="models"
    ></trailer-table-report>
@endsection
