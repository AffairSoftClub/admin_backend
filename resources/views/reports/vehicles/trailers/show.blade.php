@extends('reports.vehicles.base.show')

@section('header')Прицеп {{$model->getNumber()}}@endsection

@section('summary')
    <vehicle-info-total
        :summary="{{json_encode($summary)}}"
    ></vehicle-info-total>
@endsection

@section('buttons')
    <assign-cashbook-items-to-park-car-button
        :filter="filter"
        :filter-is-dirty="filterIsDirty"
        :park-cars="references.parkCars"
        :trailer-id="model.id"
        :assign-to-park-car-route="routes.assignToParkCar"
        :cashbook-items="model.cashbook_items_with_relations"
        @submited="setModel"
    ></assign-cashbook-items-to-park-car-button>
@endsection

@section('filter')
    <trailer-report-filter
        :references="references"
        :value="filter"
    ></trailer-report-filter>
@endsection

@section('table')
    {{--
        <trailer-car-days-stat-table
            :models="models"
            :selected-model="selectedModel"

            @updated="onModelUpdated"
            @select="onModelSelect"
            @unselect="onModelUnselect"
        ></trailer-car-days-stat-table>
    --}}

    <trailer-linking-to-park-car-log-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
        :park-cars="references.parkCars"
        :users="references.users"
        :trailer-id="model.id"
    ></trailer-linking-to-park-car-log-table>


    <park-car-shipment-modal
        :value="selectedModel"
        @unselect="onModelUnselect"
    ></park-car-shipment-modal>
@endsection

@section('form')
    <trailer-expense-groups-block
        :models="model.cashbook_items_with_relations"
        ref="form"
    ></trailer-expense-groups-block>
@endsection
