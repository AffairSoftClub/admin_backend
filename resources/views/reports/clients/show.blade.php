@extends('layouts.page.two-columns.table-and-form')

@section('header')Контрагент {{$model->getName()}}@endsection

@section('summary')
    Summary
    {{--
    <vehicle-info-total
        :summary="{{json_encode($summary)}}"
    ></vehicle-info-total>
--}}
@endsection

@section('filter')
    Filter
{{--
    <park-car-report-filter
        :references="references"
        :value="filter"
    ></park-car-report-filter>
--}}
@endsection

@section('table')
    <client-report-cashbook-table
        :models="models"

        :money-accounts="references.moneyAccounts"
        :general-money-transfer-types="references.generalMoneyTransferTypes"
        :money-transfer-type-categories="references.moneyTransferTypeCategories"
        :money-transfer-type-subcategories="references.moneyTransferTypeSubcategories"
        :cars="references.cars"
        :trailers="references.trailers"

    ></client-report-cashbook-table>
{{--
    <park-car-shipment-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></park-car-shipment-table>

    <park-car-shipment-modal
        :value="selectedModel"
        @unselect="onModelUnselect"
    ></park-car-shipment-modal>
--}}
@endsection

@section('form')
    Form
{{--
    <park-car-expense-groups-block
        :models="model.cashbook_items_with_relations"
        ref="form"
    ></park-car-expense-groups-block>
--}}
@endsection
