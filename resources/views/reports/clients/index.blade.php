@extends('layouts.page.one-column.table')

@section('header')Отчёт по контрагентам@endsection

@section('table')
    <client-report-table
        :models="models"
    ></client-report-table>
@endsection
