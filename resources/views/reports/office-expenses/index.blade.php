@extends('layouts.app')

@section('content')
    @include( '.layouts._left_sidebar')
    <div class="main-panel" id="main-panel">
        @include('layouts.navbar')
        @include('.menu.menu')

        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <div class="element"><h4 class="card-title">Затраты офиса</h4></div>
                                <kudir-office-info-total
                                    :kudirs="{{json_encode($kudirs)}}"
                                ></kudir-office-info-total>
                            </div>

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <kudir-table-office-list
                                    :kudirs-list="{{json_encode($kudirs)}}"
                                    :clients-list="{{json_encode($clientsList)}}"
                                    :trailers-list="{{json_encode($trailersList)}}"
                                    :cars-list="{{json_encode($carsList)}}"
                                    :expenses-list="{{json_encode($expensesList)}}"
                                ></kudir-table-office-list>
                                {{$kudirs->links()}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-plainxxx">
                        <div class="card-header">
                            <h4 class="card-title"> Детально</h4>
                        </div>
                        <div class="card-body">

                            <div class="container">
                                <div class="row">
                                    <kudir-form-office-show
                                        :kudirs="{{json_encode($kudirsList)}}"
                                    ></kudir-form-office-show>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        @include('.layouts._footer')

    </div>
@endsection
