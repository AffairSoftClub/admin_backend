@extends('layouts.page.two-columns.table-and-form')

@section('header')Контрагенты@endsection

@section('filter')
    <client-filter
        :value="filter"
    ></client-filter>
@endsection

@section('table')
    <client-table
        :models="models"
        :selected-model="selectedModel"
        :selected-by-checkboxes-models="selectedByCheckboxesModels"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
        ref="table"
    ></client-table>
@endsection

@section('form')
    <client-form
        :value="selectedModel"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="form"
    ></client-form>
@endsection
