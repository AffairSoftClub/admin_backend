@extends('layouts.page.one-column.table')

@section('header')Шаблоны e-mail'ов@endsection

@section('table')
    <email-template-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></email-template-table>
@endsection

@section('buttons')
@endsection

@section('modal')
    <email-template-modal-form
        :value="selectedModel"
        :visible="isModalVisible"
        @close="onModalClose"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="modal"
    ></email-template-modal-form>
@endsection
