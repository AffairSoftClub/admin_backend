@extends('layouts.page.one-column.table')

@section('header')Менеджеры@endsection

@section('buttons')
    <button-create
        @click="onNew"
    ></button-create>
@endsection

@section('table')
    <manager-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></manager-table>
@endsection

@section('modal')
    <manager-modal-form
        :value="selectedModel"
        :visible="isModalVisible"
        @close="onModalClose"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="modal"
    ></manager-modal-form>
@endsection
