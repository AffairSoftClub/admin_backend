@extends('layouts.page.one-column.table')

@section('header')Автомобили (офисные)@endsection

@section('buttons')
    <button-create
        @click="onNew"
    ></button-create>
@endsection

@section('table')
    <office-car-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></office-car-table>
@endsection

@section('modal')
    <office-car-modal-form
        :value="selectedModel"
        :visible="isModalVisible"
        @close="onModalClose"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="modal"
    ></office-car-modal-form>
@endsection
