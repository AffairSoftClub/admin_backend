@extends('layouts.page.one-column.table')

@section('header')Грузовики (автопарка)@endsection

@section('buttons')
    <button-create
        @click="onNew"
    ></button-create>
@endsection

@section('table')
    <park-car-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></park-car-table>
@endsection

@section('modal')
    <park-car-modal-form
        :value="selectedModel"
        :visible="isModalVisible"
        :references="references"
        @close="onModalClose"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="modal"
    ></park-car-modal-form>
@endsection
