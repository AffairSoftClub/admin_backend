@extends('layouts.page.one-column.table')

@section('header')Прицепы@endsection

@section('buttons')
    <button-create
        @click="onNew"
    ></button-create>
@endsection

@section('table')
    <trailer-table
        :models="models"
        :selected-model="selectedModel"

        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></trailer-table>
@endsection

@section('modal')
    <trailer-modal-form
        :value="selectedModel"
        :visible="isModalVisible"
        :references="references"
        @close="onModalClose"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="modal"
    ></trailer-modal-form>
@endsection
