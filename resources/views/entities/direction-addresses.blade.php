@extends('layouts.page.two-columns.table-and-form')

@section('header')Справочник адресов@endsection

@section('filter')
@endsection

@section('table')
    <address-table
        :models="models"
        :selected-model="selectedModel"
        @updated="onModelUpdated"
        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></address-table>
@endsection

@section('form')
    <address-form
        :directions="references.directions"
        :value="selectedModel"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        @add-relation="onAddRelation"
        ref="form"
    ></address-form>
@endsection
