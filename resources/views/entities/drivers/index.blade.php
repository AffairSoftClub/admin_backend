@extends('layouts.app')

@section('content')
@include( '.layouts._left_sidebar')
    <div class="main-panel" id="main-panel">
        @include('layouts.navbar')

        @include('.menu.menu')


        <div class="content">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Водители</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <driver-table
                                           :userlist="{{json_encode($users)}}"
                                           :categories="{{json_encode($categories)}}"
                                ></driver-table>
                            @if(true)
                                {{$users->links()}}
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-plainxxx">
                        <div class="card-header">
                            <h4 class="card-title"> Дополнительно</h4>
                        </div>
                        <div class="card-body">

                            <div class="container">
                                <div class="row">
                                    <driver-form
                                        :categories="{{json_encode($categories)}}"
                                    ></driver-form>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

@include('.layouts._footer')


    </div>
@endsection
