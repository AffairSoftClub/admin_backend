@extends('layouts.app')

@section('content')
@include( '.layouts._left_sidebar')
    <div class="main-panel" id="main-panel">
        @include('layouts.navbar')

        @include('.menu.menu')

        <div class="content">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Сотрудники офиса</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <worker-table
                                           :userlist="{{json_encode($users)}}"
                                ></worker-table>
                            @if(true)
                                {{$users->links()}}
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-plainxxx">
                        <div class="card-header">
                            <h4 class="card-title"> Дополнительно</h4>
                        </div>
                        <div class="card-body">

                            <div class="container">
                                <div class="row">
                                    <worker-form
                                        :appointments="{{json_encode($appointments)}}"
                                    ></worker-form>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

@include('.layouts._footer')


    </div>
@endsection
