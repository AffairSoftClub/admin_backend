@extends('layouts.page.one-column.table')

@section('header')Курсы валют@endsection

@section('table')
    <exchange-rate-table
        :models="models"
        :selected-model="selectedModel"

        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></exchange-rate-table>
@endsection

@section('buttons')
    <button-create
        @click="onNew"
        title="Задать на завтра"
        text="Задать на завтра"
    ></button-create>
@endsection

@section('modal')
    <exchange-rate-modal-form
        :value="selectedModel"
        :visible="isModalVisible"
        :exchange-directions="references.exchangeDirections"
        :currencies="references.currencies"
        :defaults="extras.defaults"
        @close="onModalClose"
        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
        @new="onNew"
        ref="modal"
    ></exchange-rate-modal-form>
@endsection
