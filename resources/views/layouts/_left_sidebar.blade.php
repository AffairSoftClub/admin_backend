<div class="sidebar" data-color="custom" @mouseenter="sidebarMouseEnter" @mouseleave="sidebarMouseLeave">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow | custom"
  -->
    <div class="logo">
        <a href="/" class="simple-text logo-mini d-none">
            <img src="/images/logo.png" alt="logo" class="hidden" hidden>
        </a>
        <a href="/" class="simple-text logo-normal text-center">EUROASIA CARGO</a>
        <a class="collapse-handler" @click="collapseHandlerClick"><i :class="{'now-ui-icons': true, 'arrows-1_minimal-left':!sidebar.autocollapse, 'arrows-1_minimal-right': sidebar.autocollapse}"></i></a>
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        @include('layouts.main-menu')
    </div>
</div>
