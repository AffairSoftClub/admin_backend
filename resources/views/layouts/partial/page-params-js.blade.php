<script>
    window.pagination = {!! json_encode(isset($pagination) ? $pagination : null) !!};
    window.model = {!! json_encode(isset($model) ? $model : null) !!};

    window.summary = {!! json_encode(isset($summary) ? $summary : null) !!};

    window.references = {!! json_encode(isset($references) ? $references : null) !!};
    window.extras = {!! json_encode(isset($extras) ? $extras : null) !!};

    window.routes = {!! json_encode(isset($routes) ? $routes : null) !!};

    window.filter = {!! json_encode(isset($filter) ? $filter : null) !!};

    window.behavior = {!! json_encode(isset($behavior) ? $behavior : []) !!};

    window.usedCallbacks = {!! json_encode(isset($usedCallbacks) ? $usedCallbacks : []) !!};

    window.extraModalsNames = {!! json_encode(isset($extraModalsNames) ? $extraModalsNames : []) !!};
</script>
