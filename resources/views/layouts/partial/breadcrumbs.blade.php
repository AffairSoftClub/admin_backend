@if (count($breadcrumbs))
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <a href="{{ $breadcrumb->url }}" class="navbar-brand">{{ $breadcrumb->title }}</a>
            @else
                <a href="#" class="navbar-brand">{{ $breadcrumb->title }}</a>
            @endif
        @endforeach

@endif
