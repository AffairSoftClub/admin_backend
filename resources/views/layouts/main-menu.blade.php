<ul class="nav">

    <!-- Справочники -->
    <li class=" dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="now-ui-icons education_agenda-bookmark"></i>
            Справочники
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="{{ route('directories.park-cars.index' ) }}">Грузовики (парковые)</a>
            <a class="dropdown-item" href="{{ route('directories.office-cars.index' ) }}">Автомобили (офисные)</a>
            <a class="dropdown-item" href="{{ route('directories.trailers.index' ) }}">Прицепы</a>
            <a class="dropdown-item" href="{{ route('directories.drivers.index') }}">Водители</a>
            <a class="dropdown-item" href="{{ route('directories.clients.index') }}">Контрагенты</a>
            <a class="dropdown-item" href="{{ route('directories.workers.index') }}">Сотрудники офиса</a>
            <a class="dropdown-item" href="{{ route('directories.managers.index') }}">Менеджеры</a>
            <a class="dropdown-item" href="{{ route('directories.direction-addresses.index') }}">Адреса</a>
            <a class="dropdown-item" href="{{ route('directories.drivers-categories.index') }}">Категории водителей</a>
        </div>
    </li>

</ul>
