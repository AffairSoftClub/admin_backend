<div class="col-sm-6">
    <p class="text-dark m-0">Supplier:</p>
    <p class="f-bold">EUROASIA CARGO s.r.o.</p>
    <br>
    <p class="text-dark m-0">Rumunska 1798/1</p>
    <p class="text-dark">12000 Praha</p>
    <div class="row">
        <div class="col-sm-12">
            <img src="{{$logo_src}}" style=" width: 300px;" class="img-fluid" alt="ealogo">
        </div>
        <div class="col-sm-12">
            <table class="table table-sm">
                <tbody>
                <tr>
                    <td style="white-space: nowrap;">Reg. No.:</td>
                    <td class="text-right">24283631</td>
                </tr>
                <tr>
                    <td style="white-space: nowrap;">Vat ID NO.:</td>
                    <td class="text-right"> CZ24283631</td>
                </tr>
                <tr>
                    <td style="white-space: nowrap;">tel.:</td>
                    <td class="text-right">7777103239</td>
                </tr>
                <tr>
                    <td style="white-space: nowrap;">fax.:</td>
                    <td class="text-right">+420222074201</td>
                </tr>
                <tr>
                    <td style="white-space: nowrap;">e-mail:</td>
                    <td class="text-right">s.lacinnicov@euroasiancargo.cz</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-12">
        <p class="f-bold">Registration:</p>
        <span class="small">1-Mestsky soud v Praze spisova znacka C 192996</span>
        <div class="wrap-border">
            <table class="table ">
                <tbody>
                <tr>
                    <td style="white-space: nowrap;">Account. No.:</td>
                    <td class="text-right f-bold">7587119001/5500</td>
                </tr>
                <tr>
                    <td style="white-space: nowrap;">IBAN:</td>
                    <td class="text-right f-bold"> CZ46 5500 0000 0075 8711 9001</td>
                </tr>
                <tr>
                    <td style="white-space: nowrap;">SWIFT:</td>
                    <td class="text-right f-bold">RZBC CZ PP</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
