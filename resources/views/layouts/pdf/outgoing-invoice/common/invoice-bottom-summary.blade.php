<tfoot>
<tr>
    <td colspan="2"></td>
    <td colspan="3"></td>
    <td>Ex. tax</td>
    <td>Tax</td>
    <td>Total</td>
</tr>
<tr>
    <td colspan="2">Zero rate</td>
    <td colspan="3">0%</td>
    <td>{{$invoice->total_amount}} </td>
    <td>0</td>
    <td>{{$invoice->total_amount}} </td>
</tr>
<tr>
    <td colspan="5">Total</td>
    <td>{{$invoice->total_amount}} </td>
    <td>0</td>
    <td>{{$invoice->total_amount}} </td>
</tr>

<tr>
    <td colspan="5"></td>
    <td class="f-bold" colspan="2">Total Due:</td>
    <td class="f-bold">{{$invoice->total_amount}} {{  iconv("UTF-8", "ISO-8859-1//TRANSLIT", '€') }}</td>
</tr>
</tfoot>
