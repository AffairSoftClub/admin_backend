<div class="col-sm-6">
    <p class="text-dark m-0">Remitance information: <span
            class="f-bold">{{ $invoice->number}}</span></p>
    <br><br><br>
    <div class="wrap-border m-3">
        <table class="table table">
            <tbody class="f-bold">
            <tr>
                <td style="white-space: nowrap;">Customer:</td>
                <td class="text-right">{{$invoice->getClient()->getName()}}</td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">Address:</td>
                <td class="text-right"> {{$invoice->getClient()->address_1}}</td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">Vat ID No.:</td>
                <td class="text-right">{{$invoice->getClient()->vat_id}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <br><br>

    <table class="table table-sm">
        <tbody>
        <tr>
            <td style="white-space: nowrap;">Method of payment:</td>
            <td class="text-right">Bank transfer</td>
        </tr>
        <tr>
            <td style="white-space: nowrap;">Date of issuance:</td>
            <td class="text-right"> {{ date('d-m-Y', strtotime($invoice->getIssuanceDate())) }}</td>
        </tr>
        <tr>
            <td style="white-space: nowrap;">Date of taxable supply:</td>
            <td class="text-right"> {{ date('d-m-Y', strtotime($invoice->getTaxableSupplyDate())) }}</td>
        </tr>
        <tr>
            <td style="white-space: nowrap;">Due date:</td>
            <td class="text-right">  {{ date('d-m-Y', strtotime($invoice->getDueDate())) }}</td>
        </tr>
        </tbody>
    </table>
</div>
