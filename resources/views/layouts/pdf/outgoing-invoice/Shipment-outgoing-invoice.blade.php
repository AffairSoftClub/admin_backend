@extends('layouts.pdf.outgoing-invoice.base-outgoing-invoice')

@section('invoice-details')
    @include('layouts.pdf.outgoing-invoice.invoice-details.shipment-invoice-details')
@endsection
