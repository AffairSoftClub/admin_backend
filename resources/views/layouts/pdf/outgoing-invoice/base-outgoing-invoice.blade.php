<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-DirectionType" content="text/html; charset=utf-8">
    <title>Инвойс</title>
    <style>
        body {
            font-family: DejaVu Sans;
            font-size: 80%;
        }

        img {
            width: 100%;
        }

        p {
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .f-bold {
            font-weight: bold;
        }

        .u-line-2 {
            border-bottom: solid 2px black;
        }

        .u-line-1 {
            border-bottom: solid 1px black;
        }

        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>
<header>
    <div class="container-fluid">
        <div class="row" style="border-bottom: solid 1px black;">
            <table width="100%" cellspacing="0" cellpadding="5">
                <tr>
                    <td width="50%" valign="top" class="f-bold">Invoice</td>
                    <td style="text-align: right;" width="50%" valign="top"
                        class="f-bold">{{ $invoice->number}}</td>
                </tr>
            </table>
        </div>
    </div>
</header>

<table width="100%" cellspacing="0" cellpadding="5">
    <tr>
        <td width="50%" valign="top">
            @include('layouts.pdf.outgoing-invoice.common.company-info')
        </td>
        <!--        Конец левой колонки-->
        <td width="50%" valign="top">
            @include('layouts.pdf.outgoing-invoice.common.invoice-info')
        </td>
        <!--        Конец левой колонки-->
    </tr>
</table>

<div class="block"></div>
<hr>

@yield('invoice-details')

</body>
</html>
