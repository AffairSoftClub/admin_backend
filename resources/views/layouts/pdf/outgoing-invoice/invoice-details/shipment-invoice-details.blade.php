<div class="container-fluid mt-3">

    <table style="width: 100%;" class="table table-striped">
        <thead class="u-line-2">
        <tr>
            <th>Item:</th>
            <th>Qty</th>
            <th>Unit</th>
            <th>Unit Price</th>
            <th>Tax %</th>
            <th>Ex.Tax</th>
            <th>Tax</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody class="u-line-1">

        @foreach ($invoice->details as $detail)
            <tr class="u-line-1">
                <td>{{$detail->car_number}} / {{ date('d-M-Y', strtotime($detail->departure)) }} <br>
                    {{ $detail->getCommaSeparatedLoadDirectionNames() . '-' . $detail->getCommaSeparatedUnloadDirectionNames() }}
                    /{{ date('d-M-Y', strtotime($detail->arrival)) }}
                </td>
                <td style="text-align: center;">{{$detail->getQuantity()}}</td>
                <td class="text-center">{{$detail->getUnit()}}</td>
                <td class="text-center">{{$detail->getUnitWithoutVatAmount()}}</td>
                <td class="text-center">{{$detail->getVatPercents()}}</td>
                <td class="text-center">{{$detail->getTotalWithoutVatAmount()}}</td>
                <td class="text-center">{{$detail->getTotalVatAmount()}}</td>
                <td class="text-center">{{$detail->getTotalWithVatAmount() }} {{  iconv("UTF-8", "ISO-8859-1//TRANSLIT", '€') }}  </td>
            </tr>
        @endforeach

        </tbody>

        @include('layouts.pdf.outgoing-invoice.common.invoice-bottom-summary')
    </table>

</div>
