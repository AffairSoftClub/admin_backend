<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Album example · Bootstrap</title>

    <!-- Bootstrap core CSS -->

    <!-- Favicons -->
    <meta name="theme-color" content="#563d7c">


    <style>
        .row{
            width: 100%;
        }
        .col-sm-6{
            width: 50%;
        }
        img{
            width: 100%;
        }
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        hr {
            border-top: 1px solid #495057;
        }
        .wrap-border{
            border: solid 2px black;
        }
    </style>

</head>
<body cz-shortcut-listen="true">
<header>
    <div class="container-fluid">
        <div class="row" style="border-bottom: solid 1px black;">
            <div class="col-sm-12 d-flex justify-content-between">
                <p class="text-dark p-0 m-0">Invoice</p>
                <p class="text-dark p-0 m-0">78994654656</p>
            </div>
        </div>
    </div>
</header>

<main role="main">
    <div class="container-fluid">
        <div class="row" >
            <div class="col-sm-6" style="float: left;">
                <p class="text-dark m-0">Supplier:</p>
                <p class="">EUROASIA CARGO s.r.o.</p>
                <br>
                <p class="text-dark m-0">Rumunska 1798/1</p>
                <p class="text-dark">12000 Praha</p>
                <div class="row">
                    <div class="col-sm-12">
                        <img src="pdf/ealogo.png" class="img-fluid" alt="ealogo">
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-sm">
                            <tbody>
                            <tr><td style="white-space: nowrap;">Reg. No.:</td><td class="text-right">24283631</td></tr>
                            <tr><td style="white-space: nowrap;">VAT ID NO.:</td><td class="text-right"> CZ24283631</td></tr>
                            <tr><td style="white-space: nowrap;">tel.:</td><td class="text-right">7777103239</td></tr>
                            <tr><td style="white-space: nowrap;">fax.:</td><td class="text-right">+420222074201</td></tr>
                            <tr><td style="white-space: nowrap;">e-mail:</td><td class="text-right">s.lacinnicov@euroasiancargo.cz</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-sm-12">
                    <p class="m-0">Registration:</p>
                    <span class="small">1-Mestsky soud v Praze spisova znacka C 192996</span>
                    <div class="wrap-border">
                        <table class="table ">
                            <tbody>
                            <tr><td style="white-space: nowrap;">Account. No.:</td><td class="text-right">7587119001/5500</td></tr>
                            <tr><td style="white-space: nowrap;">IBAN:</td><td class="text-right"> CZ46 5500 0000 0075 8711 9001</td></tr>
                            <tr><td style="white-space: nowrap;">SWIFT:</td><td class="text-right">RZBC CZ PP</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="col-sm-6" style="float: left;">
                <p class="text-dark m-0">Remitance information: <span class="lead">11800281</span></p>
                <br><br><br>
                <div class="wrap-border m-3">
                    <table class="table table">
                        <tbody>
                        <tr><td style="white-space: nowrap;">Customer:</td><td class="text-right">lorem lorem lorem</td></tr>
                        <tr><td style="white-space: nowrap;">Address:</td><td class="text-right"> lorem lorem lorem lorem lorem lorem lorem lorem lorem</td></tr>
                        <tr><td style="white-space: nowrap;">VAT ID No.:</td><td class="text-right">RZBC CZ PP 11800281</td></tr>
                        </tbody>
                    </table>
                </div>
                <br><br>

                <table class="table table-sm">
                    <tbody>
                    <tr><td style="white-space: nowrap;">Method of payment:</td><td class="text-right">Bank transfer</td></tr>
                    <tr><td style="white-space: nowrap;">Date of issuance:</td><td class="text-right"> 01.01.2020</td></tr>
                    <tr><td style="white-space: nowrap;">Date of taxable supply:</td><td class="text-right"> 01.01.2020</td></tr>
                    <tr><td style="white-space: nowrap;">Due date:</td><td class="text-right"> 01.01.2020</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br>

    <div class="container-fluid mt-3">
        <div class="row" >
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Item:</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>Unit Price</th>
                        <th>Tax %</th>
                        <th>Ex.Tax</th>
                        <th>Tax</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Some some soke <br>some meodm dofd</td>
                        <td>1</td>
                        <td>jizda</td>
                        <td>300.00</td>
                        <td>0</td>
                        <td>300.00</td>
                        <td>0</td>
                        <td>300.00</td>
                    </tr>
                    <tr>
                        <td>Some some soke <br>some meodm dofd</td>
                        <td>1</td>
                        <td>jizda</td>
                        <td>300.00</td>
                        <td>0</td>
                        <td>300.00</td>
                        <td>0</td>
                        <td>300.00</td>
                    </tr>
                    </tbody>

                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="3"></td>
                        <td>Ex. tax</td>
                        <td>Tax</td>
                        <td>Total</td>
                    </tr>
                    <tr>
                        <td colspan="2">Zero rate</td>
                        <td colspan="3">0%</td>
                        <td>1200.00</td>
                        <td>0</td>
                        <td>1200.00</td>
                    </tr>
                    <tr>
                        <td colspan="5">Total</td>
                        <td>1200.00</td>
                        <td>0</td>
                        <td>1200.00</td>
                    </tr>

                    <tr>
                        <td colspan="6"></td>
                        <td>Total Due:</td>
                        <td>1200.00</td>
                    </tr>

                    </tfoot>
                </table>
            </div>

        </div>
    </div>



</main>

<footer class="text-muted">

</footer>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

</body></html>
