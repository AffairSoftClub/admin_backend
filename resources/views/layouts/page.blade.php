@extends('layouts.app')

@section('content')
    @include( '.layouts._left_sidebar')
    <div class="main-panel" id="main-panel">
        @include('layouts.navbar')

        @include('.menu.menu')

        <div class="content @yield('page-class')">
            @yield('page-content')
        </div>

{{--        @include('.layouts._footer')--}}
    </div>
@endsection
