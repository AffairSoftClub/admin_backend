@extends('layouts.page.two-columns')

@section('column-1-size')
9
@endsection

@section('column-2-size')
3
@endsection

@section('column-1')
    @include('layouts.page.parts.column1')
@endsection

@section('column-2')
    @include('layouts.page.parts.column2')
@endsection
