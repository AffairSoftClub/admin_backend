@extends('layouts.page')

@section('page-content')
    <div class="row">
        <div class="col-md-@yield('column-1-size')">
            @yield('column-1')
        </div>
        <div class="col-md-@yield('column-2-size') pl-0">
            @yield('column-2')
        </div>
    </div>
@endsection
