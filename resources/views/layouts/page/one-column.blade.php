@extends('layouts.page')

@section('column-1-size')
12
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-@yield('column-1-size')">
            @yield('column-1')
        </div>
    </div>
@endsection
