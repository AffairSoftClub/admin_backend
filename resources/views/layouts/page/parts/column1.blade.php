<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-12"><h4 class="card-title">@yield('header')</h4></div>
            <div class="col-12 text-right">@yield('uploader')</div>
        </div>

        <div class="summary">@yield('summary')</div>
        <div class="buttons">@yield('buttons')</div>
    </div>

    <div class="card-body">
        @yield('filter')
        <div class="table-responsive">
            @yield('table')
            @isset($pagination)
                {{$pagination->links()}}
            @endisset
        </div>
    </div>

    <div class="modal-container">
        @yield('modal')
        @yield('modals')
    </div>
</div>
