<div class="card card-plainxxx">
    <div class="card-header">
        <h4 class="card-title">Дополнительно</h4>
    </div>
    <div class="card-body">
        @yield('form')
    </div>
</div>
