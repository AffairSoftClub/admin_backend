@extends('layouts.importer')

@section('header')
    Банковские выписки (PDF)
@endsection

@section('uploader')
    <pdf-file-uploader
        @uploaded="onFileUploaded"
    ></pdf-file-uploader>
@endsection

@section('table')
    <new-pdf-bank-statement-table
        :models="models"

        :selected-model="selectedModel"

        :currencies="references.currencies"

        @select="onModelSelect"
        @unselect="onModelUnselect"


    ></new-pdf-bank-statement-table>
@endsection

@section('form')
    <new-pdf-bank-statement-form
        :clients="references.clients"
        :expenses="references.expenses"
        :value="selectedModel"
        ref="form"

        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"

        @new="onNew"
    ></new-pdf-bank-statement-form>
@endsection
