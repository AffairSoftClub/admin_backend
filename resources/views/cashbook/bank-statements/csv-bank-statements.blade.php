@extends('layouts.importer')

@section('header')
    Банковские выписки
@endsection

@section('uploader')
    <upload-dkv
        type_upload_url="bank"
        type_upload_text="Bank"
    ></upload-dkv>
@endsection

@section('table')
    <bank-statement-table-new
        :models="models"

        :selected-model="selectedModel"

        @select="onModelSelect"
        @unselect="onModelUnselect"
    ></bank-statement-table-new>

    <bank-statement-table
        :invoices="models"
        :cars="references.cars"
        :expenses="references.expenses"
        type_upload_url="bank"
    ></bank-statement-table>
@endsection

@section('form')
    <bank-statement-form-new
        :clients="references.clients"
        :expenses="references.expenses"
        :value="selectedModel"
        ref="form"

        @created="onModelCreated"
        @updated="onModelUpdated"
        @deleted="onModelDeleted"
    ></bank-statement-form-new>

    <bank-statement-form
        :clients="references.clients"
        :expenses="references.expenses"
        type_upload_url="bank"
    ></bank-statement-form>
@endsection
