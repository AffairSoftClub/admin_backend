<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Entities Cars etc
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'id' => 'ID',
    'number' => 'Number',
    'trailer' => 'Trailer',
    'brand' => 'Brand',
    'model' => 'Model',
    'status' => 'Status',
    'repair' => 'Repairs',
    'from' => 'From',
    'to' => 'To',

];
