<?php

return [
    'csrf_token_mismatch' => 'CSRF токен не совпадает. Пожалуйста, обновите страницу (CTRL+R) и повторите действие.',
];
