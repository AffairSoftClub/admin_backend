<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Entities Cars etc
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'id' => 'ИД',
    'number' => 'Номер',
    'trailer' => 'Прицеп',
    'brand' => 'Марка',
    'model' => 'Модель',
    'status' => 'Статус',
    'repair' => 'Ремонт',
    'from' => 'От',
    'to' => 'До',

];
