# Start
- Добравить в папку `docker/storage/mysql/seeds` текущий дамп бд (если есть).
- Создать файл и настроить файл `.env` по аналогии с `env.example` (`cp .env.example .env`) (`COMPOSER_AUTH`).
- Установить подмодули git: `git submodule init && git submodule update`.
- Усановить зависимости composer'а: `docker-compose run --rm composer composer install`.
- Установить зависимости nodejs и собрать фронт: `docker-compose run --rm node bash -c "runuser -u node -- npm install && runuser -u node -- npm run dev"`.
- Сгенерировать ключ приложения: `docker-compose run --rm php-cli php artisan key:generate`.
- Выполнить миграции и сидинг (если нет дампа бд): `docker-compose run --rm php-cli php artisan migrate:fresh --seed`.
- Запустить все сервисы: `docker-compose up`.

Зайти на [http://localhost/](http://localhost/).

В случае ошибки с правами на файлы вроде:
```
UnexpectedValueException
The stream or file "/var/www/storage/logs/laravel.log" could not be opened in append mode: failed to open stream: Permission denied
```

выполнить: `docker-compose exec php-fpm chown -R www-data:www-data bootstrap/cache storage`.

# Credentials
- Basic Auth: `admin` / `11111111`
- Login: `admin@admin.com` / `iOnA6^Q5vs&p`

# Полезные часто используемые комманды
- Актуализация и сидинг БД: `php artisan migrate:fresh --seed`.
- Обновлене всех подмодулей git:`git submodule foreach git pull origin master`.
- Наблюдение за изменениями файлов фронта: `docker-compose run --rm node runuser -u node -- npm run watch`.

# Разное
- Админка на основе [Now UI](https://demos.creative-tim.com/now-ui-dashboard/docs/1.0/getting-started/introduction.html)
- Makefile на данный момент не актуализирован, но в нём можно посмотреть полезные комманды.
- [Как попросить данные из Laravel в Vue](https://laravel.demiart.ru/four-ways-to-pass-data-from-laravel-to-vue/)
