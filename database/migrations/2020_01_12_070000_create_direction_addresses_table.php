<?php

use App\Domain\Shipment\Direction\Table\DirectionColumnNamesEnum as DirectionColumnNamesEnum;
use App\Domain\Shipment\Direction\Table\DirectionTableNameValue as DirectionTableNameValue;
use App\Domain\Directories\DirectionAddress\Table\DirectionAddressColumnNamesEnum;
use App\Domain\Directories\DirectionAddress\Table\NameValue as TableNameValue;
use App\Base\Database\Migration\CreateTableMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectionAddressesTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        $table->bigIncrements(DirectionAddressColumnNamesEnum::ID);

        $this->createForeignColumn(
            $table,
            DirectionAddressColumnNamesEnum::DIRECTION_ID,
            DirectionTableNameValue::VALUE,
            DirectionColumnNamesEnum::ID,
            false,
            'Id направления'
        );

        $table->text(DirectionAddressColumnNamesEnum::ADDRESS)->nullable()->comment('Адрес');
        $table->text(DirectionAddressColumnNamesEnum::EMAIL)->nullable()->comment('E-mail');
        $table->text(DirectionAddressColumnNamesEnum::PHONE)->nullable()->comment('Телефон');
        $table->text(DirectionAddressColumnNamesEnum::COMMENT)->nullable()->comment('Комментарий');

        $table->timestamps();
    }

    protected function getTableName(): string
    {
        return TableNameValue::VALUE;
    }
}
