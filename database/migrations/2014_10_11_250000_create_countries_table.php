<?php

use App\Domain\Directories\Country\Table\CountryColumnNamesEnum;
use App\Domain\Directories\Country\Table\CountryTableNameValue;
use App\Base\Database\Migration\CreateTableMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        $table->bigIncrements(CountryColumnNamesEnum::ID);

        $table->char(CountryColumnNamesEnum::CODE)
            ->unique()
            ->comment('Код страны');

        $table->string(CountryColumnNamesEnum::NAME_EN)
            ->unique()
            ->comment('Название страны на английском');

        $table->string(CountryColumnNamesEnum::NAME_RU)
            ->unique()
            ->comment('Название страны на русском');

        $table->timestamps();
    }


    protected function getTableName(): string
    {
        return CountryTableNameValue::VALUE;
    }
}
