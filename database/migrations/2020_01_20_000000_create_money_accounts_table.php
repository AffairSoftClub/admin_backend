<?php

use App\Domain\Common\Database\Migration\AmountParamsInterface;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum as ColumnNamesEnum;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountTableNameValue as TableNameValue;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoneyAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->getTableName(), function (Blueprint $table) {
            $table->bigIncrements(ColumnNamesEnum::ID);

            $table->char(ColumnNamesEnum::NUMBER, 10)->unique()->comment('Номер');

            $table->unsignedSmallInteger(ColumnNamesEnum::MAIN_CURRENCY_ID)
                ->comment('Id основной валюты');

            $table->decimal(
                ColumnNamesEnum::AMOUNT,
                AmountParamsInterface::TOTAL,
                AmountParamsInterface::PLACES
            )->default(0)->comment('Текщая сумма');

            $table->decimal(
                ColumnNamesEnum::MONTH_START_AMOUNT,
                AmountParamsInterface::TOTAL,
                AmountParamsInterface::PLACES
            )->default(0)->comment('Сумма на начало месяца');

            $table->unsignedSmallInteger(ColumnNamesEnum::SORT)->default(0);
            $table->boolean(ColumnNamesEnum::IS_MAIN)->default(false);
            $table->boolean(ColumnNamesEnum::IS_CASH)->default(false)->comment('Наличка');

            $table->timestamps();
        });
    }

    private function getTableName(): string
    {
        return TableNameValue::VALUE;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->getTableName());
    }
}
