<?php

use App\Domain\Common\Database\Migration\AmountParamsInterface;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryTableNameValue as TableNameInterface;
use App\Base\Database\Migration\CreateTableMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverCategoriesTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        $table->bigIncrements(DriverCategoryColumnNamesEnum::ID);
        $table->char(DriverCategoryColumnNamesEnum::NAME, 50)->comment('код категория водителя 1 2 .. 5');
        $table
            ->decimal(
                DriverCategoryColumnNamesEnum::MONTH_RATE_AMOUNT,
                AmountParamsInterface::TOTAL,
                AmountParamsInterface::PLACES
            )
            ->default(0)
            ->comment('базовая сумма зароботной платы для водителя на месяц');

        $table->integer(DriverCategoryColumnNamesEnum::STATUS)->default(0)->comment('статус');

        $table->timestamps();
    }


    protected function getTableName(): string
    {
        return TableNameInterface::VALUE;
    }
}
