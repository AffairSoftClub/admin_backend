<?php

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarWorkStatusesEnum;
use App\Domain\Directories\Vehicle\Children\Car\General\Table\GeneralCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\General\Table\GeneralCarTableNameValue as TableNameInterface;
use App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerColumnNamesEnum as TrailerColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerTableNameValue as TrailerTableNameInterface;
use App\Base\Database\Migration\CreateTableMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarOnlyColumnNamesEnum;

class CreateVehiclesTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        // Base
        // id
        $table->bigIncrements(BaseVehicleColumnNamesEnum::ID);

        // type_id
        $table->unsignedSmallInteger(BaseVehicleColumnNamesEnum::TYPE_ID)
            ->index()
            ->comment('Id типа транспортного средства (0-авто, 1-прицеп)');

        // sub_type_id
        $table->unsignedSmallInteger(BaseVehicleColumnNamesEnum::SUB_TYPE_ID)
            ->nullable()
            ->index()
            ->comment('Id подтипа транспортного средства (11-парковые авто, 12-офисные авто, 21 - прицепы)');

        // number
        $table->char(BaseVehicleColumnNamesEnum::NUMBER, 255)
            ->unique()
            ->comment('Номер ТС');

        // work_status_id
        $table->unsignedSmallInteger(BaseVehicleColumnNamesEnum::WORK_STATUS_ID)
            ->default(BaseCarWorkStatusesEnum::GOOD);


        // Car

        // brand
        $table->char(BaseCarOnlyColumnNamesEnum::BRAND, 255)
            ->nullable()
            ->comment('Марка');

        // model
        $table->char(BaseCarOnlyColumnNamesEnum::MODEL, 255)
            ->nullable()
            ->comment('Модель');

        // is_archived
        $table->boolean(BaseCarOnlyColumnNamesEnum::IS_ARCHIVED)
            ->default(false)
            ->comment("Архивирован");

        // Car > Park

        // trailer_id
        $this->createForeignColumn(
            $table,
            ParkCarOnlyColumnNamesEnum::TRAILER_ID,
            TrailerTableNameInterface::VALUE,
            TrailerColumnNamesEnum::ID,
            true,
            'Прицеп',
            null,
            true
        );

        // Car > Office
        $table->string(OfficeCarOnlyColumnNamesEnum::RESPONSIBLE)
            ->nullable()
            ->comment('Ответственный');

        $table->timestamps();
    }


    protected function getTableName(): string
    {
        return TableNameInterface::VALUE;
    }
}
