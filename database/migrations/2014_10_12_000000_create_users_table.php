<?php

use App\Base\Database\Migration\CreateTableMigration;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryColumnNamesEnum as DriverCategoryColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryTableNameValue as DriverCategoryTableNameInterface;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentTableNameValue;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityTableNameValue;
use App\Domain\UsersAndRoles\Users\General\Table\GeneralUserColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\General\Table\GeneralUserTableNameValue as TableNameInterface;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        // Base
        $table->bigIncrements(GeneralUserColumnNamesEnum::ID);
        $table->string(GeneralUserColumnNamesEnum::NAME);
        $table->string(GeneralUserColumnNamesEnum::EMAIL)->unique();
        $table->timestamp(GeneralUserColumnNamesEnum::EMAIL_VERIFIED_AT)->nullable();

        $table->string(GeneralUserColumnNamesEnum::PASSWORD)->nullable();
        $table->rememberToken();

        // Admin or Manager
        $table->text(GeneralUserColumnNamesEnum::EMAIL_SIGNATURE)->nullable();

        // Employee
        $table->string(GeneralUserColumnNamesEnum::PHONE)->nullable();
        $table->text(GeneralUserColumnNamesEnum::ADDRESS)->nullable();

        // Employee > Worker
        $this->createAmountColumn(
            $table,
            GeneralUserColumnNamesEnum::MONTH_RATE_AMOUNT,
            'Месячный оклад',
            false
        );

        $this->createForeignColumn(
            $table,
            GeneralUserColumnNamesEnum::CITY_ID,
            CityTableNameValue::VALUE,
            CityColumnNamesEnum::ID,
            true,
            'Id города'
        );

        $this->createForeignColumn(
            $table,
            GeneralUserColumnNamesEnum::APPOINTMENT_ID,
            AppointmentTableNameValue::VALUE,
            AppointmentColumnNamesEnum::ID,
            true,
            'Id должности'
        );

        // Employee > Driver
        $table->text(GeneralUserColumnNamesEnum::CERTIFICATE)->nullable();

        $this->createForeignColumn(
            $table,
            GeneralUserColumnNamesEnum::DRIVER_CATEGORY_ID,
            DriverCategoryTableNameInterface::VALUE,
            DriverCategoryColumnNamesEnum::ID,
            true,
            'Id категории водителя'
        );


        $table->timestamps();
    }


    protected function getTableName(): string
    {
        return TableNameInterface::VALUE;
    }
}
