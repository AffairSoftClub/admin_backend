<?php

use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;
use App\Domain\Directories\Client\Table\ClientTableNameValue;
use App\Base\Database\Migration\CreateTableMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        $table->bigIncrements(ClientColumnNamesEnum::ID);

        $table->string(ClientColumnNamesEnum::NAME)->unique();

        $table->string(ClientColumnNamesEnum::REGISTRATION_NUMBER)->nullable()->default(null);
        $table->string(ClientColumnNamesEnum::VAT_ID)->nullable()->default(null)->unique();

        $table->string(ClientColumnNamesEnum::LEGAL_ADDRESS)->nullable()->default(null);
        $table->string(ClientColumnNamesEnum::POSTAL_ADDRESS)->nullable()->default(null);

        $table->string(ClientColumnNamesEnum::PHONE)->nullable()->default(null);
//            $table->text(ClientColumnNamesEnum::EMAILS)->nullable();
        $table->string(ClientColumnNamesEnum::EMAILS)->default(json_encode([]));

        $table->boolean(ClientColumnNamesEnum::IS_ARCHIVED)->default(false);

        $table->unsignedTinyInteger(ClientColumnNamesEnum::SYS_ID)
            ->nullable()
            ->comment('Системный ID (для специальных клиентов, например DKV)');

        $table->timestamps();
    }


    protected function getTableName(): string
    {
        return ClientTableNameValue::VALUE;
    }
}
