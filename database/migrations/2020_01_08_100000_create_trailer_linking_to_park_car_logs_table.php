<?php

use App\Base\Database\Migration\CreateTableMigration;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarTableNameValue;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table\TrailerLinkingToParkCarLogColumnNamesEnum as ColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table\TrailerLinkingToParkCarLogTableNameValue as TableNameInterface;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerColumnNamesEnum as TrailerColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerTableNameValue as TrailerTableNameInterface;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\BaseAdminOrManagerColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\BaseAdminOrManagerTableNameValue;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrailerLinkingToParkCarLogsTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        // Base
        // id
        $table->bigIncrements(ColumnNamesEnum::ID);

        $table->timestamp(ColumnNamesEnum::TIMESTAMP)
            ->comment('Timestamp изменения');

        // park_car_id
        $this->createForeignColumn(
            $table,
            ColumnNamesEnum::PARK_CAR_ID,
            ParkCarTableNameValue::VALUE,
            ParkCarColumnNamesEnum::ID,
            false,
            'Грузовик (парковый)'
        );

        // old_trailer_id
        $this->createForeignColumn(
            $table,
            ColumnNamesEnum::OLD_TRAILER_ID,
            TrailerTableNameInterface::VALUE,
            TrailerColumnNamesEnum::ID,
            true,
            'Старый id прицепа'
        );

        // new_trailer_id
        $this->createForeignColumn(
            $table,
            ColumnNamesEnum::NEW_TRAILER_ID,
            TrailerTableNameInterface::VALUE,
            TrailerColumnNamesEnum::ID,
            true,
            'Новый id прицепа'
        );

        // user_id
        $this->createForeignColumn(
            $table,
            ColumnNamesEnum::USER_ID,
            BaseAdminOrManagerTableNameValue::VALUE,
            BaseAdminOrManagerColumnNamesEnum::ID,
            false,
            'Пользователь изменивший связь'
        );
    }


    protected function getTableName(): string
    {
        return TableNameInterface::VALUE;
    }
}
