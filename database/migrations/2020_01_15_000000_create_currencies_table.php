<?php

use App\Domain\Cashbook\Currency\Table\CurrencyTableNameValue;
use App\Base\Database\Migration\CreateTableMigration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use \App\Domain\Cashbook\Currency\Table\CurrencyColumnNamesEnum;

class CreateCurrenciesTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        $table->bigIncrements(CurrencyColumnNamesEnum::ID);

        $table->smallInteger(CurrencyColumnNamesEnum::SORT_ORDER)
            ->nullable()
            ->comment('Сортировка');

        $table->char(CurrencyColumnNamesEnum::CODE, 3)
            ->comment('Код валюты');

        $table->string(CurrencyColumnNamesEnum::SYMBOL, 2)
            ->comment('Символ валюты');

        $table->string(CurrencyColumnNamesEnum::NAME_RU)
            ->comment('Название на русском языке');

        $table->timestamps();
    }


    protected function getTableName(): string
    {
        return CurrencyTableNameValue::VALUE;
    }
}
