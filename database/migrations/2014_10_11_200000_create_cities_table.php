<?php

use App\Base\Database\Migration\CreateTableMigration;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityTableNameValue;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTable extends CreateTableMigration
{
    protected function createOperations(Blueprint $table)
    {
        $table->bigIncrements(CityColumnNamesEnum::ID);
        $table->string(CityColumnNamesEnum::NAME);

        $table->timestamps();
    }

    protected function getTableName(): string
    {
        return CityTableNameValue::VALUE;
    }
}
