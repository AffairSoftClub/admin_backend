<?php

namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domain\Directories\Client\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->company,
        'vat_id'=>$faker->unique()->swiftBicNumber,
        'address_1'=>$faker->unique()->address,
        'phone'=>$faker->unique()->phoneNumber,
        'email'=>$faker->unique()->email,
    ];
});

//$factory->define(\App\Model\Entity\Trailer::class, function (Faker $faker) {
//    $carBrands=["Abarth", "Alfa Romeo", "Aston Martin", "Audi", "Bentley", "BMW", "Bugatti", "Cadillac", "Chevrolet", "Chrysler", "Citroën", "Dacia", "Daewoo", "Daihatsu", "Dodge", "Donkervoort", "DS", "Ferrari", "Fiat", "Fisker", "Ford", "Honda", "Hummer", "Hyundai", "Infiniti", "Iveco", "Jaguar", "Jeep", "Kia", "KTM", "Lada", "Lamborghini", "Lancia", "Land Rover", "Landwind", "Lexus", "Lotus", "Maserati", "Maybach", "Mazda", "McLaren", "Mercedes-Benz", "MG", "Mini", "Mitsubishi", "Morgan", "Nissan", "Opel", "Peugeot", "Porsche", "Renault", "Rolls-Royce", "Rover", "Saab", "Seat", "Skoda", "Smart", "SsangYong", "Subaru", "Suzuki", "Tesla", "Toyota", "Volkswagen", "Volvo"];
//
//    return [
//        'number' => $faker->unique()->swiftBicNumber,
//    ];
//
//});



//$table->text('name')->nullable();
//$table->char('base_id',255)->nullable();
//$table->char('vat_id',255)->unique();
//$table->text('address_1')->nullable();
//$table->text('address_2')->nullable();
//$table->text('phone')->nullable();
//$table->text('email')->nullable();
