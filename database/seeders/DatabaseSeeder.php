<?php

namespace Database\Seeders;

use App\Domain\Base\Seeds\BaseAggregateSeeder;
use App\Domain\Directories\Client\Seeds\ClientsSeeder;
use App\Domain\Directories\Country\Seeds\CountriesSeeder;
use App\Domain\Directories\DirectionAddress\Seeds\DirectionAddressesSeeder;
use App\Domain\Directories\Vehicle\Children\Car\Office\Seeds\OfficeCarsSeeder;
use App\Domain\Directories\Vehicle\Children\Car\Park\Seeds\ParkCarsSeeder;
use App\Domain\Directories\Vehicle\Children\Trailer\Seeds\TrailerSeeder;
use App\Domain\Shipment\Direction\Seeds\DirectionsSeeder;
use App\Domain\UsersAndRoles\Users\All\Seeds\UsersSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\DriverCategoriesSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Seeds\AppointmentsSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Seeds\CitiesSeeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends BaseAggregateSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::reguard();

        // SYSTEM
        $this->call(VoyagerDatabaseSeeder::class);
        $this->call(DriverCategoriesSeeder::class);

        $this->call(CountriesSeeder::class);
        $this->call(CitiesSeeder::class);

        $this->call(AppointmentsSeeder::class);
        $this->call(UsersSeeder::class);

        // todo: Create all languages en and add russian translation
        // $this->call(TranslationsPtTableSeeder::class);

        // DIRECTORIES
        $this->call(TrailerSeeder::class);

        $this->call(ParkCarsSeeder::class);
        $this->call(OfficeCarsSeeder::class);

        $this->call(ClientsSeeder::class);
        $this->call(DirectionsSeeder::class);
        $this->call(DirectionAddressesSeeder::class);
    }
}
