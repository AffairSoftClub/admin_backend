<?php

namespace Database\Seeders\Voyager;

use TCG\Voyager\Models\Permission;

class PermissionsTableSeeder extends BaseVoyagerSeeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $keys = [
            'browse_admin',
            'browse_bread',
            'browse_database',
            'browse_media',
            'browse_compass',
            'browse_hooks', // было в дампе но не было в миграциях, возможно не нужно
        ];

        foreach ($keys as $key) {
            Permission::firstOrCreate([
                'key' => $key,
                'table_name' => null,
            ]);
        }

        Permission::generateFor('menus');

        Permission::generateFor('roles');

        Permission::generateFor('users');

        Permission::generateFor('settings');

//        Permission::generateFor('admins_or_managers');
    }
}
