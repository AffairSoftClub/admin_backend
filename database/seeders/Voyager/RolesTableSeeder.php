<?php

namespace Database\Seeders\Voyager;

use App\Domain\UsersAndRoles\Role\Role;
use App\Domain\UsersAndRoles\Role\Seeds\Types\AdminInterface;
use App\Domain\UsersAndRoles\Role\Seeds\Types\DriverInterface;
use App\Domain\UsersAndRoles\Role\Seeds\Types\ManagerInterface;
use App\Domain\UsersAndRoles\Role\Seeds\Types\WorkerInterface;
use App\Domain\UsersAndRoles\Role\Table\RoleColumnNamesEnum as ColumnNameEnum;

class RolesTableSeeder extends BaseVoyagerSeeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $roles = [
            [
                ColumnNameEnum::NAME => AdminInterface::NAME,
                ColumnNameEnum::DISPLAY_NAME => AdminInterface::DISPLAY_NAME,
            ],
            [
                ColumnNameEnum::NAME => ManagerInterface::NAME,
                ColumnNameEnum::DISPLAY_NAME => ManagerInterface::DISPLAY_NAME,
            ],
//          Пока комментирую, т.к. на данный момен нет не используется.
//
//            [
//                ColumnNameEnum::NAME => UserInterface::NAME,
//                ColumnNameEnum::DISPLAY_NAME => UserInterface::DISPLAY_NAME,
//            ],
            /* В будущем вероятно появятся следующие роли со своими правами:
            - бухгалтер - начисляет ЗП и смотрит отчеты;
            - оператор - ставит маршруты;
            - менеджер - отправляет инвойсы;
            - директор - смотрит отчеты
            */
            [
                ColumnNameEnum::NAME => WorkerInterface::NAME,
                ColumnNameEnum::DISPLAY_NAME => WorkerInterface::DISPLAY_NAME,
            ],
            [
                ColumnNameEnum::NAME => DriverInterface::NAME,
                ColumnNameEnum::DISPLAY_NAME => DriverInterface::DISPLAY_NAME,
            ],
        ];

        $this->addRolesIfNotExists($roles);
    }

    private function addRolesIfNotExists(array $roles)
    {
        foreach ($roles as $role) {
            $this->addRoleIfNotExists($role);
        }
    }

    private function addRoleIfNotExists(array $role)
    {
        Role::firstOrCreate(
            [ColumnNameEnum::NAME => $role[ColumnNameEnum::NAME]],
            [ColumnNameEnum::DISPLAY_NAME => $role[ColumnNameEnum::DISPLAY_NAME]]
        );
    }
}
