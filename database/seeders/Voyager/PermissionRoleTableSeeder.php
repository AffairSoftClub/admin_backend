<?php

namespace Database\Seeders\Voyager;

use TCG\Voyager\Models\Permission;
use App\Domain\UsersAndRoles\Role\Role;

class PermissionRoleTableSeeder extends BaseVoyagerSeeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $this->addAdminPermissions();
        $this->addManagersPermissions();
    }

    private function addAdminPermissions()
    {
        $role = Role::where('name', 'admin')->firstOrFail();

        $permissions = Permission::all();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );
    }

    private function addManagersPermissions()
    {
        $role = Role::where('name', 'manager')->firstOrFail();

        $permissions = Permission::all();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );
    }
}
