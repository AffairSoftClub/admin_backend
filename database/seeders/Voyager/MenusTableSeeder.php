<?php

namespace Database\Seeders\Voyager;

use TCG\Voyager\Models\Menu;

class MenusTableSeeder extends BaseVoyagerSeeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        Menu::firstOrCreate([
            'name' => 'admin',
        ]);
    }
}
