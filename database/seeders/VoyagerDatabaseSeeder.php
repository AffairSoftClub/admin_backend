<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;
use Database\Seeders\Voyager\DataRowsTableSeeder;
use Database\Seeders\Voyager\DataTypesTableSeeder;
use Database\Seeders\Voyager\MenuItemsTableSeeder;
use Database\Seeders\Voyager\MenusTableSeeder;
use Database\Seeders\Voyager\PermissionRoleTableSeeder;
use Database\Seeders\Voyager\PermissionsTableSeeder;
use Database\Seeders\Voyager\RolesTableSeeder;
use Database\Seeders\Voyager\SettingsTableSeeder;

class VoyagerDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__ . '/';

    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $this->seed(DataTypesTableSeeder::class);
        $this->seed(DataRowsTableSeeder::class);
        $this->seed(MenusTableSeeder::class);
        $this->seed(MenuItemsTableSeeder::class);
        $this->seed(RolesTableSeeder::class);
        $this->seed(PermissionsTableSeeder::class);
        $this->seed(PermissionRoleTableSeeder::class);
        $this->seed(SettingsTableSeeder::class);
    }
}
