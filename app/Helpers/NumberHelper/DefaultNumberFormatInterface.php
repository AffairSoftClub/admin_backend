<?php

namespace App\Helpers\NumberHelper;

interface DefaultNumberFormatInterface
{
    const DECIMALS = 0;
    const DECIMAL_POINT = '.';
    const THOUSANDS_SEPARATOR = ',';
}
