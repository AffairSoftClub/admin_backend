<?php


namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadHelper
{


    /**
     * обработчик загрузчик DKV csv
     * @param string $path
     * @return array
     */
    public static function loadCsvDKVDataDEL(string $path): array
    {
//       $result=[];
//        $filePath=  Storage::disk('public')->path($path);
//        $handle = fopen($filePath,'r');
//        $ii=0;
//        while ( ($data = fgetcsv($handle,1000000,';') ) !== FALSE ) {
//            //dd($data);
//            //if($ii>11)dd($data);
//            $result[]=$data;
//            $ii++;
//        }
//        fclose($handle);
//       return $result;

        $filePath = Storage::disk('public')->path($path);
        $text = fopen($filePath, "r");
        $array = [];
        if ($text) {
            while (($buffer = fgets($text)) !== false) {
                $array[] = $buffer;
            }
        }
        fclose($text);
        $result = [];
        foreach ($array as $k => $value) {

            $r = str_getcsv($value, ';', '"');
//            if($k>11)dd($r);
            $result[] = $r;
        }
        return $result;


    }

    public static function loadCsvDKVData(string $path): array
    {
        $result = [];
        $filePath = Storage::disk('public')->path($path);
        $handle = fopen($filePath, 'r');
        $ii = 0;
        while (($data = fgetcsv($handle, 1000000, ';')) !== FALSE) {
            $result[] = $data;
            $ii++;
        }
        fclose($handle);
        return $result;
    }

    public static function getFilenameWithouExtByRequest(Request $request)
    {
        $filehashName = $request->file('file')->getClientOriginalName();
        $path_parts = pathinfo($filehashName);
        return isset($path_parts['filename']) ? $path_parts['filename'] : '';
    }
}
