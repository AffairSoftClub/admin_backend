<?php

namespace App\Helpers;

use ReflectionClass;

class ReflectionHelper
{
    public function getClassConstants(string $className): array
    {
        $reflection = new ReflectionClass($className);

        return $reflection->getConstants();
    }

    public function getClassConstantsValues(string $className): array
    {
        $classConstants = $this->getClassConstants($className);

        return array_values($classConstants);
    }
}
