<?php

namespace App\Helpers\ValuesLoader\PdfValuesLoader;

use Smalot\PdfParser\Parser as PdfParser;

class PdfFileDataLoader implements PdfFileDataLoaderInterface
{
    /** @var PdfParser */
    private $pdfParser;

    public function __construct(PdfParser $pdfParser)
    {
        $this->pdfParser = $pdfParser;
    }

    public function getDataFromFile(string $filename): array
    {
        $pdf = $this->parsePdfFile($filename);

        $ret = [];

        foreach ($pdf->getPages() as $page) {
            $ret[] = $page->getText();
        }

        return $ret;
    }

    private function parsePdfFile(string $filename)
    {
        return $this->pdfParser->parseFile($filename);
    }
}
