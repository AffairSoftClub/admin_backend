<?php

namespace App\Helpers\ValuesLoader\AnySeparatedValuesLoader;

use App\Helpers\ValuesLoader\FileDataLoaderInterface;

interface AnySeparatedValuesFileDataLoaderInterface extends FileDataLoaderInterface
{

}
