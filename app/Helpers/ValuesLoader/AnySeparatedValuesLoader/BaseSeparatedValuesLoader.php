<?php

namespace App\Helpers\ValuesLoader\AnySeparatedValuesLoader;

abstract class BaseSeparatedValuesLoader implements AnySeparatedValuesFileDataLoaderInterface
{
    public function getDataFromFile(string $filename): array
    {
        $handle = fopen($filename, 'r');

        $results = [];
        $separator = $this->getSeparator();

        while (($data = fgetcsv($handle, 0, $separator)) !== FALSE) {
            $results[] = $data;
        }

        fclose($handle);

        return $results;
    }

    abstract protected function getSeparator(): string;
}
