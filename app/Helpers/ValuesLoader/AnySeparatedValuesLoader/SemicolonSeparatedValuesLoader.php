<?php

namespace App\Helpers\ValuesLoader\AnySeparatedValuesLoader;

class SemicolonSeparatedValuesLoader extends BaseSeparatedValuesLoader
{
    protected function getSeparator(): string
    {
        return ';';
    }
}
