<?php

namespace App\Helpers\ValuesLoader\AnySeparatedValuesLoader;

class TabSeparatedValuesLoader extends BaseSeparatedValuesLoader
{
    protected function getSeparator(): string
    {
        return "\t";
    }
}
