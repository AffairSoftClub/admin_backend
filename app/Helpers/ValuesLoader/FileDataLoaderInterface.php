<?php

namespace App\Helpers\ValuesLoader;

interface FileDataLoaderInterface
{
    public function getDataFromFile(string $filename): array;
}
