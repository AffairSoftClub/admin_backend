<?php

namespace App\Helpers;

use function bcrypt;

class PasswordHasherHelper
{
    public function hash($password)
    {
        return bcrypt($password);
    }
}
