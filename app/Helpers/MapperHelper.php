<?php

namespace App\Helpers;

use InvalidArgumentException;

class MapperHelper
{
    public function getSourceValueByKey(array $mapping, $key)
    {
        if (!array_key_exists($key, $mapping)) {
            throw new InvalidArgumentException("There is no key ({$key}) in source array: " . json_encode($mapping));
        }

        return $mapping[$key];
    }

    public function getSourceValueOrNullByKey(array $mapping, $key)
    {
        if (!array_key_exists($key, $mapping)) {
            return null;
        }

        return $mapping[$key];
    }
/*    public function getMappedValue(array $mapping, array $source, $key)
    {
        $sourceKey = $this->getSourceValueByKey($mapping, $key);

        return $this->getSourceValueByKey($source, $sourceKey);
    }*/

    public function getMappedValues(array $mapping, array $source): array
    {
        $ret = [];

        foreach ($mapping as $destKey => $sourceKey) {
            $ret[$destKey] = $this->getSourceValueByKey($source, $sourceKey);
        }

        return $ret;
    }

    public function getExistsMappedValues(array $mapping, array $source): array
    {
        $ret = [];

        foreach ($mapping as $destKey => $sourceKey) {
            if (!array_key_exists($sourceKey, $source)) {
                continue;
            }

            $ret[$destKey] = $source[$sourceKey];
        }

        return $ret;
    }
}
