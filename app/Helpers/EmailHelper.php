<?php

namespace App\Helpers;

use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EmailHelper
{
    public function sendEmail(
        string $emailSubject,
        string $emailBody,
        array $to,
        array $cc,
        array $bcc,
        array $attachmentsFilenames
    )
    {
        Mail::raw($emailBody, function (Message $message) use (
            $emailSubject, $to, $cc, $bcc, $attachmentsFilenames
        ) {
            $message
                ->subject($emailSubject)
                ->to($to);

            if ($cc) {
                $message->cc($cc);
            }

            if ($bcc) {
                $message->bcc($bcc);
            }

            foreach ($attachmentsFilenames as $storageFilename => $originalFilename) {
                $message->attachData(
                    Storage::get($storageFilename),
                    $originalFilename
                );
            }
        });
    }
}
