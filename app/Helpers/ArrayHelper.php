<?php

namespace App\Helpers;

class ArrayHelper
{
    public function getValuesByKeys(array $srcArray, array $keys): array
    {
        return array_intersect_key($srcArray, array_fill_keys($keys, null));
    }

    public function getValuesWithoutKeys(array $srcArray, array $keys): array
    {
        return array_diff_key($srcArray, array_fill_keys($keys, null));
    }
}
