<?php

namespace App\Helpers;

use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountAmountFormatInterface;
use App\Domain\Invoice\Outgoing\Base\InvoiceWithDetailsAggregate\Invoice\BaseOutgoingInvoiceAmountFormatInterface;

class NumberFormatter
{
    public function formatMoneyAccountAmount(float $number): string
    {
        return number_format(
            $number,
            BaseMoneyAccountAmountFormatInterface::DECIMALS,
            BaseMoneyAccountAmountFormatInterface::DECIMAL_POINT,
            BaseMoneyAccountAmountFormatInterface::THOUSANDS_SEPARATOR
        );
    }

    public function formatInvoiceAmount(float $number): string
    {
        return number_format(
            $number,
            BaseOutgoingInvoiceAmountFormatInterface::DECIMALS,
            BaseOutgoingInvoiceAmountFormatInterface::DECIMAL_POINT,
            BaseOutgoingInvoiceAmountFormatInterface::THOUSANDS_SEPARATOR
        );
    }
}
