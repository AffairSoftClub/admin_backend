<?php

namespace App\Helpers;

use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;

class CarbonHelper
{
    public function getOverlappedDaysCountInMonth(Carbon $monthDate, Carbon $startDate, Carbon $endDate): int
    {
        $monthStartCarbon = $monthDate->copy()->startOfMonth();
        $monthEndCarbon = $monthDate->copy()->endOfMonth();

        $startDate = $startDate->max($monthStartCarbon);
        $endDate = $endDate->min($monthEndCarbon);

        return ceil($endDate->floatDiffInDays($startDate));
    }

    public function getPeriodByDateTimeStrings(string $startDateTimeString, string $endDateTimeString): CarbonPeriod
    {
        return Carbon::parse($startDateTimeString)->toPeriod($endDateTimeString);
    }

    public function getDaysPeriodsSum(CarbonPeriod $period1, CarbonPeriod $period2): CarbonPeriod
    {
        $minDate = $period1->getStartDate()->min($period2->getStartDate())->copy();
        $maxDate = $period1->getEndDate()->max($period2->getEndDate())->copy();

        return $minDate->toPeriod($maxDate);
    }

    public function expandDayPeriodToFullMonthPeriod(CarbonPeriod $period): CarbonPeriod
    {
        $startDate = $period->getStartDate()->startOfMonth();
        $endDate = $period->getEndDate()->endOfMonth();

        return $startDate->toPeriod($endDate, 1, 'month');
    }

    /**
     * @param CarbonInterface $monthDate
     * @return CarbonPeriod
     */
    public function getMonthPeriodByDate(CarbonInterface $monthDate): CarbonPeriod
    {
        $monthStart = $monthDate->copy()->startOfMonth();
        $monthEnd = $monthDate->copy()->endOfMonth();

        return $this->getMonthPeriodByDates($monthStart, $monthEnd);
    }

    public function getMonthPeriodByDates(CarbonInterface $startDate, CarbonInterface $endDate): CarbonPeriod
    {
        return $this->getPeriodByUnitName($startDate, $endDate, 'month');
    }

    private function getPeriodByUnitName(CarbonInterface $startDate, CarbonInterface $endDate, string $unit): CarbonPeriod
    {
        return $startDate->toPeriod($endDate, 1, $unit);
    }

    public function getOverlappedDaysCount(CarbonPeriod $period1, CarbonPeriod $period2): int
    {
        $overlappedDaysPeriod = $this->getOverlappedDaysPeriodByPeriods($period1, $period2);

        return $overlappedDaysPeriod->count();
    }

    private function getOverlappedDaysPeriodByPeriods(CarbonPeriod $period1, CarbonPeriod $period2): CarbonPeriod
    {
        $maxStart = $this->getMaxStartByPeriods($period1, $period2);
        $minEnd = $this->getMinEndByPeriods($period1, $period2);

        return $maxStart->toPeriod($minEnd);
    }

    private function getMaxStartByPeriods(CarbonPeriod $period1, CarbonPeriod $period2): CarbonInterface
    {
        $start1 = $period1->getStartDate();
        $start2 = $period2->getStartDate();

        return $start1->max($start2);
    }

    private function getMinEndByPeriods(CarbonPeriod $period1, CarbonPeriod $period2): CarbonInterface
    {
        $end1 = $period1->getEndDate();
        $end2 = $period2->getEndDate();

        return $end1->min($end2);
    }

    public function isTheSameDateStrings(string $oldDepartureDatatimeString, string $newDepartureDatetimeString): bool
    {
        $oldCarbon = Carbon::parse($oldDepartureDatatimeString);
        $newCarbon = Carbon::parse($newDepartureDatetimeString);

        return $this->isTheSameDateCarbons($oldCarbon, $newCarbon);
    }

    private function isTheSameDateCarbons(Carbon $datetime1, Carbon $datetime2): bool
    {
        $datetime1Day = $datetime1->startOfDay();
        $datetime2Day = $datetime2->startOfDay();

        return ($datetime1Day->diffInDays($datetime2Day) === 0);
    }
}
