<?php

namespace App\Helpers;

use App\Base\DateAndTime\DateHelper;
use App\Helpers\ActionHelper;
use App\Helpers\ArrayHelper;
use App\Helpers\ConstantHelper;
use App\Helpers\MapperHelper;
use App\Helpers\MonthNumberHelper;
use App\Helpers\NumberFormatter;
use App\Helpers\ReflectionHelper;
use App\Helpers\UploadHelper;
use App\Helpers\ValuesLoader\PdfValuesLoader\PdfFileDataLoader;
use App\Helpers\ValuesLoader\PdfValuesLoader\PdfFileDataLoaderInterface;
use Illuminate\Support\ServiceProvider;

class HelpersServiceProvider extends ServiceProvider
{
    public function register()
    {
        $app = $this->app;

        $app->singleton(ActionHelper::class);
        $app->singleton(ConstantHelper::class);
        $app->singleton(DateHelper::class);
        $app->singleton(MapperHelper::class);
        $app->singleton(MonthNumberHelper::class);
        $app->singleton(NumberFormatter::class);
        $app->singleton(ReflectionHelper::class);
        $app->singleton(UploadHelper::class);
        $app->singleton(ArrayHelper::class);

        $app->bind(PdfFileDataLoaderInterface::class, PdfFileDataLoader::class);
    }
}
