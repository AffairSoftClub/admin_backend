<?php


namespace App\Helpers;


use DateTime;

class ActionHelper
{
    public static function getCountHoursBeetvenTwoDate($dateDep, $dateArr)
    {
        $date1 = new DateTime($dateDep);
        $date2 = new DateTime($dateArr);
//        $date = DateTime::createFromFormat("Y-m-d H:i", '2000-01-01 00:00'); // задаем дату в любом формате
//        $interval = $now->diff($date); // получаем разницу в виде объекта DateInterval
        $interval = $date1->diff($date2);
        $hours = $interval->h;
        $hours = $hours + ($interval->days * 24);
        return $hours;
    }

    public static function getCountDaysByDEL($dateDep, $dateArr)
    {
        $date1 = new DateTime($dateDep);
        $date2 = new DateTime($dateArr);
//        $date = DateTime::createFromFormat("Y-m-d H:i", '2000-01-01 00:00'); // задаем дату в любом формате
//        $interval = $now->diff($date); // получаем разницу в виде объекта DateInterval
        $interval = $date1->diff($date2);
        $hours = $interval->h;
        $hours = $hours + ($interval->days * 24);
        return $hours;
    }
}
