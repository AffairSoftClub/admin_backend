<?php

namespace App\Helpers;

use App\Domain\Invoice\Incoming\Dkv\Csv\DkvCsvColumnIndexesEnum;

class ConstantHelper
{
    // статусы ответа для сущностей CAR
    const ENTITY_CREATED = 'created';
    const ENTITY_UPDATED = 'updated';
    const ENTITY_DELETED = 'deleted';
    // пользовательские ошыбки для сущностей нужно для уведомляшки
    const ENTITY_WARNING = 'warning';


    const DRIVER_CATEGORY_STATUS_ACTIVE = 0;
    const DRIVER_CATEGORY_STATUS_NOT_ACTIVE = 1;

    const MAPPING_CSV_DKV = [
        'car_number' => DkvCsvColumnIndexesEnum::CAR_NUMBER,
        'billing_date' => DkvCsvColumnIndexesEnum::BILLING_DATE,// дата выставления счета (инвойс от)
        'payment_time' => DkvCsvColumnIndexesEnum::PAYMENT_TIME,// дата выставления счета (инвойс от)
//         'sum'=>5, // сумма ?? Odbyt
        'sum' => DkvCsvColumnIndexesEnum::SUM, // сумма ?? Ukazatel brutto

        'text_1' => DkvCsvColumnIndexesEnum::TEXT_1, // Druh zboží

        'expense_type' => DkvCsvColumnIndexesEnum::EXPENSE_TYPE, // Skupina zboží - соответвсие по типу затрат

        'text_2' => DkvCsvColumnIndexesEnum::EXPENSE_TYPE, // Skupina zboží
        'text_3' => DkvCsvColumnIndexesEnum::TEXT_3, // Doplňující text karty

        'text_4' => DkvCsvColumnIndexesEnum::PLACE, // текст сборка ??
        'text_5' => DkvCsvColumnIndexesEnum::TEXT_5, // текст сборка  ??
    ];

    const MAPPING_CSV_BANK = [
        'transaction_date' => 0,
        'booking_date' => 1,
        'transaction_category' => 4,
        'desc_1' => 7,
        'desc_2' => 8,
        'desc_3' => 9,

        'vsksss_1' => 10,
        'vsksss_2' => 11,
        'vsksss_3' => 12,
        'fee' => 17,
        'amount' => 13,
    ];
}
