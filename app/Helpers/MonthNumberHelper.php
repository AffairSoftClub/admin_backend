<?php

namespace App\Helpers;

use App\Base\DateAndTime\Formats\DatabaseDateTimesFormatsEnum;
use DateTime;

class MonthNumberHelper
{
    const BASE_DATE_STRING = '2000-01-01 00:00:00';

    public function getMonthNumberByOffset(int $monthOffset): int
    {
        $currentMonthNumber = $this->getCurrentMonthNumber();

        return $currentMonthNumber + $monthOffset;
    }

    public function getCurrentMonthNumber(): int
    {
        $now = new DateTime();

        return $this->getMonthNumberByDateTime($now);
    }

    public function getMonthNumberByDateTime(DateTime $dateTime)
    {
        $date = DateTime::createFromFormat(DatabaseDateTimesFormatsEnum::DATETIME, self::BASE_DATE_STRING); // задаем дату в любом формате
        $interval = $dateTime->diff($date); // получаем разницу в виде объекта DateInterval

        return (($interval->y * 12) + $interval->m);
    }

    public function getYearMonthByNumber(int $monthNumber): string
    {
        $timestamp = strtotime("+{$monthNumber} months", strtotime(self::BASE_DATE_STRING));

        return date('Y-m', $timestamp);
    }

    public function getMonthNumberByDateString(?string $dateString)
    {
        if (!$dateString) {
            return null;
        }

        $dateTime = new DateTime($dateString);

        return $this->getMonthNumberByDateTime($dateTime);
    }
}
