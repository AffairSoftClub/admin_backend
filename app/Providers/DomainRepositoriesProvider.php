<?php


namespace App\Providers;

use App\Base\Provider\BaseSingletonsServiceProvider;
use App\Domain\Cashbook\Currency\CurrencyRepository;
use App\Domain\Cashbook\MoneyAccount\Cash\CashMoneyAccountRepository;
use App\Domain\Cashbook\MoneyAccount\General\GeneralMoneyAccountRepository;
use App\Domain\Cashbook\MoneyAccount\NonCash\NonCashMoneyAccountRepository;
use App\Domain\Directories\Client\ClientRepository;
use App\Domain\Directories\Country\CountryRepository;
use App\Domain\Directories\DirectionAddress\DirectionAddressRepository;
use App\Domain\Directories\Vehicle\Car\Office\OfficeCarRepository;
use App\Domain\Directories\Vehicle\Car\Park\ParkCarRepository;
use App\Domain\Directories\Vehicle\Trailer\LinkingToParkCarLog\TrailerLinkingToParkCarLogRepository;
use App\Domain\Directories\Vehicle\Trailer\TrailerRepository;
use App\Domain\Shipment\Direction\DirectionRepository;
use App\Domain\UsersAndRoles\Role\RoleRepository;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategoryRepository;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\AppointmentRepository;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\CityRepository;

class DomainRepositoriesProvider extends BaseSingletonsServiceProvider
{
    protected function getSingletonsClassNames(): array
    {
        return [
            RoleRepository::class,

            DirectionAddressRepository::class,

            // Currency
            CurrencyRepository::class,

            // Expense conformity
            AppointmentRepository::class,
            DriverCategoryRepository::class,

            // Vehicle
            TrailerRepository::class,
            OfficeCarRepository::class,
            ParkCarRepository::class,

            CountryRepository::class,

            CityRepository::class,

            // Main entity
            DirectionRepository::class,
            ClientRepository::class,

            TrailerLinkingToParkCarLogRepository::class,

            // Money Account
            GeneralMoneyAccountRepository::class,
            NonCashMoneyAccountRepository::class,
            CashMoneyAccountRepository::class,
        ];
    }
}
