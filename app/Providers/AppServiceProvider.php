<?php

namespace App\Providers;

use App\Domain\Company\CompanyConfig;
use App\Domain\Invoice\Outgoing\Common\Config\OutgoingInvoiceConfig;
use App\Domain\Invoice\Outgoing\Common\Config\OutgoingInvoiceDefaultsConfig;
use App\Domain\Invoice\Outgoing\Common\Email\OutgoingInvoiceEmailDefaultsConfig;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    public $singletons = [
        CompanyConfig::class,
        OutgoingInvoiceConfig::class,
        OutgoingInvoiceDefaultsConfig::class,
        OutgoingInvoiceEmailDefaultsConfig::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
    }
}
