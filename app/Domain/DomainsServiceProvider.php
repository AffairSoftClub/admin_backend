<?php

namespace App\Domain;

use App\Domain\Base\ServiceProvider\BaseAggregateDomainServiceProvider;
use App\Domain\Common\CommonServiceProvider;
use App\Domain\Company\CompanyServiceProvider;
use App\Domain\Directories\DirectoriesServiceProvider;

class DomainsServiceProvider extends BaseAggregateDomainServiceProvider
{
    protected function getServiceProvidersClassNames(): array
    {
        return [
            CommonServiceProvider::class,
            CompanyServiceProvider::class,
            DirectoriesServiceProvider::class,
        ];
    }
}
