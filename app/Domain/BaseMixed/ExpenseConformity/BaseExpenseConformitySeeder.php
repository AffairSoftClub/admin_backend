<?php

namespace App\Domain\BaseMixed\ExpenseConformity;

use App\Domain\BaseMixed\ExpenseConformity\BaseExpenseConformityRepository;
use App\Domain\Invoice\Incoming\Base\Csv\ExpenseConformity\Seeds\SeedToEntityAttributesConverter;
use App\Domain\Base\Seeds\Entity\RepositoryBased\BySeedsRepositoryBasedSeeder;

abstract class BaseExpenseConformitySeeder extends BySeedsRepositoryBasedSeeder
{
    public function __construct(
        BaseExpenseConformityRepository $repository,
        SeedToEntityAttributesConverter $seedToEntityAttributesConverter)
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }
}
