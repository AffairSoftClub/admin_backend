<?php

namespace App\Domain\BaseMixed\ExpenseConformity;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Base\Model\WithValidation\WithValidationModelTrait;
use App\Domain\Cashbook\MoneyTransferType\Type\Expense\General\GeneralExpenseMoneyTransferType;
use App\Domain\Invoice\Incoming\Base\Csv\ExpenseConformity\Table\CommonIncomingInvoiceMoneyTransferTypeIdConformityColumnNamesEnum as ColumnNamesEnum;
use Illuminate\Support\Str;

class BaseExpenseConformity extends BaseModel
{
    use WithValidationModelTrait;

    protected $fillable = [
        ColumnNamesEnum::CSV_VALUE,
        ColumnNamesEnum::MONEY_TRANSFER_TYPE_ID
    ];

    public static function getRowCode($dataList, $keyUp)
    {
        $rowCode = false;
        $index = -1;
        $key = Str::lower($keyUp);
        foreach ($dataList as $k => $items) {
            foreach ($items as $ind => $item) {
                if (Str::lower($item) == $key) {
                    $rowCode = $index = $ind;
                    break;
                }
            }
            if ($index > -1) {
                break;
            }
        }
        return $rowCode;
    }

    public function expense()
    {
        return $this->belongsTo(GeneralExpenseMoneyTransferType::class);
    }

    public function setExpenseId(?int $expenseId): self
    {
        $this->setAttribute(ColumnNamesEnum::MONEY_TRANSFER_TYPE_ID, $expenseId);

        return $this;
    }
}
