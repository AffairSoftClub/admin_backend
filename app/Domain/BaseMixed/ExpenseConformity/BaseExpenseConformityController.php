<?php

namespace App\Domain\BaseMixed\ExpenseConformity;

use App\Base\Validation\ThrowValidationExceptionTrait;
use App\Domain\Base\Http\Controller\BaseStandardIndexDomainEntityController;
use App\Domain\Cashbook\MoneyTransferType\Type\Expense\All\Subcategory\ExpenseMoneyTransferTypeSubcategoryRepository;
use App\Domain\Cashbook\MoneyTransferType\Type\Expense\General\GeneralExpenseMoneyTransferType;
use App\Domain\Cashbook\MoneyTransferType\Type\Expense\General\GeneralExpenseMoneyTransferTypeRepository;
use App\Domain\Invoice\Incoming\Base\Csv\ExpenseConformity\Http\SaveRequestKeysEnum;
use App\Domain\Invoice\Incoming\Base\Csv\ExpenseConformity\Table\CommonIncomingInvoiceMoneyTransferTypeIdConformityColumnNamesEnum as ColumnNamesEnum;
use App\Domain\Invoice\Incoming\Dkv\Csv\ExpenseConformity\DkvInvoiceExpenseConformity;
use App\Domain\Invoice\Incoming\DkvNew\DkvNewIncomingInvoiceUploadsDirectoryNameValue;
use App\Helpers\ConstantHelper;
use App\Helpers\MapperHelper;
use App\Helpers\ValuesLoader\AnySeparatedValuesLoader\SemicolonSeparatedValuesLoader;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

abstract class BaseExpenseConformityController extends BaseStandardIndexDomainEntityController
{
    use ThrowValidationExceptionTrait;

    /** @var GeneralExpenseMoneyTransferTypeRepository */
    protected $generalExpenseRepository;

    /** @var SemicolonSeparatedValuesLoader */
    protected $csvLoader;

    /** @var MapperHelper */
    protected $mapperHelper;

    /** @var ExpenseMoneyTransferTypeSubcategoryRepository */
    private $expenseMoneyTransferTypeSubcategoryRepository;


    public function __construct(
        ExpenseMoneyTransferTypeSubcategoryRepository $expenseMoneyTransferTypeSubcategoryMoneyTransferTypeRepository,
        BaseExpenseConformityRepository           $invoiceExpenseMoneyTransferTypeConformityService,
        GeneralExpenseMoneyTransferTypeRepository $generalExpenseRepository,
        SemicolonSeparatedValuesLoader            $csvLoader,
        MapperHelper                              $mapperHelper
    )
    {
        $this->expenseMoneyTransferTypeSubcategoryRepository = $expenseMoneyTransferTypeSubcategoryMoneyTransferTypeRepository;
        $this->generalExpenseRepository = $generalExpenseRepository;
        $this->csvLoader = $csvLoader;
        $this->mapperHelper = $mapperHelper;

        parent::__construct($invoiceExpenseMoneyTransferTypeConformityService);
    }

    // Entity > Index
    protected function getIndexReferences(): array
    {
        return [
            'expenses' => $this->getGeneralExpensesOrderedByName(),
            'expenseMoneyTransferTypeSubcategories' => $this->getExpenseMoneyTransferTypeSubcategories(),
        ];
    }

    /**
     * @return Collection|GeneralExpenseMoneyTransferType[]
     */
    private function getGeneralExpensesOrderedByName(): Collection
    {
        return $this->generalExpenseRepository->getAllOrderedByName();
    }

    private function getExpenseMoneyTransferTypeSubcategories(): array
    {
        return $this->expenseMoneyTransferTypeSubcategoryRepository->getAll();
    }

    ///

    abstract protected function getRouteEnds(): string;

    public function store(Request $request)
    {
        try {
            return $this->createExpenseConformityByRequest($request);
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function createExpenseConformityByRequest(Request $request): BaseExpenseConformity
    {
        $attributes = $this->getExistsExpenseConformityAttributesByRequest($request);
        $this->validateCreateAttributes($attributes);

        return $this->createExpenseConformityByAttributes($attributes);
    }

    private function getExistsExpenseConformityAttributesByRequest(Request $request): array
    {
        $mapping = [
            ColumnNamesEnum::CSV_VALUE => SaveRequestKeysEnum::CSV_VALUE,
            ColumnNamesEnum::MONEY_TRANSFER_TYPE_ID => SaveRequestKeysEnum::MONEY_TRANSFER_TYPE_ID,
        ];

        $requestData = $request->all();

        return $this->getExistsMappedValues($mapping, $requestData);
    }

    private function getExistsMappedValues($mapping, $source): array
    {
        return $this->mapperHelper->getExistsMappedValues($mapping, $source);
    }

    private function validateCreateAttributes(array $attributes)
    {
        $this->validateAttributesCsvValueForCreateIfExists($attributes);
        $this->validateAttributesExpenseId($attributes);
    }

    private function validateAttributesCsvValueForCreateIfExists(array $attributes)
    {
        if (!array_key_exists(ColumnNamesEnum::CSV_VALUE, $attributes)) {
            return;
        }

        $this->validateCsvValueForCreate($attributes[ColumnNamesEnum::CSV_VALUE]);
    }

    private function validateCsvValueForCreate(string $csvValue)
    {
        $this->validateCsvValueIsNotEmpty($csvValue);
        $this->validateCsvValueIsNotExists($csvValue);
    }

    private function validateCsvValueIsNotEmpty(string $csvValue)
    {
        if (!$csvValue) {
            $this->throwValidationException([
                SaveRequestKeysEnum::CSV_VALUE => 'Поле код не может быть пустым',
            ]);
        }
    }

    private function validateCsvValueIsNotExists(string $csvValue)
    {
        if ($this->isExpenseConformityExistsByCsvValue($csvValue)) {
            $this->throwValidationException([
                SaveRequestKeysEnum::CSV_VALUE => "Уже существует соответствие с указанным значением значения в инвойсе ({$csvValue}).",
            ]);
        }
    }

    private function isExpenseConformityExistsByCsvValue(string $csvValue): bool
    {
        return $this->entityService->isExistsByCsvValue($csvValue);
    }

    private function validateAttributesExpenseId(array $attributes)
    {
        $expenseId = $attributes[ColumnNamesEnum::MONEY_TRANSFER_TYPE_ID];

        $this->validateExpenseId($expenseId);
    }

    private function validateExpenseId(?int $expenseId)
    {
        if ($expenseId === null) {
            return;
        }

        if (!$this->isExpenseExistsById($expenseId)) {
            $this->throwValidationException([
                SaveRequestKeysEnum::MONEY_TRANSFER_TYPE_ID => "Вида начислений с указанным id ({$expenseId}) не существует",
            ]);
        }
    }

    private function isExpenseExistsById(int $expenseId): bool
    {
        return $this->generalExpenseRepository->isExists($expenseId);
    }

    private function createExpenseConformityByAttributes(array $attributes): BaseExpenseConformity
    {
        return $this->entityService->create($attributes);
    }

    public function uploadDkv(Request $request)
    {
//        $fileHashName = UploadHelper::getFilenameWithouExtByRequest($request);

        $uploadDirectoryWithPath = $this->getUploadDirectoryWithPath();
        $pathName = $request->file('file')->store($uploadDirectoryWithPath);
        $dataList = $this->loadCsvAsArray($pathName);

        //Skupina zboží
        $rowCode = DkvInvoiceExpenseConformity::getRowCode($dataList, $key = 'Skupina zboží');

        if ($rowCode) { // можно добавлять новые поля
            $message = 'Данные успешно обновлены ';
            $status = ConstantHelper::ENTITY_CREATED;

        } else {
            $message = 'Поле   Kód zboží  не найдено';
            $status = ConstantHelper::ENTITY_WARNING;
        }

        foreach ($dataList as $item) {
            if (!empty($item[$rowCode]) and $item[$rowCode] != $key) {
                $model = DkvInvoiceExpenseConformity::firstOrCreate(['code' => $item[$rowCode]]);
            }
        }

        $models = DkvInvoiceExpenseConformity::with('expenses')->orderByDesc('id')->paginate(10);
        $expenses = GeneralExpenseMoneyTransferType::all();

        return ['status' => $status, 'message' => $message, 'models' => $models, 'expenses' => $expenses];

//      return  view('entities.invoices.index',compact('clients','expenses','invoices'));
    }

    private function getUploadDirectoryWithPath(): string
    {
        return DkvNewIncomingInvoiceUploadsDirectoryNameValue::VALUE . DIRECTORY_SEPARATOR . date("Ymd");
    }

    private function loadCsvAsArray($filename): array
    {
        return $this->csvLoader->getDataFromFile($filename);
    }

    protected function updateConformityByRequest(BaseExpenseConformity $expenseConformity, Request $request)
    {
        $attributes = $this->getExistsExpenseConformityAttributesByRequest($request);
        $this->validateAttributesForUpdate($attributes);
        $this->updateExpenseConformityByAttributes($expenseConformity, $attributes);

        return $expenseConformity;
    }

    private function validateAttributesForUpdate(array $attributes)
    {
        $this->validateAttributesCsvValueForUpdateIfExists($attributes);
        $this->validateAttributesExpenseId($attributes);
    }

    private function validateAttributesCsvValueForUpdateIfExists(array $attributes)
    {
        if (!array_key_exists(ColumnNamesEnum::CSV_VALUE, $attributes)) {
            return;
        }

        $this->validateCsvValueForUpdate($attributes[ColumnNamesEnum::CSV_VALUE]);
    }

    private function validateCsvValueForUpdate(string $csvValue)
    {
        $this->validateCsvValueIsNotEmpty($csvValue);
        //todo: validate for other with the same name
    }

    private function updateExpenseConformityByAttributes($expenseConformity, $attributes)
    {
        $this->generalExpenseRepository->update($expenseConformity, $attributes);
    }

    protected function destroyModel(BaseExpenseConformity $model)
    {
        try {
            $this->deleteExpenseConformity($model);

            return $model->getKey();
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function deleteExpenseConformity(BaseExpenseConformity $model)
    {
        $this->entityService->deleteByModel($model);
    }
}
