<?php

namespace App\Domain\BaseMixed\ExpenseConformity;

interface CsvExpenseValueToIdConverterInterface
{
    public function getExpenseIdByCsvValueOrNull(string $csvExpenseValue): ?int;
}
