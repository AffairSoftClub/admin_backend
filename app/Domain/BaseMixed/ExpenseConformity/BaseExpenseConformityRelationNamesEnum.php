<?php

namespace App\Domain\BaseMixed\ExpenseConformity;

interface BaseExpenseConformityRelationNamesEnum
{
    const EXPENSE = 'expense';
}
