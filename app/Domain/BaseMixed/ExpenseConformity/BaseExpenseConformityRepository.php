<?php

namespace App\Domain\BaseMixed\ExpenseConformity;

use App\Base\Repository\Contracts\Basic\GetOrderedPaginationInterface;
use App\Base\GetAsMappingInterface;
use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Base\GetAsMappingTrait;
use App\Domain\Invoice\Incoming\Base\Csv\ExpenseConformity\FrontRelationsValues;
use App\Domain\Invoice\Incoming\Base\Csv\ExpenseConformity\Table\CommonIncomingInvoiceMoneyTransferTypeIdConformityColumnNamesEnum as ColumnNamesEnum;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * Class CommonIncomingInvoiceExpenseConformityRepository
 * @package App\Domain\Invoice\Incoming\Base\Csv\ExpenseConformity
 *
 * @method BaseExpenseConformity create(array $attributes = [])
 */
class BaseExpenseConformityRepository extends BaseRepository implements GetAsMappingInterface, GetOrderedPaginationInterface
{
    use GetAsMappingTrait;

    public function __construct(BaseExpenseConformity $model)
    {
        parent::__construct($model);
    }

    public function getOrderedPagination(Request $request): LengthAwarePaginator
    {
        return $this->getModel()
            ->with(FrontRelationsValues::VALUES)
            ->orderBy(ColumnNamesEnum::CSV_VALUE)
            ->paginate();
    }

    public function isExistsByCsvValue(string $csvValue): bool
    {
        return $this->getModel()->where(ColumnNamesEnum::CSV_VALUE, $csvValue)->exists();
    }
}
