<?php

namespace App\Domain\BaseMixed\ExpenseConformity;

use App\Base\GetAsMappingInterface;
use App\Domain\BaseMixed\ExpenseConformity\CsvExpenseValueToIdConverterInterface;
use App\Helpers\MapperHelper;

abstract class BaseCsvExpenseValueToIdConverter implements CsvExpenseValueToIdConverterInterface
{
    /** @var GetAsMappingInterface */
    private $conformityRepository;

    /** @var array */
    private $mapping;

    /** @var MapperHelper */
    private $mapperHelper;

    public function __construct(
        GetAsMappingInterface $conformityRepository,
        MapperHelper $mapperHelper
    )
    {
        $this->conformityRepository = $conformityRepository;
        $this->mapperHelper = $mapperHelper;
    }


    public function getExpenseIdByCsvValueOrNull(string $csvExpenseValue): ?int
    {
        $mapping = $this->getMappingCashed();

        return $this->getExistsMappedValue($mapping, $csvExpenseValue);
    }

    private function getMappingCashed(): array
    {
        if (!$this->mapping) {
            $this->mapping = $this->createMapping();
        }

        return $this->mapping;
    }

    private function createMapping(): array
    {
        $keyColumnName = $this->getConformityKeyColumnName();
        $valueColumnName = $this->getConformityValueColumnName();

        return $this->conformityRepository->getAsMapping($keyColumnName, $valueColumnName);
    }

    abstract protected function getConformityKeyColumnName(): string;

    abstract protected function getConformityValueColumnName(): string;

    private function getExistsMappedValue(array $mapping, $csvExpenseValue): ?int
    {
        return $this->mapperHelper->getSourceValueOrNullByKey($mapping, $csvExpenseValue);
    }
}
