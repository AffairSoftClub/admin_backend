<?php

namespace App\Domain\BaseMixed\BankStatementOrIncomingInvoice\Http;

use App\Domain\Base\Http\ByRequestUpdaterInterface;
use App\Domain\Base\Importer\Base\FromFileEntityImporterInterface;
use App\Domain\Base\MainDetailsAggregate\Importer\Base\Http\BaseMainDetailImporterController;
use App\Domain\Base\MainDetailsAggregate\Parts\Main\MainEntity;
use App\Domain\Base\Model\ServiceInterface;
use App\Domain\Cashbook\CashbookItem\Types\Base\Services\BaseMainDetailsCashbookService;
use App\Domain\Cashbook\MoneyTransferType\Type\Expense\General\GeneralExpenseMoneyTransferType;
use App\Domain\Cashbook\MoneyTransferType\Type\Expense\General\GeneralExpenseMoneyTransferTypeRepository;
use App\Domain\Directories\Client\Client;
use App\Domain\Directories\Client\ClientRepository;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseBankStatementOrIncomingInvoiceImporterController extends BaseMainDetailImporterController
{
    /*** IMPORTED ENTITY RELATIONS RELATED ***/

    /** @var ClientRepository */
    protected $clientRepository;

    /** @var BaseCarRepository */
    protected $carRepository;


    /** @var GeneralExpenseMoneyTransferTypeRepository */
    protected $generalExpenseMoneyTransferTypeRepository;

    public function __construct(
        // class only
        ClientRepository                          $clientService,
        BaseCarRepository                         $carRepository,
        GeneralExpenseMoneyTransferTypeRepository $generalExpenseRepository,

        // inherited
        ServiceInterface                          $importedEntityService,
        ByRequestUpdaterInterface                 $byRequestUpdater,
        FromFileEntityImporterInterface           $fromAsvFileInvoiceImporter,
        BaseMainDetailsCashbookService            $importedEntityCashbookService
    )
    {
        $this->clientRepository = $clientService;
        $this->carRepository = $carRepository;
        $this->generalExpenseMoneyTransferTypeRepository = $generalExpenseRepository;

        parent::__construct($importedEntityService, $byRequestUpdater, $fromAsvFileInvoiceImporter, $importedEntityCashbookService);
    }


    // Vehicle-related

    /**
     * @param Model|MainEntity $entity
     * @param array $notExistsCarNumbers
     * @return string
     */
    protected function getStoreResultMessage(Model $entity, array $notExistsCarNumbers): string
    {
        $message = parent::getStoreResultMessage($entity, $notExistsCarNumbers);

        $createdCarsCount = count($notExistsCarNumbers);
        $message .= "<br/>Несуществующих номеров ТС: {$createdCarsCount}.<br />";
        $message .= $this->arrayToHtmlList($notExistsCarNumbers);

        return $message;
    }

    private function arrayToHtmlList(array $arr): string
    {
        //todo: maybe move to html-helper class?
        //todo: and generate as links if possible?
        $items = [];

        foreach ($arr as $value) {
            $items[] = "<li>{$value}</li>";
        }

        $itemsString = implode('', $items);

        return "<ul>{$itemsString}</ul>";
    }

    // Entity > Index
    protected function getIndexReferences(): array
    {
        return [
            'cars' => $this->getCarsOrderedByNames(),
            'clients' => $this->getClientsOrderedByName(),//todo - maybe clients only in incoming invoices?
            'expenses' => $this->getExpensesOrderedByName(),
        ];
    }

    /**
     * @return Collection|Client[]
     */
    private function getClientsOrderedByName(): Collection
    {
        return $this->clientRepository->getAllOrderedByName();
    }

    /**
     * @return Collection|GeneralExpenseMoneyTransferType[]
     */
    private function getExpensesOrderedByName()
    {
        return $this->generalExpenseMoneyTransferTypeRepository->getAllOrderedByName();
    }


}
