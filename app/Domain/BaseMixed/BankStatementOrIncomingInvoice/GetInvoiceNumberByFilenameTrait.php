<?php

namespace App\Domain\BaseMixed\BankStatementOrIncomingInvoice;

trait GetInvoiceNumberByFilenameTrait
{
    protected function getInvoiceNumber(string $clientOriginalFilename, array $allData, array $mainData, array $detailsData): string
    {
        return $this->getInvoiceNumberByFilename($clientOriginalFilename);
    }

    private function getInvoiceNumberByFilename(string $filename): string
    {
        $filenameWithoutExt = $this->getFilenameWithoutExt($filename);

        return trim($filenameWithoutExt);
    }

    private function getFilenameWithoutExt(string $filename): string
    {
        return pathinfo($filename, PATHINFO_FILENAME);
    }
}
