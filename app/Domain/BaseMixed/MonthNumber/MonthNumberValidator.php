<?php

namespace App\Domain\BaseMixed\MonthNumber;

use App\Base\Validation\Validator\Domain\Simple\BetweenValuesSimpleValidator;
use App\Helpers\MonthNumberHelper;

class MonthNumberValidator extends BetweenValuesSimpleValidator
{
    /** @var MonthNumberHelper */
    private $monthNumberHelper;

    public function __construct(
        MonthNumberHelper $monthNumberHelper
    )
    {
        $this->monthNumberHelper = $monthNumberHelper;
    }

    protected function getValidationErrorKey(): string
    {
        return 'month_number';
    }

    protected function getEntityName(): string
    {
        return 'Month number';
    }

    protected function getMinValue(): int
    {
        return 0;
    }

    protected function getMaxValue(): int
    {
        return $this->monthNumberHelper->getCurrentMonthNumber();
    }
}
