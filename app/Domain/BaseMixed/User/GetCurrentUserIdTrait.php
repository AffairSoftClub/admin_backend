<?php

namespace App\Domain\BaseMixed\User;

trait GetCurrentUserIdTrait
{
    use GetCurrentUserTrait;

    protected function getCurrentUserIdOrFail(): int
    {
        $currentUser = $this->getCurrentUser();

        return $currentUser->getKey();
    }

    protected function getCurrentUserIdOrNull(): ?int
    {
        $currentUser = $this->getCurrentUser();

        return $currentUser ? $currentUser->getKey() : null;
    }
}
