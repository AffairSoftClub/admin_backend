<?php

namespace App\Domain\BaseMixed\User;

use App\Domain\UsersAndRoles\Users\Base\BaseUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

trait GetCurrentUserTrait
{
    /**
     * @return Authenticatable|BaseUser null
     */
    protected function getCurrentUser(): ?Authenticatable
    {
        return Auth::user();
    }
}
