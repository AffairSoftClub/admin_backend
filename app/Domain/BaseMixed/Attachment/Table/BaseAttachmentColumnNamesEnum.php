<?php

namespace App\Domain\BaseMixed\Attachment\Table;

use App\Domain\Base\Model\TypedModel\TypedModelColumnNamesInterface;
use App\Domain\Base\Table\Columns\Compound\IdTimestampsDeletedAtInterface;

interface BaseAttachmentColumnNamesEnum extends
    TypedModelColumnNamesInterface,
    IdTimestampsDeletedAtInterface
{
    const ORIGINAL_FILENAME = 'original_filename';
    const SERVER_FILENAME_WITH_PATH = 'server_filename_with_path';
    const SORT_ORDER = 'sort_order';

    const ATTACHABLE_ID = 'attachable_id';
    const ATTACHABLE_TYPE = 'attachable_type';
}
