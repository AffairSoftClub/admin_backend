<?php

namespace App\Domain\BaseMixed\Attachment\Table;

interface BaseAttachmentTableNameValue
{
    const VALUE = 'attachments';
}
