<?php

namespace App\Domain\BaseMixed\Attachment;

interface BaseAttachmentRelationNamesEnum
{
    const ATTACHABLE = 'attachable';
}
