<?php

namespace App\Domain\BaseMixed\Attachment;

use App\Domain\Base\Model\TypedModel\TypedModelTrait;

abstract class BaseTypedAttachment extends BaseAttachment
{
    use TypedModelTrait;
}
