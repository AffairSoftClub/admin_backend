<?php

namespace App\Domain\BaseMixed\Attachment;

use App\Domain\BaseMixed\Attachment\Table\BaseAttachmentColumnNamesEnum;
use App\Domain\BaseMixed\Attachment\Table\BaseAttachmentTableNameValue;
use App\Domain\Base\Model\BaseModel;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Likemusic\LaravelFillableRelationsWithoutAutosave\IsFillableRelationTrait;

class BaseAttachment extends BaseModel
{
    use IsFillableRelationTrait;

    protected $table = BaseAttachmentTableNameValue::VALUE;

    protected $fillable = [
        BaseAttachmentColumnNamesEnum::ORIGINAL_FILENAME,
        BaseAttachmentColumnNamesEnum::SERVER_FILENAME_WITH_PATH,
    ];

    public function attachable(): MorphTo
    {
        return $this->morphTo();
    }

    public function getOriginalFilename(): string
    {
        return $this->getAttribute(BaseAttachmentColumnNamesEnum::ORIGINAL_FILENAME);
    }

    public function getServerFilenameWithPath(): string
    {
        return $this->getAttribute(BaseAttachmentColumnNamesEnum::SERVER_FILENAME_WITH_PATH);
    }
}
