<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Seeds\BaseEmployeeSeedKeysEnum as EmployeeSeedKeysEnum;

interface DriverSeedKeysEnum extends EmployeeSeedKeysEnum
{
    const CATEGORY_NAME = 'category_name';
}
