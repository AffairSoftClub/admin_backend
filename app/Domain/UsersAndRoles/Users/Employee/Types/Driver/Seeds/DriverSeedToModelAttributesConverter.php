<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategory;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryColumnNamesEnum as DriverCategoryColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Seeds\EmployeeSeedToModelAttributesConverter as EmployeeSeedToModelAttributesConverter;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table\DriverOnlyColumnNamesEnum;

class DriverSeedToModelAttributesConverter extends EmployeeSeedToModelAttributesConverter
{
    protected function getRelationsMapping(): array
    {
        return [
            DriverOnlyColumnNamesEnum::DRIVER_CATEGORY_ID => [
                DriverCategory::class,
                [
                    DriverCategoryColumnNamesEnum::NAME => DriverSeedKeysEnum::CATEGORY_NAME,
                ]
            ]
        ];
    }
}
