<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\DriverCategoryNamesEnum;

interface DriverSeedsValues
{
    const VALUES = [
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::POPOV_OLEKSANDER,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::SENNIKOV_SERHIY,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::YEFANOF_VALERIY,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::AMBRAZHUK_ANDREI,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::SEMENOV_SERGII,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::GORBOTENKO_OLEKSANDER,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::NIMETS_SERGII,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::ROMANOV_SERHIY,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::OSTAPENKO_MYKOLA,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::BUBNOW_VIKTOR,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::MUKHIN_OLEKSANDER,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::IVAN_STRAKHOV,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::VADIM_KABANOV,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::ANDRII_PRYTULIAK,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::OLEXANDR_YARMACHENKO,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::OLEXANDR_MYKHAILYCHENKO,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::DENYS_KORSHUNOV,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::OLEKSANDR_KLOCHKO,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::YEVHEN_VYSHNEVYI,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::OLEH_KRYVOI,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::V_TAKHTAI,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::IHOR_KYLYMNYK,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::OLEKSII_KOPTIEV,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::YEVHEN_PEREVOROCHAIEV,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::DMYTRO_KRAVCHENKO,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::IHOR_ILLICHOW,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::PAVLO_HRYNKIV,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::TARAS_LAZORIV,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::IHOR_VYSHNEVYI,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::DMITRO_GONCHARUK,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::S_BILYKH,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::ALEXANDR_KAITMAZAU_JR,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::VALERII_SKAFENKO,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::VALENTYN_KRYVKO,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::VITALII_MUKHIN,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::ANDRII_KREPCHUK,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::MYKOLA_TICHONENKO,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],

        // For Shipment seeder by Excel filename
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::KORSHUNOV_DENYS,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::NIKITIN_OLEH,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_2,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::KUBARKOU_RUSLAN,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_3,
        ],
        [
            DriverSeedKeysEnum::NAME => DriverNamesEnum::BIZUNOV_KIRILL,
            DriverSeedKeysEnum::CATEGORY_NAME => DriverCategoryNamesEnum::CATEGORY_1,
        ],
    ];
}
