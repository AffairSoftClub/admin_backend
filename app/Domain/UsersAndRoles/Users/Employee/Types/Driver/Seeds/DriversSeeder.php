<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Seeds\EmployeeSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverRepository;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds\DriverSeedsValues;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds\DriverSeedToModelAttributesConverter;

class DriversSeeder extends EmployeeSeeder
{
    public function __construct(
        DriverRepository $repository,
        DriverSeedToModelAttributesConverter $seedToEntityAttributesConverter)
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }

    protected function getSeeds(): array
    {
        return DriverSeedsValues::VALUES;
    }
}
