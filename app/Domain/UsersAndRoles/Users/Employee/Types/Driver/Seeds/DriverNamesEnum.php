<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds;

interface DriverNamesEnum
{
    const ALEXANDR_KAITMAZAU_JR = 'Alexandr Kaitmazau Jr.';
    const AMBRAZHUK_ANDREI = 'Ambrazhuk Andrei';
    const ANDRII_KREPCHUK = 'Andrii Krepchuk';
    const ANDRII_PRYTULIAK = 'Andrii Prytuliak';
    const BUBNOW_VIKTOR = 'Bubnow Viktor';
    const DENYS_KORSHUNOV = 'Denys Korshunov';
    const DMITRO_GONCHARUK = 'Dmitro Goncharuk';
    const DMYTRO_KRAVCHENKO = 'Dmytro Kravchenko';
    const GORBOTENKO_OLEKSANDER = 'Gorbotenko Oleksander';
    const IHOR_ILLICHOW = 'Ihor Illichow';
    const IHOR_KYLYMNYK = 'Ihor Kylymnyk';
    const IHOR_VYSHNEVYI = 'Ihor Vyshnevyi';
    const IVAN_STRAKHOV = 'Ivan Strakhov';
    const MUKHIN_OLEKSANDER = 'Mukhin Oleksander';
    const MYKOLA_TICHONENKO = 'Mykola Tichonenko'; // 770
    const NIMETS_SERGII = 'Nimets Sergii';
    const OLEH_KRYVOI = 'Oleh Kryvoi';
    const OLEKSANDR_KLOCHKO = 'Oleksandr Klochko';
    const OLEKSII_KOPTIEV = 'Oleksii Koptiev';
    const OLEXANDR_MYKHAILYCHENKO = 'Olexandr Mykhailychenko';
    const OLEXANDR_YARMACHENKO = 'Olexandr Yarmachenko';
    const OSTAPENKO_MYKOLA = 'Ostapenko Mykola';
    const PAVLO_HRYNKIV = 'Pavlo Hrynkiv';
    const POPOV_OLEKSANDER = 'Popov Oleksander';
    const ROMANOV_SERHIY = 'Romanov Serhiy';
    const SEMENOV_SERGII = 'Semenov Sergii';
    const SENNIKOV_SERHIY = 'Sennikov Serhiy';
    const S_BILYKH = 'S. Bilykh';
    const TARAS_LAZORIV = 'Taras Lazoriv';
    const VADIM_KABANOV = 'Vadim Kabanov'; // 728
    const VALENTYN_KRYVKO = 'Valentyn Kryvko';
    const VALERII_SKAFENKO = 'Valerii Skafenko'; // 748
    const VITALII_MUKHIN = 'Vitalii Mukhin'; // 750
    const V_TAKHTAI = 'V. Takhtai';
    const YEFANOF_VALERIY = 'Yefanof Valeriy';
    const YEVHEN_PEREVOROCHAIEV = 'Yevhen Perevorochaiev';
    const YEVHEN_VYSHNEVYI = 'Yevhen Vyshnevyi';

    // For Shipment seeder by Excel filename
    const KORSHUNOV_DENYS = 'Korshunov Denys';
    const NIKITIN_OLEH = 'Nikitin Oleh';
    const KUBARKOU_RUSLAN = 'Kubarkou Ruslan';
    const BIZUNOV_KIRILL = 'Bizunov Kirill';
}
