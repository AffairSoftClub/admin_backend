<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\MappedValauesProvider;

use App\Domain\Base\Model\BaseEntityMappedValuesProvider;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverRepository;

/**
 * @property DriverRepository $internalProvider
 */
abstract class BaseDriverMappedValuesProvider extends BaseEntityMappedValuesProvider
{
    public function __construct(DriverRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    protected function getEntityName(): string
    {
        return 'Driver';
    }

    public function getIdByNameOrFail(string $name): int
    {
        return $this->getByKeyOrFail($name);
    }

    public function getIdByNameOrNull(string $name): ?int
    {
        return $this->getByKeyOrNull($name);
    }
}
