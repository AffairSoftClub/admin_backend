<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\MappedValauesProvider;

class DriverIdByIdByNameProvider extends BaseDriverMappedValuesProvider
{
    protected function getCacheValues(): array
    {
        return $this->internalProvider->getNameToIdMapping();
    }

    protected function getKeyAttributeName(): string
    {
        return 'name';
    }
}
