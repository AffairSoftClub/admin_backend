<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Helpers\PasswordHasherHelper;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategory;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Employee;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table\DriverColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table\DriverOnlyColumnNamesEnum;
use Illuminate\Support\Str;

class Driver extends Employee
{
    use ChildExtendableModelTrait;

    //todo: refactor!!! don't use static methods!
    public static function newUserByAdmin(?string $name, ?string $email, ?string $phone, ?string $certificate, ?int $categoryId, ?string $address): self
    {
        $passwordHasher = app(PasswordHasherHelper::class);

        return self::create(
            [
                DriverColumnNamesEnum::NAME => $name,
                DriverColumnNamesEnum::EMAIL => $email,
                DriverColumnNamesEnum::PHONE => $phone,
                DriverColumnNamesEnum::CERTIFICATE => $certificate,
                DriverColumnNamesEnum::DRIVER_CATEGORY_ID => $categoryId,
                DriverColumnNamesEnum::ADDRESS => $address,
            ]
        );
    }

    // Scopes
    protected static function getRoleScopeClassName(): ?string
    {
        return DriverRoleScope::class;
    }


    // Extendable
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            DriverOnlyColumnNamesEnum::CERTIFICATE,
            DriverOnlyColumnNamesEnum::DRIVER_CATEGORY_ID,
        ];
    }

    // Relations
    public function category()
    {
        return $this->belongsTo(
            DriverCategory::class,
            DriverColumnNamesEnum::DRIVER_CATEGORY_ID,
            DriverCategoryColumnNamesEnum::ID
        );
    }

    public function getDriverCategory(): DriverCategory
    {
        return $this[DriverOnlyRelationNamesEnum::CATEGORY];
    }

    // Getters / Setters

    protected function getDefaultRoleId(): ?int
    {
        return $this->roleRepository->getDriverRoleIdOrFail();
    }
}
