<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table;

use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeTableNameValue;

interface DriverTableNameValue extends EmployeeTableNameValue
{
}
