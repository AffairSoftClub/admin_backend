<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table;

interface DriverOnlyColumnNamesEnum
{
    const CERTIFICATE = 'certificate';
    const DRIVER_CATEGORY_ID = 'driver_category_id';
}
