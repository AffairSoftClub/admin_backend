<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table;

use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeColumnNamesEnum as EmployeeColumnNameInterface;

interface DriverColumnNamesEnum extends EmployeeColumnNameInterface, DriverOnlyColumnNamesEnum
{

}
