<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver;

interface DriverOnlyRelationNamesEnum
{
    const CATEGORY = 'category';
}
