<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types;

interface DriverCategory3Interface
{
    const NAME = '3';
    const MONTH_RATE_AMOUNT = 2200;
}
