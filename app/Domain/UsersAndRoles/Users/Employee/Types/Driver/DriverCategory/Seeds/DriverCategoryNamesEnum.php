<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types\DriverCategory1Interface;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types\DriverCategory2Interface;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types\DriverCategory3Interface;

interface DriverCategoryNamesEnum
{
    const CATEGORY_1 = DriverCategory1Interface::NAME;
    const CATEGORY_2 = DriverCategory2Interface::NAME;
    const CATEGORY_3 = DriverCategory3Interface::NAME;
}
