<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory;

use App\Domain\Base\Model\BaseModel;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryTableNameValue;

class DriverCategory extends BaseModel
{
    protected $table = DriverCategoryTableNameValue::VALUE;

    protected $casts = [
        DriverCategoryColumnNamesEnum::MONTH_RATE_AMOUNT => 'float',
    ];

    protected $fillable = [
        DriverCategoryColumnNamesEnum::STATUS,
        DriverCategoryColumnNamesEnum::MONTH_RATE_AMOUNT,
        DriverCategoryColumnNamesEnum::NAME,
    ];

    public function getMonthRateAmount(): float
    {
        return $this[DriverCategoryColumnNamesEnum::MONTH_RATE_AMOUNT];
    }
}
