<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryColumnNamesEnum;

class DriverCategoryRepository extends BaseRepository
{
    use GetAllOrderedByNameTrait;

    public function __construct(DriverCategory $model)
    {
        parent::__construct($model);
    }

    protected function getNameColumnName(): string
    {
        return DriverCategoryColumnNamesEnum::NAME;
    }
}
