<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table;

interface DriverCategoryTableNameValue
{
    const VALUE = 'driver_categories';
}
