<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface DriverCategoryColumnNamesEnum extends IdTimestampsInterface
{
    const NAME = 'name';
    const MONTH_RATE_AMOUNT = 'month_rate_amount';
    const STATUS = 'status';
}
