<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategoryRepository;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\DriverCategorySeeds;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class DriverCategoriesSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(DriverCategoryRepository $repository)
    {
        parent::__construct($repository);
    }

    protected function getSeeds(): array
    {
        return DriverCategorySeeds::VALUES;
    }
}
