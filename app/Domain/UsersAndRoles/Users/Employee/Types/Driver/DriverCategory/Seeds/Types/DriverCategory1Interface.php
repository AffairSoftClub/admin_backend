<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types;

interface DriverCategory1Interface
{
    const NAME = '1';
    const MONTH_RATE_AMOUNT = 1600;
}
