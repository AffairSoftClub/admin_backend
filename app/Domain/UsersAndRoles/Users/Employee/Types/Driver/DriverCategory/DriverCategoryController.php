<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory;

use App\Domain\Base\Http\Controller\BaseBullshitController;
use App\Helpers\ConstantHelper;
use Illuminate\Http\Request;

class DriverCategoryController extends BaseBullshitController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = DriverCategory::orderByDesc('id')->paginate(10);
        return view('entities.driver-categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\DriverCategoryRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DriverCategoryRequest $request)
    {
        // todo перенести в другое место STOP ЗДЕСЬ добавить валидацию на числа
        if (DriverCategory::where('id', $request['id'])->first()) {
            $message = 'Категория успешно обновлена';
            $status = ConstantHelper::ENTITY_UPDATED;
        } else {
            $message = 'Категория успешно добавлена';
            $status = ConstantHelper::ENTITY_CREATED;
        }

        $category = DriverCategory::updateOrCreate(['id' => $request['id']],
            [
                'month_rate_amount' => $request['month_rate_amount']
                , 'name' => $request['name'],
                // 'status'=>$request['status'],
                'status' => ConstantHelper::DRIVER_CATEGORY_STATUS_ACTIVE,
            ]);


        return ['status' => $status, 'message' => $message, 'model' => $category];
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategory $driverCategory
     * @return \Illuminate\Http\Response
     */
    public function show(DriverCategory $drivers_category)
    {
        return $drivers_category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategory $driverCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(DriverCategory $drivers_category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategory $driverCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DriverCategory $drivers_category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategory $driverCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(DriverCategory $drivers_category)
    {
        $id = $drivers_category->id;
        $drivers_category->delete();
        $message = 'Категория успешно удалена';
        $status = ConstantHelper::ENTITY_DELETED;
        return ['status' => $status, 'message' => $message, 'model' => $id];

    }
}
