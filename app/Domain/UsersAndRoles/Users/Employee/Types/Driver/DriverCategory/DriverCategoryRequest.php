<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory;

use Illuminate\Foundation\Http\FormRequest;

class DriverCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'month_rate_amount' => 'required|numeric|min:1',
        ];
    }
}
