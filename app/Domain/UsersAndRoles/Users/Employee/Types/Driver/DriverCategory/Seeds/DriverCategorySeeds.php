<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types\DriverCategory1Interface;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types\DriverCategory2Interface;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types\DriverCategory3Interface;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Table\DriverCategoryColumnNamesEnum as ColumnNameInterfaceEnum;

interface DriverCategorySeeds
{
    const VALUES = [
        [
            ColumnNameInterfaceEnum::NAME => DriverCategory1Interface::NAME,
            ColumnNameInterfaceEnum::MONTH_RATE_AMOUNT => DriverCategory1Interface::MONTH_RATE_AMOUNT,
        ],
        [
            ColumnNameInterfaceEnum::NAME => DriverCategory2Interface::NAME,
            ColumnNameInterfaceEnum::MONTH_RATE_AMOUNT => DriverCategory2Interface::MONTH_RATE_AMOUNT,
        ],
        [
            ColumnNameInterfaceEnum::NAME => DriverCategory3Interface::NAME,
            ColumnNameInterfaceEnum::MONTH_RATE_AMOUNT => DriverCategory3Interface::MONTH_RATE_AMOUNT,
        ],
    ];
}
