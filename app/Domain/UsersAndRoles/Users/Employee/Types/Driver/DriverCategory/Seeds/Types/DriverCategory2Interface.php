<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\Seeds\Types;

interface DriverCategory2Interface
{
    const NAME = '2';
    const MONTH_RATE_AMOUNT = 1800;
}
