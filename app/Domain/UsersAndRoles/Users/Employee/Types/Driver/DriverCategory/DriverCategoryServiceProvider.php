<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class DriverCategoryServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return DriverCategoryRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return null;
    }
}
