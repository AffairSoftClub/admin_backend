<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver;

use App\Domain\UsersAndRoles\Users\Employee\EmployeeRelationNamesEnum;

interface DriverRelationNamesEnum extends EmployeeRelationNamesEnum, DriverOnlyRelationNamesEnum
{

}
