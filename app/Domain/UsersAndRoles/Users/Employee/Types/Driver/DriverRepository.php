<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver;

use App\Domain\UsersAndRoles\Users\Employee\EmployeeRepository;

/**
 * Class DriverRepository
 *
 * @method getByIdOrFail(int $id): Driver
 */
class DriverRepository extends EmployeeRepository
{
    public function __construct(Driver $model)
    {
        parent::__construct($model);
    }
}
