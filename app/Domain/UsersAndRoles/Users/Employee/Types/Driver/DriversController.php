<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver;

use App\Domain\Base\Http\Controller\BaseBullshitController;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverCategory\DriverCategory;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table\DriverColumnNamesEnum;
use App\Helpers\ConstantHelper;
use Illuminate\Http\Request;


class DriversController extends BaseBullshitController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Driver::orderByDesc('id')->leftJoin('roles', 'users.role_id', '=', 'roles.id')
            ->where('roles.name', '=', 'driver')
            ->paginate(10, ['users.*']);
        foreach ($users as $user) {
            $user->basecategory;
        }
        $categories = DriverCategory::all();
        return view('entities.drivers.index', compact('users', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $roleName = 'driver';
//        if($user = Worker::where('email',$request['email'])->first()  ){
        if ($user = Driver::where('email', $request['email'])->first()) {
            if ($user->hasRole($roleName)) {
                $user = Driver::updateOrCreate(['email' => $request['email']], [
                    DriverColumnNamesEnum::NAME => $request['name'],
                    DriverColumnNamesEnum::PHONE => $request['phone'],
                    DriverColumnNamesEnum::CERTIFICATE => $request['certificate'],
                    DriverColumnNamesEnum::DRIVER_CATEGORY_ID => $request['driver_category_id'],

                    DriverColumnNamesEnum::ADDRESS => $request['address'],
//                    'salary' => $request['salary'],
//                    'salary_rate' => $request['salary_rate'],

                ]);
                $message = 'Запись успешно обновлена';
                $status = ConstantHelper::ENTITY_UPDATED;
            } else {
                $message = 'Данную запись не возможно отредактировать. попробуйте изменить емейл';
                $status = ConstantHelper::ENTITY_WARNING;
            }
        } else {
            $user = Driver::newUserByAdmin(
                $request['name'], $request['email'], $request['phone'], $request['certificate'],
                $request['driver_category_id'], $request['address']);
            if ($user) {
                $user->setRole($roleName);
                $message = 'Водитель успешно добавлен';
                $status = ConstantHelper::ENTITY_CREATED;
            } else {
                $message = 'Чтото пошло не так (роль не добавлена)';
                $status = ConstantHelper::ENTITY_WARNING;
            }
        }


        return ['status' => $status, 'message' => $message, 'model' => $user];

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        return $driver;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Driver $driver)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        //
    }
}
