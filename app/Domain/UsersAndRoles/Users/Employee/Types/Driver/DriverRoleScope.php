<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Driver;

use App\Domain\UsersAndRoles\Role\Seeds\RoleNamesEnum as RoleNameEnum;
use App\Domain\UsersAndRoles\Users\Base\BaseRoleScope;

class DriverRoleScope extends BaseRoleScope
{
    public function __construct()
    {
        $roleName = RoleNameEnum::DRIVER;

        parent::__construct($roleName);
    }
}
