<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Appointment;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\City;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Seeds\EmployeeSeedToModelAttributesConverter as EmployeeSeedToModelAttributesConverter;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerOnlyColumnNamesEnum;

class WorkerSeedToModelAttributesConverter extends EmployeeSeedToModelAttributesConverter
{
    protected function getCalculatedValues(array $source): array
    {
        $parentCalculatedValues = parent::getCalculatedValues($source);
        $classOnlyCalculatedValues = $this->getClassOnlyCalculatedValues($source);

        return $parentCalculatedValues + $classOnlyCalculatedValues;
    }

    private function getClassOnlyCalculatedValues(array $seed)
    {
        return [
            WorkerOnlyColumnNamesEnum::MONTH_RATE_AMOUNT => SeedsParamsInterface::MONTH_RATE_AMOUNT,
        ];
    }

    protected function getRelationsMapping(): array
    {
        return [
            WorkerOnlyColumnNamesEnum::CITY_ID => [
                City::class,
                [
                    CityColumnNamesEnum::NAME => WorkerSeedKeysEnum::CITY_NAME
                ]
            ],
            WorkerOnlyColumnNamesEnum::APPOINTMENT_ID => [
                Appointment::class,
                [
                    AppointmentColumnNamesEnum::NAME => WorkerSeedKeysEnum::APPOINTMENT_NAME,
                ]
            ],
        ];
    }
}
