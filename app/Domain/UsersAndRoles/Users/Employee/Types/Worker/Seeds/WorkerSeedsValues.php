<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Seeds\AppointmentNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Seeds\CityNamesEnum;

interface WorkerSeedsValues
{
    const VALUES = [
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::ALI_MUSTAFAEV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::AMSTERDAM,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::ACCOUNTANT,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::ELENA_PAVLOVA,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::KIEV,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::OPERATOR,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::ELIZAVETA_VYPOLZOVA,
//            SeedKeysEnum::CITY_NAME => null,
//            SeedKeysEnum::APPOINTMENT_NAME => null,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::HALIL_MUSTAFAEV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::PARIS,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::ACCOUNTANT,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::KANAN_AKHUNDOV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::PRAGUE,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::OPERATOR,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::LIUDMYLA_MIHACHOVA,
//            SeedKeysEnum::CITY_NAME => null,
//            SeedKeysEnum::APPOINTMENT_NAME => null,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::NATALIA_KUNASIUK,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::KIEV,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::ACCOUNTANT,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::NIKITA_STOLYAROV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::OSLO,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::OPERATOR,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::OLEG_BAKHISHYEV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::PARIS,
//            SeedKeysEnum::APPOINTMENT_NAME => null,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::OLEG_KAZAKOV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::PRAGUE,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::OPERATOR,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::OLEH_KORSHUNOV,
//            SeedKeysEnum::CITY_NAME => null,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::ACCOUNTANT,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::OLGA_KORNIENKO,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::KIEV,
//            SeedKeysEnum::APPOINTMENT_NAME => null,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::SAMANDAR_AKHUNDOV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::OSLO,
//            SeedKeysEnum::APPOINTMENT_NAME => null,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::SERGEY_SIMONENKO,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::PARIS,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::ACCOUNTANT,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::STAS_LACINNIKOV,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::PRAGUE,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::OPERATOR,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::TANYA_LACINNIKOVA,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::AMSTERDAM,
//            SeedKeysEnum::APPOINTMENT_NAME => null,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::VIKTORIA_KYRNOS,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::KIEV,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::OPERATOR,
        ],
        [
            WorkerSeedKeysEnum::NAME => WorkerNamesEnum::VITALII_KUNASIUK,
            WorkerSeedKeysEnum::CITY_NAME => CityNamesEnum::OSLO,
            WorkerSeedKeysEnum::APPOINTMENT_NAME => AppointmentNamesEnum::ACCOUNTANT,
        ],
    ];
}
