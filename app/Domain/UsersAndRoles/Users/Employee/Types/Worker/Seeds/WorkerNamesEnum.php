<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds;

interface WorkerNamesEnum
{
    const ALI_MUSTAFAEV = 'Ali Mustafaev';
    const ELENA_PAVLOVA = 'Elena Pavlova';
    const ELIZAVETA_VYPOLZOVA = 'Elizaveta Vypolzova';
    const HALIL_MUSTAFAEV = 'Halil Mustafaev';
    const KANAN_AKHUNDOV = 'Kanan Akhundov';
    const LIUDMYLA_MIHACHOVA = 'Liudmyla Mihachova';
    const NATALIA_KUNASIUK = 'Natalia Kunasiuk';
    const NIKITA_STOLYAROV = 'Nikita Stolyarov';
    const OLEG_BAKHISHYEV = 'Oleg Bakhishyev';
    const OLEG_KAZAKOV = 'Oleg Kazakov';
    const OLEH_KORSHUNOV = 'Oleh Korshunov';
    const OLGA_KORNIENKO = 'Olga Kornienko';
    const SAMANDAR_AKHUNDOV = 'Samandar Akhundov';
    const SERGEY_SIMONENKO = 'Sergey Simonenko';
    const STAS_LACINNIKOV = 'Stas Lacinnikov';
    const TANYA_LACINNIKOVA = 'Tanya Lacinnikova';
    const VIKTORIA_KYRNOS = 'Viktoria Kyrnos';
    const VITALII_KUNASIUK = 'Vitalii Kunasiuk';
}
