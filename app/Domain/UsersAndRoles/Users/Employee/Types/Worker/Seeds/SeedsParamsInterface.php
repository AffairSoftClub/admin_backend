<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds;

interface SeedsParamsInterface
{
    const MONTH_RATE_AMOUNT = 1800;
}
