<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Seeds\EmployeeSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds\WorkerSeedToModelAttributesConverter;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds\WorkerSeedsValues;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\WorkerRepository;

class WorkersSeeder extends EmployeeSeeder
{
    public function __construct(
        WorkerRepository $repository,
        WorkerSeedToModelAttributesConverter $seedToEntityAttributesConverter)
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }

    protected function getSeeds(): array
    {
        return WorkerSeedsValues::VALUES;
    }
}
