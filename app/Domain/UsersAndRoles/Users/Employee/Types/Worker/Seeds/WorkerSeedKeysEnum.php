<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Seeds\BaseEmployeeSeedKeysEnum as EmployeeSeedKeysEnum;

interface WorkerSeedKeysEnum extends EmployeeSeedKeysEnum
{
    const CITY_NAME = 'city_name';
    const APPOINTMENT_NAME = 'appointment_name';
}
