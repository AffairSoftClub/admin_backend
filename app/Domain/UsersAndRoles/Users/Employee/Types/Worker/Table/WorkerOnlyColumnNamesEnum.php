<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table;

interface WorkerOnlyColumnNamesEnum
{
    const MONTH_RATE_AMOUNT = 'month_rate_amount';

    const CITY_ID = 'city_id';
    const APPOINTMENT_ID = 'appointment_id';
}
