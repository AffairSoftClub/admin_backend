<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table;

use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeColumnNamesEnum;

interface WorkerColumnNamesEnum extends EmployeeColumnNamesEnum, WorkerOnlyColumnNamesEnum
{
}
