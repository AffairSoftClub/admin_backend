<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table;

use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeTableNameValue;

interface WorkerTableNameValue extends EmployeeTableNameValue
{

}
