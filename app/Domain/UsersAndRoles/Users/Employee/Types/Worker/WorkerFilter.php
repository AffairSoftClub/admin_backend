<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use App\Domain\Base\Http\Filter\BaseFilter;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerColumnNamesEnum;

class WorkerFilter extends BaseFilter
{
    public function cityId(int $cityId)
    {
        $this->where(WorkerColumnNamesEnum::CITY_ID, $cityId);
    }
}
