<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use \App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerColumnNamesEnum;

interface WorkerFilterKeysEnum
{
    const CITY_ID = WorkerColumnNamesEnum::CITY_ID;
}
