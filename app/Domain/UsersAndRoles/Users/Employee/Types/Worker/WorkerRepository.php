<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use App\Base\Repository\Traits\Base\GetByIdOrFailTrait;
use App\Domain\UsersAndRoles\Users\Employee\EmployeeRepository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class WorkerRepository
 *
 * @method getByIdOrFail(int $workerId): Worker
 */
class WorkerRepository extends EmployeeRepository
{
    use GetByIdOrFailTrait;

    public function __construct(Worker $model)
    {
        parent::__construct($model);
    }

    public function getAllWithAppointmentNameAndCityNameOrderedByName(): Collection
    {
        $nameColumn = $this->getNameColumnName();

        return $this->getModel()
            ->replicate()
            ->append(WorkerOnlyVirtualFieldNamesEnum::NAME_WITH_APPOINTMENT_AND_CITY)
            ->makeVisible(WorkerOnlyVirtualFieldNamesEnum::NAME_WITH_APPOINTMENT_AND_CITY)
            ->with([
                WorkerRelationNamesEnum::APPOINTMENT,
                WorkerRelationNamesEnum::CITY,
            ])
            ->orderBy($nameColumn)
            ->get();
    }
}
