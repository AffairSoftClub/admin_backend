<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

interface WorkerOnlyVirtualFieldNamesEnum
{
    const NAME_WITH_APPOINTMENT_AND_CITY = 'name_with_appointment_and_city';
}
