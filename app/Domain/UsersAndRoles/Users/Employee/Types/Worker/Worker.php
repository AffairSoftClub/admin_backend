<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\UsersAndRoles\Users\Employee\Employee;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Appointment;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\City;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerColumnNamesEnum as ColumnNameEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerOnlyColumnNamesEnum;
use App\Helpers\PasswordHasherHelper;
use EloquentFilter\Filterable;
use Illuminate\Support\Str;

class Worker extends Employee
{
    use Filterable, ChildExtendableModelTrait;

    protected $appends = [
        WorkerOnlyVirtualFieldNamesEnum::NAME_WITH_APPOINTMENT_AND_CITY,
    ];

    /**
     * @param $name
     * @param $email
     * @param $phone
     * @param $address
     * @param $post
     * @param $cityId
     * @param $salary
     * @return static
     */
    public static function newUserByAdmin($name, $email, $phone, $address, $post, $cityId, $salary): self
    {
        $passwordHasher = app(PasswordHasherHelper::class);

        return self::create(
            [
                ColumnNameEnum::NAME => $name,
                ColumnNameEnum::EMAIL => $email,
                ColumnNameEnum::PHONE => $phone,
                ColumnNameEnum::ADDRESS => $address,
//                ColumnNameEnum::POST => $post,
                ColumnNameEnum::CITY_ID => $cityId,
                ColumnNameEnum::MONTH_RATE_AMOUNT => $salary
            ]
        );
    }

    // Scopes
    protected static function getRoleScopeClassName(): ?string
    {
        return WorkerRoleScope::class;
    }

    // Filter
    public function modelFilter()
    {
        return $this->provideFilter(WorkerFilter::class);
    }

    // Extendable
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            WorkerOnlyColumnNamesEnum::MONTH_RATE_AMOUNT,
            WorkerOnlyColumnNamesEnum::APPOINTMENT_ID,
            WorkerOnlyColumnNamesEnum::CITY_ID,
        ];
    }

    private function getClassOnlyVisibleNotFillableFromClass(): array
    {
        return [
            WorkerOnlyVirtualFieldNamesEnum::NAME_WITH_APPOINTMENT_AND_CITY,
        ];
    }

    private function getClassOnlyRelationNamesFromClass(): array
    {
        return [
            WorkerOnlyRelationNamesEnum::APPOINTMENT,
            WorkerOnlyRelationNamesEnum::CITY,
        ];
    }

    // Relations
    public function appointment()
    {
        return $this->belongsTo(
            Appointment::class,
            WorkerOnlyColumnNamesEnum::APPOINTMENT_ID,
            AppointmentColumnNamesEnum::ID
        );
    }

    public function city()
    {
        return $this->belongsTo(
            City::class,
            WorkerOnlyColumnNamesEnum::CITY_ID,
            CityColumnNamesEnum::ID
        );
    }

    // Getters / Setters

    public function getNameWithAppointmentAndCityAttribute()
    {
        $name = $this[ColumnNameEnum::NAME];
        $appointmentName = $this[WorkerRelationNamesEnum::APPOINTMENT] ? $this[WorkerRelationNamesEnum::APPOINTMENT][AppointmentColumnNamesEnum::NAME] : '-';
        $cityName = $this[WorkerRelationNamesEnum::CITY] ? $this[WorkerRelationNamesEnum::CITY][CityColumnNamesEnum::NAME] : '-';

        $chunks = [
            $name,
            $appointmentName,
            $cityName
        ];

        $chunks = array_filter($chunks);

        return implode(' - ', $chunks);
    }

    public function getMonthRateAmount(): float
    {
        return $this[ColumnNameEnum::MONTH_RATE_AMOUNT];
    }

    public function getAppointmentId(): ?int
    {
        return $this[ColumnNameEnum::APPOINTMENT_ID];
    }

    public function getCityId(): ?int
    {
        return $this[ColumnNameEnum::CITY_ID];
    }

    protected function getDefaultRoleId(): ?int
    {
        return $this->roleRepository->getWorkerRoleIdOrFail();
    }
}
