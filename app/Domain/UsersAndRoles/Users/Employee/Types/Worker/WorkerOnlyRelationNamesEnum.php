<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

interface WorkerOnlyRelationNamesEnum
{
    const APPOINTMENT = 'appointment';
    const CITY = 'city';
}
