<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use App\Domain\Base\Http\Controller\BaseBullshitController;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\AppointmentRepository;
use App\Helpers\ConstantHelper;
use Illuminate\Http\Request;

class WorkersController extends BaseBullshitController
{
    //todo: refactor all!
    public function index(AppointmentRepository $appointmentRepository)
    {
        $users = Worker::orderByDesc('id')
            ->paginate(10, ['users.*']);

        $appointments = $appointmentRepository->getAllOrderedByName();

        return view('entities.workers.index', [
            'users' => $users,
            'appointments' => $appointments,
        ]);
    }

    public function store(Request $request)
    {
        $roleName = 'worker';
        if ($user = Worker::where('email', $request['email'])->first()) {

            if ($user->hasRole($roleName)) {
                $user = Worker::updateOrCreate(['email' => $request['email']], [
                    'name' => $request['name'],
                    'phone' => $request['phone'],
                    'address' => $request['address'],
                    'appointment_id' => $request['appointment_id'],
                    'city' => $request['city'],
                    'salary' => $request['salary'],
                ]);
                $message = 'Запись успешно обновлена';
                $status = ConstantHelper::ENTITY_UPDATED;
            } else {
                $message = 'Данную запись не возможно отредактировать. попробуйте изменить емейл';
                $status = ConstantHelper::ENTITY_WARNING;
            }


        } else {
            $user = Worker::newUserByAdmin(
                $request['name'], $request['email'], $request['phone'], $request['address'],
                $request['post'], $request['city'], $request['salary']);
            if ($user) {
                $user->setRole($roleName);
                $message = 'Работник офиса успешно добавлен';
                $status = ConstantHelper::ENTITY_CREATED;
            } else {
                $message = 'Чтото пошло не так (роль не добавлена)';
                $status = ConstantHelper::ENTITY_WARNING;
            }
        }


        return ['status' => $status, 'message' => $message, 'model' => $user];
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Worker $worker
     * @return \Illuminate\Http\Response
     */
    public function show(Worker $worker)
    {
        return $worker;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Worker $worker
     * @return \Illuminate\Http\Response
     */
    public function edit(Worker $worker)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Worker $worker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Worker $worker)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Worker $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worker $worker)
    {
        //
    }
}
