<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use \App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerColumnNamesEnum;

class WorkerService
{
    /** @var WorkerRepository */
    private $workerRepository;

    public function __construct(
        WorkerRepository $workerRepository
    )
    {
        $this->workerRepository = $workerRepository;
    }

    private function getWorkerByIdOrFail(int $workerId): Worker
    {
        return $this->workerRepository->getByIdOrFail($workerId);
    }
}
