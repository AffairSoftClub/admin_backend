<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class CityServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return CityRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return CityService::class;
    }
}
