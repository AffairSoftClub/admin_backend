<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface CityColumnNamesEnum extends IdTimestampsInterface
{
    const NAME = 'name';
}
