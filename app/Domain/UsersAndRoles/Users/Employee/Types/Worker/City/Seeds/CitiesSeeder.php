<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\CityRepository;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class CitiesSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(CityRepository $repository)
    {
        parent::__construct($repository);
    }

    protected function getSeeds(): array
    {
        return CitySeedsValues::VALUES;
    }
}
