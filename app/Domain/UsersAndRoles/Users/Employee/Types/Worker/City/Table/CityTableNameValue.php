<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table;

interface CityTableNameValue
{
    const VALUE = 'cities';
}
