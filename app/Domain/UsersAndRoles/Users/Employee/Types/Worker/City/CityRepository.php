<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Base\Repository\Traits\Advanced\Name\GetOrCreateByNameColumnTrait;
use App\Base\Repository\Traits\Base\DeleteByModelTrait;
use App\Base\Repository\Traits\Base\GetByIdOrFailTrait;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityColumnNamesEnum;

/**
 * Class CityRepository
 *
 * @method getByIdOrFail(int $id): City
 * @method create(array $attributes): City
 * @method getOrCreateByNameColumn(string $name): City
 */
class CityRepository extends BaseRepository
{
    use GetAllOrderedByNameTrait, GetByIdOrFailTrait, DeleteByModelTrait, GetOrCreateByNameColumnTrait;

    public function __construct(City $model)
    {
        parent::__construct($model);
    }

    protected function getNameColumnName(): string
    {
        return CityColumnNamesEnum::NAME;
    }
}
