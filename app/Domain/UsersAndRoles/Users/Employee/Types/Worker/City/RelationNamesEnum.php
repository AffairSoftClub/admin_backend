<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City;

interface RelationNamesEnum
{
    const DIRECTION_ADDRESSES = 'directionAddresses';
}
