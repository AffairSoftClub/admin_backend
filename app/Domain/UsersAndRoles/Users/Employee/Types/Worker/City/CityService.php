<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City;

/**
 * Class CityService
 * @package App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City
 */
class CityService
{
    /** @var CityRepository */
    private $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function getOrCreateByName(string $cityName): City
    {
        return $this->cityRepository->getOrCreateByNameColumn($cityName);
    }

    public function deleteCityByIdIfNotUsed(int $cityId): bool
    {
        $city = $this->getCityById($cityId);
        $addressesCount = $city->directionAddresses()->count();

        if ($addressesCount) {
            return false;
        }

        $this->deleteCity($city);

        return true;
    }

    private function getCityById(int $cityId): City
    {
        return $this->cityRepository->getByIdOrFail($cityId);
    }

    private function deleteCity(City $city)
    {
        $this->cityRepository->deleteByModel($city);
    }
}
