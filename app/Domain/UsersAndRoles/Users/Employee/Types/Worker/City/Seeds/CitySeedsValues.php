<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityColumnNamesEnum;

interface CitySeedsValues
{
    const VALUES = [
        [
            CityColumnNamesEnum::NAME => CityNamesEnum::AMSTERDAM,
        ],
        [
            CityColumnNamesEnum::NAME => CityNamesEnum::KIEV,
        ],
        [
            CityColumnNamesEnum::NAME => CityNamesEnum::OSLO,
        ],
        [
            CityColumnNamesEnum::NAME => CityNamesEnum::PARIS,
        ],
        [
            CityColumnNamesEnum::NAME => CityNamesEnum::PRAGUE,
        ],
    ];
}
