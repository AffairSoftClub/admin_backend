<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Seeds;

interface CityNamesEnum
{
    const AMSTERDAM = 'Амстердам';
    const KIEV = 'Киев';
    const OSLO = 'Осло';
    const PARIS = 'Париж';
    const PRAGUE = 'Прага';
}
