<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City;

use App\Domain\Base\Model\BaseModel;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\City\Table\CityTableNameValue;

class City extends BaseModel
{
    protected $table = CityTableNameValue::VALUE;

    protected $fillable = [
        CityColumnNamesEnum::NAME,
    ];
}
