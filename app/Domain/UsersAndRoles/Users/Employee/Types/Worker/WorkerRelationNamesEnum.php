<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use \App\Domain\UsersAndRoles\Users\Employee\EmployeeRelationNamesEnum;

interface WorkerRelationNamesEnum extends EmployeeRelationNamesEnum, WorkerOnlyRelationNamesEnum
{
}
