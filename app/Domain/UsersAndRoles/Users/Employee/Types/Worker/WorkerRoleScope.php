<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker;

use App\Domain\UsersAndRoles\Role\Seeds\RoleNamesEnum as RoleNameEnum;
use App\Domain\UsersAndRoles\Users\Base\BaseRoleScope;

class WorkerRoleScope extends BaseRoleScope
{
    public function __construct()
    {
        $roleName = RoleNameEnum::WORKER;

        parent::__construct($roleName);
    }
}
