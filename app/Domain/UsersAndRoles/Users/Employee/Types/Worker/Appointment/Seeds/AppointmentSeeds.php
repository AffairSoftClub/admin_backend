<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentColumnNamesEnum as ColumnNameEnum;

interface AppointmentSeeds
{
    const VALUES = [
        [
            ColumnNameEnum::NAME => AppointmentNamesEnum::OPERATOR,
        ],
        [
            ColumnNameEnum::NAME => AppointmentNamesEnum::ACCOUNTANT,
        ],
    ];
}
