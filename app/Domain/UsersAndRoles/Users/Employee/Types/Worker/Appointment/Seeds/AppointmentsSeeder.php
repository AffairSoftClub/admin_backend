<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Seeds;

use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\AppointmentRepository;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Seeds\AppointmentSeeds as AppointmentsSeedsInterface;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class AppointmentsSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(AppointmentRepository $appointmentRepository)
    {
        parent::__construct($appointmentRepository);
    }

    protected function getSeeds(): array
    {
        return AppointmentsSeedsInterface::VALUES;
    }
}
