<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Seeds;

interface AppointmentNamesEnum
{
    const OPERATOR = 'Оператор';
    const ACCOUNTANT = 'Бухгалтер';
}
