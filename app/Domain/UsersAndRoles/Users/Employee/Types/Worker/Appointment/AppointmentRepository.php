<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentColumnNamesEnum;

class AppointmentRepository extends BaseRepository
{
    use GetAllOrderedByNameTrait;

    public function __construct(Appointment $model)
    {
        parent::__construct($model);
    }

    protected function getNameColumnName(): string
    {
        return AppointmentColumnNamesEnum::NAME;
    }
}
