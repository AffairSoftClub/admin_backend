<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment;

use App\Domain\Base\Model\BaseModel;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table\AppointmentTableNameValue;

class Appointment extends BaseModel
{
    protected $table = AppointmentTableNameValue::VALUE;

    protected $fillable = [
        AppointmentColumnNamesEnum::NAME,
        AppointmentColumnNamesEnum::STATUS,
        AppointmentColumnNamesEnum::SORT,
    ];
}
