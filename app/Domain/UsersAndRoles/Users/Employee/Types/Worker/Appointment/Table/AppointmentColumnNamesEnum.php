<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface AppointmentColumnNamesEnum extends IdTimestampsInterface
{
    const NAME = 'name';
    const STATUS = 'status';
    const SORT = 'sort';
}
