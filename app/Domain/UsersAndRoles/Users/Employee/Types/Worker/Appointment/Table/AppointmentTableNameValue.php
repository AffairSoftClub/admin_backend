<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment\Table;

interface AppointmentTableNameValue
{
    const VALUE = 'appointments';
}
