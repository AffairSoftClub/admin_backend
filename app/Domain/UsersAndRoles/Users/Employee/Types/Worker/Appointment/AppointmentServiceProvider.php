<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Appointment;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class AppointmentServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return AppointmentRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return null;
    }
}
