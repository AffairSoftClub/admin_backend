<?php

namespace App\Domain\UsersAndRoles\Users\Employee;

use App\Domain\UsersAndRoles\Users\Base\BaseUserRelationNamesEnum as CommonRelationNamesEnum;

interface EmployeeRelationNamesEnum extends CommonRelationNamesEnum, EmployeeOnlyRelationNamesEnum
{

}
