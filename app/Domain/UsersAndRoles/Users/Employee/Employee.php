<?php

namespace App\Domain\UsersAndRoles\Users\Employee;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\UsersAndRoles\Users\Base\BaseUser;
use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeOnlyColumnNamesEnum;

class Employee extends BaseUser
{
    use ChildExtendableModelTrait;

    protected function hashPassword(string $password): string
    {
        return $this->passwordHasher->hash($password);
    }

    // Extendable
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            EmployeeOnlyColumnNamesEnum::ADDRESS,
            EmployeeOnlyColumnNamesEnum::PHONE,
        ];
    }

    protected function getDefaultRoleId(): ?int
    {
        return null;
    }
}
