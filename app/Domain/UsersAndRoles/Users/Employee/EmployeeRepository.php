<?php

namespace App\Domain\UsersAndRoles\Users\Employee;

use App\Domain\UsersAndRoles\Users\Base\BaseUserRepository;

class EmployeeRepository extends BaseUserRepository
{
    public function __construct(Employee $model)
    {
        parent::__construct($model);
    }
}
