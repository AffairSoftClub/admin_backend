<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Seeds;

use App\Domain\Base\Seeds\BaseAggregateSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Seeds\DriversSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Seeds\WorkersSeeder;

class EmployeesSeeder extends BaseAggregateSeeder
{
    public function run()
    {
        $this->call(DriversSeeder::class);
        $this->call(WorkersSeeder::class);
    }
}
