<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Seeds;

use App\Domain\UsersAndRoles\Users\Base\Seeds\BaseUserSeedToModelAttributesConverter;
use App\Domain\UsersAndRoles\Users\Employee\Seeds\BaseEmployeeSeedKeysEnum as SeedKeysEnum;
use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeColumnNamesEnum;

abstract class EmployeeSeedToModelAttributesConverter extends BaseUserSeedToModelAttributesConverter
{
    protected function getMapping(): array
    {
        return [
            EmployeeColumnNamesEnum::NAME => BaseEmployeeSeedKeysEnum::NAME,
        ];
    }

    protected function getNameBySeed(array $seed): string
    {
        return $seed[BaseEmployeeSeedKeysEnum::NAME];
    }
}
