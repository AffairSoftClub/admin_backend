<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Seeds;

use App\Domain\Base\Seeds\SeedToEntityAttributesConverterInterface;
use App\Domain\UsersAndRoles\Users\Employee\EmployeeRepository;
use App\Domain\Base\Seeds\Entity\RepositoryBased\BySeedsRepositoryBasedSeeder;

abstract class EmployeeSeeder extends BySeedsRepositoryBasedSeeder
{
    public function __construct(
        EmployeeRepository $repository,
        SeedToEntityAttributesConverterInterface $seedToEntityAttributesConverter)
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }
}
