<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Seeds;

use App\Domain\UsersAndRoles\Users\Base\Seeds\BaseUserSeedKeysEnum as CommonSeedKeysEnum;

interface BaseEmployeeSeedKeysEnum extends CommonSeedKeysEnum
{
}
