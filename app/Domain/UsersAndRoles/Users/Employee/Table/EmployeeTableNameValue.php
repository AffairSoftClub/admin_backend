<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserTableNameValue;

interface EmployeeTableNameValue extends BaseUserTableNameValue
{

}
