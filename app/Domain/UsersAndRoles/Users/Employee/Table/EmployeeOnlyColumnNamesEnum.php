<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Table;

interface EmployeeOnlyColumnNamesEnum
{
    const PHONE = 'phone';
    const ADDRESS = 'address';
}
