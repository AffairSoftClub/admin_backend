<?php

namespace App\Domain\UsersAndRoles\Users\Employee\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;

interface EmployeeColumnNamesEnum extends BaseUserColumnNamesEnum, EmployeeOnlyColumnNamesEnum
{
}
