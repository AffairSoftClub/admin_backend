<?php

namespace App\Domain\UsersAndRoles\Users\Employee;

interface EmployeeTypesEnum
{
    const DRIVER = 1;
    const WORKER = 2;
}
