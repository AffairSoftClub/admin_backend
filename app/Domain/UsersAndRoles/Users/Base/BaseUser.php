<?php

namespace App\Domain\UsersAndRoles\Users\Base;

use App\Domain\Base\Model\ExtendableModel\Complex\RootExtendableModelTrait;
use App\Domain\UsersAndRoles\Role\RoleRepository;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Table\AdminUserColumnNamesEnum as ColumnNameEnum;
use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserTableNameValue;
use Exception;
use Illuminate\Notifications\Notifiable;
use TCG\Voyager\Models\User as VoyagerUser;

abstract class BaseUser extends VoyagerUser
{
    //todo: move and renate common to base
    use RootExtendableModelTrait;
    use Notifiable;

    protected $table = BaseUserTableNameValue::VALUE;

    /** @var RoleRepository */
    protected $roleRepository;

    public function __construct(array $attributes = [])
    {
        $this->roleRepository = app(RoleRepository::class);
        $this->initModelProperties();

        parent::__construct($attributes);
    }

    // Role Scope
    protected static function boot()
    {
        parent::boot();

        self::addRoleScope();
    }

    private static function addRoleScope()
    {
        $roleScope = self::getRoleScope();

        if (!$roleScope) {
            return;
        }

        static::addGlobalScope($roleScope);
    }

    private static function getRoleScope(): ?BaseRoleScope
    {
        $roleScopeClassName = static::getRoleScopeClassName();

        if (!$roleScopeClassName) {
            return null;
        }

        return app($roleScopeClassName);
    }

    protected static function getRoleScopeClassName(): ?string
    {
        return null;
    }

    // Extendable
    private function getClassOnlyCastsFromClass(): array
    {
        return [
            BaseUserColumnNamesEnum::EMAIL_VERIFIED_AT => 'datetime',
        ];
    }

    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            BaseUserColumnNamesEnum::NAME,
            BaseUserColumnNamesEnum::EMAIL,
            BaseUserColumnNamesEnum::EMAIL_VERIFIED_AT,
            BaseUserColumnNamesEnum::AVATAR,
            BaseUserColumnNamesEnum::ROLE_ID,
            BaseUserColumnNamesEnum::SETTINGS,
        ];
    }

    // Getters/Setters
    private function setDefaultRoleId($attributes)
    {
        $roleId = $this->getRoleId();
        $attributes[ColumnNameEnum::ROLE_ID] = $roleId;

        return $attributes;
    }

//    abstract protected function getRoleIdOrFail(): ?int;

    private function getClassOnlyDefaultAttributesFromClass(): array
    {
        return [
            BaseUserColumnNamesEnum::ROLE_ID => $this->getDefaultRoleId(),
        ];
    }

    abstract protected function getDefaultRoleId(): ?int;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this[ColumnNameEnum::NAME];
    }

    public function getRoleId()
    {
        return $this->getAttribute(BaseUserColumnNamesEnum::ROLE_ID);
    }
}
