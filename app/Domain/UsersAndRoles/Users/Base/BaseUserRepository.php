<?php

namespace App\Domain\UsersAndRoles\Users\Base;

use App\Base\GetAsMappingInterface;
use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Base\GetAsMappingTrait;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;

abstract class BaseUserRepository extends BaseRepository implements GetAsMappingInterface
{
    use GetAllOrderedByNameTrait, GetAsMappingTrait;

    public function __construct(BaseUser $model)
    {
        parent::__construct($model);
    }

    protected function getNameColumnName(): string
    {
        return BaseUserColumnNamesEnum::NAME;
    }

    public function getNameToIdMapping(): array
    {
        return $this->getAsMapping(
            BaseUserColumnNamesEnum::NAME,
            BaseUserColumnNamesEnum::ID
        );
    }
}
