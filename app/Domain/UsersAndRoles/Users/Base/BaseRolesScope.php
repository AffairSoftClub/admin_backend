<?php

namespace App\Domain\UsersAndRoles\Users\Base;

use App\Domain\UsersAndRoles\Role\Table\RoleColumnNamesEnum as RoleColumnNameInterface;
use App\Domain\UsersAndRoles\Users\Base\Seeds\BaseAttributeNamesEnum as UserAttributeNameEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class BaseRolesScope implements Scope
{
    /** @var array */
    private $rolesNames;

    public function __construct(array $rolesNames)
    {
        $this->rolesNames = $rolesNames;
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param Builder $builder
     * @param Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereHas(UserAttributeNameEnum::ROLE, function (Builder $query) {
            $query->whereIn(RoleColumnNameInterface::NAME, $this->rolesNames);
        });
    }

}
