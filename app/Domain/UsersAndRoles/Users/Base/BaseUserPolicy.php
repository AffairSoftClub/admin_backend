<?php

namespace App\Domain\UsersAndRoles\Users\Base;

use App\Domain\Base\Model\VoyagerPolicyWrapperTrait;
use TCG\Voyager\Policies\UserPolicy;

class BaseUserPolicy extends UserPolicy
{
    use VoyagerPolicyWrapperTrait;

    protected function getPermissionKey(): string
    {
        return 'users';
    }


//
//    public function create(UserInterface $user)
//    {
//        return $this->add($user);
//    }
//
//    public function add(UserInterface $user)
//    {
//        return $this->checkPermission($user, new UserModel(), 'read');
//    }
//
//    public function update(UserInterface $user, ?Model $model = null): bool
//    {
//        return $this->edit($user, new UserModel);
//    }
//
//    public function delete(UserInterface $user, $model = null): bool
//    {
//        return $model
//            ? parent::delete($user, $model)
//            : $this->hasPermissionByMethodName(__FUNCTION__, $user, $model);
//    }
}
