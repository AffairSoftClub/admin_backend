<?php

namespace App\Domain\UsersAndRoles\Users\Base\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface BaseUserColumnNamesEnum extends IdTimestampsInterface
{
    const ROLE_ID = 'role_id';

    const NAME = 'name';

    const EMAIL = 'email';
    const EMAIL_VERIFIED_AT = 'email_verified_at';

    const AVATAR = 'avatar';

//    const PASSWORD = 'password';
//    const REMEMBER_TOKEN = 'remember_token';

    const SETTINGS = 'settings';
}
