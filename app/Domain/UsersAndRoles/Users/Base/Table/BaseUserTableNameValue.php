<?php

namespace App\Domain\UsersAndRoles\Users\Base\Table;

interface BaseUserTableNameValue
{
    const VALUE = 'users';
}
