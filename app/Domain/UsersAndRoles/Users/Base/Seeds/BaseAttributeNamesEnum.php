<?php

namespace App\Domain\UsersAndRoles\Users\Base\Seeds;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;

interface BaseAttributeNamesEnum extends BaseUserColumnNamesEnum
{
    const ROLE = 'role';
}
