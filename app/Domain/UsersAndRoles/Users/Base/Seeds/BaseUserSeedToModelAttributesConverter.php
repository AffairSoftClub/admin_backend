<?php

namespace App\Domain\UsersAndRoles\Users\Base\Seeds;

use App\Domain\Base\Seeds\Entity\RepositoryBased\BaseSeedToModelAttributesConverter;
use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;
use Illuminate\Support\Str;

abstract class BaseUserSeedToModelAttributesConverter extends BaseSeedToModelAttributesConverter
{
    protected function getCalculatedValues(array $source): array
    {
        return [
            BaseUserColumnNamesEnum::EMAIL => $this->getEmailBySeed($source),
            BaseUserColumnNamesEnum::AVATAR => 'users/default.png',
        ];
    }

    protected function getEmailBySeed(array $seed): string
    {
        $name = $this->getNameBySeed($seed);

        return $this->generateEmail($name);
    }

    abstract protected function getNameBySeed(array $seed): string;

    protected function generateEmail(string $userName): string
    {
        $login = str_replace(' ', '', $userName);
        $domain = $this->getDomain();

        return "{$login}@{$domain}";
    }

    protected function getDomain(): string
    {
        return BaseUserSeedsParamsInterface::DOMAIN;
    }

    protected function generatePassword(string $email): string
    {
        return bcrypt($email);
    }

    protected function generateRememberToken(): string
    {
        return Str::random(60);
    }
}
