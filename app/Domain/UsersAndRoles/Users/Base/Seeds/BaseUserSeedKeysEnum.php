<?php

namespace App\Domain\UsersAndRoles\Users\Base\Seeds;

interface BaseUserSeedKeysEnum
{
    const NAME = 'name';
    const EMAIL = 'email';
}
