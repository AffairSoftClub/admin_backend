<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin;

use App\Domain\UsersAndRoles\Users\Base\BaseUserRepository;

class AdminRepository extends BaseUserRepository
{
    public function __construct(Admin $model)
    {
        parent::__construct($model);
    }
}
