<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin;

use App\Domain\UsersAndRoles\Role\Seeds\RoleNamesEnum;
use App\Domain\UsersAndRoles\Users\Base\BaseRoleScope;

class AdminRoleScope extends BaseRoleScope
{
    public function __construct()
    {
        $roleName = RoleNamesEnum::ADMIN;

        parent::__construct($roleName);
    }
}
