<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin;

use App\Domain\UsersAndRoles\Users\Base\BaseUserRelationNamesEnum;

interface AdminRelationNamesEnum extends BaseUserRelationNamesEnum, AdminOnlyRelationNamesEnum
{

}
