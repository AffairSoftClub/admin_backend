<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;

interface AdminUserColumnNamesEnum extends BaseUserColumnNamesEnum, AdminOrManagerOnlyColumnNamesEnum, AdminOnlyColumnNamesEnum
{
}
