<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserTableNameValue as CommonUserTableNameInterface;

interface AdminUserTableNameValue extends CommonUserTableNameInterface
{

}
