<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\BaseAdminOrManager;
use App\Domain\UsersAndRoles\Users\Parts\AdminOrManager\GetEmailSignatureTrait;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\UserHavingEmailSignatureInterface;

class Admin extends BaseAdminOrManager implements UserHavingEmailSignatureInterface
{
    use ChildExtendableModelTrait;

    protected static function getRoleScopeClassName(): ?string
    {
        return AdminRoleScope::class;
    }

    protected function getDefaultRoleId(): int
    {
        return $this->roleRepository->getAdminRoleIdOrFail();
    }
}
