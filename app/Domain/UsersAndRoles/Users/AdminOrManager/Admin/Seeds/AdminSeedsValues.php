<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Seeds;

interface AdminSeedsValues
{
    const VALUES = [
        [
            AdminUserSeedKeysEnum::NAME => Admin1Interface::NAME,
            AdminUserSeedKeysEnum::EMAIL => Admin1Interface::EMAIL,
            AdminUserSeedKeysEnum::EMAIL_SIGNATURE => Admin1Interface::EMAIL_SIGNATURE,
            AdminUserSeedKeysEnum::PASSWORD => Admin1Interface::PASSWORD,

        ],
    ];
}
