<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Seeds;

use App\Domain\Base\Seeds\Entity\RepositoryBased\BaseSeedToModelAttributesConverter;
use App\Domain\Base\Seeds\RelationsMappingGenerator;
use App\Domain\Base\Seeds\SeedRelationsMappingToValuesConverter;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Table\AdminUserColumnNamesEnum;
use App\Helpers\MapperHelper;
use App\Helpers\PasswordHasherHelper;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class AdminSeedToModelAttributesConverter extends BaseSeedToModelAttributesConverter
{
    /** @var PasswordHasherHelper */
    private $passwordHasher;

    public function __construct(
        PasswordHasherHelper $passwordHasher,
        MapperHelper $mapperHelper,
        SeedRelationsMappingToValuesConverter $seedToEntityRelationsDefinitionsConverter,
        RelationsMappingGenerator $relationsMappingGenerator
    )
    {
        $this->passwordHasher = $passwordHasher;

        parent::__construct($mapperHelper, $seedToEntityRelationsDefinitionsConverter, $relationsMappingGenerator);
    }

    protected function getMapping(): array
    {
        return [
            AdminUserColumnNamesEnum::NAME => AdminUserSeedKeysEnum::NAME,
            AdminUserColumnNamesEnum::EMAIL => AdminUserSeedKeysEnum::EMAIL,
            AdminUserColumnNamesEnum::EMAIL_SIGNATURE => AdminUserSeedKeysEnum::EMAIL_SIGNATURE,
        ];
    }

    protected function getCalculatedValues(array $source): array
    {
        return [
            AdminUserColumnNamesEnum::PASSWORD => $this->getPasswordHash($source[AdminUserSeedKeysEnum::PASSWORD]),
            AdminUserColumnNamesEnum::REMEMBER_TOKEN => $this->generateRememberToken(),
            AdminUserColumnNamesEnum::SETTINGS => $this->getDefaultSettings(),
        ];
    }

    private function getDefaultSettings(): Collection
    {
        return new Collection(['locale' => 'ru']);
    }

    private function getPasswordHash(string $password): string
    {
        return $this->passwordHasher->hash($password);
    }

    private function generateRememberToken(): string
    {
        return Str::random(60);
    }

    protected function getRelationsMapping(): array
    {
        return [];
    }
}
