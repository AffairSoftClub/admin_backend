<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Seeds;

use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Seeds\AdminOrManagerSeedKeysEnum;

interface AdminUserSeedKeysEnum extends AdminOrManagerSeedKeysEnum
{

}
