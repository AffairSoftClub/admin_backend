<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Seeds;

use App\Domain\Base\Seeds\Entity\RepositoryBased\BySeedsRepositoryBasedSeeder;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\AdminRepository;

class AdminsSeeder extends BySeedsRepositoryBasedSeeder
{
    public function __construct(
        AdminRepository $repository,
        AdminSeedToModelAttributesConverter $seedToModelAttributesConverter
    )
    {
        parent::__construct($repository, $seedToModelAttributesConverter);
    }

    protected function getSeeds(): array
    {
        return AdminSeedsValues::VALUES;
    }
}
