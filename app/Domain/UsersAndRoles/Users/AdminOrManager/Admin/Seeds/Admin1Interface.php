<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Seeds;

interface Admin1Interface
{
    const NAME = 'Admin';
    const EMAIL = 'admin@admin.com';
    const PASSWORD = 'iOnA6^Q5vs&p';
    const REMEMBER_TOKEN = 'remember_token';
    const EMAIL_SIGNATURE = 'C уважением, команда "Euroasia Cargo"';
}
