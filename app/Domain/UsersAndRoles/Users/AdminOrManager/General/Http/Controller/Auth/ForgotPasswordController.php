<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\General\Http\Controller\Auth;

use App\Domain\Base\Http\Controller\BaseController;
use App\Domain\Base\Http\Controller\BaseNotDomainEntityController;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends BaseNotDomainEntityController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
}
