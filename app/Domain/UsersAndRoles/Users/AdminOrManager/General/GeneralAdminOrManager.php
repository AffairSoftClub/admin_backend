<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\General;

use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\BaseAdminOrManager;
use App\Domain\UsersAndRoles\Users\Base\BaseRoleScope;
use App\Domain\UsersAndRoles\Users\Base\BaseRolesScope;

class GeneralAdminOrManager extends BaseAdminOrManager
{
    protected function getDefaultRoleId(): ?int
    {
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        self::addRolesScope();
    }

    private static function addRolesScope()
    {
        $rolesScope = self::getRolesScope();

        if (!$rolesScope) {
            return;
        }

        static::addGlobalScope($rolesScope);
    }

    private static function getRolesScope(): ?BaseRolesScope
    {
        $rolesScopeClassName = static::getRolesScopeClassName();

        if (!$rolesScopeClassName) {
            return null;
        }

        return app($rolesScopeClassName);
    }

    protected static function getRolesScopeClassName(): ?string
    {
        return GeneralAdminOrManagerRolesScope::class;
    }


}
