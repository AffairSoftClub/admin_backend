<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\General;

use App\Domain\UsersAndRoles\Role\RoleRepository;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\BaseAdminOrManagerRepository;

class GeneralAdminOrManagerRepository extends BaseAdminOrManagerRepository
{
    public function __construct(GeneralAdminOrManager $model, RoleRepository $roleRepository)
    {
        parent::__construct($model, $roleRepository);
    }
}
