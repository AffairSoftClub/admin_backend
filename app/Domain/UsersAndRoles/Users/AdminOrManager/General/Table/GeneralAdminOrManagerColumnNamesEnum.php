<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\General\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;

interface GeneralAdminOrManagerColumnNamesEnum extends BaseUserColumnNamesEnum
{

}
