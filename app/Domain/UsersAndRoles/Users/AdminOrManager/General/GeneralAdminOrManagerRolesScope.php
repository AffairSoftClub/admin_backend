<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\General;

use App\Domain\UsersAndRoles\Role\Seeds\RoleNamesEnum;
use App\Domain\UsersAndRoles\Users\Base\BaseRolesScope;

class GeneralAdminOrManagerRolesScope extends BaseRolesScope
{
    public function __construct()
    {
        $rolesNames = [
            RoleNamesEnum::ADMIN,
            RoleNamesEnum::MANAGER,
        ];

        parent::__construct($rolesNames);
    }
}
