<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\BaseAdminOrManager;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\UserHavingEmailSignatureInterface;

class Manager extends BaseAdminOrManager implements UserHavingEmailSignatureInterface
{
    use ChildExtendableModelTrait;

    // Scopes
    protected static function getRoleScopeClassName(): ?string
    {
        return ManagerRoleScope::class;
    }

    // Extendable
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            AdminOrManagerOnlyColumnNamesEnum::EMAIL_SIGNATURE,
        ];
    }

    // Getters/Setters
    public function getEmailSignature(): ?string
    {
        return $this->getAttribute(AdminOrManagerOnlyColumnNamesEnum::EMAIL_SIGNATURE);
    }

    protected function getDefaultRoleId(): int
    {
        return $this->roleRepository->getManagerRoleIdOrFail();
    }

}
