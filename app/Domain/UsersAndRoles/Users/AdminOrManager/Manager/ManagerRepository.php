<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager;

use App\Base\Repository\Contracts\Basic\GetOrderedPaginationInterface;
use App\Domain\UsersAndRoles\Users\Base\BaseUserRepository;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Table\ManagerColumnNamesEnum;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class ManagerRepository extends BaseUserRepository implements GetOrderedPaginationInterface
{
    public function __construct(Manager $model)
    {
        parent::__construct($model);
    }

    public function getOrderedPagination(Request $request): LengthAwarePaginator
    {
        return $this->getModel()
            ->orderBy(ManagerColumnNamesEnum::NAME)
            ->paginate();
    }
}
