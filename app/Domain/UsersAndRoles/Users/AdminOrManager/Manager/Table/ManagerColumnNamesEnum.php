<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;

interface ManagerColumnNamesEnum extends BaseUserColumnNamesEnum, AdminOrManagerOnlyColumnNamesEnum
{

}
