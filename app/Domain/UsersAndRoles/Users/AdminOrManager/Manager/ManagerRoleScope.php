<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager;

use App\Domain\UsersAndRoles\Role\Seeds\RoleNamesEnum;
use App\Domain\UsersAndRoles\Users\Base\BaseRoleScope;

class ManagerRoleScope extends BaseRoleScope
{
    public function __construct()
    {
        $roleName = RoleNamesEnum::MANAGER;

        parent::__construct($roleName);
    }
}
