<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Services\ByRequestDataManagerService;

interface ManagerRequestKeysEnum
{
    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const EMAIL_SIGNATURE = 'email_signature';
    const PASSWORD = 'password';
}
