<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Services\ByRequestDataManagerService;

use App\Domain\Base\Http\Service\RequestDataConverter\ToEntityAttributes\BaseRequestDataToEntityAttributesConverter;
use App\Helpers\PasswordHasherHelper;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Table\ManagerColumnNamesEnum;
use App\Helpers\MapperHelper;

class ManagerRequestDataToAttributesConverter extends BaseRequestDataToEntityAttributesConverter
{
    /** @var \App\Helpers\PasswordHasherHelper */
    private $passwordHasher;

    public function __construct(
        PasswordHasherHelper $passwordHasher,
        MapperHelper         $mapperHelper
    )
    {
        $this->passwordHasher = $passwordHasher;
        parent::__construct($mapperHelper);
    }

    protected function getMapping(): array
    {
        return [
            ManagerColumnNamesEnum::ID => ManagerRequestKeysEnum::ID,
            ManagerColumnNamesEnum::NAME => ManagerRequestKeysEnum::NAME,
            ManagerColumnNamesEnum::EMAIL => ManagerRequestKeysEnum::EMAIL,
            ManagerColumnNamesEnum::EMAIL_SIGNATURE => ManagerRequestKeysEnum::EMAIL_SIGNATURE,
        ];
    }

    protected function getCalculatedValues(array $source): array
    {
        $ret = [];

        if (isset($source[ManagerRequestKeysEnum::PASSWORD])) {
            $password = $source[ManagerRequestKeysEnum::PASSWORD];
            $ret[ManagerColumnNamesEnum::PASSWORD] = $this->getPasswordHash($password);
        }

        return $ret;
    }

    private function getPasswordHash(string $password): string
    {
        return $this->passwordHasher->hash($password);
    }
}
