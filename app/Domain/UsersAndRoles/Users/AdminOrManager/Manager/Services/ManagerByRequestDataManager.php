<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Services;

use App\Domain\Base\Http\Service\ByRequestDataManager\BaseByRequestDataManagerWithEqualCreateUpdateAttributes;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\ManagerService;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Services\ByRequestDataManagerService\ManagerRequestDataToAttributesConverter;

class ManagerByRequestDataManager extends BaseByRequestDataManagerWithEqualCreateUpdateAttributes
{
    public function __construct(
        ManagerRequestDataToAttributesConverter $requestDataToManagerAttributesConverter,
        ManagerService $managerService
    )
    {
        parent::__construct($requestDataToManagerAttributesConverter, $managerService);
    }
}
