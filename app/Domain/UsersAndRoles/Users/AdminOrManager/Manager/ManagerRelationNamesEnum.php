<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager;

use App\Domain\UsersAndRoles\Users\Base\BaseUserRelationNamesEnum;

interface ManagerRelationNamesEnum extends BaseUserRelationNamesEnum, ManagerOnlyRelationNamesEnum
{

}
