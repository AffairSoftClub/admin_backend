<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager;

use App\Domain\Base\Model\BaseService;

/**
 * Class ManagerService
 * @package App\Domain\UsersAndRoles\Users\AdminOrManager\Manager
 *
 * @method Manager create(array $attributes)
 * @method Manager update(Manager $model, array $attributes)
 */
class ManagerService extends BaseService
{
    public function __construct(ManagerRepository $repository)
    {
        parent::__construct($repository);
    }

    public function deleteIfAllowed(Manager $manager)
    {
        $this->validateClientDeleteAllowed($manager);

        $this->deleteManager($manager);
    }

    private function validateClientDeleteAllowed(Manager $client)
    {
        return true;// todo: deny if manager has scheduled emails
    }

    private function deleteManager(Manager $client)
    {
        $this->repository->deleteByModel($client);
    }
}
