<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager;

use App\Domain\Base\Http\Controller\BaseStandardIndexDomainEntityController;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Services\ManagerByRequestDataManager;
use Exception;
use Illuminate\Http\Request;

class ManagerController extends BaseStandardIndexDomainEntityController
{
    /** @var ManagerByRequestDataManager */
    private $byRequestDataManagerService;

    public function __construct(
        ManagerByRequestDataManager $byRequestDataManagerService,
        ManagerService              $managerService
    )
    {
        $this->byRequestDataManagerService = $byRequestDataManagerService;

        parent::__construct($managerService);
    }

    protected function getEntityClassName(): string
    {
        return Manager::class;
    }

    // Entity > Index
    protected function getIndexViewName(): string
    {
        return 'directories.managers';
    }

    ///
    public function store(Request $request)
    {
        try {
            $requestData = $request->all();

            return $this->createManagerByRequestData($requestData);
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function createManagerByRequestData(array $requestData): Manager
    {
        return $this->byRequestDataManagerService->create($requestData);
    }

    public function update(Manager $manager, Request $request)
    {
        try {
            $requestData = $request->all();

            return $this->updateManagerByRequestData($manager, $requestData);
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function updateManagerByRequestData(Manager $manager, array $requestData): Manager
    {
        return $this->byRequestDataManagerService->update($manager, $requestData);
    }

    public function destroy(Manager $manager)
    {
        try {
            $this->deleteManagerIfAllowed($manager);

            return $manager->getKey();
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    public function deleteManagerIfAllowed(Manager $client)
    {
        return $this->entityService->deleteIfAllowed($client);
    }
}
