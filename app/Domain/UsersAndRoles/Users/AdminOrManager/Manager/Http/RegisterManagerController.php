<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Http;

use App\Domain\Base\Http\Controller\BaseNotDomainEntityController;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Manager;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Table\ManagerColumnNamesEnum;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterManagerController extends BaseNotDomainEntityController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return Manager
     */
    protected function create(array $data)
    {
        return Manager::create([
            ManagerColumnNamesEnum::NAME => $data['name'],
            ManagerColumnNamesEnum::EMAIL => $data['email'],
            ManagerColumnNamesEnum::PASSWORD => Hash::make($data['password']),
        ]);
    }
}
