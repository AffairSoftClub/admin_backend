<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds;

interface ManagerSeedsValues
{
    const VALUES = [
        [
            ManagerSeedKeysEnum::NAME => ManagerNamesEnum::MANAGER_1,
        ],
        [
            ManagerSeedKeysEnum::NAME => ManagerNamesEnum::MANAGER_2,
        ],
        [
            ManagerSeedKeysEnum::NAME => ManagerNamesEnum::MANAGER_3,
        ],
    ];
}
