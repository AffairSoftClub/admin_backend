<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds;

use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\ManagerRepository;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds\ManagerSeedsValues;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds\ManagerSeedToModelAttributesConverter;
use App\Domain\Base\Seeds\Entity\RepositoryBased\BySeedsRepositoryBasedSeeder;

class ManagersSeeder extends BySeedsRepositoryBasedSeeder
{
    public function __construct(
        ManagerRepository $repository,
        ManagerSeedToModelAttributesConverter $seedToEntityAttributesConverter)
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }

    protected function getSeeds(): array
    {
        return ManagerSeedsValues::VALUES;
    }
}
