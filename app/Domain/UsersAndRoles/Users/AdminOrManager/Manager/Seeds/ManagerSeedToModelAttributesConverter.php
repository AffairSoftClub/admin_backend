<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds;

use App\Domain\UsersAndRoles\Users\Base\Seeds\BaseUserSeedToModelAttributesConverter;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Table\ManagerColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;

class ManagerSeedToModelAttributesConverter extends BaseUserSeedToModelAttributesConverter
{
    protected function getRelationsMapping(): array
    {
        return [];
    }

    protected function getMapping(): array
    {
        return [
            ManagerColumnNamesEnum::NAME => ManagerSeedKeysEnum::NAME,
        ];
    }

    protected function getCalculatedValues(array $source): array
    {
        $parentCalculatedValues = parent::getCalculatedValues($source);
        $classOnlyCalculatedValues = $this->getClassOlyCalculatedValues($source);


        return $parentCalculatedValues + $classOnlyCalculatedValues;
    }

    private function getClassOlyCalculatedValues(array $seed): array
    {
        return [
            AdminOrManagerOnlyColumnNamesEnum::EMAIL_SIGNATURE => $this->getEmailSignatureBySeed($seed),
        ];
    }

    private function getEmailSignatureBySeed($seed)
    {
        $name = $this->getNameBySeed($seed);

        return "Best regards, {$name}.";
    }

    protected function getNameBySeed(array $seed): string
    {
        return $seed[ManagerSeedKeysEnum::NAME];
    }
}
