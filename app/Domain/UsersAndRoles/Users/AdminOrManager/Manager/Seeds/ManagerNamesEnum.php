<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds;

interface ManagerNamesEnum
{
    const MANAGER_1 = 'Manager 1';
    const MANAGER_2 = 'Manager 2';
    const MANAGER_3 = 'Manager 3';
}
