<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds;

use App\Domain\UsersAndRoles\Users\Base\Seeds\BaseUserSeedKeysEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Seeds\AdminOrManagerOnlySeedKeysEnum;

interface ManagerSeedKeysEnum extends BaseUserSeedKeysEnum, \App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Seeds\AdminOrManagerOnlySeedKeysEnum
{
}
