<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\All\Seeds;

use \App\Domain\Base\Seeds\BaseAggregateSeeder;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Seeds\AdminsSeeder;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\Seeds\ManagersSeeder;

class AdminOrManagerSeeder extends BaseAggregateSeeder
{
    public function run()
    {
        $this->call(AdminsSeeder::class);
        $this->call(ManagersSeeder::class);
    }
}
