<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base;

use App\Domain\Base\Model\BaseEntityMappedValuesProvider;
use App\Domain\UsersAndRoles\Users\AdminOrManager\General\GeneralAdminOrManagerRepository;

/**
 * Class SignatureByUserIdProvider
 * @package App\Domain\UsersAndRoles\Users\General
 *
 * @property GeneralAdminOrManagerRepository $internalProvider
 */
class SignatureByUserIdProvider extends BaseEntityMappedValuesProvider
{
    public function __construct(GeneralAdminOrManagerRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    protected function getCacheValues(): array
    {
        return $this->internalProvider->getSignaturesWithUserIdAsKeys();
    }

    public function getSignatureByUserId(int $userId): ?string
    {
        return $this->getByKeyOrFail($userId);
    }

    protected function getEntityName(): string
    {
        return 'User';
    }

    protected function getKeyAttributeName(): string
    {
        return 'id';
    }
}
