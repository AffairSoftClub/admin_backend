<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Base\BaseUser;

abstract class BaseAdminOrManager extends BaseUser
{
    use ChildExtendableModelTrait, GetEmailSignatureTrait;

    private function getClassOnlyFillableNotVisibleFromClass(): array
    {
        return [
            AdminOrManagerOnlyColumnNamesEnum::PASSWORD,
            AdminOrManagerOnlyColumnNamesEnum::REMEMBER_TOKEN,
        ];
    }
}
