<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base;

use App\Domain\Base\Model\BaseRepository;
use App\Domain\UsersAndRoles\Role\RoleRepository;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\BaseAdminOrManagerColumnNamesEnum;

class BaseAdminOrManagerRepository extends BaseRepository
{
    /** @var RoleRepository */
    private $roleRepository;

    public function __construct(
        BaseAdminOrManager $model,
        RoleRepository $roleRepository
    )
    {
        $this->roleRepository = $roleRepository;
        parent::__construct($model);
    }

    public function getSignaturesWithUserIdAsKeys(): array
    {
        $rolesIdsHavingUserSignatures = $this->getRoleIdsHavingUserSignatures();

        return $this->getModel()
            ->whereIn(BaseAdminOrManagerColumnNamesEnum::ROLE_ID, $rolesIdsHavingUserSignatures)
            ->pluck(BaseAdminOrManagerColumnNamesEnum::EMAIL_SIGNATURE, BaseAdminOrManagerColumnNamesEnum::ID)
            ->toArray();
    }

    private function getRoleIdsHavingUserSignatures(): array
    {
        return $this->roleRepository->getRoleIdsHavingUserEmailSignature();
    }

    public function getIdByEmail(string $email): int
    {
        return $this->getModel()
            ->where(BaseAdminOrManagerColumnNamesEnum::EMAIL, $email)
            ->value(BaseAdminOrManagerColumnNamesEnum::ID);
    }
}
