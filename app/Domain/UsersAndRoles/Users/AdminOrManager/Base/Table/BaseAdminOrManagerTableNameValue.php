<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserTableNameValue;

interface BaseAdminOrManagerTableNameValue extends BaseUserTableNameValue
{

}
