<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;

interface BaseAdminOrManagerColumnNamesEnum extends BaseUserColumnNamesEnum, AdminOrManagerOnlyColumnNamesEnum
{

}
