<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table;

interface AdminOrManagerOnlyColumnNamesEnum
{
    const PASSWORD = 'password';
    const REMEMBER_TOKEN = 'remember_token';
    const EMAIL_SIGNATURE = 'email_signature';
}
