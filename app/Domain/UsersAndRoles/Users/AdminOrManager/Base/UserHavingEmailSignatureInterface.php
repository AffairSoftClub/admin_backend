<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base;

interface UserHavingEmailSignatureInterface
{
    public function getEmailSignature(): ?string;
}
