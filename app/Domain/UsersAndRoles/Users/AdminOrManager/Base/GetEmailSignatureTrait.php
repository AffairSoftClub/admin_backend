<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base;

use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;

trait GetEmailSignatureTrait
{
    public function getEmailSignature(): ?string
    {
        return $this->getAttribute(AdminOrManagerOnlyColumnNamesEnum::EMAIL_SIGNATURE);
    }
}
