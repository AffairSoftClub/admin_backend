<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Seeds;

use App\Domain\UsersAndRoles\Users\Base\Seeds\BaseUserSeedKeysEnum;

interface AdminOrManagerSeedKeysEnum extends BaseUserSeedKeysEnum, AdminOrManagerOnlySeedKeysEnum
{

}
