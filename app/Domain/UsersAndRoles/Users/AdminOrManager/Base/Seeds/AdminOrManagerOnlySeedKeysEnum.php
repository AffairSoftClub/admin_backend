<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Seeds;

use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;

interface AdminOrManagerOnlySeedKeysEnum
{
    const PASSWORD = 'password';
    const EMAIL_SIGNATURE = AdminOrManagerOnlyColumnNamesEnum::EMAIL_SIGNATURE;
}
