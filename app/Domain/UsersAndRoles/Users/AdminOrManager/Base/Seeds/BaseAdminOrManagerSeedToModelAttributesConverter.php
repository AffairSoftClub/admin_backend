<?php

namespace App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Seeds;

use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Base\Seeds\BaseUserSeedToModelAttributesConverter;

abstract class BaseAdminOrManagerSeedToModelAttributesConverter extends BaseUserSeedToModelAttributesConverter
{
    protected function getCalculatedValues(array $source): array
    {
        $parentValues = parent::getCalculatedValues($source);
        $classOnlyValues = $this->getClassOnlyCalculatedValues($source);

        return $parentValues + $classOnlyValues;
    }

    private function getClassOnlyCalculatedValues(array $source): array
    {
        $email = $this->getEmailBySeed($source);

        return [
            AdminOrManagerOnlyColumnNamesEnum::PASSWORD => $this->generatePassword($email),
            AdminOrManagerOnlyColumnNamesEnum::REMEMBER_TOKEN => $this->generateRememberToken(),
        ];
    }
}
