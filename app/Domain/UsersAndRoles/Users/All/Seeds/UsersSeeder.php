<?php

namespace App\Domain\UsersAndRoles\Users\All\Seeds;

use App\Domain\Base\Seeds\BaseAggregateSeeder;
use App\Domain\UsersAndRoles\Users\AdminOrManager\All\Seeds\AdminOrManagerSeeder;
use App\Domain\UsersAndRoles\Users\Employee\Seeds\EmployeesSeeder;

class UsersSeeder extends BaseAggregateSeeder
{
    public function run()
    {
        $this->call(AdminOrManagerSeeder::class);
        $this->call(EmployeesSeeder::class);
    }
}
