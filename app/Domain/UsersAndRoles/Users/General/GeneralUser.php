<?php

namespace App\Domain\UsersAndRoles\Users\General;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\AdminOnlyRelationNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Admin\Table\AdminOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\GetEmailSignatureTrait;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\UserHavingEmailSignatureInterface;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Manager\ManagerOnlyRelationNamesEnum;
use App\Domain\UsersAndRoles\Users\Base\BaseUser;
use App\Domain\UsersAndRoles\Users\Employee\EmployeeOnlyRelationNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\DriverOnlyRelationNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table\DriverOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\WorkerOnlyRelationNamesEnum;
use App\Helpers\ReflectionHelper;

class GeneralUser extends BaseUser implements UserHavingEmailSignatureInterface
{
    use ChildExtendableModelTrait, GetEmailSignatureTrait;

    /** @var ReflectionHelper */
    private $reflectionHelper;

    public function __construct(array $attributes = [])
    {
        $this->reflectionHelper = app(ReflectionHelper::class);

        parent::__construct($attributes);
    }

    protected function getDefaultRoleId(): ?int
    {
        return null;
    }

    // Extendable
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        $adminFillable = $this->getAdminOnlyFillableAndVisible();
        $managerFillable = $this->getAdminOrManagerOnlyFillableAndVisible();
        $employeeFillable = $this->getEmployeeOnlyFillableAndVisible();
        $workerFillable = $this->getWorkerOnlyFillableAndVisible();
        $driverFillable = $this->getDriverOnlyFillableAndVisible();

        return array_merge(
            $adminFillable,
            $managerFillable,
            $employeeFillable,
            $workerFillable,
            $driverFillable
        );
    }

    private function getAdminOnlyFillableAndVisible(): array
    {
        return $this->getClassConstants(AdminOnlyColumnNamesEnum::class);
    }

    private function getClassConstants(string $className): array
    {
        return $this->reflectionHelper->getClassConstants($className);
    }

    private function getAdminOrManagerOnlyFillableAndVisible(): array
    {
        return $this->getClassConstants(AdminOrManagerOnlyColumnNamesEnum::class);
    }

    private function getEmployeeOnlyFillableAndVisible(): array
    {
        return $this->getClassConstants(EmployeeOnlyColumnNamesEnum::class);
    }

    private function getWorkerOnlyFillableAndVisible(): array
    {
        return $this->getClassConstants(WorkerOnlyColumnNamesEnum::class);
    }

    private function getDriverOnlyFillableAndVisible(): array
    {
        return $this->getClassConstants(DriverOnlyColumnNamesEnum::class);
    }

    private function getClassOnlyRelationNamesFromClass(): array
    {
        $adminRelations = $this->getAdminOnlyRelationNames();
        $managerRelations = $this->getManagerOnlyRelationNames();
        $employeeRelations = $this->getEmployeeOnlyRelationNames();
        $workerRelations = $this->getWorkerOnlyRelationNames();
        $driverRelations = $this->getDriverOnlyRelationNames();

        return array_merge(
            $adminRelations,
            $managerRelations,
            $employeeRelations,
            $workerRelations,
            $driverRelations
        );
    }

    private function getAdminOnlyRelationNames(): array
    {
        return $this->getClassConstants(AdminOnlyRelationNamesEnum::class);
    }

    private function getManagerOnlyRelationNames(): array
    {
        return $this->getClassConstants(ManagerOnlyRelationNamesEnum::class);
    }

    private function getEmployeeOnlyRelationNames(): array
    {
        return $this->getClassConstants(EmployeeOnlyRelationNamesEnum::class);
    }

    private function getWorkerOnlyRelationNames(): array
    {
        return $this->getClassConstants(WorkerOnlyRelationNamesEnum::class);
    }

    private function getDriverOnlyRelationNames(): array
    {
        return $this->getClassConstants(DriverOnlyRelationNamesEnum::class);
    }
}
