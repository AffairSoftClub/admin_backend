<?php

namespace App\Domain\UsersAndRoles\Users\General\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserTableNameValue as CommonUserTableNameInterface;

interface GeneralUserTableNameValue extends CommonUserTableNameInterface
{

}
