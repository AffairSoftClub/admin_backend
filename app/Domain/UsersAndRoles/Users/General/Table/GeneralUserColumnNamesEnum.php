<?php

namespace App\Domain\UsersAndRoles\Users\General\Table;

use App\Domain\UsersAndRoles\Users\Base\Table\BaseUserColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Table\EmployeeOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Driver\Table\DriverOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\Employee\Types\Worker\Table\WorkerOnlyColumnNamesEnum;
use App\Domain\UsersAndRoles\Users\AdminOrManager\Base\Table\AdminOrManagerOnlyColumnNamesEnum;

interface GeneralUserColumnNamesEnum extends
    BaseUserColumnNamesEnum,
    AdminOrManagerOnlyColumnNamesEnum,
    EmployeeOnlyColumnNamesEnum,
    WorkerOnlyColumnNamesEnum,
    DriverOnlyColumnNamesEnum
{

}
