<?php

namespace App\Domain\UsersAndRoles\Role;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Domain\UsersAndRoles\Role\Seeds\RoleNamesEnum as RoleNameEnum;
use App\Domain\UsersAndRoles\Role\Table\RoleColumnNamesEnum as RoleColumnNameEnum;

//use TCG\Voyager\Models\Role;

class RoleRepository extends BaseRepository
{
    private $roleNameToModelMap = [];

    use GetAllOrderedByNameTrait;

    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    public function getAdminRoleIdOrFail(): int
    {
        $adminRole = $this->getAdminRoleOrFail();

        return $adminRole->getKey();
    }

    public function getAdminRoleOrFail(): Role
    {
        return $this->getByNameOrFail(RoleNameEnum::ADMIN);
    }

    private function getByNameOrFail(string $roleName): Role
    {
        if (!array_key_exists($roleName, $this->roleNameToModelMap)) {
            $role = $this->model
                ->where(RoleColumnNameEnum::NAME, $roleName)
                ->firstOrFail();

            $this->roleNameToModelMap[$roleName] = $role;
        }

        return $this->roleNameToModelMap[$roleName];
    }

    public function getManagerRoleIdOrFail(): int
    {
        $adminRole = $this->getManagerRoleOrFail();

        return $adminRole->getKey();
    }

    public function getManagerRoleOrFail(): Role
    {
        return $this->getByNameOrFail(RoleNameEnum::MANAGER);
    }

    public function getDriverRoleIdOrFail(): int
    {
        $driverRole = $this->getDriverRoleOrFail();

        return $driverRole->getKey();
    }

    public function getDriverRoleOrFail(): Role
    {
        return $this->getByNameOrFail(RoleNameEnum::DRIVER);
    }

    public function getWorkerRoleIdOrFail(): int
    {
        $driverRole = $this->getWorkerRoleOrFail();

        return $driverRole->getKey();
    }

    public function getWorkerRoleOrFail(): Role
    {
        return $this->getByNameOrFail(RoleNameEnum::WORKER);
    }

    protected function getNameColumnName(): string
    {
        return RoleColumnNameEnum::DISPLAY_NAME;
    }

    public function getRoleIdsHavingUserEmailSignature(): array
    {
        $roleNamesHavingUserEmailSignature = [
            RoleNameEnum::ADMIN,
            RoleNameEnum::MANAGER,
        ];

        return $this->getRoleIdsByNames($roleNamesHavingUserEmailSignature);
    }

    private function getRoleIdsByNames(array $roleNames): array
    {
        return $this->getModel()
            ->whereIn(RoleColumnNameEnum::NAME, $roleNames)
            ->pluck(RoleColumnNameEnum::ID)
            ->toArray();
    }
}
