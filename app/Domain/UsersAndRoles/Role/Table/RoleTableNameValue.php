<?php

namespace App\Domain\UsersAndRoles\Role\Table;

interface RoleTableNameValue
{
    const VALUE = 'roles';
}
