<?php

namespace App\Domain\UsersAndRoles\Role\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface RoleColumnNamesEnum extends IdTimestampsInterface
{
    const NAME = 'name';
    const DISPLAY_NAME = 'display_name';
}
