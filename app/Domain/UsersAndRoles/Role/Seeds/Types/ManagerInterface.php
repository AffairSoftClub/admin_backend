<?php

namespace App\Domain\UsersAndRoles\Role\Seeds\Types;
/**
 * Interface ManagerInterface
 * @package App\Domain\UsersAndRoles\Role\Seeds\Types
 *
 * На данный момент менеджеры могут пользоваться сайтом. Каждый имеет свою подпись письма.
 */
interface ManagerInterface
{
    const NAME = 'manager';
    const DISPLAY_NAME = 'Manager';
}
