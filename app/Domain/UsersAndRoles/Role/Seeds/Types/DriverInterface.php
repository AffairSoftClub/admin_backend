<?php

namespace App\Domain\UsersAndRoles\Role\Seeds\Types;

interface DriverInterface
{
    const NAME = 'driver';
    const DISPLAY_NAME = 'Driver';
}
