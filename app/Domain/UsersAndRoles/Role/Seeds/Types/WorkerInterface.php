<?php

namespace App\Domain\UsersAndRoles\Role\Seeds\Types;

interface WorkerInterface
{
    const NAME = 'worker';
    const DISPLAY_NAME = 'Worker';
}
