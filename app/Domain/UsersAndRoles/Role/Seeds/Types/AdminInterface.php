<?php

namespace App\Domain\UsersAndRoles\Role\Seeds\Types;

interface AdminInterface
{
    const NAME = 'admin';
    const DISPLAY_NAME = 'Administrator';
}
