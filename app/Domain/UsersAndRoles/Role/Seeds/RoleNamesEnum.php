<?php

namespace App\Domain\UsersAndRoles\Role\Seeds;

use App\Domain\UsersAndRoles\Role\Seeds\Types\AdminInterface;
use App\Domain\UsersAndRoles\Role\Seeds\Types\DriverInterface;
use App\Domain\UsersAndRoles\Role\Seeds\Types\ManagerInterface;
use App\Domain\UsersAndRoles\Role\Seeds\Types\WorkerInterface;

interface RoleNamesEnum
{
    const ADMIN = AdminInterface::NAME;
    const MANAGER = ManagerInterface::NAME;
    const WORKER = WorkerInterface::NAME;
    const DRIVER = DriverInterface::NAME;
}
