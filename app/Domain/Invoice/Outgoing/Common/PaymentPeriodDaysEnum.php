<?php

namespace App\Domain\Invoice\Outgoing\Common;

interface PaymentPeriodDaysEnum
{
    // 0, 7, 15, 30, 45, 60, 90
    const P0 = 0;
    const P7 = 7;
    const P15 = 15;
    const P30 = 30;
    const P45 = 45;
    const P60 = 60;
    const P90 = 90;
}
