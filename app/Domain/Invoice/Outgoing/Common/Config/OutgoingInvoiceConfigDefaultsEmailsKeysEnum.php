<?php

namespace App\Domain\Invoice\Outgoing\Common\Config;

interface OutgoingInvoiceConfigDefaultsEmailsKeysEnum
{
    const EMAILS_SUBJECT_TEMPLATE = 'emails_subject_template';
    const ADD_SIGNATURE = 'add_signature';
    const FRONT_ATTACHMENTS = 'front_attachments';
}
