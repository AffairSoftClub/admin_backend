<?php

namespace App\Domain\Invoice\Outgoing\Common\Config;

interface OutgoingInvoiceConfigDefaultsEmailsAttachmentKeysEnum
{
    const ID = 'id';
    const TITLE = 'title';
    const URL = 'url';
}
