<?php

namespace App\Domain\Invoice\Outgoing\Common\Config;

use App\Domain\Invoice\Outgoing\Base\Http\RequestKeys\BaseOutgoingInvoiceAggregateRequestKeysEnum;
use App\Domain\Invoice\Outgoing\Common\Config\OutgoingInvoiceConfig;

class OutgoingInvoiceDefaultsConfig extends OutgoingInvoiceConfig
{
    public function getUseVat(): ?bool
    {
        return $this->getConfigValueByKey(BaseOutgoingInvoiceAggregateRequestKeysEnum::USE_VAT);
    }

    public function getPaymentPeriodDays(): ?int
    {
        return $this->getConfigValueByKey(BaseOutgoingInvoiceAggregateRequestKeysEnum::PAYMENT_PERIOD_DAYS);
    }

    public function getCurrencyId(): ?int
    {
        return $this->getConfigValueByKey(BaseOutgoingInvoiceAggregateRequestKeysEnum::CURRENCY_ID);
    }

    public function getExchangeRate(): ?float
    {
        return $this->getConfigValueByKey(BaseOutgoingInvoiceAggregateRequestKeysEnum::EXCHANGE_RATE);
    }

    public function isClientEmailNotificationsEnabled(): ?float
    {
        return $this->getConfigValueByKey(BaseOutgoingInvoiceAggregateRequestKeysEnum::IS_CLIENT_EMAIL_NOTIFICATIONS_ENABLED);
    }

    public function getEmailsConfig(): ?array
    {
        return $this->getConfigValueByKey(BaseOutgoingInvoiceAggregateRequestKeysEnum::EMAILS_CONFIG);
    }

    protected function getDomainConfigPath(): string
    {
        $parentPath = parent::getDomainConfigPath();
        $classOnlyPath = $this->getClassOnlyDomainConfigPath();

        return "{$parentPath}.{$classOnlyPath}";
    }

    private function getClassOnlyDomainConfigPath(): string
    {
        return 'defaults';
    }
}
