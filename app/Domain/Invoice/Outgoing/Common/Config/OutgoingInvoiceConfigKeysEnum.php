<?php

namespace App\Domain\Invoice\Outgoing\Common\Config;

interface OutgoingInvoiceConfigKeysEnum
{
    const VAT_PERCENTS = 'vat_percents';
    const PAYMENT_PERIODS_DAYS = 'payment_periods_days';
    const DEFAULTS = 'defaults';
}
