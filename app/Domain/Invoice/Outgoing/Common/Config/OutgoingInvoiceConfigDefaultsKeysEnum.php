<?php

namespace App\Domain\Invoice\Outgoing\Common\Config;

interface OutgoingInvoiceConfigDefaultsKeysEnum
{
    const USE_VAT = 'use_vat';
    const PAYMENT_PERIOD_DAYS = 'payment_period_days';
    const CURRENCY_ID = 'currency_id';
    const EXCHANGE_RATE = 'exchange_rate';
    const IS_CLIENT_EMAIL_NOTIFICATIONS_ENABLED = 'is_client_email_notifications_enabled';
    const EMAILS_CONFIG = 'emails_config';

}
