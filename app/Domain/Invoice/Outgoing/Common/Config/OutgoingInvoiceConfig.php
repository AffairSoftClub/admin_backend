<?php

namespace App\Domain\Invoice\Outgoing\Common\Config;

use App\Base\BaseConfig;
use App\Domain\Company\OutgoingInvoicesParamsNamesEnum;
use App\Domain\Base\BaseDomainConfig;

class OutgoingInvoiceConfig extends BaseDomainConfig
{
    public function getVatPercents(): int
    {
        return $this->getConfigValueByKey(OutgoingInvoiceConfigKeysEnum::VAT_PERCENTS);
    }

    public function getPaymentPeriodsDays(): array
    {
        return $this->getConfigValueByKey(OutgoingInvoiceConfigKeysEnum::PAYMENT_PERIODS_DAYS);
    }

    public function getDefaults(): array
    {
        return $this->getConfigValueByKey(OutgoingInvoiceConfigKeysEnum::DEFAULTS);
    }

    protected function getDomainConfigPath(): string
    {
        return 'outgoing-invoice';
    }
}
