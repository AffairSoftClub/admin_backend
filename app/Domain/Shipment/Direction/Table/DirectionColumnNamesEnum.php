<?php

namespace App\Domain\Shipment\Direction\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface DirectionColumnNamesEnum extends IdTimestampsInterface
{
    const CODE = 'code';
    const DESCRIPTION = 'description';
    const SORT = 'sort';
    const STATUS = 'status';
}
