<?php

namespace App\Domain\Shipment\Direction\Table;

interface DirectionTableNameValue
{
    const VALUE = 'directions';
}
