<?php

namespace App\Domain\Shipment\Direction\Seeds;

use App\Domain\Shipment\Direction\DirectionRepository;
use App\Domain\Shipment\Direction\Seeds\DirectionsInterface as DirectionsSeedsInterface;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class DirectionsSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(DirectionRepository $directionRepository)
    {
        parent::__construct($directionRepository);
    }

    protected function getSeeds(): array
    {
        return DirectionsSeedsInterface::VALUES;
    }
}
