<?php

namespace App\Domain\Shipment\Direction\Seeds;

use App\Domain\Shipment\Direction\Table\DirectionColumnNamesEnum as ColumnNameEnum;

interface DirectionsInterface
{
    const VALUES = [
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::AMS,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::MST,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::NUE,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::FRA,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::OSL,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::LIL,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::LHR,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::LHU,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::HHN,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::FRA_K_AND_N,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::BRU,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::LGG,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::CDG,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::CGN,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::WORMS,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::NIEDER_OLM,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::DUISBG,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::FMO,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::BUNDE,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::GRABEN,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::MUC,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::BRE,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::HAM,
        ],
        [
            ColumnNameEnum::CODE => DirectionCodesEnum::HAJ,
        ],
    ];
}
