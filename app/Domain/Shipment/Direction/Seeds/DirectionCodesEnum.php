<?php

namespace App\Domain\Shipment\Direction\Seeds;

interface DirectionCodesEnum
{
    const AMS = 'AMS';
    const MST = 'MST';
    const NUE = 'NUE';
    const FRA = 'FRA';
    const OSL = 'OSL';
    const LIL = 'LIL';
    const LHR = 'LHR';
    const LHU = 'LHU';
    const HHN = 'HHN';
    const FRA_K_AND_N = 'FRA K&N';
    const BRU = 'BRU';
    const LGG = 'LGG';
    const CDG = 'CDG';
    const CGN = 'CGN';

    // For Shipments seeding by Excel file
    const WORMS = 'Worms';
    const NIEDER_OLM = 'Nieder Olm';
    const DUISBG = 'Duisbg.';
    const FMO = 'FMO';
    const BUNDE = 'Bünde';
    const GRABEN = 'Graben';
    const MUC = 'MUC';
    const BRE = 'BRE';
    const HAM = 'HAM';
    const HAJ = 'HAJ';
}
