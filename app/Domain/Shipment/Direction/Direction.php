<?php

namespace App\Domain\Shipment\Direction;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Shipment\Direction\Table\DirectionColumnNamesEnum;
use App\Domain\Shipment\Direction\Table\DirectionTableNameValue as TableNameValue;

class Direction extends BaseModel
{
    protected $table = TableNameValue::VALUE;

    protected $fillable = [
        DirectionColumnNamesEnum::CODE,
        DirectionColumnNamesEnum::DESCRIPTION,
        DirectionColumnNamesEnum::STATUS,
        DirectionColumnNamesEnum::SORT,
    ];
}
