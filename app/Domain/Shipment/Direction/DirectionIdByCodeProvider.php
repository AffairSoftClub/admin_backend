<?php

namespace App\Domain\Shipment\Direction;

use App\Base\Provider\BaseByProviderCachedValuesProvider;
use \App\Domain\Base\Model\BaseEntityMappedValuesProvider;

/**
 * @property DirectionRepository $internalProvider
 */
class DirectionIdByCodeProvider extends BaseEntityMappedValuesProvider
{
    public function __construct(DirectionRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    protected function getCacheValues(): array
    {
        return $this->internalProvider->getCodeToIdMapping();
    }

    public function getIdByCodeOrFail(string $dicrectionCode): int
    {
        return $this->getByKeyOrFail($dicrectionCode);
    }

    protected function getEntityName(): string
    {
        return 'Direction';
    }

    protected function getKeyAttributeName(): string
    {
        return 'code';
    }
}
