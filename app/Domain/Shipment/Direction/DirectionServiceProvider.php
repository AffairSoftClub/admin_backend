<?php

namespace App\Domain\Shipment\Direction;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class DirectionServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return DirectionRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return null;
    }
}
