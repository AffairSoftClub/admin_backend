<?php

namespace App\Domain\Shipment\Direction;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Base\GetManyByColumnValuesTrait;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Base\Repository\Traits\Advanced\Name\GetOrCreateByNameColumnTrait;
use App\Domain\Shipment\Direction\Table\DirectionColumnNamesEnum;
use Illuminate\Database\Eloquent\Collection;
use App\Base\GetAsMappingInterface;
use App\Base\Repository\Traits\Advanced\Base\GetAsMappingTrait;

/**
 * Class DirectionRepository
 * @method create(array $attributes) : Direction
 * @method getOrCreateByNameColumn(string $name): Direction
 */
class DirectionRepository extends BaseRepository implements GetAsMappingInterface
{
    use GetManyByColumnValuesTrait, GetAllOrderedByNameTrait, GetOrCreateByNameColumnTrait, GetAsMappingTrait;

    public function __construct(Direction $model)
    {
        parent::__construct($model);
    }

    public function getOrCreateByCode(string $directionCode): Direction
    {
        return $this->getOrCreateByNameColumn($directionCode);
    }

    public function getManyByCodeValues(array $codeValues): Collection
    {
        return $this->getManyByColumnValues(DirectionColumnNamesEnum::CODE, $codeValues);
    }

    protected function getNameColumnName(): string
    {
        return DirectionColumnNamesEnum::CODE;
    }

    public function getCodeToIdMapping(): array
    {
        return $this->getAsMapping(DirectionColumnNamesEnum::CODE, DirectionColumnNamesEnum::ID);
    }
}
