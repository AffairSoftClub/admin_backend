<?php

namespace App\Domain\Directories\Vehicle\Common;

use App\Domain\Cashbook\CashbookItem\Types\Base\BaseCashbookItemRelationsNamesEnum;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait CashbookItemsWithRelationsTrait
{
    public function cashbookItemsWithRelations(): HasMany
    {
        return $this->cashbookItems()->with([
            BaseCashbookItemRelationsNamesEnum::EXPENSE,

            BaseCashbookItemRelationsNamesEnum::CLIENT,
            BaseCashbookItemRelationsNamesEnum::DRIVER_ID,

            BaseCashbookItemRelationsNamesEnum::CAR,
            BaseCashbookItemRelationsNamesEnum::TRAILER,

            BaseCashbookItemRelationsNamesEnum::DRIVER_SALARY_PAYOUT,
            BaseCashbookItemRelationsNamesEnum::WORKER_SALARY_PAYOUT,
        ]);
    }
}
