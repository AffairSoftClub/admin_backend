<?php

namespace App\Domain\Directories\Vehicle\Common;

interface CashbookItemsWithRelationsRelationNameEnumItem
{
    const CASHBOOK_ITEMS_WITH_RELATIONS = 'cashbookItemsWithRelations';
}
