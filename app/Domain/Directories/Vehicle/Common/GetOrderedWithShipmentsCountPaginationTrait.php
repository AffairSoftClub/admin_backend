<?php

namespace App\Domain\Directories\Vehicle\Common;

use App\Base\Repository\Traits\GetModelTrait;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

trait GetOrderedWithShipmentsCountPaginationTrait
{
    use GetModelTrait;

    public function getOrderedWithShipmentsCountPagination(Request $request = null): LengthAwarePaginator
    {
        $frontRelationNames = $this->getPaginationRelationNames();

        $paginator = $this->getModel()
            ->with($frontRelationNames)
            ->withCount(ShipmentsRelationNameEnumItem::SHIPMENTS)
            ->orderBy(BaseVehicleColumnNamesEnum::NUMBER)
            ->paginate();

        $paginator->makeVisible('shipments_count');

        return $paginator;
    }

    protected function getPaginationRelationNames(): array
    {
        return [];
    }
}
