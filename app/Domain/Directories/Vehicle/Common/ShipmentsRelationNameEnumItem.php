<?php

namespace App\Domain\Directories\Vehicle\Common;

interface ShipmentsRelationNameEnumItem
{
    const SHIPMENTS = 'shipments';
}
