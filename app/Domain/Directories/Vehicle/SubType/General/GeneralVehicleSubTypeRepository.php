<?php

namespace App\Domain\Directories\Vehicle\SubType\General;

use App\Domain\Directories\Vehicle\Children\Car\CarSubType\CarSubTypeRepository;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\TrailerSubTypeRepository;

class GeneralVehicleSubTypeRepository
{
    /** @var CarSubTypeRepository */
    private $carSubTypeRepository;

    /** @var TrailerSubTypeRepository */
    private $trailerSubTypeRepository;

    public function __construct(
        CarSubTypeRepository     $carSubTypeRepository,
        TrailerSubTypeRepository $trailerSubTypeRepository
    )
    {
        $this->carSubTypeRepository = $carSubTypeRepository;
        $this->trailerSubTypeRepository = $trailerSubTypeRepository;
    }

    public function getAll(): array
    {
        $nestedRepositories = $this->getNestedRepositories();

        return $this->getAllByRepositories($nestedRepositories);
    }

    private function getNestedRepositories(): array
    {
        return [
            $this->carSubTypeRepository,
            $this->trailerSubTypeRepository,
        ];
    }

    private function getAllByRepositories(array $repositories): array
    {
        $ret = [];

        foreach ($repositories as $repository) {
            $repositoryValues = $repository->getAll();
            $ret = array_merge($ret, $repositoryValues);
        }

        return $ret;
    }
}
