<?php

namespace App\Domain\Directories\Vehicle\SubType\Base;

use App\Domain\Base\Model\ByColumnScoped\ByColumnScope;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;

class BaseVehicleSubTypeScope extends ByColumnScope
{
    public function __construct($columnValue)
    {
        parent::__construct($columnValue, BaseVehicleColumnNamesEnum::SUB_TYPE_ID);
    }
}
