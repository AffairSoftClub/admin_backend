<?php

namespace App\Domain\Directories\Vehicle\SubType\Base;

class BaseVehicleSubType
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $typeId;

    public function __construct(int $id, string $name, int $typeId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->typeId = $typeId;
    }
}
