<?php

namespace App\Domain\Directories\Vehicle\Type\Seeds;

class CarVehicleTypeSeed
{
    const ID = 1;
    const NAME = "Car";
}
