<?php

namespace App\Domain\Directories\Vehicle\Type\Seeds;

class TrailerVehicleTypeSeed
{
    const ID = 2;
    const NAME = "Trailer";
}
