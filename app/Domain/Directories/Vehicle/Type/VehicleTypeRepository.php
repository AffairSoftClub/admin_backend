<?php

namespace App\Domain\Directories\Vehicle\Type;

use App\Domain\Directories\Vehicle\Type\Instances\CarVehicleType;
use App\Domain\Directories\Vehicle\Type\Instances\TrailerVehicleType;

class VehicleTypeRepository
{
    public function getAll(): array
    {
        return [
            new CarVehicleType(),
            new TrailerVehicleType(),
        ];
    }
}
