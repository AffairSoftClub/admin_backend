<?php

namespace App\Domain\Directories\Vehicle\Type\Instances;

use App\Domain\Directories\Vehicle\Type\VehicleType;
use \App\Domain\Directories\Vehicle\Type\Seeds\CarVehicleTypeSeed;

class CarVehicleType extends VehicleType
{
    public function __construct()
    {
        parent::__construct(CarVehicleTypeSeed::ID, CarVehicleTypeSeed::NAME);
    }
}
