<?php

namespace App\Domain\Directories\Vehicle\Type\Instances;

use App\Domain\Directories\Vehicle\Type\Seeds\TrailerVehicleTypeSeed;
use App\Domain\Directories\Vehicle\Type\VehicleType;

class TrailerVehicleType extends VehicleType
{
    public function __construct()
    {
        parent::__construct(TrailerVehicleTypeSeed::ID, TrailerVehicleTypeSeed::NAME);
    }
}
