<?php

namespace App\Domain\Directories\Vehicle\Children\All;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;
use App\Domain\Directories\Vehicle\Children\All\AllVehicleSelectWithGroupsDataProvider;

class AllVehicleServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return null;
    }

    protected function getServiceClassName(): ?string
    {
        return AllVehicleSelectWithGroupsDataProvider::class;
    }
}
