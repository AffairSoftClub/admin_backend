<?php

namespace App\Domain\Directories\Vehicle\Children\All;

use App\Base\Control\SelectWithGroups\Groups\ThreeGroupsSelectDataProvider;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarTypeSelectGroupProvider;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarTypeSelectGroupProvider;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerSelectGroupProvider;

class AllVehicleSelectWithGroupsDataProvider extends ThreeGroupsSelectDataProvider
{
    public function __construct(
        OfficeCarTypeSelectGroupProvider $groupsProvider1,
        ParkCarTypeSelectGroupProvider $groupsProvider2,
        TrailerSelectGroupProvider $groupsProvider3
    )
    {
        parent::__construct($groupsProvider1, $groupsProvider2, $groupsProvider3);
    }
}
