<?php

namespace App\Domain\Directories\Vehicle\Children\General;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;

class GeneralVehicleRepository extends BaseVehicleRepository
{
    public function __construct(GeneralVehicle $model)
    {
        parent::__construct($model);
    }
}
