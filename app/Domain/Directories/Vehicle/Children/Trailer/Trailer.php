<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\Cashbook\CashbookItem\Types\Base\BaseCashbookItem;
use App\Domain\Cashbook\CashbookItem\Types\Base\Table\BaseCashbookItemColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicle;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\Instances\One\OneTrailerSubTypeSeed;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\TrailerSubTypeScope;
use App\Domain\Directories\Vehicle\Common\CashbookItemsWithRelationsTrait;
use App\Domain\Directories\Vehicle\Type\Seeds\TrailerVehicleTypeSeed;
use App\Domain\Shipment\Shipment;
use App\Domain\Shipment\ShipmentRelationNamesEnum;
use App\Domain\Shipment\Table\ShipmentColumnNamesEnum;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Trailer extends BaseVehicle
{
    use CashbookItemsWithRelationsTrait, ChildExtendableModelTrait;

    const VEHICLE_TYPE_ID = TrailerVehicleTypeSeed::ID;
    const VEHICLE_SUB_TYPE_ID = OneTrailerSubTypeSeed::ID;

    // VEHICLE TYPE SCOPE
    static protected function getTypeScopeClassName(): ?string
    {
        return TrailerVehicleTypeScope::class;
    }

    static protected function getSubTypeScopeClassName(): ?string
    {
        return TrailerSubTypeScope::class;
    }

    // RELATIONS
    public function car(): HasOne
    {
        return $this->hasOne(
            ParkCar::class,
            ParkCarOnlyColumnNamesEnum::TRAILER_ID,
            TrailerColumnNamesEnum::ID
        );
    }

    public function shipments(): HasMany
    {
        return $this->hasMany(
            Shipment::class,
            ShipmentColumnNamesEnum::TRAILER_ID,
            TrailerColumnNamesEnum::ID
        );
    }

    /**
     * @return mixed| \App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar[]
     */
    public function getCar()
    {
        return $this->getRelationValue(TrailerRelationNamesEnum::CAR);
    }

    public function cashbookItems(): HasMany
    {
        return $this->hasMany(
            BaseCashbookItem::class,
            BaseCashbookItemColumnNamesEnum::TRAILER_ID,
            BaseCarColumnNamesEnum::ID
        );
    }

    public function getFilteredShipmentsParkCarsWithRelationsPagination(array $filter)
    {
        return $this->shipments()
            ->orderByDesc(ShipmentColumnNamesEnum::DEPARTURE_DATETIME)
            ->filter($filter)
            ->with([
                ShipmentRelationNamesEnum::CAR,
                ShipmentRelationNamesEnum::TRAILER,
                ShipmentRelationNamesEnum::DRIVER1,
                ShipmentRelationNamesEnum::DRIVER2,
                ShipmentRelationNamesEnum::CLIENT,
                ShipmentRelationNamesEnum::LOAD_DIRECTIONS,
                ShipmentRelationNamesEnum::UNLOAD_DIRECTIONS,
            ])
            ->paginateFilter();
    }
}
