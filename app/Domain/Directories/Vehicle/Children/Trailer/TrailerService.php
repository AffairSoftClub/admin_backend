<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicle;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleService;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRepository;
use Illuminate\Validation\ValidationException;

class TrailerService extends BaseVehicleService
{
    public function __construct(TrailerRepository $repository)
    {
        parent::__construct($repository);
    }

    protected function validateDeleteAllowed(BaseVehicle $vehicle)
    {
        $this->validateNotBelongsToCar($vehicle);
    }

    /**
     * @param \App\Domain\Directories\Vehicle\Children\Base\BaseVehicle|Trailer $trailer
     * @throws ValidationException
     */
    private function validateNotBelongsToCar(BaseVehicle $trailer)
    {
        $car = $trailer->getCar();

        if (!$car) {
            return;
        }

        $carNumber = $car->getNumber();
        $carId = $car->getKey();

        $this->throwValidationException([
            'trailer_id' => "Прицеп используется в парковом автомобиле ({$carId} - {$carNumber}).",
        ]);
    }
}
