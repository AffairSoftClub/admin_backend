<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRepository;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerService;

class TrailerServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return TrailerRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return TrailerService::class;
    }
}
