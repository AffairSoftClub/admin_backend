<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table\TrailerLinkingToParkCarLogColumnNamesEnum as ColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table\TrailerLinkingToParkCarLogTableNameValue as TableNameValue;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;
use App\Domain\UsersAndRoles\Users\AdminOrManager\General\GeneralAdminOrManager;
use App\Domain\UsersAndRoles\Users\AdminOrManager\General\Table\GeneralAdminOrManagerColumnNamesEnum;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TrailerLinkingToParkCarLog extends BaseModel
{
    protected $table = TableNameValue::VALUE;

    protected $fillable = [
        ColumnNamesEnum::TIMESTAMP,
        ColumnNamesEnum::PARK_CAR_ID,
        ColumnNamesEnum::OLD_TRAILER_ID,
        ColumnNamesEnum::NEW_TRAILER_ID,
        ColumnNamesEnum::USER_ID,
    ];

    public $timestamps = false;

    // relations
    public function parkCar(): BelongsTo
    {
        return $this->belongsTo(
            ParkCar::class,
            ColumnNamesEnum::PARK_CAR_ID,
            ParkCarColumnNamesEnum::ID
        );
    }

    public function oldTrailer(): BelongsTo
    {
        return $this->belongsTo(
            Trailer::class,
            ColumnNamesEnum::OLD_TRAILER_ID,
            TrailerColumnNamesEnum::ID
        );
    }

    public function newTrailer(): BelongsTo
    {
        return $this->belongsTo(
            Trailer::class,
            ColumnNamesEnum::NEW_TRAILER_ID,
            TrailerColumnNamesEnum::ID
        );
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(
            GeneralAdminOrManager::class,
            ColumnNamesEnum::USER_ID,
            GeneralAdminOrManagerColumnNamesEnum::ID
        );
    }
}
