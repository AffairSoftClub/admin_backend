<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog;

use App\Domain\Base\Model\BaseService;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\TrailerLinkingToParkCarLogRepository;

class TrailerLinkingToParkCarLogService extends BaseService
{
    public function __construct(TrailerLinkingToParkCarLogRepository $repository)
    {
        parent::__construct($repository);
    }
}
