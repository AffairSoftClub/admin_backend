<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table;

interface TrailerLinkingToParkCarLogTableNameValue
{
    const VALUE = 'trailer_linking_to_park_car_logs';
}
