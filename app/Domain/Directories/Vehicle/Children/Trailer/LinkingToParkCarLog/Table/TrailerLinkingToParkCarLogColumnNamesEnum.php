<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table;

use App\Domain\Base\Table\Columns\Basic\IdInterface;

interface TrailerLinkingToParkCarLogColumnNamesEnum extends IdInterface
{
    const TIMESTAMP = 'timestamp';
    const PARK_CAR_ID = 'park_car_id';
    const OLD_TRAILER_ID = 'old_trailer_id';
    const NEW_TRAILER_ID = 'new_trailer_id';
    const USER_ID = 'user_id';
}
