<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog;

use App\Domain\Base\Model\BaseRepository;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table\TrailerLinkingToParkCarLogColumnNamesEnum as ColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\TrailerLinkingToParkCarLog;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class TrailerLinkingToParkCarLogRepository extends BaseRepository
{
    public function __construct(TrailerLinkingToParkCarLog $model)
    {
        parent::__construct($model);
    }

    public function getPaginationByOldAndNewTrailerId(int $trailerId): LengthAwarePaginator
    {
        return $this->getModel()
            ->where(ColumnNamesEnum::OLD_TRAILER_ID, $trailerId)
            ->orWhere(ColumnNamesEnum::NEW_TRAILER_ID, $trailerId)
            ->orderByDesc(ColumnNamesEnum::TIMESTAMP)
            ->paginate();
    }
}
