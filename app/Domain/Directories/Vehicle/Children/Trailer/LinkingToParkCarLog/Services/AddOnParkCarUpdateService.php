<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Services;

use App\Domain\BaseMixed\User\GetCurrentUserIdTrait;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Table\TrailerLinkingToParkCarLogColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\TrailerLinkingToParkCarLog;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\TrailerLinkingToParkCarLogService;

class AddOnParkCarUpdateService
{
    use GetCurrentUserIdTrait;

    /** @var TrailerLinkingToParkCarLogService */
    private $entityService;

    public function __construct(TrailerLinkingToParkCarLogService $trailerLinkingToParkCarLogService)
    {
        $this->entityService = $trailerLinkingToParkCarLogService;
    }

    public function addIfRequired(ParkCar $parkCar, ?int $oldTrailerId)
    {
        $newTrailerId = $parkCar->getTrailerId();

        if ($oldTrailerId === $newTrailerId) {
            return;
        }

        $this->addLogByParams($parkCar, $oldTrailerId);
    }

    private function addLogByParams(ParkCar $parkCar, ?int $oldTrailerId)
    {
        $logAttributes = $this->getLogAttributesByParams($parkCar, $oldTrailerId);

        $this->addLog($logAttributes);
    }

    private function getLogAttributesByParams(ParkCar $parkCar, ?int $oldTrailerId): array
    {
        return [
            TrailerLinkingToParkCarLogColumnNamesEnum::TIMESTAMP => $parkCar->getUpdateAt(),
            TrailerLinkingToParkCarLogColumnNamesEnum::PARK_CAR_ID => $parkCar->getKey(),
            TrailerLinkingToParkCarLogColumnNamesEnum::OLD_TRAILER_ID => $oldTrailerId,
            TrailerLinkingToParkCarLogColumnNamesEnum::NEW_TRAILER_ID => $parkCar->getTrailerId(),
            TrailerLinkingToParkCarLogColumnNamesEnum::USER_ID => $this->getCurrentUserIdOrFail(),
        ];
    }

    private function addLog(array $logAttributes): TrailerLinkingToParkCarLog
    {
        return $this->entityService->create($logAttributes);
    }
}
