<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarWorkStatusesEnum;

interface TrailerWorkStatusesEnumBase extends BaseCarWorkStatusesEnum
{

}
