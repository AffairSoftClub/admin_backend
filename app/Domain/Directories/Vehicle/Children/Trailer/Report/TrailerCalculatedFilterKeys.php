<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Report;

use App\Domain\Directories\Vehicle\Children\Base\Report\BaseVehicleReportFilterKeys;

interface TrailerCalculatedFilterKeys extends BaseVehicleReportFilterKeys
{
    const CAR_ID = 'car_id';
}
