<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType;

use App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\Instances\One\OneTrailerSubTypeSeed;
use App\Domain\Directories\Vehicle\SubType\Base\BaseVehicleSubTypeScope;

class TrailerSubTypeScope extends BaseVehicleSubTypeScope
{
    public function __construct()
    {
        parent::__construct(OneTrailerSubTypeSeed::ID);
    }
}
