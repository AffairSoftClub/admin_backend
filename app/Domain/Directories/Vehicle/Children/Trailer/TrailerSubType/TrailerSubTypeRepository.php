<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType;

use App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\Instances\One\OneTrailerSubType;

class TrailerSubTypeRepository
{
    public function getAll(): array
    {
        return [
            new OneTrailerSubType(),
        ];
    }
}
