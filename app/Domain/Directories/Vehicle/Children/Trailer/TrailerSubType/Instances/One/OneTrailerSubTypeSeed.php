<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\Instances\One;

class OneTrailerSubTypeSeed
{
    const ID = '21';
    const NAME = "Прицепы";
}
