<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\Instances\One;

use App\Domain\Directories\Vehicle\Children\Trailer\TrailerSubType\TrailerSubType;
use App\Domain\Directories\Vehicle\Type\Seeds\TrailerVehicleTypeSeed;

class OneTrailerSubType extends TrailerSubType
{
    public function __construct()
    {
        parent::__construct(OneTrailerSubTypeSeed::ID, OneTrailerSubTypeSeed::NAME, TrailerVehicleTypeSeed::ID);
    }

}
