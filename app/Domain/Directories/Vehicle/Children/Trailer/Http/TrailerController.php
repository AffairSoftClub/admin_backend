<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleController;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarRepository;
use App\Domain\Directories\Vehicle\Children\Trailer\Http\TrailerByRequestDataService;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRelationNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TrailerController extends \App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleController
{
    /** @var ParkCarRepository */
    private $parkCarRepository;

    public function __construct(
        ParkCarRepository $parkCarService,
        TrailerByRequestDataService $byRequestDataVehicleService,
        TrailerService $vehicleService
    )
    {
        $this->parkCarRepository = $parkCarService;

        parent::__construct($byRequestDataVehicleService, $vehicleService);
    }

    protected function getEntityClassName(): string
    {
        return Trailer::class;
    }

    protected function getIndexViewName(): string
    {
        return 'directories.vehicles.trailers';
    }

    protected function getIndexReferences(): array
    {
        return [
            'parkCars' => $this->getParkCars(),
        ];
    }

    private function getParkCars(): Collection
    {
        return $this->parkCarRepository->getAllOrderedByName();
    }

    ///
    public function update(Trailer $trailer, Request $request)
    {
        return $this->updateByEntity($trailer, $request);
    }

    public function destroy(Trailer $trailer)
    {
        return $this->destroyByEntity($trailer);
    }

    protected function getFrontRelations(): array
    {
        $parentFrontRelations = parent::getFrontRelations();
        $classOnlyFrontRelations = $this->getClassOnlyFrontRelations();

        return array_merge($parentFrontRelations, $classOnlyFrontRelations);
    }

    private function getClassOnlyFrontRelations(): array
    {
        return [
            TrailerRelationNamesEnum::CAR,
        ];
    }

}
