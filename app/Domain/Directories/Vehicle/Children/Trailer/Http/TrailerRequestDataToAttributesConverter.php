<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestDataToAttributesConverter;

class TrailerRequestDataToAttributesConverter extends \App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestDataToAttributesConverter
{
}
