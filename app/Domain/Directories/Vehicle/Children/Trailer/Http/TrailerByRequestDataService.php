<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleByRequestDataWithEqualCreateUpdateAttributes;
use App\Domain\Directories\Vehicle\Children\Trailer\Http\TrailerRequestDataToAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerService;

class TrailerByRequestDataService extends BaseVehicleByRequestDataWithEqualCreateUpdateAttributes
{
    public function __construct(
        TrailerRequestDataToAttributesConverter $requestDataToEntityAttributesConverter,
        TrailerService $entityService
    )
    {
        parent::__construct($requestDataToEntityAttributesConverter, $entityService);
    }
}
