<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestKeysEnum;

interface TrailerRequestKeysEnum extends BaseVehicleRequestKeysEnum
{

}
