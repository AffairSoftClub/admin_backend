<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleSelectGroupProvider;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRepository;

class TrailerSelectGroupProvider extends BaseVehicleSelectGroupProvider
{
    public function __construct(TrailerRepository $vehicleRepository)
    {
        parent::__construct($vehicleRepository);
    }

    protected function getGroupLabel(): string
    {
        return 'Прицепы';
    }

    protected function getGroupId(): int
    {
        return Trailer::VEHICLE_SUB_TYPE_ID;
    }
}
