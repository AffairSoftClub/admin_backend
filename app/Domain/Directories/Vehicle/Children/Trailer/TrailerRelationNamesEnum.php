<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Directories\Vehicle\Common\ShipmentsRelationNameEnumItem;

interface TrailerRelationNamesEnum extends ShipmentsRelationNameEnumItem
{
    const CAR = 'car';
}
