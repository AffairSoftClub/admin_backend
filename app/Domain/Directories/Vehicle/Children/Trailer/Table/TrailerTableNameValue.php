<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Table;

use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleTableNameValue;

interface TrailerTableNameValue extends BaseVehicleTableNameValue
{
}
