<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Table;

use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerOnlyColumnNamesEnum;

interface TrailerColumnNamesEnum extends \App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum, TrailerOnlyColumnNamesEnum
{
}
