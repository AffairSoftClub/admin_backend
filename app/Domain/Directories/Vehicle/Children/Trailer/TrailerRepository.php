<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Base\Repository\Contracts\Basic\GetOrderedPaginationInterface;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;
use App\Domain\Directories\Vehicle\Common\GetOrderedWithShipmentsCountPaginationTrait;

class TrailerRepository extends BaseVehicleRepository implements GetOrderedPaginationInterface
{
    use GetOrderedWithShipmentsCountPaginationTrait;

    public function __construct(Trailer $model)
    {
        parent::__construct($model);
    }

    protected function getPaginationRelationNames(): array
    {
        return [
            TrailerRelationNamesEnum::CAR,
        ];
    }
}
