<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleTypeScope;

class TrailerVehicleTypeScope extends BaseVehicleTypeScope
{
    public function __construct()
    {
        parent::__construct(Trailer::VEHICLE_TYPE_ID);
    }
}
