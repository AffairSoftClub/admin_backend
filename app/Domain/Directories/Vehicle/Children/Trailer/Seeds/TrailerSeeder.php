<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Seeds;

use App\Domain\Directories\Vehicle\Children\Trailer\Seeds\TrailerSeedsValues as TrailersSeedsInterface;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRepository;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class TrailerSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(TrailerRepository $trailerRepository)
    {
        parent::__construct($trailerRepository);
    }

    protected function getSeeds(): array
    {
        return TrailersSeedsInterface::VALUES;
    }
}
