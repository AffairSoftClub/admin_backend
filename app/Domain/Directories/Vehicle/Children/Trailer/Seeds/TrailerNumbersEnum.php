<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Seeds;

interface TrailerNumbersEnum
{
    const N_2SY2502 = '2SY2502';
    const N_4AY3218 = '4AY3218';
    const N_4AY4079 = '4AY4079';
    const N_4AY4595 = '4AY4595';
    const N_4AY8517 = '4AY8517';
    const N_4AY8541 = '4AY8541'; // 2
    const N_4AY8542 = '4AY8542';
    const N_4SC9784 = '4SC9784';
    const N_4SH6871 = '4SH6871';
    const N_4SH6885 = '4SH6885';
    const N_5AJ1325 = '5AJ1325';
    const N_5AJ5800 = '5AJ5800'; // 3
    const N_5AJ7318 = '5AJ7318';
    const N_5AJ8746 = '5AJ8746';
    const N_5AJ8829 = '5AJ8829';
    const N_5AJ8830 = '5AJ8830';
    const N_5AJ9313 = '5AJ9313';
    const N_5AJ9314 = '5AJ9314';
    const N_5AJ9877 = '5AJ9877';
    const N_5AK0161 = '5AK0161';
    const N_5AK1232 = '5AK1232';
    const N_5AK1688 = '5AK1688';
    const N_5AK2465 = '5AK2465';
    const N_5AK2509 = '5AK2509';
    const N_5AK2516 = '5AK2516';
    const N_5AK2517 = '5AK2517';
    const N_5AK2519 = '5AK2519';
    const N_5AK3144 = '5AK3144';
    const N_5AK3145 = '5AK3145';
    const N_5AK3146 = '5AK3146';
    const N_5AK3170 = '5AK3170';
//    const N_6AY7501 = '6AY7501';
//    const N_7AN2041 = '7AN2041';
}
