<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer\Seeds;

use App\Domain\Directories\Vehicle\Children\Trailer\Seeds\TrailerNumbersEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerColumnNamesEnum as ColumnNameEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerWorkStatusesEnumBase;

interface TrailerSeedsValues
{
    const VALUES = [
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4AY8542,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4AY8541,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ5800,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4SC9784,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4SH6885,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4SH6871,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4AY4595,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4AY4079,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ1325,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4AY8517,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ7318,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
//        [
//            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_6AY7501,
//            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
//        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ8829,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ8830,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ9313,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ8746,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ9314,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK2517,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AJ9877,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK0161,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
//        [
//            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_7AN2041,
//            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
//        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK1232,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK1688,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK2465,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_2SY2502,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK3144,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK3145,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK3146,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK3170,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK2509,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK2516,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_5AK2519,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
        [
            ColumnNameEnum::NUMBER => TrailerNumbersEnum::N_4AY3218,
            ColumnNameEnum::WORK_STATUS_ID => TrailerWorkStatusesEnumBase::GOOD,
        ],
    ];
}
