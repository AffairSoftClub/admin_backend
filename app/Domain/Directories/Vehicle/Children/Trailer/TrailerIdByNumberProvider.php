<?php

namespace App\Domain\Directories\Vehicle\Children\Trailer;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleNumberToIdProvider;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRepository;

class TrailerIdByNumberProvider extends BaseVehicleNumberToIdProvider
{
    public function __construct(TrailerRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    protected function getEntityName(): string
    {
        return 'Trailer';
    }
}
