<?php

namespace App\Domain\Directories\Vehicle\Children\Base;

use App\Domain\Base\Model\BaseEntityMappedValuesProvider;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;

/**
 * @property BaseVehicleRepository $internalProvider
 */
abstract class BaseVehicleNumberToIdProvider extends BaseEntityMappedValuesProvider
{
    public function __construct(BaseVehicleRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    public function getIdByNumberOrFail(string $number): int
    {
        return $this->getByKeyOrFail($number);
    }

    protected function getKeyAttributeName(): string
    {
        return 'number';
    }

    protected function getCacheValues(): array
    {
        return $this->internalProvider->getNumberToIdMap();
    }
}
