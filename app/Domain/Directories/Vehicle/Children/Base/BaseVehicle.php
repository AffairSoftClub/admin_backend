<?php

namespace App\Domain\Directories\Vehicle\Children\Base;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Base\Model\ExtendableModel\Complex\RootExtendableModelTrait;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleTableNameValue;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarOnlyColumnNamesEnum;
use Illuminate\Support\Carbon;

abstract class BaseVehicle extends BaseModel
{
    use RootExtendableModelTrait;

    const VEHICLE_TYPE_ID = null;
    const VEHICLE_SUB_TYPE_ID = null;

    protected $table = BaseVehicleTableNameValue::VALUE;

//    public function __construct(array $attributes = [])
//    {
//        $this->initModelProperties();
//        parent::__construct($attributes);
//    }

    // VEHICLE TYPE SCOPE
    protected static function boot()
    {
        parent::boot();
        static::addVehicleTypeScope();
    }

    private static function addVehicleTypeScope()
    {
        $vehicleTypeScope = static::getVehicleTypeScope();

        if (!$vehicleTypeScope) {
            return;
        }

        static::addGlobalScope($vehicleTypeScope);
    }

    private static function getVehicleTypeScope(): ?BaseVehicleTypeScope
    {
        $vehicleTypeScopeClassName = static::getTypeScopeClassName();

        if (!$vehicleTypeScopeClassName) {
            return null;
        }

        return new $vehicleTypeScopeClassName;
    }

    static protected function getTypeScopeClassName(): ?string
    {
        return null;
    }

    static protected function getSubTypeScopeClassName(): ?string
    {
        return null;
    }


    // EXTENDABLE MODEL
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            BaseVehicleColumnNamesEnum::NUMBER,
            BaseVehicleColumnNamesEnum::WORK_STATUS_ID,
        ];
    }

    private function getClassOnlyVisibleNotFillableFromClass(): array
    {
        return [
            BaseVehicleColumnNamesEnum::TYPE_ID,
            BaseVehicleColumnNamesEnum::SUB_TYPE_ID,
        ];
    }

    private function getClassOnlyDefaultAttributesFromClass(): array
    {
        return [
            BaseVehicleColumnNamesEnum::TYPE_ID => $this->getDefaultVehicleTypeId(),
            BaseVehicleColumnNamesEnum::SUB_TYPE_ID => $this->getDefaultVehicleSubTypeId(),
        ];
    }

    // GETTERS/SETTERS

    // Type
    protected function getDefaultVehicleTypeId(): ?int
    {
        return static::VEHICLE_TYPE_ID;
    }

    // SubType
    protected function getDefaultVehicleSubTypeId(): ?int
    {
        return static::VEHICLE_SUB_TYPE_ID;
    }


    public function getUpdateAt(): Carbon
    {
        $updateAtColumn = $this->getUpdatedAtColumn();

        return $this->getAttribute($updateAtColumn);
    }

    public function getNumber(): string
    {
        return $this->getAttribute(BaseVehicleColumnNamesEnum::NUMBER);
    }

}
