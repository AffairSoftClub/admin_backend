<?php

namespace App\Domain\Directories\Vehicle\Children\Base;

use App\Base\GetAsMappingInterface;
use App\Base\Repository\Contracts\Basic\GetOrderedPaginationInterface;
use App\Base\Repository\Traits\Advanced\Base\GetAsMappingTrait;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Base\Repository\Traits\Advanced\Name\GetOrderedByNamePaginationTrait;
use App\Domain\Base\Model\BaseRepository;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

abstract class BaseVehicleRepository extends BaseRepository implements GetOrderedPaginationInterface, GetAsMappingInterface
{
    use GetAllOrderedByNameTrait, GetAsMappingTrait, GetOrderedByNamePaginationTrait;

    public function __construct(BaseVehicle $model)
    {
        parent::__construct($model);
    }

    protected function getNameColumnName(): string
    {
        return BaseVehicleColumnNamesEnum::NUMBER;
    }

    public function getNumberToIdMap(): array
    {
        return $this->getAsMapping(BaseVehicleColumnNamesEnum::NUMBER, BaseVehicleColumnNamesEnum::ID);
    }

    public function getOrderedPagination(Request $request): LengthAwarePaginator
    {
        return $this->getOrderedByNamePagination($request);
    }
}
