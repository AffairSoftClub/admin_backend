<?php

namespace App\Domain\Directories\Vehicle\Children\Base;

use App\Domain\Base\Model\TypedModel\BaseTypeIdScope;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;

class BaseVehicleTypeScope extends BaseTypeIdScope
{
    public function __construct($columnValue)
    {
        parent::__construct($columnValue, BaseVehicleColumnNamesEnum::TYPE_ID);
    }
}
