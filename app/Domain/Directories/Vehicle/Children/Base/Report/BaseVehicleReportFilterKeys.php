<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Report;

use App\Domain\Base\Http\Filter\PeriodFilterKeyEnumItem;

interface BaseVehicleReportFilterKeys extends PeriodFilterKeyEnumItem
{
}
