<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Report;

use App\Base\TypeCaster\TypeCasterInterface;
use App\Domain\Base\Http\Controller\BaseIndexFilteredEntityController;
use App\Domain\Base\Http\Controller\Entity\ShowEntityControllerTrait;
use App\Domain\Base\Model\BaseModel;
use App\Domain\Cashbook\CashbookItem\Types\Base\BaseCashbookItemRepository;
use App\Domain\Cashbook\CashbookItem\Types\Base\Services\CashbookItemSummaryService;
use App\Domain\Directories\Client\Client;
use App\Domain\Directories\Client\ClientRepository;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicle;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;
use App\Domain\Directories\Vehicle\Common\CashbookItemsWithRelationsRelationNameEnumItem;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

abstract class BaseVehicleReportController extends BaseIndexFilteredEntityController
{
    use ShowEntityControllerTrait;

    /** @var \App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository */
    protected $entityService;

    /** @var ClientRepository */
    protected $clientRepository;

    /** @var BaseCashbookItemRepository */
    protected $cashBookItemRepository;

    /** @var CashbookItemSummaryService */
    protected $cashbookItemSummaryService;

    public function __construct(
        BaseVehicleRepository $vehicleService,
        ClientRepository $clientRepository,
        TypeCasterInterface $typeCaster,
        BaseCashbookItemRepository $cashBookItemRepository
    )
    {
        $this->entityService = $vehicleService;
        $this->clientRepository = $clientRepository;
        $this->cashBookItemRepository = $cashBookItemRepository;

        parent::__construct($typeCaster, $vehicleService);
    }

    protected function getIndexPagination(Request $request): LengthAwarePaginator
    {
        return $this->getEntityService()->getOrderedWithShipmentsCountPagination($request);
    }

    protected function getShowRoutes(): array
    {
        return [];
    }

    /**
     * @param BaseModel|BaseVehicle $model
     * @param array $filter
     */
    protected function loadFilteredRelations(BaseModel $model, array $filter)
    {
        $model->load([
            CashbookItemsWithRelationsRelationNameEnumItem::CASHBOOK_ITEMS_WITH_RELATIONS => function (HasMany $query) use ($filter) {
                $query->filter($filter);
            },
        ])
            ->makeVisible(CashbookItemsWithRelationsRelationNameEnumItem::CASHBOOK_ITEMS_WITH_RELATIONS);
    }

    protected function getShowReferences(): array
    {
        return [
            'clients' => $this->getClientsOrderedByName(),
        ];
    }

    /**
     * @return Collection|Client[]
     */
    private function getClientsOrderedByName(): Collection
    {
        return $this->clientRepository->getAllOrderedByName();
    }
}
