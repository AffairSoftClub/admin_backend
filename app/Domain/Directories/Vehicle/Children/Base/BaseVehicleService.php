<?php

namespace App\Domain\Directories\Vehicle\Children\Base;

use App\Domain\Base\Model\BaseService;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicle;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;

abstract class BaseVehicleService extends BaseService
{
    public function __construct(BaseVehicleRepository $repository)
    {
        parent::__construct($repository);
    }

    public function deleteIfAllowed(BaseVehicle $vehicle)
    {
        $this->validateDeleteAllowed($vehicle);

        $this->delete($vehicle);
    }

    abstract protected function validateDeleteAllowed(BaseVehicle $vehicle);

    private function delete(BaseVehicle $baseCar)
    {
        $this->repository->deleteByModel($baseCar);
    }
}
