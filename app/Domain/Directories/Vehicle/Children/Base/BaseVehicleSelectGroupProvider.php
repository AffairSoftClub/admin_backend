<?php

namespace App\Domain\Directories\Vehicle\Children\Base;

use App\Base\Control\SelectWithGroups\Group\BaseSelectGroupProviderWithDefaultKeys;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;

abstract class BaseVehicleSelectGroupProvider extends BaseSelectGroupProviderWithDefaultKeys
{
    /** @var BaseVehicleRepository */
    private $vehicleRepository;

    public function __construct(BaseVehicleRepository $vehicleRepository)
    {
        $this->vehicleRepository = $vehicleRepository;
    }

    protected function getGroupValues(): iterable
    {
        return $this->vehicleRepository->getAllOrderedByName();
    }
}
