<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Http;

use App\Domain\Base\Http\Service\RequestDataConverter\ToEntityAttributes\BaseRequestDataToEntityAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestKeysEnum;

abstract class BaseVehicleRequestDataToAttributesConverter extends BaseRequestDataToEntityAttributesConverter
{
    protected function getMapping(): array
    {
        return [
            BaseVehicleColumnNamesEnum::NUMBER => BaseVehicleRequestKeysEnum::NUMBER,
            BaseVehicleColumnNamesEnum::WORK_STATUS_ID => BaseVehicleRequestKeysEnum::WORK_STATUS_ID,
        ];
    }

    protected function getCalculatedValues(array $source): array
    {
        return [];
    }
}
