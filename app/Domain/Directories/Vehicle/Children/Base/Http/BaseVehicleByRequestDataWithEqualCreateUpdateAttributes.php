<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Http;

use App\Domain\Base\Http\Service\ByRequestDataManager\BaseByRequestDataManagerWithEqualCreateUpdateAttributes;
use App\Domain\Base\Http\Service\RequestDataConverter\ToEntityAttributes\RequestDataToEntityAttributesConverterInterface;
use App\Domain\Base\Model\ServiceInterface;

class BaseVehicleByRequestDataWithEqualCreateUpdateAttributes extends BaseByRequestDataManagerWithEqualCreateUpdateAttributes
{
    public function __construct(
        RequestDataToEntityAttributesConverterInterface $requestDataToEntityAttributesConverter,
        ServiceInterface $entityService
    )
    {
        parent::__construct($requestDataToEntityAttributesConverter, $entityService);
    }
}
