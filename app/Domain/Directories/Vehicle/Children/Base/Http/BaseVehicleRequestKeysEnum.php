<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Http;

use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;

interface BaseVehicleRequestKeysEnum
{
    const NUMBER = BaseVehicleColumnNamesEnum::NUMBER;
    const WORK_STATUS_ID = BaseVehicleColumnNamesEnum::WORK_STATUS_ID;
}
