<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Http;

use App\Domain\Base\Http\Controller\BaseStandardIndexDomainEntityController;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicle;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleService;
use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleByRequestDataWithEqualCreateUpdateAttributes;
use Exception;
use Illuminate\Http\Request;

abstract class BaseVehicleController extends BaseStandardIndexDomainEntityController
{
    /** @var BaseVehicleByRequestDataWithEqualCreateUpdateAttributes */
    protected $entityByRequestDataService;

    public function __construct(
        BaseVehicleByRequestDataWithEqualCreateUpdateAttributes $byRequestDataVehicleService,
        BaseVehicleService $vehicleService
    )
    {
        $this->entityByRequestDataService = $byRequestDataVehicleService;

        parent::__construct($vehicleService);
    }

    // Entity controller > Index

    public function store(Request $request)
    {
        try {
            $requestData = $request->all();

            $car = $this->createEntityByRequestData($requestData);

            $this->loadRelations($car);

            return $car;
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function createEntityByRequestData(array $requestData): BaseVehicle
    {
        return $this->entityByRequestDataService->create($requestData);
    }

    private function loadRelations(BaseVehicle $vehicle)
    {
        $frontRelations = $this->getFrontRelations();

        $vehicle->load($frontRelations);
    }

    protected function getFrontRelations(): array
    {
        return [];
    }

    protected function updateByEntity(BaseVehicle $car, Request $request)
    {
        try {
            $requestData = $request->all();

            $car = $this->updateEntityByRequestData($car, $requestData);
            $this->loadRelations($car);

            return $car;
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    protected function updateEntityByRequestData(BaseVehicle $manager, array $requestData): BaseVehicle
    {
        return $this->entityByRequestDataService->update($manager, $requestData);
    }

    protected function destroyByEntity(BaseVehicle $vehicle)
    {
        try {
            $this->deleteEntityIfAllowed($vehicle);

            return $vehicle->getKey();
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    protected function deleteEntityIfAllowed(BaseVehicle $car)
    {
        $this->entityService->deleteIfAllowed($car);
    }
}
