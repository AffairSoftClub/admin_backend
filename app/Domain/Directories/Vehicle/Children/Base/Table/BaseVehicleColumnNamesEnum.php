<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;
use \App\Domain\Base\Model\TypedModel\TypedModelColumnNamesInterface;

interface BaseVehicleColumnNamesEnum extends IdTimestampsInterface, TypedModelColumnNamesInterface
{
    const NUMBER = 'number';
    const WORK_STATUS_ID = 'work_status_id';

    const SUB_TYPE_ID = 'sub_type_id';
}
