<?php

namespace App\Domain\Directories\Vehicle\Children\Base\Table;

interface BaseVehicleTableNameValue
{
    const VALUE = 'vehicles';
}
