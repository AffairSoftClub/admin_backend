<?php

namespace App\Domain\Directories\Vehicle\Children\Car\All;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;
use App\Domain\Directories\Vehicle\Children\Car\All\AllTypesCarSelectControlWithGroupsDataProvider;

class AllCarVehicleServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return null;
    }

    protected function getServiceClassName(): ?string
    {
        return AllTypesCarSelectControlWithGroupsDataProvider::class;
    }
}
