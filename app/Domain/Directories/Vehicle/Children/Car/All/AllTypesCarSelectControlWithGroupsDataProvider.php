<?php

namespace App\Domain\Directories\Vehicle\Children\Car\All;

use App\Base\Control\SelectWithGroups\Groups\TwoGroupsSelectDataProvider;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarTypeSelectGroupProvider;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarTypeSelectGroupProvider;

class AllTypesCarSelectControlWithGroupsDataProvider extends TwoGroupsSelectDataProvider
{
    public function __construct(
        OfficeCarTypeSelectGroupProvider $officeCarTypeGroupOptionProvider,
        ParkCarTypeSelectGroupProvider $parkCarTypeGroupOptionProvider
    )
    {
        parent::__construct(
            $officeCarTypeGroupOptionProvider,
            $parkCarTypeGroupOptionProvider
        );
    }
}
