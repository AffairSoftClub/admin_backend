<?php

namespace App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Office;

class OfficeCarSubTypeSeed
{
    const ID = 12;
    const NAME = "Офисные автомобили";
}
