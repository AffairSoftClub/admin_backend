<?php

namespace App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Office;

use App\Domain\Directories\Vehicle\Children\Car\CarSubType\CarSubType;

class OfficeCarSubType extends CarSubType
{
    public function __construct()
    {
        parent::__construct(OfficeCarSubTypeSeed::ID, OfficeCarSubTypeSeed::NAME);
    }
}
