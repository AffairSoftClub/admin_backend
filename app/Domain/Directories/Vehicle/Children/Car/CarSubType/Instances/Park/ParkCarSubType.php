<?php

namespace App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Park;

use App\Domain\Directories\Vehicle\Children\Car\CarSubType\CarSubType;

class ParkCarSubType extends CarSubType
{
    public function __construct()
    {
        parent::__construct(ParkCarSubTypeSeed::ID, ParkCarSubTypeSeed::NAME);
    }
}
