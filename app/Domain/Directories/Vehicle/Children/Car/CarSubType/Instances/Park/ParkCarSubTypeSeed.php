<?php

namespace App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Park;

class ParkCarSubTypeSeed
{
    const ID = 11;
    const NAME = "Парковые автомобили";
}
