<?php

namespace App\Domain\Directories\Vehicle\Children\Car\CarSubType;

use App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Office\OfficeCarSubType;
use App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Park\ParkCarSubType;

class CarSubTypeRepository
{
    public function getAll(): array
    {
        return [
            new ParkCarSubType(),
            new OfficeCarSubType(),
        ];
    }
}
