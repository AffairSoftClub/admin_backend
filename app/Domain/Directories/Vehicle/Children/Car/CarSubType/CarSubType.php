<?php

namespace App\Domain\Directories\Vehicle\Children\Car\CarSubType;

use App\Domain\Directories\Vehicle\SubType\Base\BaseVehicleSubType;
use App\Domain\Directories\Vehicle\Type\Seeds\CarVehicleTypeSeed;

class CarSubType extends BaseVehicleSubType
{
    public function __construct(int $id, string $name)
    {
        parent::__construct($id, $name, CarVehicleTypeSeed::ID);
    }

}
