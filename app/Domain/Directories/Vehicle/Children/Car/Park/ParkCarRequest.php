<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRequest;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;

class ParkCarRequest extends BaseCarRequest
{
    public function rules(): array
    {
        $parentRules = parent::rules();
        $classOnlyRules = $this->getClassOnlyRules();

        return array_merge($parentRules, $classOnlyRules);
    }

    private function getClassOnlyRules(): array
    {
        //todo: refactor
        return [
            'trailer_id' => [function ($attribute, $value, $fail) {
                $trailer = Trailer::where('id', $value)->first();
                $request = $this->request->all();
                if ($trailer) {
                    $car = $trailer->car;
                    if ($car) {
                        if ($car->number != $request['number']) {
                            $fail('Прицеп ужe прикреплен к другому автомобилю');
                        }
                    }
                }
            }],
        ];
    }
}
