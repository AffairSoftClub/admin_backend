<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Common\GetOrderedWithShipmentsCountPaginationTrait;

class ParkCarRepository extends BaseCarRepository
{
    use GetOrderedWithShipmentsCountPaginationTrait;

    public function __construct(ParkCar $model)
    {
        parent::__construct($model);
    }

    public function getByTrailerId(int $trailerId): ?ParkCar
    {
        return $this->getModel()
            ->firstWhere(ParkCarOnlyColumnNamesEnum::TRAILER_ID, $trailerId);
    }

    protected function getPaginationRelationNames(): array
    {
        return [
            ParkCarRelationNamesEnum::TRAILER,
        ];
    }
}
