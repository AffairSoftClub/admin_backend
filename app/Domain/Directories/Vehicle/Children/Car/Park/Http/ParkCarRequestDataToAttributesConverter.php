<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarRequestDataToAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Http\ParkCarOnlyRequestKeysEnum;

class ParkCarRequestDataToAttributesConverter extends BaseCarRequestDataToAttributesConverter
{
    protected function getMapping(): array
    {
        $parentMapping = parent::getMapping();
        $classOnlyMapping = $this->getClassOnlyMapping();

        return $parentMapping + $classOnlyMapping;
    }

    private function getClassOnlyMapping(): array
    {
        return [
            ParkCarOnlyColumnNamesEnum::TRAILER_ID => ParkCarOnlyRequestKeysEnum::TRAILER_ID,
        ];
    }
}
