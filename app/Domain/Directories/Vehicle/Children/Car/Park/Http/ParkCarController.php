<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarController;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarFrontRelationsEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarService;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ParkCarController extends BaseCarController
{
    /** @var TrailerRepository */
    private $trailerRepository;

    public function __construct(
        TrailerRepository           $trailerService,
        ParkCarByRequestDataService $byRequestDataVehicleService,
        ParkCarService              $entityService
    )
    {
        $this->trailerRepository = $trailerService;

        parent::__construct($byRequestDataVehicleService, $entityService);
    }

    protected function getEntityClassName(): string
    {
        return ParkCar::class;
    }

    // Entity > Index
    protected function getIndexViewName(): string
    {
        return 'directories.vehicles.cars.park';
    }

    protected function getIndexReferences(): array
    {
        return [
            'trailers' => $this->getTrailers(),
        ];
    }

    ///
    public function update(ParkCar $parkCar, Request $request)
    {
        return $this->updateByEntity($parkCar, $request);
    }

    public function destroy(ParkCar $parkCar)
    {
        return $this->destroyByEntity($parkCar);
    }

    private function getTrailers(): Collection
    {
        return $this->trailerRepository->getAllOrderedByName();
    }

    protected function getFrontRelations(): array
    {
        $parentFrontRelations = parent::getFrontRelations();
        $classOnlyFrontRelations = $this->getClassOnlyFrontRelations();

        return array_merge($parentFrontRelations, $classOnlyFrontRelations);
    }

    private function getClassOnlyFrontRelations(): array
    {
        return [
            ParkCarFrontRelationsEnum::TRAILER,
        ];
    }

    public function show(ParkCar $parkCar): ParkCar
    {
        return $parkCar;
    }
}
