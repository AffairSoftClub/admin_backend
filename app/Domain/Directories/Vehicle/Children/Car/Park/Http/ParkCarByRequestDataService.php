<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarByRequestDataService;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarService;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Http\ParkCarRequestDataToAttributesConverter;
use Illuminate\Database\Eloquent\Model;

class ParkCarByRequestDataService extends BaseCarByRequestDataService
{
    /** @var \App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarRepository */
    private $parkCarRepository;

    public function __construct(
        ParkCarRepository $parkCarRepository,
        ParkCarRequestDataToAttributesConverter $requestDataToEntityAttributesConverter,
        ParkCarService $entityService
    )
    {
        $this->parkCarRepository = $parkCarRepository;
        parent::__construct($requestDataToEntityAttributesConverter, $entityService);
    }

    /**
     * @param array $entityAttributes
     * @param Model|\App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar|null $model
     */
    protected function validateEntityAttributes(array $entityAttributes, ?Model $model = null)
    {
        parent::validateEntityAttributes($entityAttributes, $model);

        $this->validateTrailerId($entityAttributes, $model);
    }

    private function validateTrailerId(array $parkCarAttributes, ?ParkCar $parkCar = null)
    {
        $trailerId = $parkCarAttributes[ParkCarColumnNamesEnum::TRAILER_ID];

        if (!$trailerId) {
            return;
        }

        $this->validateTrailerIsNotUsed($trailerId, $parkCar);
    }

    private function validateTrailerIsNotUsed(int $trailerId, ?ParkCar $updatedParkCar)
    {
        $existsParkCar = $this->getParkCarByTrailerId($trailerId);

        if (!$existsParkCar) {
            return;
        }

        if ($existsParkCar->is($updatedParkCar)) {
            return;
        }

        $existsParkCarNumber = $existsParkCar->getNumber();
        $errorMessage = "Выбранный прицеп уже используется с грузовиком {$existsParkCarNumber}";
        $this->throwValidationException(['trailer_id' => $errorMessage]);
    }

    private function getParkCarByTrailerId(int $trailerId): ?ParkCar
    {
        return $this->parkCarRepository->getByTrailerId($trailerId);
    }
}
