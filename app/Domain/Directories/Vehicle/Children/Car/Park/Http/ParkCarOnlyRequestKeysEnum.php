<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Http;

use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;

interface ParkCarOnlyRequestKeysEnum
{
    const TRAILER_ID = ParkCarOnlyColumnNamesEnum::TRAILER_ID;
}
