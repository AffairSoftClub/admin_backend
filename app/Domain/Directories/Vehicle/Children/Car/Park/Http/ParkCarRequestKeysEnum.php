<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarRequestKeysEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Http\ParkCarOnlyRequestKeysEnum;

interface ParkCarRequestKeysEnum extends \App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarRequestKeysEnum, ParkCarOnlyRequestKeysEnum
{

}
