<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Report;

use App\Domain\Directories\Vehicle\Children\Base\Report\BaseVehicleReportFilterKeys;
use App\Domain\Shipment\ShipmentCalculatedFilterKeysEnum;

interface ParkCarCalculatedFilterKeys extends \App\Domain\Directories\Vehicle\Children\Base\Report\BaseVehicleReportFilterKeys
{
    const CLIENT_ID = ShipmentCalculatedFilterKeysEnum::CLIENT_ID;
}
