<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarSeedToEntityAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Seeds\ParkCarSeedKeysEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;

class ParkCarSeedToEntityAttributesConverter extends BaseCarSeedToEntityAttributesConverter
{
    protected function getRelationsMapping(): array
    {
        return [
            ParkCarColumnNamesEnum::TRAILER_ID => [
                Trailer::class,
                [
                    TrailerColumnNamesEnum::NUMBER => ParkCarSeedKeysEnum::TRAILER_NUMBER,
                ]
            ]
        ];
    }
}
