<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarWorkStatusesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Seeds\ParkCarNumbersEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Seeds\ParkCarSeedKeysEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Seeds\TrailerNumbersEnum;

interface ParkCarsSeedValues
{
    const VALUES = [
        [
            ParkCarSeedKeysEnum::NUMBER => '5AK9570',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_2SY2502
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => ParkCarNumbersEnum::N_5AI7068,
            ParkCarSeedKeysEnum::BRAND => 'Mercedes',
            ParkCarSeedKeysEnum::MODEL => 'Actros',
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4AY3218
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => ParkCarNumbersEnum::N_6AS5531,
            ParkCarSeedKeysEnum::BRAND => 'Mercedes',
            ParkCarSeedKeysEnum::MODEL => '323232',
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4AY4079,
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => ParkCarNumbersEnum::N_6AL6750,
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,
//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => 'DW7U058',
            ParkCarSeedKeysEnum::BRAND => 'Mercedes',
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4AY8517
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => 'DW3U620',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4AY8541
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '4AX1285',
            ParkCarSeedKeysEnum::BRAND => 'Mercedes',
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4AY8542
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '4AU2213',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '5AY9920',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => 'XF460 FT',
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4SC9784
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '5AM1827',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => 'XF460 FT',
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4SH6871
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '6AY7500',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => 'XF460 FT',
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_4SH6885
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '6AY7501',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => 'XF460 FT',
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AC7496',
            ParkCarSeedKeysEnum::BRAND => 'DAF',
            ParkCarSeedKeysEnum::MODEL => 'XF460 FT',
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_5AJ1325
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AC7497',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_5AJ5800
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AE9006',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

            ParkCarSeedKeysEnum::TRAILER_NUMBER => TrailerNumbersEnum::N_5AJ7318
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AE9007',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AE9008',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AF4279',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AH7436',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AI9788',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AN2041',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AN2320',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AN6418',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AN6449',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '4SM3745',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AU9672',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AU9673',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AU9674',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AU9429',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AX4123',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AY6496',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '7AY8847',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => '4AS4054',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::REPAIR,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
        [
            ParkCarSeedKeysEnum::NUMBER => 'DW6N419',
            ParkCarSeedKeysEnum::BRAND => null,
            ParkCarSeedKeysEnum::MODEL => null,
            ParkCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,

//            SeedKeysEnum::TRAILER_NUMBER => null
        ],
    ];
}
