<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarSeedKeysEnum;

interface ParkCarSeedKeysEnum extends BaseCarSeedKeysEnum
{
    const TRAILER_NUMBER = 'trailer_number';
}
