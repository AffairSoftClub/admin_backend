<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Seeds;

interface ParkCarNumbersEnum
{
    const N_5AI7068 = '5AI7068'; // 2
    const N_6AS5531 = '6AS5531'; // 3
    const N_6AL6750 = '6AL6750'; // 4
}
