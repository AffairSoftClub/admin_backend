<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarsSeeder;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarRepository;

class ParkCarsSeeder extends BaseCarsSeeder
{
    public function __construct(
        ParkCarRepository                      $repository,
        ParkCarSeedToEntityAttributesConverter $seedToEntityAttributesConverter
    )
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }

    protected function getSeeds(): array
    {
        return ParkCarsSeedValues::VALUES;
    }
}
