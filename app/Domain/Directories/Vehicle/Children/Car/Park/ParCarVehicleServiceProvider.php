<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarService;

class ParCarVehicleServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return ParkCarRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return ParkCarService::class;
    }
}
