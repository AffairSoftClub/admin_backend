<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

interface ParkCarCountFieldNames
{
    const SHIPMENTS_COUNT = 'shipments_count';
}
