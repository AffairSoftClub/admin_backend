<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Directories\Vehicle\SubType\Base\BaseVehicleSubTypeScope;
use App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Park\ParkCarSubTypeSeed;

class ParkCarSubTypeScope extends BaseVehicleSubTypeScope
{
    public function __construct()
    {
        parent::__construct(ParkCarSubTypeSeed::ID);
    }
}
