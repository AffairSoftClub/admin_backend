<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarTypeSelectGroupProvider;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCar;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarRepository;

class ParkCarTypeSelectGroupProvider extends BaseCarTypeSelectGroupProvider
{
    public function __construct(ParkCarRepository $carRepository)
    {
        parent::__construct($carRepository);
    }

    protected function getGroupLabel(): string
    {
        return 'Парковые автомобили';
    }

    protected function getGroupId(): int
    {
        return ParkCar::VEHICLE_SUB_TYPE_ID;
    }
}
