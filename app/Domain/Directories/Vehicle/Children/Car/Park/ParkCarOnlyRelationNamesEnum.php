<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

interface ParkCarOnlyRelationNamesEnum
{
    const TRAILER = 'trailer';
    const CASHBOOK_ITEMS = 'cashbookItems';
    const PART_OR_FULL_PAID_SHIPMENTS = 'partOrFullPaidShipments';
}
