<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Table;

use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarTableNameValue;

interface ParkCarTableNameValue extends \App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarTableNameValue
{

}
