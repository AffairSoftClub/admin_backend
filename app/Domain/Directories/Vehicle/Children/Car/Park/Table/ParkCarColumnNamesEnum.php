<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Table;

use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;

interface ParkCarColumnNamesEnum extends BaseCarColumnNamesEnum, ParkCarOnlyColumnNamesEnum
{
}
