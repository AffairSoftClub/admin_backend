<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park\Table;

interface ParkCarOnlyColumnNamesEnum
{
    const TRAILER_ID = 'trailer_id';
}
