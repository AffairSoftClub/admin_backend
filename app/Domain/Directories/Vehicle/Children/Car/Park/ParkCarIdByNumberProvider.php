<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleNumberToIdProvider;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarRepository;

/**
 * @property ParkCarRepository $internalProvider
 */
class ParkCarIdByNumberProvider extends BaseVehicleNumberToIdProvider
{
    public function __construct(ParkCarRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    protected function getEntityName(): string
    {
        return 'Park car';
    }
}
