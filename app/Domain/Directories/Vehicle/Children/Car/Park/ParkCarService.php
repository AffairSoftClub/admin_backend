<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarService;
use App\Domain\Directories\Vehicle\Children\Trailer\LinkingToParkCarLog\Services\AddOnParkCarUpdateService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

class ParkCarService extends BaseCarService
{
    /** @var AddOnParkCarUpdateService */
    private $addOnParkCarUpdateService;

    public function __construct(
        AddOnParkCarUpdateService $addOnParkCarUpdateService,
        ParkCarRepository         $repository
    )
    {
        $this->addOnParkCarUpdateService = $addOnParkCarUpdateService;

        parent::__construct($repository);
    }

    /**
     * @param Model |ParkCar $model
     * @param array $attributes
     * @return BaseModel
     * @throws Throwable
     */
    public function update(Model $model, array $attributes): Model
    {
        return DB::transaction(function () use ($model, $attributes) {
            $beforeTrailerId = $model->getTrailerId();
            $parkCar = parent::update($model, $attributes);

            $this->logTrailerLinkingChangeIfRequired($model, $beforeTrailerId);

            return $parkCar;
        });
    }

    private function logTrailerLinkingChangeIfRequired(ParkCar $parkCar, $beforeTrailerId)
    {
        $this->addOnParkCarUpdateService->addIfRequired($parkCar, $beforeTrailerId);
    }

    protected function isCreateByNumberSupported(): bool
    {
        return true;
    }
}
