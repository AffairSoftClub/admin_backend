<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Directories\Vehicle\Children\Car\Park\ParkCarOnlyRelationNamesEnum;
use App\Domain\Directories\Vehicle\Common\CashbookItemsWithRelationsRelationNameEnumItem;
use App\Domain\Directories\Vehicle\Common\ShipmentsRelationNameEnumItem;

interface ParkCarRelationNamesEnum extends ShipmentsRelationNameEnumItem,
    CashbookItemsWithRelationsRelationNameEnumItem, ParkCarOnlyRelationNamesEnum
{
}
