<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Park;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\Cashbook\CashbookItem\Types\Base\BaseCashbookItem;
use App\Domain\Cashbook\CashbookItem\Types\Base\Table\BaseCashbookItemColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCar;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Park\ParkCarSubTypeSeed;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Table\TrailerColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Trailer\Trailer;
use App\Domain\Directories\Vehicle\Common\CashbookItemsWithRelationsTrait;
use App\Domain\Invoice\Base\Invoice\PaymentStatusIdsEnum;
use App\Domain\Invoice\Outgoing\Base\InvoiceWithDetailsAggregate\Invoice\Table\BaseOutgoingInvoiceColumnNamesEnum;
use App\Domain\Shipment\Shipment;
use App\Domain\Shipment\ShipmentRelationNamesEnum;
use App\Domain\Shipment\Table\ShipmentColumnNamesEnum;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ParkCar extends BaseCar
{
    use CashbookItemsWithRelationsTrait, ChildExtendableModelTrait;

    const VEHICLE_SUB_TYPE_ID = ParkCarSubTypeSeed::ID;

    // Scopes
    static protected function getSubTypeScopeClassName(): ?string
    {
        return ParkCarSubTypeScope::class;
    }

    // Extendable
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            ParkCarColumnNamesEnum::TRAILER_ID,
        ];
    }

    private function getClassOnlyRelationNamesFromClass(): array
    {
        return [
            ParkCarOnlyRelationNamesEnum::TRAILER,
            ParkCarOnlyRelationNamesEnum::CASHBOOK_ITEMS,
            ParkCarOnlyRelationNamesEnum::PART_OR_FULL_PAID_SHIPMENTS,
        ];
    }

    // Relations

//    public function trailer(): HasOne
//    {
//        return $this->hasOne(
//            Trailer::class,
//            TrailerColumnNamesEnum::ID,
//            ParkCarColumnNamesEnum::TRAILER_ID
//        );
//    }

    public function trailer(): BelongsTo
    {
        return $this->belongsTo(
            Trailer::class,
            ParkCarColumnNamesEnum::TRAILER_ID,
            TrailerColumnNamesEnum::ID
        );
    }

    // Extendable model methods

    public function getPartOrFullPaidFilteredShipmentsWithRelationsPagination(array $filter)
    {
        return $this->partOrFullPaidShipments()
            ->orderByDesc(ShipmentColumnNamesEnum::DEPARTURE_DATETIME)
            ->filter($filter)
            ->with([
                ShipmentRelationNamesEnum::CAR,
                ShipmentRelationNamesEnum::TRAILER,
                ShipmentRelationNamesEnum::DRIVER1,
                ShipmentRelationNamesEnum::DRIVER2,
                ShipmentRelationNamesEnum::CLIENT,
                ShipmentRelationNamesEnum::LOAD_DIRECTIONS,
                ShipmentRelationNamesEnum::UNLOAD_DIRECTIONS,
            ])
            ->paginateFilter();
    }

    public function partOrFullPaidShipments()
    {
        return $this->shipments()
            ->whereHas(ShipmentRelationNamesEnum::INVOICE_DETAIL, function ($query) {
                $query->whereHas('invoice', function ($query) {
                    $query->whereIn(
                        BaseOutgoingInvoiceColumnNamesEnum::PAYMENT_STATUS,
                        [
                            PaymentStatusIdsEnum::PARTLY_PAY,
                            PaymentStatusIdsEnum::FULL_PAY,
                        ]
                    );
                });
            });
    }

    // Relations > Simple

    public function shipments(): HasMany
    {
        return $this->hasMany(
            Shipment::class,
            ShipmentColumnNamesEnum::CAR_ID,
            BaseCarColumnNamesEnum::ID
        );
    }

    public function shipmentsWithRelations(): HasMany
    {
        return $this->shipments()->with([
            ShipmentRelationNamesEnum::CAR,
            ShipmentRelationNamesEnum::TRAILER,
            ShipmentRelationNamesEnum::DRIVER1,
            ShipmentRelationNamesEnum::DRIVER2,
            ShipmentRelationNamesEnum::CLIENT,
            ShipmentRelationNamesEnum::LOAD_DIRECTIONS,
            ShipmentRelationNamesEnum::UNLOAD_DIRECTIONS,
        ]);
    }


    // Relations > Complex

    public function cashbookItems(): HasMany
    {
        return $this->hasMany(
            BaseCashbookItem::class,
            BaseCashbookItemColumnNamesEnum::CAR_ID,
            BaseCarColumnNamesEnum::ID
        );
    }

    /**
     * @return Collection|BaseCashbookItem[]
     */
    public function getCashbookItemsWithRelations(): Collection
    {
        return $this->getAttribute(ParkCarRelationNamesEnum::CASHBOOK_ITEMS_WITH_RELATIONS);
    }

    public function getNumber(): string
    {
        return $this->getAttribute(BaseCarColumnNamesEnum::NUMBER);
    }

    public function getShipmentsCount(): int
    {
        return $this->getAttribute(ParkCarCountFieldNames::SHIPMENTS_COUNT);
    }

    // Getters / Setters

    // OTHER
//    public function getShipmentsTotalAmountSum(): float
//    {
//        return $this->shipments()->sum(ShipmentColumnNamesEnum::TOTAL_AMOUNT);
//    }

//    public function getCashbookItemsTotalAmountSum(): float
//    {
//        return $this->cashbookItems()->sum(CashbookItemColumnNamesEnum::TOTAL_AMOUNT);
//    }

    // Getters and Setters
    public function getTrailerId(): ?int
    {
        return $this->getAttribute(ParkCarOnlyColumnNamesEnum::TRAILER_ID);
    }
}
