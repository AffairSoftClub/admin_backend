<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base;

use App\Base\Repository\Contracts\Basic\GetOrderedPaginationInterface;
use App\Base\Repository\Traits\Advanced\Name\GetOrderedByNamePaginationTrait;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleRepository;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;

/**
 * Class CarRepository
 * @package App\Domain\Directories\Vehicle\Car\Base
 * @method BaseCar getByIdOrFail(int $id)
 * @method BaseCar create(array $attributes = [])
 */
class BaseCarRepository extends BaseVehicleRepository implements GetOrderedPaginationInterface
{
    use GetOrderedByNamePaginationTrait;

    public function __construct(BaseCar $model)
    {
        parent::__construct($model);
    }

    public function isExistsByNumber(string $carNumber): bool
    {
        return $this->getModel()->where(BaseCarColumnNamesEnum::NUMBER, $carNumber)->exists();
    }

    public function getByNumber(string $carNumber): ?BaseCar
    {
        return $this->getModel()->where(BaseCarColumnNamesEnum::NUMBER, $carNumber)->first();
    }
}
