<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicle;
use App\Domain\Directories\Vehicle\SubType\Base\BaseVehicleSubTypeScope;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\CarVehicleTypeScope;
use App\Domain\Directories\Vehicle\Type\Seeds\CarVehicleTypeSeed;

class BaseCar extends BaseVehicle
{
    use ChildExtendableModelTrait;

    const VEHICLE_TYPE_ID = CarVehicleTypeSeed::ID;
    const VEHICLE_SUB_TYPE_ID = null;

    // VEHICLE TYPE SCOPE
    static protected function getTypeScopeClassName(): ?string
    {
        return CarVehicleTypeScope::class;
    }

    // CAR TYPE SCOPE
    protected static function boot()
    {
        parent::boot();
        static::addCarTypeScope();
    }

    private static function addCarTypeScope()
    {
        $carTypeScope = static::getCarTypeScope();

        if (!$carTypeScope) {
            return;
        }

        static::addGlobalScope($carTypeScope);
    }

    private static function getCarTypeScope(): ?BaseVehicleSubTypeScope
    {
        $carTypeScopeClassName = static::getSubTypeScopeClassName();

        if (!$carTypeScopeClassName) {
            return null;
        }

        return new $carTypeScopeClassName;
    }

    // EXTENDABLE MODEL
    private function getClassOnlyCastsFromClass(): array
    {
        return [
            BaseCarOnlyColumnNamesEnum::IS_ARCHIVED => 'bool',
        ];
    }

    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            BaseCarOnlyColumnNamesEnum::BRAND,
            BaseCarOnlyColumnNamesEnum::MODEL,
            BaseCarOnlyColumnNamesEnum::IS_ARCHIVED,
        ];
    }
}
