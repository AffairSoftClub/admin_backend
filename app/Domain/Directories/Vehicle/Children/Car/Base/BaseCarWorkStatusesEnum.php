<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base;

interface BaseCarWorkStatusesEnum
{
    const GOOD = 0;
    const REPAIR = 1;
}
