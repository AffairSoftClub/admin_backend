<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestDataToAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarOnlyRequestKeysEnum;

class BaseCarRequestDataToAttributesConverter extends \App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestDataToAttributesConverter
{
    protected function getMapping(): array
    {
        $parentMapping = parent::getMapping();
        $classOnlyMapping = $this->getClassOnlyMapping();

        return $classOnlyMapping + $parentMapping;
    }

    private function getClassOnlyMapping(): array
    {
        return [
            BaseCarOnlyColumnNamesEnum::MODEL => BaseCarOnlyRequestKeysEnum::MODEL,
            BaseCarOnlyColumnNamesEnum::BRAND => BaseCarOnlyRequestKeysEnum::BRAND,
            BaseCarOnlyColumnNamesEnum::IS_ARCHIVED => BaseCarOnlyRequestKeysEnum::IS_ARCHIVED,
        ];
    }
}
