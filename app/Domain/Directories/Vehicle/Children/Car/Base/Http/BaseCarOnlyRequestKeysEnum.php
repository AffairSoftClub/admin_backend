<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;

interface BaseCarOnlyRequestKeysEnum
{
    const MODEL = BaseCarColumnNamesEnum::MODEL;
    const BRAND = BaseCarColumnNamesEnum::BRAND;
    const IS_ARCHIVED = BaseCarColumnNamesEnum::IS_ARCHIVED;
}
