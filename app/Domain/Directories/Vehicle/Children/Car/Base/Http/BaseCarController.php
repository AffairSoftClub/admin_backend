<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleController;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarService;
use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarByRequestDataService;

/**
 * Class BaseCarController
 * @package App\Domain\Directories\Vehicle\Car\Base\Http
 */
abstract class BaseCarController extends \App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleController
{
    public function __construct(
        BaseCarByRequestDataService $byRequestDataVehicleService,
        BaseCarService $vehicleService
    )
    {
        parent::__construct(
            $byRequestDataVehicleService,
            $vehicleService
        );
    }
}
