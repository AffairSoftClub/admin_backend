<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleByRequestDataWithEqualCreateUpdateAttributes;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCar;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarService;
use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarRequestDataToAttributesConverter;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ByRequestDataBaseCarService
 * @package App\Domain\Directories\Vehicle\Car\Base\Http
 *
 * @method BaseCar create(array $requestData)
 * @method BaseCar update(Model $entity, array $requestData)
 */
class BaseCarByRequestDataService extends BaseVehicleByRequestDataWithEqualCreateUpdateAttributes
{
    public function __construct(
        BaseCarRequestDataToAttributesConverter $requestDataToEntityAttributesConverter,
        BaseCarService $entityService
    )
    {
        parent::__construct($requestDataToEntityAttributesConverter, $entityService);
    }
}
