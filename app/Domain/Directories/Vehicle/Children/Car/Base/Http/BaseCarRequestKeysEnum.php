<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Http;

use App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestKeysEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarOnlyRequestKeysEnum;

interface BaseCarRequestKeysEnum extends \App\Domain\Directories\Vehicle\Children\Base\Http\BaseVehicleRequestKeysEnum, BaseCarOnlyRequestKeysEnum
{
}
