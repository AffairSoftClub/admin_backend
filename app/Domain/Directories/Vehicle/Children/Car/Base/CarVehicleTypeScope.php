<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleTypeScope;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCar;

class CarVehicleTypeScope extends BaseVehicleTypeScope
{
    public function __construct()
    {
        parent::__construct(BaseCar::VEHICLE_TYPE_ID);
    }
}
