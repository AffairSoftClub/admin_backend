<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Seeds;

interface BaseCarSeedKeysEnum
{
    const NUMBER = 'number';
    const MODEL = 'model';
    const BRAND = 'brand';
    const WORK_STATUS_ID = 'work_status_id';
}
