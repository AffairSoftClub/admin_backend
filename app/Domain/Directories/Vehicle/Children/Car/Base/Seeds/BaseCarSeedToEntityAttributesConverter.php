<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Seeds;

use App\Domain\Base\Seeds\Entity\RepositoryBased\BaseSeedToModelAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarSeedKeysEnum;

abstract class BaseCarSeedToEntityAttributesConverter extends BaseSeedToModelAttributesConverter
{
    protected function getMapping(): array
    {
        return [
            BaseCarColumnNamesEnum::NUMBER => BaseCarSeedKeysEnum::NUMBER,
            BaseCarColumnNamesEnum::MODEL => BaseCarSeedKeysEnum::MODEL,
            BaseCarColumnNamesEnum::BRAND => BaseCarSeedKeysEnum::BRAND,
            BaseCarColumnNamesEnum::WORK_STATUS_ID => BaseCarSeedKeysEnum::WORK_STATUS_ID,
        ];
    }

    protected function getCalculatedValues(array $source): array
    {
        return [];
    }
}
