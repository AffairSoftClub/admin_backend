<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Seeds;

use App\Domain\Base\Seeds\SeedToEntityAttributesConverterInterface;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository;
use App\Domain\Base\Seeds\Entity\RepositoryBased\BySeedsRepositoryBasedSeeder;

abstract class BaseCarsSeeder extends BySeedsRepositoryBasedSeeder
{
    public function __construct(
        BaseCarRepository $repository,
        SeedToEntityAttributesConverterInterface $seedToEntityAttributesConverter
    )
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }
}
