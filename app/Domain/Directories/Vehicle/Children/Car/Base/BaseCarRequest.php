<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base;

use Illuminate\Foundation\Http\FormRequest;

class BaseCarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'number' => 'required|min:3',
        ];
    }
}
//$validator=  Validator($request->json()->all(), [
//    'number' => 'required|min:3',
//    'trailer_id' => [function ($attribute, $value, $fail)use($request) {
//        $trailer=Trailer::where('id',$value)->first();
//        if($trailer){
//            $car=$trailer->car;
//            if($car){
//                if ($car->number != $request['number'] ){
//                    $fail('Прицеп ужe прикреплен к другому автомобилю');
//                }
//            }
//        }
//    }],
//]);
//if ($validator->fails()) {
//    return response()->json($validator->errors(), 422);
//}
