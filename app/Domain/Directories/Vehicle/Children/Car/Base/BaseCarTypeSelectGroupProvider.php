<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleSelectGroupProvider;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository;

abstract class BaseCarTypeSelectGroupProvider extends BaseVehicleSelectGroupProvider
{
    public function __construct(BaseCarRepository $carRepository)
    {
        parent::__construct($carRepository);
    }
}
