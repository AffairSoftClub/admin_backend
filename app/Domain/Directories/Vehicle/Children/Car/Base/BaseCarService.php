<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base;

use App\Domain\Directories\Vehicle\Children\Base\BaseVehicle;
use App\Domain\Directories\Vehicle\Children\Base\BaseVehicleService;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Invoice\General\Invoice\GeneralInvoiceRepository;
use App\Domain\Invoice\Incoming\DkvNew\Invoice\DkvNewIncomingInvoiceRepository;
use LogicException;

/**
 * Class CarService
 * @package App\Domain\Directories\Vehicle\Car\Base
 * @property BaseCarRepository $repository
 */
class BaseCarService extends BaseVehicleService
{
    public function __construct(
        BaseCarRepository $repository
    )
    {
        parent::__construct($repository);
    }

    public function getOrCreateByNumber(string $carNumber, array &$notExistsCarNumbers, bool $isCreateNotExistsCars): ?BaseCar
    {
        $car = $this->getByNumberOrNull($carNumber);

        if ($car) {
            return $car;
        }

        if ($carNumber && !in_array($carNumber, $notExistsCarNumbers)) {
            $notExistsCarNumbers[] = $carNumber;
        }

        if ($isCreateNotExistsCars) {
            if (!$this->isCreateByNumberSupported()) {
                throw new LogicException("BaseCarService doesn't support auto-creation. Use Park/Office car services.");
            }

            return $this->createByNumber($carNumber);
        }

        return null;
    }

    public function getByNumberOrNull(string $carNumber): ?BaseCar
    {
        return $this->repository->getByNumber($carNumber);
    }

    protected function isCreateByNumberSupported(): bool
    {
        return false;
    }

    public function createByNumber(string $carNumber): BaseCar
    {
        $attributes = [
            BaseCarColumnNamesEnum::NUMBER => $carNumber,
        ];

        return $this->repository->create($attributes);
    }

    protected function validateDeleteAllowed(BaseVehicle $vehicle)
    {
    }
}
