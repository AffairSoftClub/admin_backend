<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Table;

use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleTableNameValue;

interface BaseCarTableNameValue extends \App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleTableNameValue
{
}
