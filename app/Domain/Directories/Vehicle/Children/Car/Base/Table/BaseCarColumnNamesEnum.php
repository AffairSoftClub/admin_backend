<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Table;

use App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarOnlyColumnNamesEnum;

interface BaseCarColumnNamesEnum extends \App\Domain\Directories\Vehicle\Children\Base\Table\BaseVehicleColumnNamesEnum, BaseCarOnlyColumnNamesEnum
{
}
