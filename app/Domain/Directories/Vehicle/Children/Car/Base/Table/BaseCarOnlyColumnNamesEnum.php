<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Base\Table;

interface BaseCarOnlyColumnNamesEnum
{
    const BRAND = 'brand';
    const MODEL = 'model';
    const IS_ARCHIVED = 'is_archived';
}
