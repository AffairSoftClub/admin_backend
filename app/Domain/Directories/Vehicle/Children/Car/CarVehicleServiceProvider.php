<?php

namespace App\Domain\Directories\Vehicle\Children\Car;

use App\Domain\Base\ServiceProvider\BaseAggregateDomainServiceProvider;
use App\Domain\Directories\Vehicle\Children\Car\All\AllCarVehicleServiceProvider;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarVehicleServiceProvider;
use App\Domain\Directories\Vehicle\Children\Car\Park\ParCarVehicleServiceProvider;

class CarVehicleServiceProvider extends BaseAggregateDomainServiceProvider
{
    protected function getServiceProvidersClassNames(): array
    {
        return [
            AllCarVehicleServiceProvider::class,
            OfficeCarVehicleServiceProvider::class,
            ParCarVehicleServiceProvider::class,
        ];
    }
}
