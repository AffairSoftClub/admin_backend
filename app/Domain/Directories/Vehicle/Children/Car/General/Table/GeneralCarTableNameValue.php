<?php

namespace App\Domain\Directories\Vehicle\Children\Car\General\Table;

use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarTableNameValue;

interface GeneralCarTableNameValue extends \App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarTableNameValue
{

}
