<?php

namespace App\Domain\Directories\Vehicle\Children\Car\General\Table;

use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum;

interface GeneralCarColumnNamesEnum extends BaseCarColumnNamesEnum, \App\Domain\Directories\Vehicle\Children\Car\Park\Table\ParkCarOnlyColumnNamesEnum, \App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarOnlyColumnNamesEnum
{

}
