<?php

namespace App\Domain\Directories\Vehicle\Children\Car\General;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\General\GeneralCar;

class GeneralCarRepository extends BaseCarRepository
{
    public function __construct(GeneralCar $model)
    {
        parent::__construct($model);
    }
}
