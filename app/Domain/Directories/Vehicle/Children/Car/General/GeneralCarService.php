<?php

namespace App\Domain\Directories\Vehicle\Children\Car\General;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarService;
use App\Domain\Invoice\General\Invoice\GeneralInvoiceRepository;

class GeneralCarService extends BaseCarService
{
    public function __construct(GeneralCarRepository $repository)
    {
        parent::__construct($repository);
    }
}
