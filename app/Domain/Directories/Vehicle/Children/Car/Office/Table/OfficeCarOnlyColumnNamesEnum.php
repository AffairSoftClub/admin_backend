<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Table;

interface OfficeCarOnlyColumnNamesEnum
{
    const RESPONSIBLE = 'responsible';
}
