<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Table;

use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarTableNameValue;

interface OfficeCarTableNameValue extends \App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarTableNameValue
{

}
