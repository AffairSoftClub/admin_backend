<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Table;

use App\Domain\Directories\Vehicle\Children\Car\Base\Table\BaseCarColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarOnlyColumnNamesEnum;

interface OfficeCarColumnNamesEnum extends BaseCarColumnNamesEnum, OfficeCarOnlyColumnNamesEnum
{

}
