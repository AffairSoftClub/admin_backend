<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office;

use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarRepository;

class IsOfficeCarResolver
{
    /** @var OfficeCarRepository */
    private $officeCarRepository;

    public function __construct(OfficeCarRepository $officeCarRepository)
    {
        $this->officeCarRepository = $officeCarRepository;
    }

    public function isOfficeCarByCarId(?int $carId): bool
    {
        if (!$carId) {
            return false;
        }

        $officeCarIds = $this->getCachedOfficeCarIds();

        return in_array($carId, $officeCarIds);
    }

    private function getCachedOfficeCarIds()
    {
        return $this->officeCarRepository->getAllIds();
    }
}
