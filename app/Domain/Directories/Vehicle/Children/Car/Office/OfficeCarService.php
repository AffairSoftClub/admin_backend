<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarService;
use App\Domain\Invoice\General\Invoice\GeneralInvoiceRepository;

class OfficeCarService extends BaseCarService
{
    public function __construct(
        OfficeCarRepository $repository
    )
    {
        parent::__construct($repository);
    }

    protected function isCreateByNumberSupported(): bool
    {
        return true;
    }
}
