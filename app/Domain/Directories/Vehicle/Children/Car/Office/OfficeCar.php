<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCar;
use App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Office\OfficeCarSubTypeSeed;
use App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarOnlyColumnNamesEnum;

class OfficeCar extends BaseCar
{
    use ChildExtendableModelTrait;

    const VEHICLE_SUB_TYPE_ID = OfficeCarSubTypeSeed::ID;

    static protected function getSubTypeScopeClassName(): ?string
    {
        return OfficeCarSubTypeScope::class;
    }

    // Extendable
    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            OfficeCarOnlyColumnNamesEnum::RESPONSIBLE,
        ];
    }
}
