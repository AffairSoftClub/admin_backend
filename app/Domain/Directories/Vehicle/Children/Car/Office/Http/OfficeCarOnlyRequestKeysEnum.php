<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Http;

use App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarOnlyColumnNamesEnum;

interface OfficeCarOnlyRequestKeysEnum
{
    const RESPONSIBLE = OfficeCarOnlyColumnNamesEnum::RESPONSIBLE;
}
