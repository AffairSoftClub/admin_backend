<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarRequestDataToAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarOnlyColumnNamesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Office\Http\OfficeCarOnlyRequestKeysEnum;

class OfficeCarRequestDataToAttributesConverter extends BaseCarRequestDataToAttributesConverter
{
    protected function getMapping(): array
    {
        $parentMapping = parent::getMapping();
        $classOnlyMapping = $this->getClassOnlyMapping();

        return $parentMapping + $classOnlyMapping;
    }

    private function getClassOnlyMapping(): array
    {
        return [
            OfficeCarOnlyColumnNamesEnum::RESPONSIBLE => OfficeCarOnlyRequestKeysEnum::RESPONSIBLE,
        ];
    }
}
