<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarByRequestDataService;
use App\Domain\Directories\Vehicle\Children\Car\Office\Http\OfficeCarRequestDataToAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarService;

class OfficeCarByRequestDataService extends BaseCarByRequestDataService
{
    public function __construct(
        OfficeCarRequestDataToAttributesConverter $requestDataToEntityAttributesConverter,
        OfficeCarService $entityService
    )
    {
        parent::__construct($requestDataToEntityAttributesConverter, $entityService);
    }
}
