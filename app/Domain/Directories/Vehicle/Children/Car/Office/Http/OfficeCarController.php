<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Http;

use App\Domain\Directories\Vehicle\Children\Car\Base\Http\BaseCarController;
use App\Domain\Directories\Vehicle\Children\Car\Office\Http\OfficeCarByRequestDataService;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCar;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarService;
use Illuminate\Http\Request;

class OfficeCarController extends BaseCarController
{
    public function __construct(
        OfficeCarByRequestDataService $byRequestDataVehicleService,
        OfficeCarService              $officeVehicleService
    )
    {
        parent::__construct($byRequestDataVehicleService, $officeVehicleService);
    }

    protected function getEntityClassName(): string
    {
        return OfficeCar::class;
    }


    // Entity > Index
    protected function getIndexViewName(): string
    {
        return 'directories.vehicles.cars.office';
    }

    public function update(OfficeCar $officeCar, Request $request)
    {
        return $this->updateByEntity($officeCar, $request);
    }

    public function destroy(OfficeCar $officeCar)
    {
        return $this->destroyByEntity($officeCar);
    }
}
