<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarService;

class OfficeCarVehicleServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return OfficeCarRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return OfficeCarService::class;
    }
}
