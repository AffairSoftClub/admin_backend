<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office;

use App\Base\Repository\Traits\Base\GetAllIdsTrait;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCar;
use App\Domain\Directories\Vehicle\Children\Car\Office\Table\OfficeCarColumnNamesEnum;

class OfficeCarRepository extends BaseCarRepository
{
    use GetAllIdsTrait;

    public function __construct(OfficeCar $model)
    {
        parent::__construct($model);
    }

    protected function getIdColumnName(): string
    {
        return OfficeCarColumnNamesEnum::ID;
    }
}
