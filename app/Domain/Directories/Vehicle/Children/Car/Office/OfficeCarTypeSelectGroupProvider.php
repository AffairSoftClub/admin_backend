<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarTypeSelectGroupProvider;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCar;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarRepository;

class OfficeCarTypeSelectGroupProvider extends BaseCarTypeSelectGroupProvider
{
    public function __construct(OfficeCarRepository $carRepository)
    {
        parent::__construct($carRepository);
    }

    protected function getGroupLabel(): string
    {
        return 'Офисные автомобили';
    }

    protected function getGroupId(): int
    {
        return OfficeCar::VEHICLE_SUB_TYPE_ID;
    }
}
