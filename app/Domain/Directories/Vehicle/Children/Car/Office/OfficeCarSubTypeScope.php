<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office;

use App\Domain\Directories\Vehicle\SubType\Base\BaseVehicleSubTypeScope;
use App\Domain\Directories\Vehicle\Children\Car\CarSubType\Instances\Office\OfficeCarSubTypeSeed;

class OfficeCarSubTypeScope extends BaseVehicleSubTypeScope
{
    public function __construct()
    {
        parent::__construct(OfficeCarSubTypeSeed::ID);
    }
}
