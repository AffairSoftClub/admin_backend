<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarsSeeder;
use App\Domain\Directories\Vehicle\Children\Car\Office\OfficeCarRepository;
use App\Domain\Directories\Vehicle\Children\Car\Park\Seeds\ParkCarSeedToEntityAttributesConverter;
use App\Domain\Directories\Vehicle\Children\Car\Office\Seeds\OfficeCarsSeedValues;

class OfficeCarsSeeder extends \App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarsSeeder
{
    public function __construct(
        OfficeCarRepository                                                                            $repository,
        \App\Domain\Directories\Vehicle\Children\Car\Park\Seeds\ParkCarSeedToEntityAttributesConverter $seedToEntityAttributesConverter
    )
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }

    protected function getSeeds(): array
    {
        return OfficeCarsSeedValues::VALUES;
    }
}
