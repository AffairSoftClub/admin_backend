<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarSeedKeysEnum;

interface OfficeCarSeedKeysEnum extends BaseCarSeedKeysEnum
{

}
