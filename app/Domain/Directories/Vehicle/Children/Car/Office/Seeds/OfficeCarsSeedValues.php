<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarWorkStatusesEnum;
use App\Domain\Directories\Vehicle\Children\Car\Office\Seeds\OfficeCarNumbersEnum;
use App\Domain\Directories\Vehicle\Children\Car\Office\Seeds\OfficeCarSeedKeysEnum;

interface OfficeCarsSeedValues
{
    const VALUES = [
        [
            OfficeCarSeedKeysEnum::NUMBER => OfficeCarNumbersEnum::N_4SM3746,
            OfficeCarSeedKeysEnum::MODEL => null,
            OfficeCarSeedKeysEnum::BRAND => null,
            OfficeCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,
        ],
        [
            OfficeCarSeedKeysEnum::NUMBER => OfficeCarNumbersEnum::N_5AC9567,
            OfficeCarSeedKeysEnum::MODEL => null,
            OfficeCarSeedKeysEnum::BRAND => null,
            OfficeCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,
        ],
        [
            OfficeCarSeedKeysEnum::NUMBER => OfficeCarNumbersEnum::N_6AU5558,
            OfficeCarSeedKeysEnum::MODEL => null,
            OfficeCarSeedKeysEnum::BRAND => null,
            OfficeCarSeedKeysEnum::WORK_STATUS_ID => BaseCarWorkStatusesEnum::GOOD,
        ],
    ];
}
