<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Seeds;

use App\Domain\Directories\Vehicle\Children\Car\Base\Seeds\BaseCarSeedToEntityAttributesConverter;

class OfficeCarSeedToEntityAttributesConverter extends BaseCarSeedToEntityAttributesConverter
{
    protected function getRelationsMapping(): array
    {
        return [];
    }
}
