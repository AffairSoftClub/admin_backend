<?php

namespace App\Domain\Directories\Vehicle\Children\Car\Office\Seeds;

interface OfficeCarNumbersEnum
{
    const N_4SM3746 = '4SM3746'; // 2
    const N_5AC9567 = '5AC9567'; // 3
    const N_6AU5558 = '6AU5558'; // 4
}
