<?php

namespace App\Domain\Directories\Vehicle;

use App\Domain\Base\ServiceProvider\BaseAggregateDomainServiceProvider;
use App\Domain\Directories\Vehicle\Children\All\AllVehicleServiceProvider;
use App\Domain\Directories\Vehicle\Children\Car\CarVehicleServiceProvider;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerServiceProvider;

class VehicleServiceProvider extends BaseAggregateDomainServiceProvider
{
    protected function getServiceProvidersClassNames(): array
    {
        return [
            AllVehicleServiceProvider::class,
            CarVehicleServiceProvider::class,
            TrailerServiceProvider::class,
        ];
    }

}
