<?php

namespace App\Domain\Directories\Country;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Base\GetAsMappingTrait;
use App\Domain\Directories\Country\Table\CountryColumnNamesEnum;

class CountryRepository extends BaseRepository
{
    use GetAsMappingTrait;

    public function __construct(Country $model)
    {
        parent::__construct($model);
    }

    public function getIdToCodeMap(): array
    {
        return $this->getAsMapping(
            CountryColumnNamesEnum::ID,
            CountryColumnNamesEnum::CODE
        );
    }
}
