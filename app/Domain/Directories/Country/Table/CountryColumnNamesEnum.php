<?php

namespace App\Domain\Directories\Country\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface CountryColumnNamesEnum extends IdTimestampsInterface
{
    const CODE = 'code';
    const NAME_EN = 'name_en';
    const NAME_RU = 'name_ru';
}
