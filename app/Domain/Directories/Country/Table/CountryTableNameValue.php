<?php

namespace App\Domain\Directories\Country\Table;

interface CountryTableNameValue
{
    const VALUE = 'countries';
}
