<?php

namespace App\Domain\Directories\Country;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Directories\Country\Table\CountryColumnNamesEnum;
use App\Domain\Directories\Country\Table\CountryTableNameValue;

class Country extends BaseModel
{
    protected $table = CountryTableNameValue::VALUE;

    protected $fillable = [
        CountryColumnNamesEnum::CODE,
        CountryColumnNamesEnum::NAME_EN,
        CountryColumnNamesEnum::NAME_RU,
    ];
}
