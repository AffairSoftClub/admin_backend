<?php

namespace App\Domain\Directories\Country\Seeds;

use App\Domain\Directories\Country\Table\CountryColumnNamesEnum;

interface CountrySeedsValues
{
    const VALUES = [
        [
            CountryColumnNamesEnum::CODE => 'DE',
            CountryColumnNamesEnum::NAME_EN => 'Germany',
            CountryColumnNamesEnum::NAME_RU => 'Германия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'Sl',
            CountryColumnNamesEnum::NAME_EN => 'Sierra Leone',
            CountryColumnNamesEnum::NAME_RU => 'Сьерра-Леоне',
        ],
        [
            CountryColumnNamesEnum::CODE => 'HR',
            CountryColumnNamesEnum::NAME_EN => 'Croatia',
            CountryColumnNamesEnum::NAME_RU => 'Хорватия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'LI',
            CountryColumnNamesEnum::NAME_EN => 'Liechtenstein',
            CountryColumnNamesEnum::NAME_RU => 'Лихтенштейн',
        ],
        [
            CountryColumnNamesEnum::CODE => 'ME',
            CountryColumnNamesEnum::NAME_EN => 'Montenegro',
            CountryColumnNamesEnum::NAME_RU => 'Черногория',
        ],
        [
            CountryColumnNamesEnum::CODE => 'MK',
            CountryColumnNamesEnum::NAME_EN => 'Macedonia',
            CountryColumnNamesEnum::NAME_RU => 'Македония',
        ],
        [
            CountryColumnNamesEnum::CODE => 'BG',
            CountryColumnNamesEnum::NAME_EN => 'Bulgaria',
            CountryColumnNamesEnum::NAME_RU => 'Болгария',
        ],
        [
            CountryColumnNamesEnum::CODE => 'SK',
            CountryColumnNamesEnum::NAME_EN => 'Slovakia',
            CountryColumnNamesEnum::NAME_RU => 'Словакия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'GI',
            CountryColumnNamesEnum::NAME_EN => 'Gibraltar',
            CountryColumnNamesEnum::NAME_RU => 'Гибралтар',
        ],
        [
            CountryColumnNamesEnum::CODE => 'BE',
            CountryColumnNamesEnum::NAME_EN => 'Belgium',
            CountryColumnNamesEnum::NAME_RU => 'Бельгия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'LU',
            CountryColumnNamesEnum::NAME_EN => 'Luxembourg',
            CountryColumnNamesEnum::NAME_RU => 'Люксембург',
        ],
        [
            CountryColumnNamesEnum::CODE => 'DK',
            CountryColumnNamesEnum::NAME_EN => 'Denmark',
            CountryColumnNamesEnum::NAME_RU => 'Дания',
        ],
        [
            CountryColumnNamesEnum::CODE => 'FR',
            CountryColumnNamesEnum::NAME_EN => 'France',
            CountryColumnNamesEnum::NAME_RU => 'Франция',
        ],
        [
            CountryColumnNamesEnum::CODE => 'IT',
            CountryColumnNamesEnum::NAME_EN => 'Italy',
            CountryColumnNamesEnum::NAME_RU => 'Италия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'NL',
            CountryColumnNamesEnum::NAME_EN => 'Netherlands',
            CountryColumnNamesEnum::NAME_RU => 'Нидерланды',
        ],
        [
            CountryColumnNamesEnum::CODE => 'AT',
            CountryColumnNamesEnum::NAME_EN => 'Austria',
            CountryColumnNamesEnum::NAME_RU => 'Австрия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'CH',
            CountryColumnNamesEnum::NAME_EN => 'Switzerland',
            CountryColumnNamesEnum::NAME_RU => 'Швейцария',
        ],
        [
            CountryColumnNamesEnum::CODE => 'SE',
            CountryColumnNamesEnum::NAME_EN => 'Sweden',
            CountryColumnNamesEnum::NAME_RU => 'Швеция',
        ],
        [
            CountryColumnNamesEnum::CODE => 'GR',
            CountryColumnNamesEnum::NAME_EN => 'Greece',
            CountryColumnNamesEnum::NAME_RU => 'Греция',
        ],
        [
            CountryColumnNamesEnum::CODE => 'GB',
            CountryColumnNamesEnum::NAME_EN => 'United Kingdom',
            CountryColumnNamesEnum::NAME_RU => 'Великобритания',
        ],
        [
            CountryColumnNamesEnum::CODE => 'NO',
            CountryColumnNamesEnum::NAME_EN => 'Norway',
            CountryColumnNamesEnum::NAME_RU => 'Норвегия',
        ],

        [
            CountryColumnNamesEnum::CODE => 'HU',
            CountryColumnNamesEnum::NAME_EN => 'Hungary',
            CountryColumnNamesEnum::NAME_RU => 'Венгрия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'CZ',
            CountryColumnNamesEnum::NAME_EN => 'Czech Republic',
            CountryColumnNamesEnum::NAME_RU => 'Чешская Республика',
        ],
        [
            CountryColumnNamesEnum::CODE => 'RO',
            CountryColumnNamesEnum::NAME_EN => 'Romania',
            CountryColumnNamesEnum::NAME_RU => 'Румыния',
        ],
        [
            CountryColumnNamesEnum::CODE => 'TR',
            CountryColumnNamesEnum::NAME_EN => 'Turkey',
            CountryColumnNamesEnum::NAME_RU => 'Турция',
        ],
        [
            CountryColumnNamesEnum::CODE => 'ES',
            CountryColumnNamesEnum::NAME_EN => 'Spain',
            CountryColumnNamesEnum::NAME_RU => 'Испания',
        ],
        [
            CountryColumnNamesEnum::CODE => 'FI',
            CountryColumnNamesEnum::NAME_EN => 'Finland',
            CountryColumnNamesEnum::NAME_RU => 'Finland',
        ],
        [
            CountryColumnNamesEnum::CODE => 'MA',
            CountryColumnNamesEnum::NAME_EN => 'Morocco',
            CountryColumnNamesEnum::NAME_RU => 'Марокко',
        ],
        [
            CountryColumnNamesEnum::CODE => 'IE',
            CountryColumnNamesEnum::NAME_EN => 'Ireland',
            CountryColumnNamesEnum::NAME_RU => 'Ирландия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'PL',
            CountryColumnNamesEnum::NAME_EN => 'Poland',
            CountryColumnNamesEnum::NAME_RU => 'Польша',
        ],
        [
            CountryColumnNamesEnum::CODE => 'PT',
            CountryColumnNamesEnum::NAME_EN => 'Portugal',
            CountryColumnNamesEnum::NAME_RU => 'Португалия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'TN',
            CountryColumnNamesEnum::NAME_EN => 'Tunisia',
            CountryColumnNamesEnum::NAME_RU => 'Тунис',
        ],

        [
            CountryColumnNamesEnum::CODE => 'CY',
            CountryColumnNamesEnum::NAME_EN => 'Cyprus',
            CountryColumnNamesEnum::NAME_RU => 'Кипр',
        ],
        [
            CountryColumnNamesEnum::CODE => 'AL',
            CountryColumnNamesEnum::NAME_EN => 'Albania',
            CountryColumnNamesEnum::NAME_RU => 'Албания',
        ],
        [
            CountryColumnNamesEnum::CODE => 'AD',
            CountryColumnNamesEnum::NAME_EN => 'Andorra',
            CountryColumnNamesEnum::NAME_RU => 'Андорра',
        ],
        [
            CountryColumnNamesEnum::CODE => 'RU',
            CountryColumnNamesEnum::NAME_EN => 'Russia',
            CountryColumnNamesEnum::NAME_RU => 'Россия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'EE',
            CountryColumnNamesEnum::NAME_EN => 'Estonia',
            CountryColumnNamesEnum::NAME_RU => 'Эстония',
        ],
        [
            CountryColumnNamesEnum::CODE => 'LV',
            CountryColumnNamesEnum::NAME_EN => 'Latvia',
            CountryColumnNamesEnum::NAME_RU => 'Латвия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'LT',
            CountryColumnNamesEnum::NAME_EN => 'Lithuania',
            CountryColumnNamesEnum::NAME_RU => 'Литва',
        ],

        [
            CountryColumnNamesEnum::CODE => 'BY',
            CountryColumnNamesEnum::NAME_EN => 'Belarus',
            CountryColumnNamesEnum::NAME_RU => 'Беларусь',
        ],
        [
            CountryColumnNamesEnum::CODE => 'UA',
            CountryColumnNamesEnum::NAME_EN => 'Ukraine',
            CountryColumnNamesEnum::NAME_RU => 'Украина',
        ],
        [
            CountryColumnNamesEnum::CODE => 'MD',
            CountryColumnNamesEnum::NAME_EN => 'Moldova',
            CountryColumnNamesEnum::NAME_RU => 'Молдова',
        ],
        [
            CountryColumnNamesEnum::CODE => 'AM',
            CountryColumnNamesEnum::NAME_EN => 'Armenia',
            CountryColumnNamesEnum::NAME_RU => 'Армения',
        ],
        [
            CountryColumnNamesEnum::CODE => 'AZ',
            CountryColumnNamesEnum::NAME_EN => 'Azerbaijan',
            CountryColumnNamesEnum::NAME_RU => 'Азербайджан',
        ],
        [
            CountryColumnNamesEnum::CODE => 'GE',
            CountryColumnNamesEnum::NAME_EN => 'Georgia',
            CountryColumnNamesEnum::NAME_RU => 'Грузия',
        ],
        [
            CountryColumnNamesEnum::CODE => 'KZ',
            CountryColumnNamesEnum::NAME_EN => 'Kazakhstan',
            CountryColumnNamesEnum::NAME_RU => 'Казахстан',
        ],
        [
            CountryColumnNamesEnum::CODE => 'KG',
            CountryColumnNamesEnum::NAME_EN => 'Kyrgyzstan',
            CountryColumnNamesEnum::NAME_RU => 'Кыргызстан',
        ],
        [
            CountryColumnNamesEnum::CODE => 'TJ',
            CountryColumnNamesEnum::NAME_EN => 'Tajikistan',
            CountryColumnNamesEnum::NAME_RU => 'Таджикистан',
        ],
        [
            CountryColumnNamesEnum::CODE => 'charged in $',
            CountryColumnNamesEnum::NAME_EN => 'charged in $',
            CountryColumnNamesEnum::NAME_RU => 'charged in $',
        ],
    ];
}
