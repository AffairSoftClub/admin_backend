<?php

namespace App\Domain\Directories\Country\Seeds;

use App\Domain\Directories\Country\CountryRepository;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class CountriesSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(CountryRepository $repository)
    {
        parent::__construct($repository);
    }

    protected function getSeeds(): array
    {
        return CountrySeedsValues::VALUES;
    }
}
