<?php

namespace App\Domain\Directories\Country\Seeds;

interface CountryNamesEnum
{
    const BELARUS = 'Belarus';
    const UKRAINE = 'Ukraine';
    const RUSSIA = 'Russia';
}
