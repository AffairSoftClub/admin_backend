<?php

namespace App\Domain\Directories\Country;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class CountryServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return CountryRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return null;
    }
}
