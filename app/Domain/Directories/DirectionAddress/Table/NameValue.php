<?php

namespace App\Domain\Directories\DirectionAddress\Table;

interface NameValue
{
    const VALUE = 'direction_addresses';
}
