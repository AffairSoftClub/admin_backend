<?php

namespace App\Domain\Directories\DirectionAddress\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface DirectionAddressColumnNamesEnum extends IdTimestampsInterface
{
    const DIRECTION_ID = 'direction_id';
    const ADDRESS = 'address';
    const EMAIL = 'email';
    const PHONE = 'phone';
    const COMMENT = 'comment';
}
