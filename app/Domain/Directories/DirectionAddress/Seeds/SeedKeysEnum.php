<?php

namespace App\Domain\Directories\DirectionAddress\Seeds;

use App\Domain\Directories\DirectionAddress\Table\DirectionAddressColumnNamesEnum;

interface SeedKeysEnum
{
    const DIRECTION_CODE = 'direction_code';

    const ADDRESS = DirectionAddressColumnNamesEnum::ADDRESS;
    const EMAIL = DirectionAddressColumnNamesEnum::EMAIL;
    const PHONE = DirectionAddressColumnNamesEnum::PHONE;
    const COMMENT = DirectionAddressColumnNamesEnum::COMMENT;
}
