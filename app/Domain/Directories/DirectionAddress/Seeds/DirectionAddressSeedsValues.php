<?php

namespace App\Domain\Directories\DirectionAddress\Seeds;

use App\Domain\Shipment\Direction\Seeds\DirectionCodesEnum;

interface DirectionAddressSeedsValues
{
    const VALUES = [
        [
            SeedKeysEnum::DIRECTION_CODE => DirectionCodesEnum::AMS,
        ],
        [
            SeedKeysEnum::DIRECTION_CODE => DirectionCodesEnum::NUE,
        ],
        [
            SeedKeysEnum::DIRECTION_CODE => DirectionCodesEnum::FRA,
        ],
//        [
//            SeedKeysEnum::DIRECTION_CODE => DirectionCodesEnum::CGN,
//        ],
//        [
//            SeedKeysEnum::DIRECTION_CODE => DirectionCodesEnum::BUNDE,
//        ],
    ];
}
