<?php

namespace App\Domain\Directories\DirectionAddress\Seeds;

use App\Domain\Directories\DirectionAddress\DirectionAddressRepository;
use App\Domain\Directories\DirectionAddress\Seeds\DirectionAddressSeedsValues;
use App\Domain\Directories\DirectionAddress\Seeds\SeedToModelAttributesConverter;
use App\Domain\Base\Seeds\Entity\RepositoryBased\BySeedsRepositoryBasedSeeder;

class DirectionAddressesSeeder extends BySeedsRepositoryBasedSeeder
{
    public function __construct(
        DirectionAddressRepository     $repository,
        SeedToModelAttributesConverter $seedToEntityAttributesConverter
    )
    {
        parent::__construct($repository, $seedToEntityAttributesConverter);
    }

    protected function getSeeds(): array
    {
        return DirectionAddressSeedsValues::VALUES;
    }
}
