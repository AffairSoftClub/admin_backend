<?php

namespace App\Domain\Directories\DirectionAddress\Seeds;

use App\Domain\Base\Seeds\Entity\RepositoryBased\BaseSeedToModelAttributesConverter as BaseSeedToModelAttributesConverter;
use App\Domain\Shipment\Direction\Direction;
use App\Domain\Shipment\Direction\Table\DirectionColumnNamesEnum;
use App\Domain\Directories\DirectionAddress\Table\DirectionAddressColumnNamesEnum;

class SeedToModelAttributesConverter extends BaseSeedToModelAttributesConverter
{
    protected function getMapping(): array
    {
        return [
//            ColumnNamesEnum::ADDRESS => SeedKeysEnum::ADDRESS,
//            ColumnNamesEnum::EMAIL => SeedKeysEnum::EMAIL,
//            ColumnNamesEnum::PHONE => SeedKeysEnum::PHONE,
//            ColumnNamesEnum::COMMENT => SeedKeysEnum::COMMENT,
        ];
    }

    protected function getCalculatedValues(array $source): array
    {
        return [];
    }

    protected function getRelationsMapping(): array
    {
        return [
            DirectionAddressColumnNamesEnum::DIRECTION_ID => [
                Direction::class,
                [
                    DirectionColumnNamesEnum::CODE => SeedKeysEnum::DIRECTION_CODE,
                ]
            ],
        ];
    }
}
