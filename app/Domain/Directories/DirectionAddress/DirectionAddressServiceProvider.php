<?php

namespace App\Domain\Directories\DirectionAddress;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class DirectionAddressServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return DirectionAddressRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return DirectionAddressService::class;
    }
}
