<?php

namespace App\Domain\Directories\DirectionAddress;

interface RequestKeysEnum
{
    const ID = 'id';
    const DIRECTION_ID = 'direction_id';
    const DIRECTION_CODE = 'direction_code';

    const EMAIL = 'email';
    const PHONE = 'phone';
    const ADDRESS = 'address';
    const COMMENT = 'comment';
}
