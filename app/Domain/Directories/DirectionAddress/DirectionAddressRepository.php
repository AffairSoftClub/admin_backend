<?php

namespace App\Domain\Directories\DirectionAddress;

use App\Base\Repository\Contracts\Basic\GetOrderedPaginationInterface;
use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Base\GetByIdOrFailTrait;
use App\Domain\Directories\DirectionAddress\Table\DirectionAddressColumnNamesEnum;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * Class DirectionAddressRepository
 * @method create(array $attributes): DirectionAddress
 * @method getByIdOrFail(int $id): DirectionAddress
 * @method update(DirectionAddress $model, array $attributes)
 */
class DirectionAddressRepository extends BaseRepository implements GetOrderedPaginationInterface
{
    use GetByIdOrFailTrait;

    public function __construct(DirectionAddress $model)
    {
        parent::__construct($model);
    }

    public function getOrderedPagination(Request $request): LengthAwarePaginator
    {
        return $this->getModel()
            ->with([
                RelationNamesEnum::DIRECTION,
            ])
            ->orderByDesc(DirectionAddressColumnNamesEnum::ID)
            ->paginate();
    }
}
