<?php

namespace App\Domain\Directories\DirectionAddress;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Shipment\Direction\Direction;
use App\Domain\Shipment\Direction\Table\DirectionColumnNamesEnum as DirectionColumnNamesEnum;
use App\Domain\Directories\DirectionAddress\Table\DirectionAddressColumnNamesEnum;
use App\Domain\Directories\DirectionAddress\Table\NameValue as TableNameValue;

class DirectionAddress extends BaseModel
{
    protected $table = TableNameValue::VALUE;

    protected $fillable = [
        DirectionAddressColumnNamesEnum::DIRECTION_ID,
        DirectionAddressColumnNamesEnum::ADDRESS,
        DirectionAddressColumnNamesEnum::EMAIL,
        DirectionAddressColumnNamesEnum::PHONE,
        DirectionAddressColumnNamesEnum::COMMENT,
    ];

    public function direction()
    {
        return $this->belongsTo(
            Direction::class,
            DirectionAddressColumnNamesEnum::DIRECTION_ID,
            DirectionColumnNamesEnum::ID
        );
    }
}
