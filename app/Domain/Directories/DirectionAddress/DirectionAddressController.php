<?php

namespace App\Domain\Directories\DirectionAddress;

use App\Domain\Base\Http\Controller\BaseStandardDomainEntityController;
use App\Domain\Base\Http\Controller\BaseStandardIndexDomainEntityController;
use App\Domain\Base\Http\Controller\Entity\Index\IndexViewDataKeysEnum;
use App\Domain\Shipment\Direction\DirectionRepository;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use InvalidArgumentException;

class DirectionAddressController extends BaseStandardIndexDomainEntityController
{
    /** @var DirectionAddressService */
    private $directionAddressService;

    /** @var DirectionRepository */
    private $directionRepository;

    public function __construct(
        DirectionAddressService    $directionAddressService,
        DirectionAddressRepository $directionAddressRepository,
        DirectionRepository        $directionRepository
    )
    {
        $this->directionAddressService = $directionAddressService;
        $this->directionRepository = $directionRepository;

        parent::__construct($directionAddressRepository);
    }

    protected function getEntityClassName(): string
    {
        return DirectionAddress::class;
    }


    // Entity > Index
    protected function getIndexViewName(): string
    {
        return 'entities.direction-addresses';
    }

    protected function getIndexReferences(): array
    {
        return [
            'directions' => $this->getDirectionsAllOrderedByName(),
        ];
    }

    private function getDirectionsAllOrderedByName(): Collection
    {
        return $this->directionRepository->getAllOrderedByName();
    }

    ///

    public function store(Request $request)
    {
        try {
            $requestData = $request->all();

            $directionId = $requestData[RequestKeysEnum::DIRECTION_ID];
            $directionCode = trim($requestData[RequestKeysEnum::DIRECTION_CODE]);

            if (!$directionCode and !$directionId) {
                throw new InvalidArgumentException('Выберите существующее направление или задайте новое.');
            }

            $addressId = $requestData[RequestKeysEnum::ID];

            if (!$addressId) {
                $directionAddress = $this->createDirectionByRequestData($requestData);
            } else {
                $directionAddress = $this->updateByRequestData($addressId, $requestData);
            }

            $this->loadRelations($directionAddress);

            return $directionAddress;
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    public function update(DirectionAddress $directionAddress, Request $request)
    {
        try {
            $model = $this->updateByRequestData($directionAddress, $request->all());
            $this->loadRelations($model);

            return $model;
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function createDirectionByRequestData(array $addressAttributes): DirectionAddress
    {
        return $this->directionAddressService->createWithRelationsByRequestData($addressAttributes);
    }

    private function updateByRequestData(DirectionAddress $directionAddress, array $requestData): DirectionAddress
    {
        return $this->directionAddressService->updateWithRelationsByRequestData($directionAddress, $requestData);
    }

    private function loadRelations(DirectionAddress $directionAddress)
    {
        $directionAddress->load(RelationNamesEnum::DIRECTION);
    }

    /**
     * Display the specified resource.
     *
     * @param DirectionAddress $directionAddress
     * @return DirectionAddress
     */
    public function show(DirectionAddress $directionAddress)
    {
        $this->loadRelations($directionAddress);

        return $directionAddress;
    }
}
