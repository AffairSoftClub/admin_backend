<?php

namespace App\Domain\Directories\DirectionAddress;

use App\Domain\Shipment\Direction\Direction;
use App\Domain\Shipment\Direction\DirectionRepository;
use App\Domain\Directories\DirectionAddress\RequestKeysEnum as StoreRequestKeysEnum;
use App\Domain\Directories\DirectionAddress\Table\DirectionAddressColumnNamesEnum;
use App\Helpers\MapperHelper;
use Illuminate\Support\Facades\DB;

class DirectionAddressService
{
    /** @var DirectionAddressRepository */
    private $directionAddressRepository;

    /** @var DirectionRepository */
    private $directionRepository;

    /** @var MapperHelper */
    private $mapperHelper;

    public function __construct(
        DirectionAddressRepository $directionAddressRepository,
        DirectionRepository $directionRepository,
        MapperHelper $mapperHelper
    )
    {
        $this->directionAddressRepository = $directionAddressRepository;
        $this->directionRepository = $directionRepository;
        $this->mapperHelper = $mapperHelper;
    }

    public function createWithRelationsByRequestData(array $requestData): DirectionAddress
    {
        return DB::transaction(function () use ($requestData) {
            $relationAttributes = $this->createRelationsIfRequiredByRequestData($requestData);
            $mappedAttributes = $this->convertRequestDataToDirectionAddressAttributes($requestData);
            $attributes = $relationAttributes + $mappedAttributes;

            return $this->createDirectionAddress($attributes);
        });
    }

    private function createRelationsIfRequiredByRequestData(array $requestData)
    {
        $relationAttributes = [];

        if ($direction = $this->getOrCreateDirectionByRequestDataIfRequired($requestData)) {
            $relationAttributes[DirectionAddressColumnNamesEnum::DIRECTION_ID] = $direction->getKey();
        }

        return $relationAttributes;
    }

    private function getOrCreateDirectionByRequestDataIfRequired(array $requestData): ?Direction
    {
        if (!$directionCode = $requestData[StoreRequestKeysEnum::DIRECTION_CODE]) {
            return null;
        }

        return $this->getOrCreateDirectionByCode($directionCode);
    }

    private function getOrCreateDirectionByCode(string $directionCode): ?Direction
    {
        return $this->directionRepository->getOrCreateByCode($directionCode);
    }

    private function convertRequestDataToDirectionAddressAttributes(array $requestData)
    {
        $mapping = [
            DirectionAddressColumnNamesEnum::DIRECTION_ID => RequestKeysEnum::DIRECTION_ID,
            DirectionAddressColumnNamesEnum::ADDRESS => RequestKeysEnum::ADDRESS,
            DirectionAddressColumnNamesEnum::EMAIL => RequestKeysEnum::EMAIL,
            DirectionAddressColumnNamesEnum::PHONE => RequestKeysEnum::PHONE,
            DirectionAddressColumnNamesEnum::COMMENT => RequestKeysEnum::COMMENT,
        ];

        return $this->getMappedValues($mapping, $requestData);
    }

    private function getMappedValues(array $mapping, array $source)
    {
        return $this->mapperHelper->getMappedValues($mapping, $source);
    }

    private function createDirectionAddress(array $attributes): DirectionAddress
    {
        return $this->directionAddressRepository->create($attributes);
    }

    public function updateWithRelationsByRequestData(DirectionAddress $directionAddress, array $requestData): DirectionAddress
    {
        return DB::transaction(function () use ($directionAddress, $requestData) {
            $relationAttributes = $this->createRelationsIfRequiredByRequestData($requestData);
            $mappedAttributes = $this->convertRequestDataToDirectionAddressAttributes($requestData);
            $attributes = $relationAttributes + $mappedAttributes;

            return $this->updateDirectionAddress($directionAddress, $attributes);
        });
    }

    private function updateDirectionAddress(DirectionAddress $directionAddress, array $attributes): DirectionAddress
    {
        return $this->directionAddressRepository->update($directionAddress, $attributes);
    }

//    public function update(int $id, array $attributes): DirectionAddress
//    {
//        return $this->directionAddressRepository->updateByAttributes($id, $attributes);
//    }
}
