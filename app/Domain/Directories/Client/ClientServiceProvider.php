<?php

namespace App\Domain\Directories\Client;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;
use App\Domain\Directories\Client\Providers\ClientIdBySysIdProvider;

class ClientServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return ClientRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return ClientService::class;
    }

    protected function getOtherSingletons(): array
    {
        $parentValues = parent::getOtherSingletons();
        $classOnlyValues = $this->getClassOnlyOtherSingletonsNames();

        return array_merge($parentValues, $classOnlyValues);
    }

    private function getClassOnlyOtherSingletonsNames(): array
    {
        return [
            ClientIdBySysIdProvider::class,
        ];
    }
}
