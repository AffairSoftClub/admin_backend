<?php

namespace App\Domain\Directories\Client\Services\ByRequestDataClientService;

use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;

interface ClientRequestKeysEnum
{
    const NAME = ClientColumnNamesEnum::NAME;

    const REGISTRATION_NUMBER = ClientColumnNamesEnum::REGISTRATION_NUMBER;
    const VAT_ID = ClientColumnNamesEnum::VAT_ID;

    const PHONE = ClientColumnNamesEnum::PHONE;
    const EMAILS = ClientColumnNamesEnum::EMAILS;

    const LEGAL_ADDRESS = ClientColumnNamesEnum::LEGAL_ADDRESS;
    const POSTAL_ADDRESS = ClientColumnNamesEnum::POSTAL_ADDRESS;

    const IS_ARCHIVED = ClientColumnNamesEnum::IS_ARCHIVED;
}
