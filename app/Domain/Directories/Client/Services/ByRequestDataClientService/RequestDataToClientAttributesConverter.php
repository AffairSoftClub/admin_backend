<?php

namespace App\Domain\Directories\Client\Services\ByRequestDataClientService;

use App\Domain\Base\Http\Service\RequestDataConverter\ToEntityAttributes\BaseRequestDataToEntityAttributesConverter;
use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;

class RequestDataToClientAttributesConverter extends BaseRequestDataToEntityAttributesConverter
{
    protected function getMapping(): array
    {
        return [
            ClientColumnNamesEnum::NAME => ClientRequestKeysEnum::NAME,

            ClientColumnNamesEnum::REGISTRATION_NUMBER => ClientRequestKeysEnum::REGISTRATION_NUMBER,
            ClientColumnNamesEnum::VAT_ID => ClientRequestKeysEnum::VAT_ID,

            ClientColumnNamesEnum::PHONE => ClientRequestKeysEnum::PHONE,
            ClientColumnNamesEnum::EMAILS => ClientRequestKeysEnum::EMAILS,

            ClientColumnNamesEnum::LEGAL_ADDRESS => ClientRequestKeysEnum::LEGAL_ADDRESS,
            ClientColumnNamesEnum::POSTAL_ADDRESS => ClientRequestKeysEnum::POSTAL_ADDRESS,

            ClientColumnNamesEnum::IS_ARCHIVED => ClientRequestKeysEnum::IS_ARCHIVED
        ];
    }

    protected function getCalculatedValues(array $source): array
    {
        return [];
    }
}
