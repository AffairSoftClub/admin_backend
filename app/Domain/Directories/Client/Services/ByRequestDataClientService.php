<?php

namespace App\Domain\Directories\Client\Services;

use App\Domain\Directories\Client\Client;
use App\Domain\Directories\Client\ClientService;
use App\Domain\Directories\Client\Services\ByRequestDataClientService\RequestDataToClientAttributesConverter;

class ByRequestDataClientService
{
    /** @var RequestDataToClientAttributesConverter */
    private $requestDataToClientAttributesConverter;

    /** @var ClientService */
    private $clientService;

    public function __construct(
        RequestDataToClientAttributesConverter $requestDataToClientAttributesConverter,
        ClientService $clientService
    )
    {
        $this->requestDataToClientAttributesConverter = $requestDataToClientAttributesConverter;
        $this->clientService = $clientService;
    }

    public function create(array $requestData): Client
    {
        $clientAttributes = $this->getClientAttributesByRequestData($requestData);

        return $this->createClientByAttributes($clientAttributes);
    }

    private function getClientAttributesByRequestData(array $requestData)
    {
        return $this->requestDataToClientAttributesConverter->convert($requestData);
    }

    private function createClientByAttributes(array $clientAttributes): Client
    {
        return $this->clientService->create($clientAttributes);
    }

    public function update(Client $client, array $requestData): Client
    {
        $clientAttributes = $this->getClientAttributesByRequestData($requestData);

        return $this->updateClientByAttributes($client, $clientAttributes);
    }

    private function updateClientByAttributes(Client $client, array $clientAttributes): Client
    {
        return $this->clientService->update($client, $clientAttributes);
    }
}
