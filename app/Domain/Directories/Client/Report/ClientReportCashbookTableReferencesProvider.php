<?php

namespace App\Domain\Directories\Client\Report;

use App\Domain\Base\Http\Controller\Entity\Index\ReferencesProviderInterface;
use App\Domain\Cashbook\MoneyAccount\General\GeneralMoneyAccountRepository;
use App\Domain\Cashbook\MoneyTransferType\Category\MoneyTransferTypeCategoryRepository;
use App\Domain\Cashbook\MoneyTransferType\Subcategory\MoneyTransferTypeSubcategoryRepository;
use App\Domain\Cashbook\MoneyTransferType\Type\General\GeneralMoneyTransferTypeRepository;
use App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository;
use App\Domain\Directories\Vehicle\Children\Trailer\TrailerRepository;
use Illuminate\Database\Eloquent\Collection;

class ClientReportCashbookTableReferencesProvider implements ReferencesProviderInterface
{
    /** @var GeneralMoneyAccountRepository */
    private $generalMoneyAccountRepository;

    /** @var GeneralMoneyTransferTypeRepository */
    private $generalMoneyTransferTypeRepository;

    /** @var \App\Domain\Cashbook\MoneyTransferType\MoneyTransferTypeCategoryRepository */
    private $moneyTransferTypeCategoryRepository;

    /** @var MoneyTransferTypeSubcategoryRepository */
    private $moneyTransferTypeSubcategoryRepository;

    /** @var \App\Domain\Directories\Vehicle\Children\Car\Base\BaseCarRepository */
    private $carRepository;

    /** @var TrailerRepository */
    private $trailerRepository;

    public function __construct(
        GeneralMoneyAccountRepository      $generalMoneyAccountRepository,
        GeneralMoneyTransferTypeRepository $generalMoneyTransferTypeRepository,
        MoneyTransferTypeCategoryRepository $moneyTransferTypeCategoryRepository,
        MoneyTransferTypeSubcategoryRepository $moneyTransferTypeSubcategoryRepository,
        BaseCarRepository $carRepository,
        TrailerRepository $trailerRepository
    )
    {
        $this->generalMoneyAccountRepository = $generalMoneyAccountRepository;
        $this->generalMoneyTransferTypeRepository = $generalMoneyTransferTypeRepository;
        $this->moneyTransferTypeCategoryRepository = $moneyTransferTypeCategoryRepository;
        $this->moneyTransferTypeSubcategoryRepository = $moneyTransferTypeSubcategoryRepository;
        $this->carRepository = $carRepository;
        $this->trailerRepository = $trailerRepository;
    }

    public function getReferences(): array
    {
        return [
            'moneyAccounts' => $this->getMoneyAccounts(),
            'generalMoneyTransferTypes' => $this->getGeneralMoneyTransferTypes(),
            'moneyTransferTypeCategories' => $this->getMoneyTransferTypeCategories(),
            'moneyTransferTypeSubcategories' => $this->getMoneyTransferTypeSubcategories(),
            'cars' => $this->getCars(),
            'trailers' => $this->getTrailers(),
        ];
    }

    private function getMoneyAccounts(): Collection
    {
        return $this->generalMoneyAccountRepository->getAllOrderedByName();
    }

    private function getGeneralMoneyTransferTypes(): Collection
    {
        return $this->generalMoneyTransferTypeRepository->getAllOrderedByName();
    }

    private function getMoneyTransferTypeCategories(): array
    {
        return $this->moneyTransferTypeCategoryRepository->getAll();
    }

    private function getMoneyTransferTypeSubcategories(): array
    {
        return $this->moneyTransferTypeSubcategoryRepository->getAll();
    }

    private function getCars(): Collection
    {
        return $this->carRepository->getAllOrderedByName();
    }

    private function getTrailers(): Collection
    {
        return $this->trailerRepository->getAllOrderedByName();
    }

}
