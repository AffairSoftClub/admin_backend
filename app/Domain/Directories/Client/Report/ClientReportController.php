<?php

namespace App\Domain\Directories\Client\Report;

use App\Base\TypeCaster\TypeCasterInterface;
use App\Domain\Base\Http\Controller\BaseIndexFilteredEntityController;
use App\Domain\Base\Http\Controller\Entity\ShowEntityControllerTrait;
use App\Domain\Base\Model\BaseModel;
use App\Domain\Directories\Client\Client;
use App\Domain\Directories\Client\ClientRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * @method ClientRepository getEntityService()
 */
class ClientReportController extends BaseIndexFilteredEntityController
{
    use ShowEntityControllerTrait;

    /** @var ClientReportCashbookTableReferencesProvider */
    private $clientReportCashbookTableReferencesProvider;

    public function __construct(
        ClientReportCashbookTableReferencesProvider $clientReportCashbookTableReferencesProvider,
        TypeCasterInterface $typeCaster,
        ClientRepository $entityRepository
    )
    {
        $this->clientReportCashbookTableReferencesProvider = $clientReportCashbookTableReferencesProvider;

        parent::__construct($typeCaster, $entityRepository);
    }

    // Entity > Index
    protected function getIndexFilterKeys(): array
    {
        return [];
    }

    protected function getIndexViewName(): string
    {
        return 'reports.clients.index';
    }

    protected function getIndexPagination(Request $request): LengthAwarePaginator
    {
        $clientRepository = $this->getEntityService();

        return $clientRepository->getWithCashbookTotalsPagination($request);
    }

    // Entity > Show
    public function show(Client $client, Request $request)
    {
        return $this->showByEntity($client, $request);
    }

    protected function loadFilteredRelations(BaseModel $model, array $filter)
    {
        // TODO: Implement loadFilteredRelations() method.
    }

    protected function getShowViewName(): string
    {
        return 'reports.clients.show';
    }

    /**
     * @param BaseModel|Client $model
     * @param array $filter
     * @return mixed
     */
    protected function getShowPagination(BaseModel $model, array $filter)
    {
        return $model->cashbookItemsPagination($filter);
    }

    protected function getShowReferences(): array
    {
        return $this->clientReportCashbookTableReferencesProvider->getReferences();
    }
}
