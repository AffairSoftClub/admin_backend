<?php

namespace App\Domain\Directories\Client;

interface ClientRelationColumnNamesEnum
{
    const SHIPMENTS = 'shipments';
    const CASHBOOK_ITEMS = 'cashbookItems';
}
