<?php

namespace App\Domain\Directories\Client\Validators\DeleteAllowed;

use App\Base\Validation\ThrowValidationExceptionTrait;
use Illuminate\Validation\ValidationException;

abstract class BaseClientDeleteAllowedValidator
{
    use ThrowValidationExceptionTrait;

    public function validate(int $clientId)
    {
        $isValid = $this->isValid($clientId);

        if ($isValid) {
            return;
        }

        $this->throwClientValidationException($clientId);
    }

    abstract protected function isValid(int $clientId): bool;

    protected function throwClientValidationException(int $clientId)
    {
        $message = $this->getExceptionMessage($clientId);

        $this->throwValidationException(['client' => $message]);
    }

    abstract protected function getExceptionMessage(int $clientId);
}
