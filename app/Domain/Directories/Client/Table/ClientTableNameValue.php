<?php

namespace App\Domain\Directories\Client\Table;

interface ClientTableNameValue
{
    const VALUE = 'clients';
}
