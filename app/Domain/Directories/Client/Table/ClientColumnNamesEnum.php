<?php

namespace App\Domain\Directories\Client\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface ClientColumnNamesEnum extends IdTimestampsInterface
{
    const NAME = 'name';

    const REGISTRATION_NUMBER = 'registration_number';
    const VAT_ID = 'vat_id';

    const PHONE = 'phone';
    const EMAILS = 'emails';

    const LEGAL_ADDRESS = 'legal_address';
    const POSTAL_ADDRESS = 'postal_address';

    const IS_ARCHIVED = 'is_archived';

    const SYS_ID = 'sys_id';
}
