<?php

namespace App\Domain\Directories\Client;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Contracts\Basic\GetOrderedPaginationInterface;
use App\Base\Repository\Traits\Advanced\Base\GetAsMappingTrait;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Domain\Cashbook\CashbookItem\Types\Base\BaseCashbookItem;
use App\Domain\Cashbook\CashbookItem\Types\Base\Table\BaseCashbookItemColumnNamesEnum;
use App\Domain\Common\EntityTableNamesProvider;
use App\Domain\Directories\Client\Seeds\ClientSysIdsEnum;
use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ClientRepository extends BaseRepository implements GetOrderedPaginationInterface
{
    use GetAllOrderedByNameTrait, GetAsMappingTrait;

    /** @var EntityTableNamesProvider */
    private $entityTableNamesProvider;

    public function __construct(
        Client                   $model,
        EntityTableNamesProvider $entityTableNamesProvider
    )
    {
        $this->entityTableNamesProvider = $entityTableNamesProvider;

        parent::__construct($model);
    }

    protected function getNameColumnName(): string
    {
        return ClientColumnNamesEnum::NAME;
    }

    public function getOrderedPagination(Request $request): LengthAwarePaginator
    {
        return $this->getModel()
            ->filter($request->all())
            ->orderBy(ClientColumnNamesEnum::IS_ARCHIVED)
            ->orderBy(ClientColumnNamesEnum::NAME)
            ->paginateFilter();
    }

    public function getWithCashbookTotalsPagination(Request $request): LengthAwarePaginator
    {
        $clientTableName = $this->getClientTableName();
        $cashbookTableName = $this->getCashbookTableName();
        $cashbookTotalAmountColumnName = BaseCashbookItemColumnNamesEnum::TOTAL_AMOUNT;

        return $this->getModel()
            ->mergeCasts([
                'total_amount' => 'float',
            ])
            ->filter($request->all())
            ->leftJoin(
                $cashbookTableName,
                $clientTableName . '.' . ClientColumnNamesEnum::ID,
                '=',
                $cashbookTableName . '.' . BaseCashbookItemColumnNamesEnum::CLIENT_ID
            )
            ->groupBy($clientTableName . '.' . ClientColumnNamesEnum::ID)
            ->select([
                $clientTableName . '.' . ClientColumnNamesEnum::ID,
                $clientTableName . '.' . ClientColumnNamesEnum::NAME,
            ])
            ->selectRaw("SUM({$cashbookTotalAmountColumnName}) as total_amount")
            ->orderBy(ClientColumnNamesEnum::IS_ARCHIVED)
            ->orderBy(ClientColumnNamesEnum::NAME)
            ->paginateFilter();
    }

    private function getClientTableName(): string
    {
        return $this->getTableNameByModelClassName(Client::class);
    }

    private function getCashbookTableName(): string
    {
        return $this->getTableNameByModelClassName(BaseCashbookItem::class);
    }

    private function getTableNameByModelClassName(string $className): string
    {
        return $this->entityTableNamesProvider->getForModel($className);
    }

    public function getNotArchivedOrderedByName(): Collection
    {
        $nameColumn = $this->getNameColumnName();

        return $this->getModel()
            ->where(ClientColumnNamesEnum::IS_ARCHIVED, false)
            ->orderBy($nameColumn)->get();
    }

    public function getDkvName(): string
    {
        return $this->getNameBySysId(ClientSysIdsEnum::DKV);
    }

    private function getNameBySysId(int $sysId): string
    {
        return $this->getModel()
            ->where(ClientColumnNamesEnum::SYS_ID, $sysId)
            ->value(ClientColumnNamesEnum::NAME);
    }

    public function getSysIdToIdMapping(): array
    {
        return $this->getAsMapping(ClientColumnNamesEnum::SYS_ID, ClientColumnNamesEnum::ID);
    }

    public function getNameToIdMapping(): array
    {
        return $this->getAsMapping(ClientColumnNamesEnum::NAME, ClientColumnNamesEnum::ID);
    }
}
