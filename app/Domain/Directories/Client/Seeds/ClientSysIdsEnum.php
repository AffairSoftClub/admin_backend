<?php

namespace App\Domain\Directories\Client\Seeds;

interface ClientSysIdsEnum
{
    const DKV = 1;
    const ALLIANCE = 2;
}
