<?php

namespace App\Domain\Directories\Client\Seeds;

use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;

interface ClientSeedsValues
{
    const VALUES = [
        // With sys_id
        [
            ClientColumnNamesEnum::NAME => 'DKV EURO SERVICE GmbH + Co. KG',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => 'DE119375450',
            ClientColumnNamesEnum::LEGAL_ADDRESS => 'Balcke-Dürr-Allee 3, D-40882 Ratingen',
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => '+49 2102 5518-0',
            ClientColumnNamesEnum::EMAILS => ['info@dkv-euroservice.loc'],
            ClientColumnNamesEnum::SYS_ID => ClientSysIdsEnum::DKV,
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],

        [
            ClientColumnNamesEnum::NAME => 'PRA Pan European Roadtransport Alliance B.V.',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => 'NL8170.48.273.B01',
            ClientColumnNamesEnum::LEGAL_ADDRESS => 'Postfach 31309 6503 CH Nijmegen Netherlands',
            ClientColumnNamesEnum::POSTAL_ADDRESS => 'P.O. BOX 31309 6503 CH Nijmegen Netherlands',
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['alliance@dkv-euroservice.loc'],
            ClientColumnNamesEnum::SYS_ID => ClientSysIdsEnum::ALLIANCE,
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],

        // Without sys_id
        [
            ClientColumnNamesEnum::NAME => ClientNamesEnum::MONDIAL_AIRLINE_SERVICES_GMBH,
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '239595541',
            ClientColumnNamesEnum::VAT_ID => 'DE239595541',
            ClientColumnNamesEnum::LEGAL_ADDRESS => 'Kurhessenstr.11 D-64546 Mörfelden- Walldorf',
            ClientColumnNamesEnum::POSTAL_ADDRESS => 'Kurhessenstr.11 D-64546 Mörfelden- Walldorf',
            ClientColumnNamesEnum::PHONE => '+49610529720',
            ClientColumnNamesEnum::EMAILS => ['mail1@mondial.loc', 'mail2@mondial.loc', 'mail3@mondial.loc']
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_YES_MATCH
        ],

        [
            ClientColumnNamesEnum::NAME => 'Airborne International GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '245126537',
            ClientColumnNamesEnum::VAT_ID => 'DE245126537',
            ClientColumnNamesEnum::LEGAL_ADDRESS => 'Cargo City Süd – Geb. 556 D-60549 Frankfurt',
            ClientColumnNamesEnum::POSTAL_ADDRESS => 'Main – Flughafen',
            ClientColumnNamesEnum::PHONE => '+49 69 69715560',
            ClientColumnNamesEnum::EMAILS => ['email@airborne.loc'],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],

        [
            ClientColumnNamesEnum::NAME => 'VIP WALLENBORN GmbH Internationalle Spedition',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '239595541',
            ClientColumnNamesEnum::VAT_ID => 'DE114226479',
            ClientColumnNamesEnum::LEGAL_ADDRESS => 'Flughafen, Cargo City Süd, Geb. 532 DE, 60549 Frankfurtt',
            ClientColumnNamesEnum::POSTAL_ADDRESS => 'Kurhessenstr.11 D-64546 Mörfelden- Walldorf',
            ClientColumnNamesEnum::PHONE => '+496969022341',
            ClientColumnNamesEnum::EMAILS => ['mail@vip_wallenborn.loc'],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],

        [
            ClientColumnNamesEnum::NAME => ClientNamesEnum::SWT,
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => [],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],
        [
            ClientColumnNamesEnum::NAME => 'XPO Logistics',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => [],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],
        [
            ClientColumnNamesEnum::NAME => 'Gepe Service aircargotrucking GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => [],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],
        [
            ClientColumnNamesEnum::NAME => 'Raiffeisen-Leasing,s.r.o.',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '61467863',
            ClientColumnNamesEnum::VAT_ID => 'CZ61467863',
            ClientColumnNamesEnum::LEGAL_ADDRESS => 'Hvězdova 1716/2b 140 78 Praha 4',
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => '+420 221 511 611',
            ClientColumnNamesEnum::EMAILS => [],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],
        [
            ClientColumnNamesEnum::NAME => 'DK Transporte',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => '/',
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => [],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],
        [
            ClientColumnNamesEnum::NAME => ClientNamesEnum::SWWA,
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '10',
            ClientColumnNamesEnum::VAT_ID => '10',
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@swwa.loc', 'email2@swwa.loc', 'email3@swwa.loc'],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],
        [
            ClientColumnNamesEnum::NAME => 'EHD',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '11',
            ClientColumnNamesEnum::VAT_ID => '11',
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => [],
//            ClientColumnNamesEnum::ADDRESS_MATCH => Client::STATUS_NO_MATCH
        ],
        [
            ClientColumnNamesEnum::NAME => 'Skymovers',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '9',
            ClientColumnNamesEnum::VAT_ID => '9',
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => [],
            ClientColumnNamesEnum::IS_ARCHIVED => true,
        ],
        [
            ClientColumnNamesEnum::NAME => ClientNamesEnum::ATH,
            ClientColumnNamesEnum::REGISTRATION_NUMBER => '9',
            ClientColumnNamesEnum::VAT_ID => '12',
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@ath.loc', 'email2@ath.loc', 'email3@ath.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => true,
        ],

        // For shipping seeder by Excel file
        [
            ClientColumnNamesEnum::NAME => 'Schenker Deutschland AG',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@sdag.loc', 'email2@sdag.loc', 'email3@sdag.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'SMS Spedition Management Service GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@sms-sms.loc', 'email2@sms-sms.loc', 'email3@sms-sms.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        //SIA Consulting GmbH
        [
            ClientColumnNamesEnum::NAME => 'SIA Consulting GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@siac.loc', 'email2@siac.loc', 'email3@siac.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'Silk Way West Airlines',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@swwa.loc', 'email2@swwa.loc', 'email3@swwa.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'SM-Log.UG',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@swwa.loc', 'email2@swwa.loc', 'email3@swwa.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'Aircargo trucking & handling GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@ath.loc', 'email2@ath.loc', 'email3@ath.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'Trans Sib ATC Air Service GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@tsaas.loc', 'email2@tsaas.loc', 'email3@tsaas.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'TLS Cargo GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@tlsc.loc', 'email2@tlsc.loc', 'email3@tlsc.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'Chapman Freeborn Airmarketing GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@cfa.loc', 'email2@cfa.loc', 'email3@cfa.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'RUNAIR GmbH',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@runair.loc', 'email2@runair.loc', 'email3@runair.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],

        [
            ClientColumnNamesEnum::NAME => 'Neutral Airfreight Consultants GmbH i.G.',
            ClientColumnNamesEnum::REGISTRATION_NUMBER => null,
            ClientColumnNamesEnum::VAT_ID => null,
            ClientColumnNamesEnum::LEGAL_ADDRESS => null,
            ClientColumnNamesEnum::POSTAL_ADDRESS => null,
            ClientColumnNamesEnum::PHONE => null,
            ClientColumnNamesEnum::EMAILS => ['email1@nacg.loc', 'email2@nacg.loc', 'email3@nacg.loc'],
            ClientColumnNamesEnum::IS_ARCHIVED => false,
        ],
    ];
}
