<?php

namespace App\Domain\Directories\Client\Seeds;

interface ClientNamesEnum
{
    const MONDIAL_AIRLINE_SERVICES_GMBH = 'Mondial Airline Services GmbH'; // 1
    const SWT = 'SWT'; //4
    const SWWA = 'SWWA'; //10
    const ATH = 'ATH'; //13
}
