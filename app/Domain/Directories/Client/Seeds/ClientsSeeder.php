<?php

namespace App\Domain\Directories\Client\Seeds;

use App\Domain\Directories\Client\ClientRepository;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class ClientsSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(ClientRepository $buyerRepository)
    {
        parent::__construct($buyerRepository);
    }

    protected function getSeeds(): array
    {
        return ClientSeedsValues::VALUES;
    }
}
