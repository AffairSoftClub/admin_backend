<?php

namespace App\Domain\Directories\Client;

use App\Base\TypeCaster\TypeCasterInterface;
use App\Domain\Base\Http\Controller\BaseIndexFilteredEntityController;
use App\Domain\Directories\Client\Services\ByRequestDataClientService;
use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;
use Exception;
use Illuminate\Http\Request;

class ClientController extends BaseIndexFilteredEntityController
{
    /** @var ByRequestDataClientService */
    private $byRequestDataClientService;

    /** @var ClientService */
    private $clientService;

    public function __construct(
        ByRequestDataClientService $byRequestDataClientService,
        ClientService $clientService,
        TypeCasterInterface $typeCaster,
        ClientRepository $clientRepository
    )
    {
        $this->byRequestDataClientService = $byRequestDataClientService;
        $this->clientService = $clientService;

        parent::__construct($typeCaster, $clientRepository);
    }

    protected function getEntityClassName(): string
    {
        return Client::class;
    }

    // Entity > Index
    protected function getIndexViewName(): string
    {
        return 'directories.clients';
    }

    ///

    public function store(Request $request)
    {
        try {
            $requestData = $request->all();

            return $this->createClientByRequestData($requestData);
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function createClientByRequestData(array $requestData)
    {
        return $this->byRequestDataClientService->create($requestData);
    }

    public function show(Client $client)
    {
        return $client;
    }

    public function update(Client $client, Request $request)
    {
        try {
            $requestData = $request->all();

            return $this->updateClientByRequestData($client, $requestData);
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    private function updateClientByRequestData(Client $client, array $requestData): Client
    {
        return $this->byRequestDataClientService->update($client, $requestData);
    }

    public function destroy(Client $client)
    {
        try {
            $this->deleteClientIfAllowed($client);

            return $client->getKey();
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }

    public function deleteClientIfAllowed(Client $client)
    {
        return $this->clientService->deleteIfAllowed($client);
    }

    protected function getIndexFilterKeys(): array
    {
        return [
            ClientColumnNamesEnum::NAME,
            ClientColumnNamesEnum::VAT_ID,
        ];
    }
}
