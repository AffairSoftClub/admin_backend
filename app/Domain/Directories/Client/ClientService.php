<?php

namespace App\Domain\Directories\Client;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Base\Model\BaseService;
use App\Domain\Directories\Client\Validators\ClientDeleteAllowedValidator;
use App\Domain\Log\EntityTypeIdsEnum;
use App\Domain\Log\LoggerService;
use App\Domain\Log\OperationTypeIdsEnum;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientService
 * @package App\Domain\Directories\Client
 */
class ClientService extends BaseService
{
    const ENTITY_TYPE_ID = EntityTypeIdsEnum::CLIENT;

    private $clientDeleteAllowedValidator;

    public function __construct(
        ClientDeleteAllowedValidator $clientDeleteAllowedValidator,
        ClientRepository $repository
    )
    {
        $this->clientDeleteAllowedValidator = $clientDeleteAllowedValidator;

        parent::__construct($repository);
    }

    public function deleteIfAllowed(Client $client)
    {
        $this->validateClientDeleteAllowed($client);

        $this->deleteClient($client);
    }

    private function validateClientDeleteAllowed(Client $client)
    {
        $this->clientDeleteAllowedValidator->validate($client);
    }

    private function deleteClient(Client $client)
    {
        $this->repository->deleteByModel($client);
    }

    /**
     * @param array $attributes
     * @return Model|Client
     */
    public function create(array $attributes = []): Model
    {
        /** @var Client $client */
        $client = parent::create($attributes);

        return $client;
    }

    /**
     * @param Model|Client $model
     * @param array $attributes
     * @return Model|Client
     */
    public function update(Model $model, array $attributes): Model
    {
        /** @var Client $client */
        $client = parent::update($model, $attributes);
//        $this->logClientUpdate($client);

        return $client;
    }
}
