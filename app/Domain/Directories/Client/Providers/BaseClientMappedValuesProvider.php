<?php

namespace App\Domain\Directories\Client\Providers;

use App\Domain\Base\Model\BaseEntityMappedValuesProvider;
use App\Domain\Directories\Client\ClientRepository;

/**
 * @property ClientRepository $internalProvider
 */
abstract class BaseClientMappedValuesProvider extends BaseEntityMappedValuesProvider
{
    public function __construct(ClientRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    protected function getEntityName(): string
    {
        return 'Client';
    }
}
