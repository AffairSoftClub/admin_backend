<?php

namespace App\Domain\Directories\Client\Providers;

use App\Domain\Directories\Client\ClientRepository;

/**
 * @property ClientRepository $internalProvider
 */
class ClientIdBySysIdProvider extends BaseClientMappedValuesProvider
{
    protected function getCacheValues(): array
    {
        return $this->internalProvider->getSysIdToIdMapping();
    }

    public function getIdBySysId(int $clientSysId): int
    {
        return $this->getByKeyOrFail($clientSysId);
    }

    protected function getKeyAttributeName(): string
    {
        return 'sys_id';
    }
}
