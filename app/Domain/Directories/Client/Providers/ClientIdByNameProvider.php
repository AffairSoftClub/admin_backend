<?php

namespace App\Domain\Directories\Client\Providers;

class ClientIdByNameProvider extends BaseClientMappedValuesProvider
{
    protected function getCacheValues(): array
    {
        return $this->internalProvider->getNameToIdMapping();
    }

    public function getIdByName(string $clientName): int
    {
        return $this->getByKeyOrFail($clientName);
    }

    protected function getKeyAttributeName(): string
    {
        return 'name';
    }
}
