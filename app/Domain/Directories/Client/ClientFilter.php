<?php

namespace App\Domain\Directories\Client;

use App\Domain\Base\Http\Filter\BaseFilter;
use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;

class ClientFilter extends BaseFilter
{
    public function name(string $name)
    {
        $this->whereLike(ClientColumnNamesEnum::NAME, $name);
    }

    public function vatId(string $vatId)
    {
        $this->whereLike(ClientColumnNamesEnum::VAT_ID, $vatId);
    }
}
