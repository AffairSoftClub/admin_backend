<?php

namespace App\Domain\Directories\Client;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Cashbook\CashbookItem\Types\Base\BaseCashbookItem;
use App\Domain\Cashbook\CashbookItem\Types\Base\Table\BaseCashbookItemColumnNamesEnum;
use App\Domain\Directories\Client\Table\ClientColumnNamesEnum;
use App\Domain\Directories\Client\Table\ClientTableNameValue;
use App\Domain\Shipment\Shipment;
use App\Domain\Shipment\Table\ShipmentColumnNamesEnum;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends BaseModel
{
    use Filterable;

    protected $table = ClientTableNameValue::VALUE;

    protected $casts = [
        ClientColumnNamesEnum::IS_ARCHIVED => 'boolean',
        ClientColumnNamesEnum::EMAILS => 'array',
    ];

    protected $fillable = [
        ClientColumnNamesEnum::NAME,

        ClientColumnNamesEnum::REGISTRATION_NUMBER,
        ClientColumnNamesEnum::VAT_ID,

        ClientColumnNamesEnum::PHONE,
        ClientColumnNamesEnum::EMAILS,

        ClientColumnNamesEnum::LEGAL_ADDRESS,
        ClientColumnNamesEnum::POSTAL_ADDRESS,

        ClientColumnNamesEnum::IS_ARCHIVED,
        ClientColumnNamesEnum::SYS_ID,
    ];

    // Filter
    public function modelFilter()
    {
        return $this->provideFilter(ClientFilter::class);
    }


    // Relations

    // Relations > Shipments
    public function shipments(): HasMany
    {
        return $this->hasMany(
            Shipment::class,
            ShipmentColumnNamesEnum::CLIENT_ID,
            ClientColumnNamesEnum::ID
        );
    }

    /**
     * @return Collection|Shipment[]
     */
    public function getShipments(): Collection
    {
        return $this[ClientRelationColumnNamesEnum::SHIPMENTS];
    }

    // Relations > CashbookItems
    public function cashbookItems(): HasMany
    {
        return $this->hasMany(
            BaseCashbookItem::class,
            BaseCashbookItemColumnNamesEnum::CLIENT_ID,
            ClientColumnNamesEnum::ID
        );
    }

    public function getCashbookItems(): Collection
    {
        return $this[ClientRelationColumnNamesEnum::CASHBOOK_ITEMS];
    }

    public function cashbookItemsPagination(array $filter)
    {
        $relations = $this->getCashbookItemsPaginationRelationNames();

        return $this->cashbookItems()
            ->orderByDesc(BaseCashbookItemColumnNamesEnum::ID)
            ->filter($filter)
            ->with($relations)
            ->paginateFilter();
    }

    private function getCashbookItemsPaginationRelationNames(): array
    {
        return [];
    }

    // Getters/Setters
    // Getters/Setters > Name
    public function getName(): string
    {
        return $this->getAttribute(ClientColumnNamesEnum::NAME);
    }

    public function setName(string $name)
    {
        $this->setAttribute(ClientColumnNamesEnum::NAME, $name);
    }

    // Getters/Setters > RegistrationNumber
    public function getRegistrationNumber(): string
    {
        return $this->getAttribute(ClientColumnNamesEnum::REGISTRATION_NUMBER);
    }

    public function setRegistrationNumber(?string $registrationNumber)
    {
        $this->setAttribute(ClientColumnNamesEnum::REGISTRATION_NUMBER, $registrationNumber);
    }

    // Getters/Setters > VatId
    public function getVatId(): ?string
    {
        return $this->getAttribute(ClientColumnNamesEnum::VAT_ID);
    }

    public function setVatId(?string $vatId)
    {
        $this->setAttribute(ClientColumnNamesEnum::VAT_ID, $vatId);
    }

    // Getters/Setters > Phone
    public function getPhone(): ?string
    {
        return $this->getAttribute(ClientColumnNamesEnum::PHONE);
    }

    public function setPhone(?string $phone)
    {
        $this->setAttribute(ClientColumnNamesEnum::PHONE, $phone);
    }

    // Getters/Setters > Emails
    public function getEmails(): ?array
    {
        return (array)$this->getAttribute(ClientColumnNamesEnum::EMAILS);
    }

    public function setEmails(?array $emails)
    {
        $this->setAttribute(ClientColumnNamesEnum::EMAILS, $emails);
    }


    // Getters/Setters > LegalAddress
    public function getLegalAddress(): ?string
    {
        return $this->getAttribute(ClientColumnNamesEnum::LEGAL_ADDRESS);
    }

    public function setLegalAddress(?string $legalAddress)
    {
        $this->setAttribute(ClientColumnNamesEnum::LEGAL_ADDRESS, $legalAddress);
    }

    // Getters/Setters > PostalAddress
    public function getPostalAddress(): ?string
    {
        return $this->getAttribute(ClientColumnNamesEnum::POSTAL_ADDRESS);
    }

    public function setPostalAddress(?string $postalAddress)
    {
        $this->setAttribute(ClientColumnNamesEnum::POSTAL_ADDRESS, $postalAddress);
    }

    // Getters/Setters > IsArchived
    public function getIsArchived(): bool
    {
        return $this->getAttribute(ClientColumnNamesEnum::IS_ARCHIVED);
    }

    public function setIsArchived(bool $isArchived)
    {
        $this->setAttribute(ClientColumnNamesEnum::IS_ARCHIVED, $isArchived);
    }
}
