<?php

namespace App\Domain\Directories;

use App\Domain\Base\ServiceProvider\BaseAggregateDomainServiceProvider;
use App\Domain\Directories\Client\ClientServiceProvider;
use App\Domain\Directories\Country\CountryServiceProvider;
use App\Domain\Directories\DirectionAddress\DirectionAddressServiceProvider;
use App\Domain\Directories\Vehicle\VehicleServiceProvider;

class DirectoriesServiceProvider extends BaseAggregateDomainServiceProvider
{
    protected function getServiceProvidersClassNames(): array
    {
        return [
            ClientServiceProvider::class,
            CountryServiceProvider::class,
            DirectionAddressServiceProvider::class,
            VehicleServiceProvider::class,
        ];
    }
}
