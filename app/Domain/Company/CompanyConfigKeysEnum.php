<?php

namespace App\Domain\Company;

interface CompanyConfigKeysEnum
{
    // Company params
    const LOGO = 'logo';
    const NAME = 'name';
    const EMAILS = 'emails';

    // Invoice default values
    const DEFAULT_EMAILS_SUBJECT_TEMPLATE = 'default_emails_subject_template';
}
