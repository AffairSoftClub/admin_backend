<?php

namespace App\Domain\Company;

use App\Base\BaseConfig;

class CompanyConfig extends BaseConfig
{
    public function getLogoRelativePath(): string
    {
        return $this->getConfigValueByKey(CompanyConfigKeysEnum::LOGO);
    }

    public function getName(): string
    {
        return $this->getConfigValueByKey(CompanyConfigKeysEnum::NAME);
    }

    public function getEmails(): array
    {
        return $this->getConfigValueByKey(CompanyConfigKeysEnum::EMAILS);
    }

    protected function getConfigPath(): string
    {
        return 'company';
    }
}
