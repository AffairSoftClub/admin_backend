<?php

namespace App\Domain\Company;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class CompanyServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return null;
    }

    protected function getServiceClassName(): ?string
    {
        return CompanyConfig::class;
    }
}
