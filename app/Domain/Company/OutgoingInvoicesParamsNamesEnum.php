<?php

namespace App\Domain\Company;

interface OutgoingInvoicesParamsNamesEnum
{
    const VAT_PERCENTS = 'vat_percents';
    const PAYMENT_PERIODS_DAYS = 'payment_periods_days';
}
