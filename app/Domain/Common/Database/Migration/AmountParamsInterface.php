<?php

namespace App\Domain\Common\Database\Migration;

interface AmountParamsInterface
{
    const TOTAL = 9;
    const PLACES = 2;
}
