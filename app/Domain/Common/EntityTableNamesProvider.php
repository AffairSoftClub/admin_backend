<?php

namespace App\Domain\Common;

use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;

class EntityTableNamesProvider
{
    private $modelClassNameToTableNameMap = [];

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getForModel(string $className): string
    {
        $cache = &$this->modelClassNameToTableNameMap;

        if (!array_key_exists($className, $cache)) {
            $cache[$className] = $this->getForModelReal($className);
        }

        return $cache[$className];
    }

    private function getForModelReal(string $className): string
    {
        $model = $this->getModelByClassName($className);

        return $model->getTable();
    }

    private function getModelByClassName(string $className): Model
    {
        return $this->container->make($className);
    }
}
