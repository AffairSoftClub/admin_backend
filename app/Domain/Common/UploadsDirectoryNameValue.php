<?php

namespace App\Domain\Common;

interface UploadsDirectoryNameValue
{
    const VALUE = 'uploads';
}
