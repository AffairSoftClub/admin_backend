<?php

namespace App\Domain\Common\Http\Controller\Index;

/**
 * Interface JsCallbacksEnum
 * @package App\Domain\Base
 *
 * @see resources/js/callbacks.js
 */
interface JsCallbacksEnum
{
    const DUPLICATE_SHIPMENT = 'duplicateShipment';
    const CAN_BE_SHIPMENT_SELECTED_BY_CHECKBOXES = 'canBeShipmentSelectedByCheckboxes';
}
