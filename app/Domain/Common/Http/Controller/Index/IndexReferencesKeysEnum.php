<?php

namespace App\Domain\Common\Http\Controller\Index;

interface IndexReferencesKeysEnum
{
    const CARS = 'cars';
    const DRIVERS = 'drivers';
    const TRAILERS = 'trailers';
    const CLIENTS = 'clients';
    const DIRECTIONS = 'directions';
    const TYPES = 'types';
    const PAYMENT_PERIODS = 'paymentPeriods';
    const CURRENCIES = 'currencies';
    const COMPANY_EMAILS = 'companyEmails';
    const INVOICE_TYPES = 'invoiceTypes';
}
