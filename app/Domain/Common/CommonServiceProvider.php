<?php

namespace App\Domain\Common;

use App\Base\Provider\BaseSingletonsServiceProvider;

class CommonServiceProvider extends BaseSingletonsServiceProvider
{
    protected function getSingletonsClassNames(): array
    {
        return [
            EntityTableNamesProvider::class,
        ];
    }
}
