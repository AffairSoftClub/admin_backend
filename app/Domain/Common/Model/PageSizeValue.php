<?php

namespace App\Domain\Common\Model;

interface PageSizeValue
{
    const VALUE = 20;
}
