<?php

namespace App\Domain\Cashbook\MoneyAccount;

use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccount;
use App\Domain\Cashbook\MoneyAccount\Cash\CashMoneyAccount;
use App\Domain\Cashbook\MoneyAccount\Cash\CashMoneyAccountService;
use App\Domain\Cashbook\MoneyAccount\General\GeneralMoneyAccountRepository;
use App\Domain\Cashbook\MoneyAccount\NonCash\NonCashMoneyAccount;
use App\Domain\Cashbook\MoneyAccount\NonCash\NonCashMoneyAccountService;

class MoneyAccountService
{
    /** @var CashMoneyAccountService */
    private $cashMoneyAccountService;

    /** @var NonCashMoneyAccountService */
    private $nonCashMoneyAccountService;

    /** @var GeneralMoneyAccountRepository */
    private $generalMoneyAccountRepository;

    public function __construct(
        CashMoneyAccountService $cashMoneyAccountService,
        NonCashMoneyAccountService $nonCashMoneyAccountService,
        GeneralMoneyAccountRepository $generalMoneyAccountRepository
    )
    {
        $this->cashMoneyAccountService = $cashMoneyAccountService;
        $this->nonCashMoneyAccountService = $nonCashMoneyAccountService;
        $this->generalMoneyAccountRepository = $generalMoneyAccountRepository;
    }

    public function getMoneyAccountByTypeId(int $moneyAccountTypeId): BaseMoneyAccount
    {
        return ($moneyAccountTypeId === MoneyAccountTypesEnum::CASH)
            ? $this->getFirstCashAccount()
            : $this->getFirstNonCashAccount();
    }

    public function getNonCashMoneyAccountId(): int
    {
        return $this->getFirstMoneyAccountIdByTypeId(MoneyAccountTypesEnum::NON_CASH);
    }

    public function getFirstMoneyAccountIdByTypeId(int $moneyAccountTypeId): int
    {
        $moneyAccount = $this->getMoneyAccountByTypeId($moneyAccountTypeId);

        return  $moneyAccount->getKey();
    }

    private function getFirstCashAccount(): CashMoneyAccount
    {
        return $this->cashMoneyAccountService->getFirstMainAccount();
    }

    private function getFirstNonCashAccount(): NonCashMoneyAccount
    {
        return $this->nonCashMoneyAccountService->getFirstMainAccount();
    }
}
