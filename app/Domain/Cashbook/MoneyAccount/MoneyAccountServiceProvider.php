<?php

namespace App\Domain\Cashbook\MoneyAccount;

use App\Domain\Base\ServiceProvider\BaseAggregateDomainServiceProvider;
use App\Domain\Cashbook\MoneyAccount\Cash\CashMoneyAccountServiceProvider;
use App\Domain\Cashbook\MoneyAccount\General\GeneralMoneyAccountServiceProvider;
use App\Domain\Cashbook\MoneyAccount\NonCash\NonCashMoneyAccountServiceProvider;

class MoneyAccountServiceProvider extends BaseAggregateDomainServiceProvider
{
    protected function getServiceProvidersClassNames(): array
    {
        return [
            CashMoneyAccountServiceProvider::class,
            GeneralMoneyAccountServiceProvider::class,
            NonCashMoneyAccountServiceProvider::class,
        ];
    }
}
