<?php

namespace App\Domain\Cashbook\MoneyAccount;

interface MoneyAccountTypesEnum
{
    const NON_CASH = 0;
    const CASH = 1;
}
