<?php

namespace App\Domain\Cashbook\MoneyAccount\Base\Validator;

use App\Base\Validation\Validator\Domain\Collection\BaseIdsByIdsProviderValidator;
use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountRepository;

abstract class BaseMoneyAccountIdsValidator extends BaseIdsByIdsProviderValidator
{
    public function __construct(BaseMoneyAccountRepository $idsProvider)
    {
        parent::__construct($idsProvider);
    }

    protected function getValidationErrorKey(): string
    {
        return 'money_account_ids';
    }
}
