<?php

namespace App\Domain\Cashbook\MoneyAccount\Base;

use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class BaseCashScope implements Scope
{
    private $isCashValue;

    public function __construct(bool $isCashValue)
    {
        $this->isCashValue = $isCashValue;
    }

    public function apply(Builder $builder, Model $model)
    {
        $builder->where(BaseMoneyAccountColumnNamesEnum::IS_CASH, $this->isCashValue);
    }
}
