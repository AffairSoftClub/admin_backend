<?php

namespace App\Domain\Cashbook\MoneyAccount\Base;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Name\GetAllOrderedByNameTrait;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CommonMoneyAccountRepository
 * @package App\Domain\Cashbook\MoneyAccount\Base
 * @method BaseMoneyAccount getByIdOrFail(int $id)
 */
class BaseMoneyAccountRepository extends BaseRepository
{
    use GetAllOrderedByNameTrait;

    public function __construct(BaseMoneyAccount $model)
    {
        parent::__construct($model);
    }

    /**
     * @param array $columns
     * @return Collection|BaseMoneyAccount[]
     */
    public function getOrderedMainNonCashAccounts($columns = []): Collection
    {
        return $this->model
            ->where(BaseMoneyAccountColumnNamesEnum::IS_MAIN, true)
            ->where(BaseMoneyAccountColumnNamesEnum::IS_CASH, false)
            ->orderBy(BaseMoneyAccountColumnNamesEnum::SORT)
            ->get($columns);
    }


    /**
     * @param array $columns
     * @return Collection|BaseMoneyAccount[]
     */
    public function getOrderedMainAccounts($columns = []): Collection
    {
        return $this->model
            ->where(BaseMoneyAccountColumnNamesEnum::IS_MAIN, true)
            ->orderBy(BaseMoneyAccountColumnNamesEnum::SORT)
            ->get($columns);
    }

    public function getFirst(): BaseMoneyAccount
    {
        return $this->model->first();
    }

    protected function getNameColumnName(): string
    {
        return BaseMoneyAccountColumnNamesEnum::NUMBER;
    }
}
