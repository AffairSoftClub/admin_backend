<?php

namespace App\Domain\Cashbook\MoneyAccount\Base;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Base\Model\ExtendableModel\Complex\RootExtendableModelTrait;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum as ColumnNamesEnum;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountTableNameValue as TableNameValue;

class BaseMoneyAccount extends BaseModel
{
    use RootExtendableModelTrait;

    protected $table = TableNameValue::VALUE;

    // Extendable
    private function getClassOnlyCastsFromClass(): array
    {
        return [
            ColumnNamesEnum::AMOUNT => 'float',
            ColumnNamesEnum::IS_MAIN => 'boolean',
            ColumnNamesEnum::IS_CASH => 'boolean',
        ];
    }

    private function getClassOnlyFillableAndVisibleFromClass(): array
    {
        return [
            ColumnNamesEnum::NUMBER,
            ColumnNamesEnum::MAIN_CURRENCY_ID,
            ColumnNamesEnum::AMOUNT,
            ColumnNamesEnum::SORT,
            ColumnNamesEnum::IS_MAIN,
            ColumnNamesEnum::IS_CASH,
        ];
    }

    // Getters/Setters

    public function getAmount(): float
    {
        return $this->getAttribute(ColumnNamesEnum::AMOUNT);
    }

    public function getNumber(): string
    {
        return $this->getAttribute(ColumnNamesEnum::NUMBER);
    }

    public function decreaseTotalAmount(float $decreaseAmount)
    {
        return $this->decrement(ColumnNamesEnum::AMOUNT, $decreaseAmount);
    }
}
