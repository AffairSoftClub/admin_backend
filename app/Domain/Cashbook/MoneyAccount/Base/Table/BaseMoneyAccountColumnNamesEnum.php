<?php

namespace App\Domain\Cashbook\MoneyAccount\Base\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface BaseMoneyAccountColumnNamesEnum extends IdTimestampsInterface
{
    const NUMBER = 'number';
    const MAIN_CURRENCY_ID = 'main_currency_id';
    const AMOUNT = 'amount';
    const MONTH_START_AMOUNT = 'month_start_amount';
    const SORT = 'sort';
    const IS_MAIN = 'is_main';
    const IS_CASH = 'is_cash';
}
