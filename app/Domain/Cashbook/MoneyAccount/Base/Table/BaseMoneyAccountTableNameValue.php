<?php

namespace App\Domain\Cashbook\MoneyAccount\Base\Table;

interface BaseMoneyAccountTableNameValue
{
    const VALUE = 'money_accounts';
}
