<?php

namespace App\Domain\Cashbook\MoneyAccount\Base;

use App\Helpers\NumberHelper\DefaultNumberFormatInterface;

interface BaseMoneyAccountAmountFormatInterface
{
    const DECIMALS = 0;
    const DECIMAL_POINT = DefaultNumberFormatInterface::DECIMAL_POINT;
    const THOUSANDS_SEPARATOR = ' ';
}
