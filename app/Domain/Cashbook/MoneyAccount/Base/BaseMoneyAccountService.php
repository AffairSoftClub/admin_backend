<?php

namespace App\Domain\Cashbook\MoneyAccount\Base;

use App\Base\DateAndTime\DateHelper;
use App\Domain\Cashbook\Currency\CurrencyService;
use App\Domain\Cashbook\Currency\MappedValuesProvider\CurrencySymbolsProvider;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum as ColumnNamesEnum;
use App\Helpers\NumberFormatter;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;


class BaseMoneyAccountService
{
    /** @var BaseMoneyAccountRepository */
    protected $moneyAccountRepository;

    /** @var CurrencyService */
    private $currencyService;

    /** @var NumberFormatter */
    private $numberHelper;

    /** @var DateHelper */
    private $dateHelper;

    /** @var CurrencySymbolsProvider */
    private $currencySymbolsProvider;

    public function __construct(
        BaseMoneyAccountRepository $moneyAccountRepository,
        CurrencyService            $currencyService,
        NumberFormatter            $numberHelper,
        DateHelper                 $dateHelper,
        CurrencySymbolsProvider    $currencySymbolsProvider
    )
    {
        $this->moneyAccountRepository = $moneyAccountRepository;
        $this->currencyService = $currencyService;
        $this->numberHelper = $numberHelper;
        $this->dateHelper = $dateHelper;
        $this->currencySymbolsProvider = $currencySymbolsProvider;
    }

    public function getMoneyAccountsSummary(): array
    {
        $mainMoneyAccounts = $this->getOrderedMainAccounts(
            [
                ColumnNamesEnum::ID,
                ColumnNamesEnum::NUMBER,
                ColumnNamesEnum::AMOUNT,
                ColumnNamesEnum::MONTH_START_AMOUNT,
                ColumnNamesEnum::MAIN_CURRENCY_ID,
            ]
        );

        $mainAccountsInfo = $this->convertMoneyAccountsToMoneyAccountInfo($mainMoneyAccounts);
        $mainCurrencyId = $this->getMainCurrencyId();

        $this->validateMoneyAccountsCurrencyId($mainMoneyAccounts, $mainCurrencyId);
        $mainCurrencySymbol = $this->getCurrencySymbolByCurrencyId($mainCurrencyId);

        $totalsInfo = $this->getTotalsInfo($mainMoneyAccounts, $mainCurrencySymbol);

        return [
            'accounts' => $mainAccountsInfo,
            'stats' => $totalsInfo,
        ];
    }

    /**
     * @param string[] $columns
     * @return Collection|BaseMoneyAccount[]
     */
    private function getOrderedMainAccounts(array $columns = ['*']): Collection
    {
        return $this->moneyAccountRepository->getOrderedMainAccounts($columns);
    }

    /**
     * @param Collection|BaseMoneyAccount[] $moneyAccounts
     * @return array
     */
    private function convertMoneyAccountsToMoneyAccountInfo(Collection $moneyAccounts): array
    {
        $ret = [];

        /** @var BaseMoneyAccount $moneyAccount */
        foreach ($moneyAccounts as $moneyAccount) {
            $ret[$moneyAccount->getNumber()] = $this->convertMoneyAccountToInfo($moneyAccount);
        }

        return $ret;
    }

    private function convertMoneyAccountToInfo(BaseMoneyAccount $moneyAccount): array
    {
        return [
            'amount' => $this->formatNumber($moneyAccount->getAmount()),
            'currency_symbol' => $this->getCurrencySymbolByCurrencyId($moneyAccount[ColumnNamesEnum::MAIN_CURRENCY_ID])
        ];
    }

    /**
     * @param float $number
     * @return string
     */
    private function formatNumber(float $number): string
    {
        return $this->numberHelper->formatMoneyAccountAmount($number);
    }

    /**
     * @param int $currencyId
     * @return string
     */
    private function getCurrencySymbolByCurrencyId(int $currencyId): string
    {
        return $this->currencySymbolsProvider->getSymbolByCurrencyId($currencyId);
    }

    private function getMainCurrencyId(): int
    {
        return $this->currencyService->getMainCurrencyId();
    }

    /**
     * @param Collection $mainMoneyAccounts
     * @param int $mainCurrencyId
     */
    private function validateMoneyAccountsCurrencyId(Collection $mainMoneyAccounts, int $mainCurrencyId)
    {
        foreach ($mainMoneyAccounts as $moneyAccount) {
            $this->validateMoneyAccountCurrencyId($moneyAccount, $mainCurrencyId);
        }
    }

    private function validateMoneyAccountCurrencyId(BaseMoneyAccount $moneyAccount, int $mainCurrencyId)
    {
        $accountCurrencyId = $moneyAccount[ColumnNamesEnum::MAIN_CURRENCY_ID];

        if ($accountCurrencyId != $mainCurrencyId) {
            $errorMessage = "Main money account currency id ({$accountCurrencyId}) is different from site main currency id ({$mainCurrencyId})";
            throw new InvalidArgumentException($errorMessage);
        }
    }

    /**
     * @param Collection|BaseMoneyAccount[] $mainMoneyAccounts
     * @param string $mainCurrencySymbol
     * @return array
     */
    private function getTotalsInfo(Collection $mainMoneyAccounts, string $mainCurrencySymbol): array
    {
        return [
            'month_start' => $this->getTotalMonthStart($mainMoneyAccounts, $mainCurrencySymbol),
            'current' => $this->getTotalCurrent($mainMoneyAccounts, $mainCurrencySymbol),
        ];
    }

    /**
     * @param Collection|BaseMoneyAccount[] $mainMoneyAccounts
     * @param string $mainCurrencySymbol
     * @return array
     */
    private function getTotalMonthStart(Collection $mainMoneyAccounts, string $mainCurrencySymbol): array
    {
        $totalMonthStartAmount = $this->getTotalMonthStartAmount($mainMoneyAccounts);

        return [
            'amount' => $this->formatNumber($totalMonthStartAmount),
            'currency_symbol' => $mainCurrencySymbol,
        ];
    }

    /**
     * @param Collection|BaseMoneyAccount[] $mainMoneyAccounts
     * @return float
     */
    private function getTotalMonthStartAmount(Collection $mainMoneyAccounts)
    {
        $totalAmount = 0;

        /** @var BaseMoneyAccount $moneyAccount */
        foreach ($mainMoneyAccounts as $moneyAccount) {
            $totalAmount += $moneyAccount[ColumnNamesEnum::MONTH_START_AMOUNT];
        }

        return $totalAmount;
    }

    /**
     * @param Collection|BaseMoneyAccount[] $mainMoneyAccounts
     * @param string $mainCurrencySymbol
     * @return array
     */
    private function getTotalCurrent(Collection $mainMoneyAccounts, string $mainCurrencySymbol): array
    {
        $totalCurrentAmount = $this->getTotalCurrentAmount($mainMoneyAccounts);

        return [
            'amount' => $this->formatNumber($totalCurrentAmount),
            'currency_symbol' => $mainCurrencySymbol,
            'date' => $this->getCurrentDateString(),
        ];
    }

    /**
     * @param Collection|BaseMoneyAccount[] $mainMoneyAccounts
     * @return float
     */
    private function getTotalCurrentAmount(Collection $mainMoneyAccounts)
    {
        $total = 0;

        foreach ($mainMoneyAccounts as $moneyAccount) {
            $total += $moneyAccount[ColumnNamesEnum::AMOUNT];
        }

        return $total;
    }

    private function getCurrentDateString()
    {
        return $this->dateHelper->getCurrentViewDateString();
    }

    public function changeMoneyAccountAmount(BaseMoneyAccount $moneyAccount, float $changeAmount): float
    {
        $this->incrementMoneyAccountAmount($moneyAccount, $changeAmount);
//        $moneyAccount->refresh(); // todo: надо ли?

        $amount = $moneyAccount->getAmount();

        $this->validateMoneyAccountAmount($amount);

        return $amount;
    }

    public function decreaseMoneyAccount(int $moneyAccountId, float $totalAmount): float
    {
        $moneyAccount = $this->getMoneyAccountById($moneyAccountId);
        $moneyAccount->decreaseTotalAmount($totalAmount);

        return $moneyAccount->getAmount();
    }

    public function changeMoneyAccountAmountById(int $moneyAccountId, float $amount): float
    {
        $moneyAccount = $this->getMoneyAccountById($moneyAccountId);

        return $this->changeMoneyAccountAmount($moneyAccount, $amount);
    }

    public function increaseAmount(BaseMoneyAccount $commonMoneyAccount, float $amount): float
    {
        return $this->changeMoneyAccountAmount($commonMoneyAccount, $amount);
    }

    public function increaseAmountById(int $moneyAccountId, float $amount): float
    {
        return $this->changeMoneyAccountAmountById($moneyAccountId, $amount);
    }

    public function decreaseAmount(BaseMoneyAccount $commonMoneyAccount, float $amount): float
    {
        return $this->changeMoneyAccountAmount($commonMoneyAccount, -$amount);
    }

    public function decreaseAmountById(int $moneyAccountId, float $amount): float
    {
        return $this->changeMoneyAccountAmountById($moneyAccountId, -$amount);
    }

    private function saveMoneyAccount(BaseMoneyAccount $moneyAccount)
    {
        $this->generalMoneyAccountRepository->save($moneyAccount);
    }


    private function incrementMoneyAccountAmount(BaseMoneyAccount $moneyAccount, float $incrementValue): int
    {
        return $moneyAccount->increment(ColumnNamesEnum::AMOUNT, $incrementValue);
    }

    private function validateMoneyAccountAmount(float $amount)
    {
        if ($amount < 0) {
            throw ValidationException::withMessages(['amount' => 'Сумма на счете не может быть отрицательной']);
        }
    }

    public function getFirstMainAccount(): BaseMoneyAccount
    {
        return $this->moneyAccountRepository->getFirst();
    }

    public function getMoneyAccountById(int $moneyAccountId): BaseMoneyAccount
    {
        return $this->moneyAccountRepository->getByIdOrFail($moneyAccountId);
    }
}
