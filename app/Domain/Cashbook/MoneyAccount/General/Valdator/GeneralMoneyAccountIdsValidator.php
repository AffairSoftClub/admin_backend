<?php

namespace App\Domain\Cashbook\MoneyAccount\General\Valdator;

use App\Domain\Cashbook\MoneyAccount\Base\Validator\BaseMoneyAccountIdsValidator;
use App\Domain\Cashbook\MoneyAccount\General\GeneralMoneyAccountRepository;

class GeneralMoneyAccountIdsValidator extends BaseMoneyAccountIdsValidator
{
    public function __construct(GeneralMoneyAccountRepository $idsProvider)
    {
        parent::__construct($idsProvider);
    }
}
