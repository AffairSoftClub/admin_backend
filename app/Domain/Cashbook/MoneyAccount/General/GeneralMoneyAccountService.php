<?php

namespace App\Domain\Cashbook\MoneyAccount\General;

use App\Base\DateAndTime\DateHelper;
use App\Domain\Cashbook\Currency\CurrencyService;
use App\Domain\Cashbook\Currency\MappedValuesProvider\CurrencySymbolsProvider;
use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountService;
use App\Helpers\NumberFormatter;

/**
 * Class GeneralMoneyAccountService
 * @package App\Domain\Cashbook\MoneyAccount\General
 * @method GeneralMoneyAccount getMoneyAccountById(int $moneyAccountId)
 */
class GeneralMoneyAccountService extends BaseMoneyAccountService
{
    public function __construct(
        GeneralMoneyAccountRepository $moneyAccountRepository,
        CurrencyService $currencyService,
        NumberFormatter $numberHelper,
        DateHelper $dateHelper,
        CurrencySymbolsProvider $currencySymbolsProvider
    )
    {
        parent::__construct($moneyAccountRepository, $currencyService, $numberHelper, $dateHelper, $currencySymbolsProvider);
    }
}
