<?php

namespace App\Domain\Cashbook\MoneyAccount\General\Seeds;

use App\Domain\Cashbook\Currency\Seeds\CurrencyIdsEnum;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum as ColumnNameEnum;
use App\Domain\Cashbook\MoneyAccount\General\Seeds\GeneralMoneyAccountNumbersEnum as NumbersEnum;

interface MoneyAccountsInterface
{
    const VALUES = [
        NumbersEnum::ACCOUNT_CASH => [
            ColumnNameEnum::NUMBER => NumbersEnum::ACCOUNT_CASH,
            ColumnNameEnum::MAIN_CURRENCY_ID => CurrencyIdsEnum::EURO,
            ColumnNameEnum::AMOUNT => 30000,
            ColumnNameEnum::MONTH_START_AMOUNT => 500,
            ColumnNameEnum::SORT => 0,
            ColumnNameEnum::IS_MAIN => true,
            ColumnNameEnum::IS_CASH => true,
        ],
        NumbersEnum::ACCOUNT_1 => [
            ColumnNameEnum::NUMBER => NumbersEnum::ACCOUNT_1,
            ColumnNameEnum::MAIN_CURRENCY_ID => CurrencyIdsEnum::EURO,
            ColumnNameEnum::AMOUNT => 30000,
            ColumnNameEnum::MONTH_START_AMOUNT => 500,
            ColumnNameEnum::SORT => 1,
            ColumnNameEnum::IS_MAIN => true,
            ColumnNameEnum::IS_CASH => false,
        ],
        NumbersEnum::ACCOUNT_2 => [
            ColumnNameEnum::NUMBER => NumbersEnum::ACCOUNT_2,
            ColumnNameEnum::MAIN_CURRENCY_ID => CurrencyIdsEnum::EURO,
            ColumnNameEnum::AMOUNT => 2000,
            ColumnNameEnum::MONTH_START_AMOUNT => 1500,
            ColumnNameEnum::SORT => 2,
            ColumnNameEnum::IS_MAIN => true,
            ColumnNameEnum::IS_CASH => false,
        ],
        NumbersEnum::ACCOUNT_3 => [
            ColumnNameEnum::NUMBER => NumbersEnum::ACCOUNT_3,
            ColumnNameEnum::MAIN_CURRENCY_ID => CurrencyIdsEnum::EURO,
            ColumnNameEnum::AMOUNT => 3000,
            ColumnNameEnum::MONTH_START_AMOUNT => 2500,
            ColumnNameEnum::SORT => 3,
            ColumnNameEnum::IS_MAIN => false,
            ColumnNameEnum::IS_CASH => false,
        ],
        NumbersEnum::ACCOUNT_4 => [
            ColumnNameEnum::NUMBER => NumbersEnum::ACCOUNT_4,
            ColumnNameEnum::MAIN_CURRENCY_ID => CurrencyIdsEnum::EURO,
            ColumnNameEnum::AMOUNT => 4000,
            ColumnNameEnum::MONTH_START_AMOUNT => 3500,
            ColumnNameEnum::SORT => 4,
            ColumnNameEnum::IS_MAIN => false,
            ColumnNameEnum::IS_CASH => false,
        ],
    ];
}
