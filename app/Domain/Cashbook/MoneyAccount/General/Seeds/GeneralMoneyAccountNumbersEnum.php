<?php

namespace App\Domain\Cashbook\MoneyAccount\General\Seeds;

interface GeneralMoneyAccountNumbersEnum
{
    const ACCOUNT_1 = '7587119001'; // кроны и евро (основной)
    const ACCOUNT_2 = '7510808001'; // кроны и евро (основной)
    const ACCOUNT_3 = '207666002'; // кроны и евро (строительный у Директора)
    const ACCOUNT_4 = '207666010'; // кроны и евро (Саши из Германии)
    const ACCOUNT_CASH = 'CASH'; //наличка евро
}
