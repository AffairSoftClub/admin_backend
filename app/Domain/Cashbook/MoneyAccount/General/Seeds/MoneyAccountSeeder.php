<?php

namespace App\Domain\Cashbook\MoneyAccount\General\Seeds;

use App\Domain\Cashbook\MoneyAccount\General\GeneralMoneyAccountRepository;
use App\Domain\Cashbook\MoneyAccount\General\Seeds\MoneyAccountsInterface as MoneyAccountsSeedsInterface;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class MoneyAccountSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(GeneralMoneyAccountRepository $repository)
    {
        parent::__construct($repository);
    }

    protected function getSeeds(): array
    {
        return MoneyAccountsSeedsInterface::VALUES;
    }
}
