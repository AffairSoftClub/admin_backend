<?php

namespace App\Domain\Cashbook\MoneyAccount\General\Table;

use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum;

interface GeneralMoneyAccountColumnNamesEnum extends BaseMoneyAccountColumnNamesEnum
{

}
