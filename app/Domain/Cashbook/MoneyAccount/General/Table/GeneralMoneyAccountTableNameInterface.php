<?php

namespace App\Domain\Cashbook\MoneyAccount\General\Table;

use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountTableNameValue;

interface GeneralMoneyAccountTableNameInterface extends BaseMoneyAccountTableNameValue
{

}
