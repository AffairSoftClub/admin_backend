<?php

namespace App\Domain\Cashbook\MoneyAccount\General;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class GeneralMoneyAccountServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return GeneralMoneyAccountRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return GeneralMoneyAccountService::class;
    }
}
