<?php

namespace App\Domain\Cashbook\MoneyAccount\General;

use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountRepository;

class GeneralMoneyAccountRepository extends BaseMoneyAccountRepository
{
    public function __construct(GeneralMoneyAccount $model)
    {
        parent::__construct($model);
    }
}
