<?php

namespace App\Domain\Cashbook\MoneyAccount\General;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccount;

class GeneralMoneyAccount extends BaseMoneyAccount
{
    use ChildExtendableModelTrait;
}
