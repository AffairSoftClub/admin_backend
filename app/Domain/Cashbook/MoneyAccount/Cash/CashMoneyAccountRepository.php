<?php

namespace App\Domain\Cashbook\MoneyAccount\Cash;

use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountRepository;

/**
 * Class CashMoneyAccountRepository
 * @package App\Domain\Cashbook\MoneyAccount\Cash
 * @method CashMoneyAccount getFirst()
 */
class CashMoneyAccountRepository extends BaseMoneyAccountRepository
{
    public function __construct(CashMoneyAccount $model)
    {
        parent::__construct($model);
    }
}
