<?php

namespace App\Domain\Cashbook\MoneyAccount\Cash;

use App\Domain\Cashbook\MoneyAccount\Base\BaseCashScope;

class IsCashScope extends BaseCashScope
{
    public function __construct()
    {
        parent::__construct(true);
    }
}
