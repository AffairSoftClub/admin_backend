<?php

namespace App\Domain\Cashbook\MoneyAccount\Cash;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccount;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum;

class CashMoneyAccount extends BaseMoneyAccount
{
    use ChildExtendableModelTrait;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsCashScope);
    }

    private function getClassOnlyDefaultAttributesFromClass(): array
    {
        return [
            BaseMoneyAccountColumnNamesEnum::IS_CASH => true,
        ];
    }
}
