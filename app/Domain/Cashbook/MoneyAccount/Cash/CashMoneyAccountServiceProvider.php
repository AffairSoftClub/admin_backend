<?php

namespace App\Domain\Cashbook\MoneyAccount\Cash;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class CashMoneyAccountServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return CashMoneyAccountRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return CashMoneyAccountService::class;
    }
}
