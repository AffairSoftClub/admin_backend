<?php

namespace App\Domain\Cashbook\MoneyAccount\Cash\Validator;

use App\Domain\Cashbook\MoneyAccount\Base\Validator\BaseMoneyAccountIdsValidator;
use App\Domain\Cashbook\MoneyAccount\Cash\CashMoneyAccountRepository;

class CashMoneyAccountIdsValidator extends BaseMoneyAccountIdsValidator
{
    public function __construct(CashMoneyAccountRepository $idsProvider)
    {
        parent::__construct($idsProvider);
    }

    protected function getEntityName(): string
    {
        return 'Cash Money Account';
    }
}
