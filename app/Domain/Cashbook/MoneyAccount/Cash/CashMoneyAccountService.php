<?php

namespace App\Domain\Cashbook\MoneyAccount\Cash;

use App\Base\DateAndTime\DateHelper;
use App\Domain\Cashbook\Currency\CurrencyService;
use App\Domain\Cashbook\Currency\MappedValuesProvider\CurrencySymbolsProvider;
use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountService;
use App\Helpers\NumberFormatter;

/**
 * Class CashMoneyAccountService
 * @package App\Domain\Cashbook\MoneyAccount\Cash
 * @property CashMoneyAccountRepository $moneyAccountRepository
 */
class CashMoneyAccountService extends BaseMoneyAccountService
{
    public function __construct(
        CashMoneyAccountRepository $moneyAccountRepository,
        CurrencyService $currencyService,
        NumberFormatter $numberHelper,
        DateHelper $dateHelper,
        CurrencySymbolsProvider $currencySymbolsProvider
    )
    {
        parent::__construct(
            $moneyAccountRepository,
            $currencyService,
            $numberHelper,
            $dateHelper,
            $currencySymbolsProvider
        );
    }

    public function getFirstAccountId(): int
    {
        $firstAccount = $this->getFirstAccount();

        return $firstAccount->getKey();
    }

    private function getFirstAccount(): CashMoneyAccount
    {
        return $this->moneyAccountRepository->getFirst();
    }
}
