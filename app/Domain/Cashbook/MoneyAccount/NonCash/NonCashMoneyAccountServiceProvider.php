<?php

namespace App\Domain\Cashbook\MoneyAccount\NonCash;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class NonCashMoneyAccountServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return NonCashMoneyAccountRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return NonCashMoneyAccountService::class;
    }
}
