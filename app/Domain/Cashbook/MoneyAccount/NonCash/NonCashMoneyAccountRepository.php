<?php

namespace App\Domain\Cashbook\MoneyAccount\NonCash;

use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountRepository;

/**
 * Class NonCashMoneyAccountRepository
 * @package App\Domain\Cashbook\MoneyAccount\NonCash
 * @method NonCashMoneyAccount getFirst()
 */
class NonCashMoneyAccountRepository extends BaseMoneyAccountRepository
{
    public function __construct(NonCashMoneyAccount $model)
    {
        parent::__construct($model);
    }
}
