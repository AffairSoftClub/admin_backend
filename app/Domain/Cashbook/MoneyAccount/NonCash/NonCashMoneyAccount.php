<?php

namespace App\Domain\Cashbook\MoneyAccount\NonCash;

use App\Domain\Base\Model\ExtendableModel\Complex\ChildExtendableModelTrait;
use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccount;
use App\Domain\Cashbook\MoneyAccount\Base\Table\BaseMoneyAccountColumnNamesEnum;

class NonCashMoneyAccount extends BaseMoneyAccount
{
    use ChildExtendableModelTrait;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsNotCashScope);
    }

    private function getClassOnlyDefaultAttributesFromClass(): array
    {
        return [
            BaseMoneyAccountColumnNamesEnum::IS_CASH => false,
        ];
    }
}
