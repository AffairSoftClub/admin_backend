<?php

namespace App\Domain\Cashbook\MoneyAccount\NonCash;

use App\Domain\Cashbook\MoneyAccount\Base\BaseCashScope;

class IsNotCashScope extends BaseCashScope
{
    public function __construct()
    {
        parent::__construct(false);
    }
}
