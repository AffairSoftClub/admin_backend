<?php

namespace App\Domain\Cashbook\MoneyAccount\NonCash\Validator;

use App\Domain\Cashbook\MoneyAccount\Base\Validator\BaseMoneyAccountIdsValidator;
use App\Domain\Cashbook\MoneyAccount\NonCash\NonCashMoneyAccountRepository;

class NonCashMoneyAccountIdsValidator extends BaseMoneyAccountIdsValidator
{
    public function __construct(NonCashMoneyAccountRepository $idsProvider)
    {
        parent::__construct($idsProvider);
    }

    protected function getEntityName(): string
    {
        return 'Non Cash Money Account Ids';
    }
}
