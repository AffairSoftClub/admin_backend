<?php

namespace App\Domain\Cashbook\MoneyAccount\NonCash;

use App\Base\DateAndTime\DateHelper;
use App\Domain\Cashbook\Currency\CurrencyService;
use App\Domain\Cashbook\Currency\MappedValuesProvider\CurrencySymbolsProvider;
use App\Domain\Cashbook\MoneyAccount\Base\BaseMoneyAccountService;
use App\Helpers\NumberFormatter;

class NonCashMoneyAccountService extends BaseMoneyAccountService
{
    public function __construct(
        NonCashMoneyAccountRepository $moneyAccountRepository,
        CurrencyService $currencyService,
        NumberFormatter $numberHelper,
        DateHelper $dateHelper,
        CurrencySymbolsProvider $currencySymbolsProvider
    )
    {
        parent::__construct($moneyAccountRepository, $currencyService, $numberHelper, $dateHelper, $currencySymbolsProvider);
    }
}
