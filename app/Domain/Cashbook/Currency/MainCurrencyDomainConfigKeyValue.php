<?php

namespace App\Domain\Cashbook\Currency;

interface MainCurrencyDomainConfigKeyValue
{
    const VALUE = 'main_currency_id';
}
