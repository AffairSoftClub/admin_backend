<?php

namespace App\Domain\Cashbook\Currency;

use App\Domain\Base\Model\BaseModel;
use App\Domain\Cashbook\Currency\Table\CurrencyColumnNamesEnum;
use App\Domain\Cashbook\Currency\Table\CurrencyTableNameValue;

class Currency extends BaseModel
{
    protected $table = CurrencyTableNameValue::VALUE;

    protected $fillable = [
        CurrencyColumnNamesEnum::SORT_ORDER,
        CurrencyColumnNamesEnum::CODE,
        CurrencyColumnNamesEnum::SYMBOL,
        CurrencyColumnNamesEnum::NAME_RU,
    ];
}
