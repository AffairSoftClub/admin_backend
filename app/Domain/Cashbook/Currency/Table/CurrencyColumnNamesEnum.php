<?php

namespace App\Domain\Cashbook\Currency\Table;

use App\Domain\Base\Table\Columns\Compound\IdTimestampsInterface;

interface CurrencyColumnNamesEnum extends IdTimestampsInterface
{
    const SORT_ORDER = 'sort_order';
    const CODE = 'code';
    const SYMBOL = 'symbol';
    const NAME_RU = 'name_ru'; // todo: move to translations
}
