<?php

namespace App\Domain\Cashbook\Currency\Table;

interface CurrencyTableNameValue
{
    const VALUE = 'currencies';
}
