<?php

namespace App\Domain\Cashbook\Currency;

use App\Domain\Base\Model\BaseService;

class CurrencyService extends BaseService
{
    /** @var CurrencyConfig */
    private $currencyConfig;

    public function __construct(
        CurrencyRepository $currencyRepository,
        CurrencyConfig     $currencyConfig
    )
    {
        parent::__construct($currencyRepository);
        $this->currencyConfig = $currencyConfig;
    }

    public function getMainCurrencyId(): int
    {
        return $this->currencyConfig->getMainCurrencyId();
    }
}
