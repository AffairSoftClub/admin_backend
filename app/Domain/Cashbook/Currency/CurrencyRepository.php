<?php

namespace App\Domain\Cashbook\Currency;

use App\Domain\Base\Model\BaseRepository;
use App\Base\Repository\Traits\Advanced\Base\GetAsMappingTrait;
use App\Domain\Cashbook\Currency\Table\CurrencyColumnNamesEnum;

class CurrencyRepository extends BaseRepository
{
    use GetAsMappingTrait;

    public function __construct(Currency $model)
    {
        parent::__construct($model);
    }

    public function getCurrencyIdToSymbolMap(): array
    {
        return $this->getAsMapping(CurrencyColumnNamesEnum::ID, CurrencyColumnNamesEnum::SYMBOL);
    }

    public function getCurrencyCodeToIdMap(): array
    {
        return $this->getAsMapping(CurrencyColumnNamesEnum::CODE, CurrencyColumnNamesEnum::ID);
    }

    public function getCurrencyIdToCodeMap(): array
    {
        return $this->getAsMapping(CurrencyColumnNamesEnum::ID, CurrencyColumnNamesEnum::CODE);
    }

    public function selectIds(array $ids): array
    {
        return $this->getModel()->pluck(CurrencyColumnNamesEnum::ID)->toArray();
    }
}
