<?php

namespace App\Domain\Cashbook\Currency\MappedValuesProvider;

class CurrencySymbolsProvider extends BaseCurrencyMappedValuesProvider
{
    protected function getCacheValues(): array
    {
        return $this->internalProvider->getCurrencyIdToSymbolMap();
    }

    public function getSymbolByCurrencyId(int $currencyId): string
    {
        return $this->getByKeyOrFail($currencyId);
    }

    protected function getKeyAttributeName(): string
    {
        return 'id';
    }
}
