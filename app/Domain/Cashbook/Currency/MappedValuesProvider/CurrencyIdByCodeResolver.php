<?php

namespace App\Domain\Cashbook\Currency\MappedValuesProvider;

use App\Domain\Cashbook\Currency\CurrencyRepository;

class CurrencyIdByCodeResolver extends BaseCurrencyMappedValuesProvider
{
    public function __construct(CurrencyRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    public function getCurrencyIdByCode(string $code): int
    {
        return $this->getByKeyOrFail($code);
    }

    protected function getCacheValues(): array
    {
        /** @var CurrencyRepository $currencyRepository */
        $currencyRepository = $this->internalProvider;

        return $currencyRepository->getCurrencyCodeToIdMap();
    }

    protected function getEntityName(): string
    {
        return 'Currency';
    }

    protected function getKeyAttributeName(): string
    {
        return 'code';
    }
}
