<?php

namespace App\Domain\Cashbook\Currency\MappedValuesProvider;

use App\Domain\Base\Model\BaseEntityMappedValuesProvider;
use App\Domain\Cashbook\Currency\CurrencyRepository;

/**
 * @protected CurrencyRepository $internalProvider
 */
abstract class BaseCurrencyMappedValuesProvider extends BaseEntityMappedValuesProvider
{
    public function __construct(CurrencyRepository $internalProvider)
    {
        parent::__construct($internalProvider);
    }

    protected function getEntityName(): string
    {
        return 'Currency';
    }
}
