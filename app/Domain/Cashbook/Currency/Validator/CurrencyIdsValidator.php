<?php

namespace App\Domain\Cashbook\Currency\Validator;

use App\Base\Validation\Validator\Domain\Collection\BaseIdsByIdsProviderValidator;
use App\Domain\Cashbook\Currency\CurrencyRepository;

class CurrencyIdsValidator extends BaseIdsByIdsProviderValidator
{
    public function __construct(CurrencyRepository $idsProvider)
    {
        parent::__construct($idsProvider);
    }

    protected function getValidationErrorKey(): string
    {
        return 'currency_ids';
    }

    protected function getEntityName(): string
    {
        return 'Currency';
    }
}
