<?php

namespace App\Domain\Cashbook\Currency;

use App\Domain\Base\ServiceProvider\BaseEntityDomainServiceProvider;

class CurrencyServiceProvider extends BaseEntityDomainServiceProvider
{
    protected function getRepositoryClassName(): ?string
    {
        return CurrencyRepository::class;
    }

    protected function getServiceClassName(): ?string
    {
        return CurrencyService::class;
    }
}
