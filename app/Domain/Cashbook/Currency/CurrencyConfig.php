<?php

namespace App\Domain\Cashbook\Currency;

use App\Domain\Base\BaseDomainConfig;

class CurrencyConfig extends BaseDomainConfig
{
    protected function getDomainConfigPath(): string
    {
        return 'currency';
    }
}
