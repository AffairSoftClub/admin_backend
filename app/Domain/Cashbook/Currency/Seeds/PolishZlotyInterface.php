<?php

namespace App\Domain\Cashbook\Currency\Seeds;

interface PolishZlotyInterface
{
    const ID = 3;
    const SORT_ORDER = 3;
    const CODE = 'PLN';
    const SYMBOL = 'zł';
    const NAME_RU = 'Польский злотый';
}
