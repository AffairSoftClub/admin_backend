<?php

namespace App\Domain\Cashbook\Currency\Seeds;

interface CzechCrownInterface
{
    const ID = 2;
    const SORT_ORDER = 2;
    const CODE = 'CZK';
    const SYMBOL = 'Kč';
    const NAME_RU = 'Чешская крона';
}
