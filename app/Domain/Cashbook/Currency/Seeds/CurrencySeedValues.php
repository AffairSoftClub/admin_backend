<?php

namespace App\Domain\Cashbook\Currency\Seeds;

use App\Domain\Cashbook\Currency\Table\CurrencyColumnNamesEnum;

interface CurrencySeedValues
{
    const VALUES = [
        [
            CurrencyColumnNamesEnum::ID => EuroInterface::ID,
            CurrencyColumnNamesEnum::SORT_ORDER => EuroInterface::SORT_ORDER,
            CurrencyColumnNamesEnum::CODE => EuroInterface::CODE,
            CurrencyColumnNamesEnum::SYMBOL => EuroInterface::SYMBOL,
            CurrencyColumnNamesEnum::NAME_RU => EuroInterface::NAME_RU,
        ],
        [
            CurrencyColumnNamesEnum::ID => CzechCrownInterface::ID,
            CurrencyColumnNamesEnum::SORT_ORDER => CzechCrownInterface::SORT_ORDER,
            CurrencyColumnNamesEnum::CODE => CzechCrownInterface::CODE,
            CurrencyColumnNamesEnum::SYMBOL => CzechCrownInterface::SYMBOL,
            CurrencyColumnNamesEnum::NAME_RU => CzechCrownInterface::NAME_RU,
        ],
        [
            CurrencyColumnNamesEnum::ID => PolishZlotyInterface::ID,
            CurrencyColumnNamesEnum::SORT_ORDER => PolishZlotyInterface::SORT_ORDER,
            CurrencyColumnNamesEnum::CODE => PolishZlotyInterface::CODE,
            CurrencyColumnNamesEnum::SYMBOL => PolishZlotyInterface::SYMBOL,
            CurrencyColumnNamesEnum::NAME_RU => PolishZlotyInterface::NAME_RU,
        ],
    ];
}
