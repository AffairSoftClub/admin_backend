<?php

namespace App\Domain\Cashbook\Currency\Seeds;

use App\Domain\Cashbook\Currency\CurrencyRepository;
use App\Domain\Cashbook\Currency\Seeds\CurrencySeedValues;
use App\Domain\Base\Seeds\Entity\RepositoryBased\ByAttributesRepositoryBasedSeeder;

class CurrenciesSeeder extends ByAttributesRepositoryBasedSeeder
{
    public function __construct(CurrencyRepository $repository)
    {
        parent::__construct($repository);
    }

    protected function getSeeds(): array
    {
        return CurrencySeedValues::VALUES;
    }
}
