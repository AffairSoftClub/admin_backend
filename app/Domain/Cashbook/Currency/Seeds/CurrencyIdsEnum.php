<?php

namespace App\Domain\Cashbook\Currency\Seeds;

interface CurrencyIdsEnum
{
    const EURO = EuroInterface::ID;
    const CZECH_CROWN = CzechCrownInterface::ID;
    const POLISH_ZLOTY = PolishZlotyInterface::ID;
}
