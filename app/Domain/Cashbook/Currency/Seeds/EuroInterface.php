<?php

namespace App\Domain\Cashbook\Currency\Seeds;

interface EuroInterface
{
    const ID = 1;
    const SORT_ORDER = 1;
    const CODE = 'EUR';
    const SYMBOL = '€';
    const NAME_RU = 'Eвро';
}
