<?php

use App\Domain\Company\CompanyConfigKeysEnum;

return [
    CompanyConfigKeysEnum::LOGO => 'images/ealogo.png',
    CompanyConfigKeysEnum::NAME => 'Euroasia Cargo',
    CompanyConfigKeysEnum::EMAILS => [
        'likemusic@yandex.by',
        'likemusicdev@gmail.com',
        'vmyywvjpoq@novaemail.com',
    ],
];
