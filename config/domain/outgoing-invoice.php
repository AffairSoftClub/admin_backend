<?php

use App\Domain\Invoice\Outgoing\Common\Config\OutgoingInvoiceConfigKeysEnum;
use App\Domain\Invoice\Outgoing\Common\PaymentPeriodDaysEnum;

return [
    OutgoingInvoiceConfigKeysEnum::PAYMENT_PERIODS_DAYS => [
        PaymentPeriodDaysEnum::P0,
        PaymentPeriodDaysEnum::P7,
        PaymentPeriodDaysEnum::P15,
        PaymentPeriodDaysEnum::P30,
        PaymentPeriodDaysEnum::P45,
        PaymentPeriodDaysEnum::P60,
        PaymentPeriodDaysEnum::P90,
    ],

    OutgoingInvoiceConfigKeysEnum::VAT_PERCENTS => 21,
];
