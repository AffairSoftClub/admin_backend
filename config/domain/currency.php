<?php

use App\Domain\Cashbook\Currency\MainCurrencyDomainConfigKeyValue;
use App\Domain\Cashbook\Currency\Seeds\EuroInterface;

return [
    MainCurrencyDomainConfigKeyValue::VALUE => EuroInterface::ID,
];
