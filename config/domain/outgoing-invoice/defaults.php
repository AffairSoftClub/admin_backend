<?php

use App\Domain\Cashbook\Currency\Seeds\EuroInterface;
use App\Domain\Invoice\Outgoing\Common\Config\OutgoingInvoiceConfigDefaultsEmailsAttachmentKeysEnum;
use App\Domain\Invoice\Outgoing\Common\Config\OutgoingInvoiceConfigDefaultsEmailsKeysEnum;
use App\Domain\Invoice\Outgoing\Common\Config\OutgoingInvoiceConfigDefaultsKeysEnum;
use App\Domain\Invoice\Outgoing\Common\PaymentPeriodDaysEnum;

return [
    OutgoingInvoiceConfigDefaultsKeysEnum::USE_VAT => true,

    OutgoingInvoiceConfigDefaultsKeysEnum::PAYMENT_PERIOD_DAYS => PaymentPeriodDaysEnum::P30,
    OutgoingInvoiceConfigDefaultsKeysEnum::CURRENCY_ID => EuroInterface::ID,
    OutgoingInvoiceConfigDefaultsKeysEnum::EXCHANGE_RATE => 28.55,
    OutgoingInvoiceConfigDefaultsKeysEnum::IS_CLIENT_EMAIL_NOTIFICATIONS_ENABLED => true,

    OutgoingInvoiceConfigDefaultsKeysEnum::EMAILS_CONFIG => [
        OutgoingInvoiceConfigDefaultsEmailsKeysEnum::EMAILS_SUBJECT_TEMPLATE => 'Инвойс №{invoiceNumber} от {issuanceDate} {clientName}',
        OutgoingInvoiceConfigDefaultsEmailsKeysEnum::ADD_SIGNATURE => true,
//        OutgoingInvoiceConfigDefaultsEmailsKeysEnum::FRONT_ATTACHMENTS => [
//            [
//                OutgoingInvoiceConfigDefaultsEmailsAttachmentKeysEnum::ID => null,
//                OutgoingInvoiceConfigDefaultsEmailsAttachmentKeysEnum::TITLE => 'PDF-версия инвойса (будет доступна после создания инвойса)',
//                OutgoingInvoiceConfigDefaultsEmailsAttachmentKeysEnum::URL => null,
//            ],
//        ],
    ],
];
