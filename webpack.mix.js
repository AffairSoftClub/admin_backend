const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/js')
    .js('resources/js/Domain/Shipment/duplicateShipment.js', 'public/js/duplicateShipment.js')
    // .js('resources/verstka/assets/js/now-ui-dashboard.js', 'public/js/now-ui-dashboard.js')
    .extract([
        // dependencies
        'materialize-css',
        'moment',
        'object-hash',
        'object-to-formdata',
        'tslib',
        'uid',
        'vue-fragment',
        'vue-json-component',
        'vue-just-super',
        'vue-multiselect',
        'vue-quick-edit',
        'vue-toastr',
        'vue2-daterange-picker',
        'vue2-datepicker',
        'axios',
        'bootstrap',
        'vue',
        "jquery",
        "lodash",
        "popper.js",
    ])
    .vue({ version: 2 })
    // .babel('public/js/app.js', 'public/js/app.js')
    .sourceMaps()
    .sass('resources/sass/app.scss', 'public/css')
    .copy('resources/images', 'public/images')
    .copy('resources/verstka/assets/js/plugins/perfect-scrollbar.jquery.min.js', 'public/js/perfect-scrollbar.jquery.min.js')
    .copy('resources/verstka/assets/js/now-ui-dashboard.js', 'public/js/now-ui-dashboard.js')
;

// mix
//     .js('resources/js/Domain/Shipment/duplicateShipment.js', 'public/js/callbacks/')
//     .sourceMaps()
// ;


mix.webpackConfig({
    module: {
        rules: [
            {
                resourceQuery: /blockType=template-extends/,
                use: 'vue-template-extends'
            },
        ]
    }
});
